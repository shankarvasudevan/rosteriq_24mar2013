import models.Company;
import models.Location;
import models.Person;
import models.Position;
import org.junit.Test;
import play.mvc.Http.Response;
import play.test.FunctionalTest;

public class ApplicationTest extends FunctionalTest {

    @Test
    public void testThatIndexPageWorks() {
        Response response = GET("/");
        assertIsOk(response);
        assertContentType("text/html", response);
        assertCharset(play.Play.defaultWebEncoding, response);
    }

    @Test
    public void testHelloWorld() {
        Company company = new Company("Hello world");
        assertTrue(company.companyName.compareToIgnoreCase("hello world") == 0) ;
    }


    @Test
    public void testPersonAddition() {
        Company company = new Company("MyTestCompany");
        Person person = new Person(company,"pwd","John","Smith","john.smith@gmail.com","0123456789");
        company.persons.add(person);
        assertTrue(company.persons.size() == 1);
    }

    @Test
    public void testPersonDelete() {
        Company company = new Company("MyTestCompany");
        Person person = new Person(company,"pwd","John","Smith","john.smith@gmail.com","0123456789");
        company.persons.add(person);
        company.persons.remove(person);
        assertTrue(company.persons.size() == 0);
    }

    @Test
    public void testPersonAdditionCascadesToPosition() {
        Company company = new Company("MyTestCompany");
        Person person = new Person(company,"pwd","John","Smith","john.smith@gmail.com","0123456789");
        company.persons.add(person);
        Position position = new Position("MyTestPosition",company);
        position.persons.add(person);
        person.positions.add(position);
        assertTrue(position.persons.size() == 1);
        assertTrue(person.positions.size() == 1);
    }

    @Test
    public void testPersonAdditionCascadesToLocation() {
        Company company = new Company("MyTestCompany");
        Person person = new Person(company,"pwd","John","Smith","john.smith@gmail.com","0123456789");
        company.persons.add(person);
        Location location = new Location(company,"MyTestLocation");
        person.locations.add(location);
        location.persons.add(person);
        assertTrue(location.persons.size() == 1);
        assertTrue(person.locations.size() == 1);
    }

    @Test
    public void testPersonDeleteCascadesToPosition() {
        Company company = new Company("MyTestCompany");
        Person person = new Person(company,"pwd","John","Smith","john.smith@gmail.com","0123456789");
        company.persons.add(person);
        Position position = new Position("MyTestPosition",company);
        position.persons.add(person);
        person.positions.add(position);
        person.delete();
        assertTrue(position.persons.size() == 0);
    }

   /* @Test
    public void testPersonDeleteCascadesToLocationAndPosition() {
        Company company = new Company("MyTestCompany");
        Person person = new Person(company,"pwd","John","Smith","john.smith@gmail.com","0123456789");
        company.persons.add(person);
        Location location = new Location(company,"MyTestLocation");
        person.locations.add(location);
        location.persons.add(person);
        Position position = new Position("MyTestPosition",company);
        position.persons.add(person);
        person.positions.add(position);
        company.persons.remove(person);
        person.delete();
        assertTrue(company.persons.size() == 0);
        assertTrue(location.persons.size() == 0);
        assertTrue(position.persons.size() == 0);
    }*/
    
}