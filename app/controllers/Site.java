package controllers;

import com.google.gson.Gson;
import models.*;
import notifiers.EmailNotifier;
import play.Logger;
import play.mvc.Controller;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: pdatar
 * Date: 7/12/12
 * Time: 11:16 AM
 * To change this template use File | Settings | File Templates.
 */
public class Site extends Controller {


    public static class companyManager{
        public String firstName;
        public String lastName;
        public String email;
        public String password;
    }

    public static class companyLocation{
        public String location;
    }

    public static class companyRole{
        public String role;
    }

    public static class companyEmployee{
        public String name;
        public String email;
        public List<employeeLocation> locations;
        public List<employeeRole> roles;
    }

    public static class employeeLocation{
        public String location;
    }

    public static class employeeRole{
        public String role;
    }

    public static class company{
        public String name;
        public List<companyLocation> companyLocations;
        public List<companyRole> companyRoles;
        public List<companyEmployee> companyEmployees;
    }

    public static class JsonObject{
        public List<companyManager> companyManager;
        public List<company> company;
    }


    public static class ApplicationLogger{
        public boolean index = false;
        public boolean onboardUser = true;
    }

    static final ApplicationLogger applicationLog = new ApplicationLogger();

    public static void index() {
        if ( applicationLog.index ) Logger.info("Inside the index method");
        render();
    }

    public static void onboardUser(String signUpJson){
        if ( applicationLog.onboardUser ) Logger.info("Inside the onboardUser method");
        //Then we need to get the post data i.e the json object
        if ( applicationLog.onboardUser ) Logger.info(signUpJson);
        Gson gson = new Gson();
        JsonObject signUpObject = gson.fromJson(signUpJson,JsonObject.class);
        //So now we have all the details in the object signUpObject

        //We will create the company first
        Company newCompany = new Company(signUpObject.company.get(0).name);
        newCompany.save();
        //We'll then add the company manager account
        for(companyLocation location : signUpObject.company.get(0).companyLocations){
            Location newLocation = new Location(newCompany,location.location);
            newLocation.save();
            //Then we need to add the persons at this location
            newCompany.locations.add(newLocation);
        }
        for(companyRole role : signUpObject.company.get(0).companyRoles){
            Position newPosition = new Position(role.role, newCompany);
            newPosition.save();
            newCompany.positions.add(newPosition);
        }
        newCompany.save();
        //Now that the roles and locations have been added  we'll add the employees
        for(companyEmployee employee : signUpObject.company.get(0).companyEmployees){
            //The first employee we'll add is the company manager but we'll do a check to make sure
            if(!(employee.email.compareToIgnoreCase(signUpObject.companyManager.get(0).email) == 0)){
                Person newCompanyEmployee = new Person(newCompany, ApplicationHelper.getAlphaNumeric(6), employee.name.split(" ")[0],"", employee.email,"");
                newCompanyEmployee.save();
                newCompany.persons.add(newCompanyEmployee);
                if(employee.name.split(" ").length == 2){
                    //This means that the user has given the last name
                    newCompanyEmployee.lastName = employee.name.split(" ")[1];
                }
                for(employeeLocation employeeLocation : employee.locations ){
                    Location companyLocation = null;
                    for (Location l : newCompany.locations) {
                        if (l.locationName.equalsIgnoreCase(employeeLocation.location)) {
                            companyLocation = l;
                        }
                    }
                    boolean employeeLocationExists = false;
                    for (int i=0; i < newCompanyEmployee.locations.size() && !employeeLocationExists; i++) {
                        if (newCompanyEmployee.locations.get(i).locationName.equalsIgnoreCase(employeeLocation.location)) {
                            employeeLocationExists = true;
                        }
                    }
                    if (!employeeLocationExists && companyLocation != null) {
                        newCompanyEmployee.locations.add(companyLocation);
                        companyLocation.persons.add(newCompanyEmployee);
                        companyLocation.save();
                    }
                }
                for(employeeRole employeeRole : employee.roles){
                    Position companyPosition = null;
                    for (Position p : newCompany.positions) {
                        if (p.title.equalsIgnoreCase(employeeRole.role)) {
                            companyPosition = p;
                        }
                    }
                    boolean employeeRoleExists = false;
                    for (int i=0; i < newCompanyEmployee.positions.size() && !employeeRoleExists; i++) {
                        if (newCompanyEmployee.positions.get(i).title.equalsIgnoreCase(employeeRole.role)) {
                            employeeRoleExists = true;
                        }
                    }
                    if (!employeeRoleExists && companyPosition != null) {
                        newCompanyEmployee.positions.add(companyPosition);
                        companyPosition.persons.add(newCompanyEmployee);
                        companyPosition .save();
                    }
                }
                //After everything is added save the employee and the relations
                newCompanyEmployee.save();
                //We'll then send the emails to the company employees to invite them to the app
                EmailNotifier.inviteEmployee(newCompanyEmployee);
            }else{
                //This means that this is the company manager employee
                Person companyManager = new Person(newCompany, signUpObject.companyManager.get(0).password, signUpObject.companyManager.get(0).firstName, signUpObject.companyManager.get(0).lastName, signUpObject.companyManager.get(0).email,"");
                companyManager.accountType = AccountType.COMPANY_MANAGER;
                companyManager.save();
                //We will then add the locations and roles to the company
                for(employeeLocation employeeLocation : employee.locations ){
                    Location companyLocation = null;
                    for (Location l : newCompany.locations) {
                        if (l.locationName.equalsIgnoreCase(employeeLocation.location)) {
                            companyLocation = l;
                        }
                    }
                    boolean companyManagerLocationExists = false;
                    for (int i=0; i < companyManager.locations.size() && !companyManagerLocationExists; i++) {
                        if (companyManager.locations.get(i).locationName.equalsIgnoreCase(employeeLocation.location)) {
                            companyManagerLocationExists = true;
                        }
                    }
                    if (!companyManagerLocationExists && companyLocation != null) {
                        companyManager.locations.add(companyLocation);
                        companyLocation.persons.add(companyManager);
                        companyLocation.save();
                    }
                }
                for(employeeRole employeeRole : employee.roles){
                    Position companyPosition = null;
                    for (Position p : newCompany.positions) {
                        if (p.title.equalsIgnoreCase(employeeRole.role)) {
                            companyPosition = p;
                        }
                    }
                    boolean companyManagerPositionExists = false;
                    for (int i=0; i < companyManager.positions.size() && !companyManagerPositionExists; i++) {
                        if (companyManager.positions.get(i).title.equalsIgnoreCase(employeeRole.role)) {
                            companyManagerPositionExists = true;
                        }
                    }
                    if (!companyManagerPositionExists && companyPosition != null) {
                        companyManager.positions.add(companyPosition);
                        companyPosition.persons.add(companyManager);
                        companyPosition.save();
                    }
                }
                //Then we'll send the email to the company manager
                EmailNotifier.accountCreated(companyManager.id);
            }
        }
        newCompany.save();
        render(newCompany);
    }

}
