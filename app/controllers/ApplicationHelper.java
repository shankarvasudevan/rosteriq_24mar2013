package controllers;

import models.*;
import notifiers.EmailNotifier;
import play.Logger;
import play.db.jpa.JPA;
import play.libs.Crypto;

import javax.persistence.Query;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * UserRole: shankarvasudevan
 * Date: 31/08/12
 * Time: 8:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class ApplicationHelper {
    static final int NUM_DAYS_AHEAD_FOR_PUBLISHED_SHIFTS = 7;
    static final String[] DAY_NAMES = new String[]{"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
    private static final String ALPHA_NUM = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";


    //Admin update objects

    public static class locationObject{
        public String locationName;
        public String locationAddress;
        public long id;
        public String operation;
    }

    public static class roleObject{
        public String roleName;
        public String roleDesc;
    }

    public static class rolesObject{
        List<roleObject> roles = new ArrayList<roleObject>();
    }

    public static class employeeObject{
        public String firstName;
        public String lastName;
        public String email;
        public long id;
        public String operation;
        public List<String> locations;
        public List<String> roles;
    }

    // Classes that used with JSON
    public static class CalShift {
        public long id;
        public boolean unassigned;
        public long personId;
        public String personName;
        public long positionId;
        public String positionTitle;
        public String locationName;
        public int dayOfWeek;
        public String start;
        public String finish;
        public boolean draft;
        public boolean open;
        public boolean swapShiftRequestPendingStatus;
        public boolean cancelled;
        public boolean confirmed;
        public boolean conflictStatus;
    }

    public static class EmailExists {
        public boolean exists;
    }

    public static class PlannerData
    {
        public List<CalEmployee> locationEmployees;
        public List<CalPosition> locationPositions;
        public List<CalShift> weekShifts;
        public List<CalShift> dayShifts;
        public List<SchedulerUnavailability> weekUnavailabilities;
        public List<SchedulerUnavailability> dayUnavailabilities;
    }

    public static class UnavailabilityData
    {
        public List<CalEmployee> locationEmployees;
        public List<CalPosition> locationPositions;
    }

    public static class SchedulerUnavailabilityBlock {
        public long id;
        public long personId;
        public String personName;
        public String title;
        public String description;
        public String startDate;
        public String endDate;
        List<SchedulerUnavailability> unavailabilities = new ArrayList<SchedulerUnavailability>();
    }

    public static class SchedulerLeaveRequest {
        public long id;
        public long personId;
        public String personName;
        public String startDate;
        public String endDate;
        public String comment;
        public String status;
    }

    public static class SchedulerUnavailability {
        public long id;
        public long dayOfWeek;
        public long personId;
        public String startTime;
        public String endTime;
        public long unavailabilityBlockId;
        public String title;
        public String description;
    }

    public static class CalEmployee {
        public long id;
        public String username;
        public String firstName;
        public String lastName;
        public List<CalEmployeePosition> positions = new ArrayList<CalEmployeePosition>();
        List<CalShift> calShifts = new ArrayList<CalShift>();
    }

    public static class CalEmployeePosition{
        public String title;
        public String description;
        public long id;
    }

    public static class CalPositionEmployee{
        public long id;
        public String username;
        public String firstName;
        public String lastName;
    }

    public static class CalPosition {
        public String title;
        public String description;
        public long id;
        public List<CalPositionEmployee> employees = new ArrayList<CalPositionEmployee>();
        List<CalShift> calShifts = new ArrayList<CalShift>();
    }

    public static class CalEmployeeProfile {
        public long id;
        public boolean isAdminView;
        public boolean isSelfView;
        public boolean isManagerView;
        public String password;
        public String firstName;
        public String lastName;
        public String emailAddress;
        public String address;
        public String suburb;
        public String state;
        public String country;
        public String postCode;
        public String contactNumber;
        public int dobDD;
        public int dobMM;
        public int dobYYYY;
        public float wage;
        public int minHoursBetweenShifts;
        public int maxHoursPerWeek;
        public List<CalPosition> positions;
        public List<CalLocation> locations;

    }

    public static class CalLocation {
        public long id;
        public String address;
        public String locationName;
        public String suburb;
        public String state;
        public String postcode;
        public String country;
        public String phoneNumber;
    }

    public static class CalRosterPositionCost {
        public long id;
        public String positionTitle;
        public float dayCost;
        public float dayHoursWorked;
        public float weekCost;
        public float weekHoursWorked;
    }

    public static class CalRosterEmployeeCost {
        public long id;
        public String firstName;
        public float dayCost;
        public float dayHoursWorked;
        public float weekCost;
        public float weekHoursWorked;
    }

    public static class CalRosterCost {
        public float dayHoursWorked;
        public float dayCost;
        public float weekHoursWorked;
        public float weekCost;
    }

    public static class CalSwapShiftRequest {
        public long id;
        public long fromShiftId;
        public String fromStrStart;
        public String fromStrFinish;
        public long fromDayOfWeek;
        public long toShiftId;
        public String toStrStart;
        public String toStrFinish;
        public long toDayOfWeek;
        public String fromPersonName;
        public String toPersonName;
        public String description;
        public ResponseType response;
    }

    public static class CalDropShiftRequest {
        public long id;
        public long fromShiftId;
        public String fromStrStart;
        public String fromStrFinish;
        public long fromDayOfWeek;
        public String fromPersonName;
        public String description;
        public String status;
    }

    public static boolean hasSetMiscPosition(Company company) {
        for (Position p : company.positions) {
            if (p.misc == true) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasSetMiscEmployee(Company company) {
        for (Person p : company.persons) {
            if (p.misc == true) {
                return true;
            }
        }
        return false;
    }

    public static String convertToReadableDate( Calendar date)
    {
        String returnString = date.get(Calendar.DATE)+"-"+date.get(Calendar.MONTH)+"-"+date.get(Calendar.YEAR);
        return returnString;
    }

    public static List<Shift> convertShiftsFromDraftToPublished(List<Shift> draftShifts, String loggedInUser) {
        for (Shift shift : draftShifts)  {
            shift.draft = false;
            shift.publishedStart = shift.start;
            shift.publishedFinish = shift.finish;
            shift.publishedPersonId = shift.person.id;
            shift.publishedPositionId = shift.position.id;
            shift.publishedOpen = shift.open;
            shift.lastUpdatedDate = new GregorianCalendar();
            shift.lastUpdatedBy = loggedInUser;
            shift.publishedDate = shift.lastUpdatedDate;
            shift.save();
        }
        return draftShifts;
    }

    public static List<Shift> getAllShiftsToBePublished(Calendar from, Calendar to) {
        String queryStr = "select s from Shift s where s.start >= ? and s.finish < ? and s.draft = ?";
        Query query = JPA.em().createQuery(queryStr);
        query.setParameter(1,from);
        query.setParameter(2,to);
        query.setParameter(3,true);
        List<Shift> shifts = (List<Shift>) query.getResultList();
        return shifts;
    }

    public static List<Person> getAllEmployeesWithDraftShifts(List<Shift> draftShifts, String loggedInUser) {
        List<Person> locationEmployees = getCurrentUserLocation(loggedInUser).persons;
        List<Person> employeesWithDraftShifts = new ArrayList<Person>();
        for (Person employee : locationEmployees) {
            for (Shift shift : draftShifts) {
                if (shift.person.equals(employee)) {
                    if (!employeesWithDraftShifts.contains(employee)) {
                        employeesWithDraftShifts.add(employee);
                    }
                }
            }
        }
        return employeesWithDraftShifts;
    }

    public static boolean shiftOverlapsWithShifts(List<Shift> shifts, Shift shift) {
        for (Shift s : shifts) {
            if (((s.start.getTimeInMillis() <= shift.start.getTimeInMillis()) &&
                    (s.finish.getTimeInMillis() >= shift.start.getTimeInMillis()))  ||
                    ((s.start.getTimeInMillis() <= shift.finish.getTimeInMillis()) &&
                            (s.finish.getTimeInMillis() >= shift.finish.getTimeInMillis())) &&
                            (s.person.id == shift.person.id)) {
                return true;
            }
        }
        return false;
    }




    // any shift that starts within this period (not necessarily finishes)
    public static List<Shift> getAllShiftsForEmployeeWithinPeriod(Calendar from, Calendar to, long personId) {
        String queryStr = "select s from Shift s where start >= ? and start < ? and person_id = ?";
        Query query = JPA.em().createQuery(queryStr);
        query.setParameter(1,from);
        query.setParameter(2,to);
        query.setParameter(3,personId);
        List<Shift> shifts = (List<Shift>) query.getResultList();
        return shifts;
    }

    public static List<Shift> getAllShiftsForLocationWithinPeriod(Calendar from, Calendar to, long locationId) {
        String queryStr = "select s from Shift s where start >= ? and start < ? and location_id = ?";
        Query query = JPA.em().createQuery(queryStr);
        query.setParameter(1,from);
        query.setParameter(2,to);
        query.setParameter(3,locationId);
        List<Shift> shifts = (List<Shift>) query.getResultList();
        return shifts;
    }

    public static List<Shift> getAllShiftsForPositionWithinPeriod(Calendar from, Calendar to, long positionId) {
        String queryStr = "select s from Shift s where start >= ? and start < ? and position_id = ?";
        Query query = JPA.em().createQuery(queryStr);
        query.setParameter(1,from);
        query.setParameter(2,to);
        query.setParameter(3,positionId);
        List<Shift> shifts = (List<Shift>) query.getResultList();
        return shifts;
    }


    public static List<Shift> getUpcomingShiftsForEmployee(Calendar today, long personId) {
        Logger.info("Getting upcoming shifts for employee");
        Calendar laterDate = (Calendar) today.clone();
        today.set(Calendar.HOUR_OF_DAY,0);
        today.set(Calendar.MINUTE,0);
        laterDate.add(Calendar.DATE, NUM_DAYS_AHEAD_FOR_PUBLISHED_SHIFTS);
        laterDate.set(Calendar.HOUR_OF_DAY,23);
        laterDate.set(Calendar.MINUTE,0);
        Logger.debug("today = " + today.getTime().toString());
        Logger.debug("laterDate = " + laterDate.getTime().toString());
        String queryStr = "select s from Shift s where start >= ? and finish < ? and draft = ? and person_id = ?";
        Query query = JPA.em().createQuery(queryStr);
        query.setParameter(1,today);
        query.setParameter(2,laterDate);
        query.setParameter(3,false);
        query.setParameter(4,personId);
        List<Shift> shifts = (List<Shift>) query.getResultList();
        return shifts;
    }

    public static void sendNotificationsAndReminders(List<Shift> draftShifts, List<Person> employees, Calendar today) {
        for (Person employee : employees) {
            Logger.info("*** Checking the following employee: " + employee.firstName);
            List<Shift> draftShiftsForEmployee = new ArrayList<Shift>();
            List<Shift> upcomingShifts = getUpcomingShiftsForEmployee(today,employee.id);
            List<Shift> newDraftShifts = new ArrayList<Shift>();
            List<Shift> updatedDraftShifts = new ArrayList<Shift>();
            List<Shift> cancelledDraftShifts = new ArrayList<Shift>();
            for (Shift shift : draftShifts) {
                if (shift.person.id == employee.id) {
                    draftShiftsForEmployee.add(shift);
                }
            }
            Logger.info("This employee has " + draftShiftsForEmployee.size() + " draft shifts");
            for (Shift shift : draftShiftsForEmployee) {
                Logger.info("*** Checking the following draft shiftId = " + shift.id);
                if (shift.publishedDate == null) {
                    Logger.info("Its a new shift");
                    newDraftShifts.add(shift);
                } else {
                    if (shift.cancelled == true) {
                        Logger.info("Its a cancelled shift");
                        cancelledDraftShifts.add(shift);
                    } else {
                        Logger.info("Its an updated shift");
                        if (shift.person.id != shift.publishedPersonId) {
                            Logger.info("Its a changed person shift");
                            cancelledDraftShifts.add(shift);
                            newDraftShifts.add(shift);
                        } else {
                            Logger.info("Its a normal updated shift");
                            updatedDraftShifts.add(shift);
                        }
                    }
                }
            }

            EmailNotifier.newShiftsNotification(newDraftShifts, upcomingShifts);
            EmailNotifier.updatedShiftsNotification(updatedDraftShifts,upcomingShifts);
            EmailNotifier.cancelledShiftNotification(cancelledDraftShifts,upcomingShifts);

        }
    }

    // Currently quite inefficient: look to speed this up if possible
    public static void sendNotificationsForUnassignedShifts(List<Shift> draftUnassignedShifts, String loggedInUser) {
        Location currentLoc = getCurrentUserLocation(loggedInUser);
        for (Position pos : currentLoc.company.positions) {
            Logger.info("*** Position = " + pos.title);
            List<Shift> draftUnassignedShiftsForPosition = new ArrayList<Shift>();
            for (Shift s : draftUnassignedShifts) {
                if (s.position.id == pos.id) {
                    Logger.info("*** Draft unassigned shifts for " + pos.title);
                    draftUnassignedShiftsForPosition.add(s);
                }
            }
            if (draftUnassignedShiftsForPosition.size() > 0) {
                Logger.info("*** Has some draft unassigned shifts");
                for (Person p : pos.persons) {
                    if (p.misc == false && p.locations.contains(currentLoc)) {
                        Logger.info("*** Sending an openshift notification to " + p.firstName);
                        EmailNotifier.openShiftNotification(draftUnassignedShiftsForPosition,p);
                    }
                }
            }
        }
    }

    public static Shift updateShift(Calendar weekStart, Calendar start, Calendar finish, float numHoursBreak,
                                     Position position, Person person, long shiftId, String lastUpdatedBy) {
        Logger.info("*** Inside the updateShift method ***");
        Shift shift = Shift.findById(shiftId);
        shift.weekStartDate = weekStart;
        shift.start = start;
        shift.finish = finish;
        shift.position = position;
        shift.person = person;
        if (shift.person.misc == true) {
            shift.open = true;
        }  else {
            shift.open = false;
        }
        shift.numHoursBreak = numHoursBreak;
        shift.lastUpdatedDate = new GregorianCalendar();
        shift.lastUpdatedBy = lastUpdatedBy;
        if (shift.publishedDate != null) {
            shift.draft = true;
        }
        shift.updateHoursWorked();
        shift.updateCost();
        shift.save();
        return shift;
    }

    public static CalShift convertToCalShift(Shift shift, int timeZoneOffset) {
        CalShift returnShift = new CalShift();
        returnShift.id = shift.id;
        returnShift.unassigned = shift.person.misc;
        returnShift.personId = shift.person.id;
        returnShift.personName = shift.person.firstName;
        returnShift.positionId = shift.position.id;
        returnShift.positionTitle = shift.position.title;
        returnShift.locationName = shift.location.locationName;
        // converting back from UTC to client timezone
        Calendar start = (Calendar) shift.start.clone();
        start.add(Calendar.MINUTE,-timeZoneOffset);
        Calendar finish = (Calendar) shift.finish.clone();
        finish.add(Calendar.MINUTE,-timeZoneOffset);
        returnShift.start = convertToReadableString(start);
        returnShift.finish = convertToReadableString(finish);
        returnShift.dayOfWeek = start.get(Calendar.DAY_OF_WEEK);
        returnShift.draft = shift.draft;
        returnShift.open = shift.open;
        returnShift.swapShiftRequestPendingStatus = shift.swapShiftRequestPending;
        returnShift.cancelled = shift.cancelled;
        returnShift.confirmed = shift.confirmed;
        returnShift.conflictStatus = shift.conflictStatus;
        return returnShift;
    }

    public static CalShift deleteShift(long shiftId, int timeZoneOffset) {
        Shift shift = Shift.findById(shiftId); //getShiftById(shiftId);
        Logger.debug("The shiftId to be deleted: " + shift.id );
        CalShift returnShift = convertToCalShift(shift, timeZoneOffset);
        long personId = shift.publishedPersonId;
        if (!shift.draft) {
            Logger.info("Shift is not a draft");
            List<Shift> cancelledShifts = new ArrayList<Shift>();
            cancelledShifts.add(shift);
            List<Shift> upcomingShifts = getUpcomingShiftsForEmployee(new GregorianCalendar(),personId);
            Logger.info("About to send the cancelled shift notification");
            EmailNotifier.cancelledShiftNotification(cancelledShifts,upcomingShifts);
        }
        shift.delete();
        Logger.info("Shift was successfully deleted");
        return returnShift;
    }


    public static Shift createShift(Location location,Calendar start, Calendar finish,float shiftBreak,Person person,
                                     Position position,Calendar lastUpdatedDate, Calendar weekStartDate, String lastUpdatedBy) {
        Logger.info("Inside the create shift method");
        Shift shift = new Shift(weekStartDate,start,finish,location,person,position,lastUpdatedDate,lastUpdatedBy);
        if (shift.person.misc == true) {
            shift.open = true;
        }
        shift.numHoursBreak = shiftBreak;
        shift.updateHoursWorked();
        shift.updateCost();
        shift.save();
        Logger.info("Just created shiftId = " + shift.id + " || draft = " + shift.draft);
        return shift;
    }

    public static ShiftSeries createShiftRecurrence(Shift sampleShift, Calendar endDate,
                                                     Frequency frequency, int increment, String lastUpdatedBy) {
        Logger.info("Inside the create shift recurrence method");
        ShiftSeries shiftSeries = new ShiftSeries(sampleShift,endDate,frequency,increment,lastUpdatedBy);
        shiftSeries.save();
        sampleShift.shiftSeries = shiftSeries;
        sampleShift.save();
        Logger.info("CREATING THE RECURRENCE");
        shiftSeries.createRecurrence(sampleShift, endDate, frequency, increment, lastUpdatedBy);
        return shiftSeries;
    }

    public static boolean compareDates(Calendar date1, Calendar date2)
    {
        if ( (date1.get(Calendar.DATE) == date2.get(Calendar.DATE)) && ( ( date1.get(Calendar.MONTH) ) == date2.get(Calendar.MONTH)) && (date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR)) )
            return true;
        else
            return false;
    }



    public static List<Shift> getShiftsFromShiftSeries(long shiftSeriesId) {
        String queryStr = "select s from Shift s where shiftSeries_id = ?";
        Query query = JPA.em().createQuery(queryStr);
        query.setParameter(1,shiftSeriesId);
        List<Shift> shifts = (List<Shift>) query.getResultList();
        return shifts;
    }


    // In format mm-dd-yyyy
    public static Calendar createCalFromUSShortDateStr(String shiftDateStr, String delimiter) {
        Logger.info("Inside the createCalFromShortDateStr method");
        Logger.debug("shiftDateStr=" + shiftDateStr);
        String[] dateComponents = shiftDateStr.split(delimiter);
        Logger.info("Parsed! Now about to take each component");
        String dd = dateComponents[1];
        String mm = dateComponents[0];
        String yyyy = dateComponents[2];
        int date = Integer.parseInt(dd);
        int month = Integer.parseInt(mm); // minus 1 because January = 0 (will need to put in a check)
        int year = Integer.parseInt(yyyy);
        Calendar cal = new GregorianCalendar();
        cal.set(year,month-1,date,0,0,0);
        return cal;
    }


    // In format dd-mm-yyyy (if delimeter = "-")
    public static Calendar createCalFromShortDateStr(String shiftDateStr, String delimiter) {
        //Logger.info("Inside the createCalFromShortDateStr method");
        //Logger.debug("shiftDateStr=" + shiftDateStr);
        String[] dateComponents = shiftDateStr.split(delimiter);
        //Logger.info("Parsed! Now about to take each component");
        String dd = dateComponents[0];
        String mm = dateComponents[1];
        String yyyy = dateComponents[2];
        int date = Integer.parseInt(dd);
        int month = Integer.parseInt(mm); // minus 1 because January = 0 (will need to put in a check)
        int year = Integer.parseInt(yyyy);
        Calendar cal = new GregorianCalendar();
        cal.set(year,month,date,0,0,0);
        return cal;
    }

    public static Location getCurrentUserLocation(String loggedInUser) {
        Location location = Person.findByEmailAddress(loggedInUser).getFirstLocation();
        return location;
    }

    public static float timeDifferenceInHours(Calendar start, Calendar finish) {
        float timeDiff = 0;
        int yearDiff = finish.get(Calendar.YEAR) - start.get(Calendar.YEAR);
        int monthDiff = finish.get(Calendar.MONTH) - start.get(Calendar.MONTH);
        int dayDiff = finish.get(Calendar.DATE) - start.get(Calendar.DATE);
        int hoursDiff = finish.get(Calendar.HOUR_OF_DAY) - start.get(Calendar.HOUR_OF_DAY);
        int minsDiff = finish.get(Calendar.MINUTE) - start.get(Calendar.MINUTE);
        int totalMonths = yearDiff * 12 + monthDiff;
        int numDaysInMonth = start.getActualMaximum(Calendar.MONTH) ;
        int totalDays = totalMonths * numDaysInMonth + dayDiff; // Assumption: shift only spans 1 month
        int totalHours = totalDays * 24 + hoursDiff;
        int totalMins = totalHours * 60 + minsDiff;
        timeDiff = (float) totalMins / 60 ;
        return timeDiff;
    }

    public static Calendar getPrevSunday(Calendar shiftDate)
    {
        Logger.info("Inside getPrevSunday");
        Logger.info("Date: " + convertToReadableString(shiftDate) + " | " + "Day of week before: " + shiftDate.get(Calendar.DAY_OF_WEEK) );
        shiftDate.set(Calendar.DAY_OF_WEEK,1);
        shiftDate.set(Calendar.HOUR_OF_DAY,0);
        shiftDate.set(Calendar.MINUTE,0);
        shiftDate.set(Calendar.SECOND,0);
        return shiftDate;
    }

    public static String convertToReadableString(Calendar date)
    {
        String returnString = date.get(Calendar.DATE)+"-"+date.get(Calendar.MONTH)+"-"+date.get(Calendar.YEAR)+" "+String.format("%02d",date.get(Calendar.HOUR_OF_DAY))+":"+String.format("%02d",date.get(Calendar.MINUTE));
        return returnString;
    }

    public static float calculateHoursWorkedInShifts(List<Shift> shifts) {
        float totalHours = 0;
        for (Shift s : shifts) {
            float hoursWorked = timeDifferenceInHours(s.start, s.finish);
            float realHoursWorked = hoursWorked - s.numHoursBreak;
            totalHours += realHoursWorked;
        }
        return totalHours;
    }

    public static List<Shift> getPublishedShiftsForLocationAndPosition(long locationId, long positionId, long personId) {
        Calendar today = new GregorianCalendar();
        String queryStr = "select s from Shift s where location_id = ? and position_id = ? and person_id <> ? and start > ? and draft = false";
        Query query = JPA.em().createQuery(queryStr);
        query.setParameter(1,locationId);
        query.setParameter(2,positionId);
        query.setParameter(3,personId);
        query.setParameter(4,today);
        List<Shift> shifts = (List<Shift>) query.getResultList();
        return shifts;

    }

    public static boolean checkIfCalendarsOverlap(Calendar start1, Calendar finish1, Calendar start2, Calendar finish2) {
        if(((start1.compareTo(start2) >= 0) && (start1.compareTo(finish2) <= 0) ) ||
                ((finish1.compareTo(start2) >= 0) && (finish1.compareTo(finish2) <= 0))) {
            return true;
        }
        return false;
    }

    public static boolean conflictsWithUnavailabilities(List<UnavailabilityBlock> unavailabilityBlocks, Shift shift) {
        for (UnavailabilityBlock ub : unavailabilityBlocks) {
            if (checkIfCalendarsOverlap(shift.start, shift.finish, ub.startDate, ub.endDate)) {
                for (Unavailability u : ub.unavailabilities) {
                    if (u.overlapsWithShift(shift)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public static boolean conflictsWithLeaveRequests(List<LeaveRequest> leaveRequests, Shift shift) {
        for (LeaveRequest lr : leaveRequests) {
            if (checkIfCalendarsOverlap(shift.start,shift.finish,lr.startDate,lr.endDate)) {
                return true;
            }
        }
        return false;
    }

    public static boolean conflictsWithExistingShifts(List<Shift> shifts, Shift shift) {
        for (Shift s : shifts) {
            if (!s.draft && checkIfCalendarsOverlap(shift.start,shift.finish,s.start,s.finish)) {
                return true;
            }
        }
        return false;
    }

    public static int getMinuteOfDayFrom24HourTimeString(String timeStr) {
        List<String> timeComponents = Arrays.asList(timeStr.split(":"));
        int time = 0;
        if (timeComponents.size() > 0) {
            time = Integer.parseInt(timeComponents.get(0)) * 60 + Integer.parseInt(timeComponents.get(1));
        }
        return time;
    }

    public static int getMinuteOfDayFromCalendar(Calendar cal) {
        return cal.get(Calendar.HOUR_OF_DAY) * 60 + cal.get(Calendar.MINUTE);
    }

    public static CalSwapShiftRequest convertToCalSwapShiftRequest(SwapShiftRequest swapShiftRequest, int timeZoneOffset) {
        CalSwapShiftRequest request = new CalSwapShiftRequest();
        request.id = swapShiftRequest.id;
        Calendar fromStart = (Calendar)swapShiftRequest.from.start.clone();
        Calendar fromFinish = (Calendar)swapShiftRequest.from.finish.clone();
        // converting from UTC to local time zone
        fromStart.add(Calendar.MINUTE,-1*timeZoneOffset);
        fromFinish.add(Calendar.MINUTE,-1*timeZoneOffset);
        request.fromStrStart = convertToReadableString(fromStart);
        request.fromStrFinish = convertToReadableString(fromFinish);
        request.fromShiftId = swapShiftRequest.from.id;
        request.fromDayOfWeek = swapShiftRequest.from.start.get(Calendar.DAY_OF_WEEK);
        Calendar toStart = (Calendar)swapShiftRequest.to.start.clone();
        Calendar toFinish = (Calendar)swapShiftRequest.to.finish.clone();
        // converting from UTC to local time zone
        toStart.add(Calendar.MINUTE,-1*timeZoneOffset);
        toFinish.add(Calendar.MINUTE,-1*timeZoneOffset);
        request.toStrStart = convertToReadableString(toStart);
        request.toStrFinish = convertToReadableString(toFinish);
        request.toShiftId = swapShiftRequest.to.id;
        request.toDayOfWeek = swapShiftRequest.to.start.get(Calendar.DAY_OF_WEEK);
        request.fromPersonName = swapShiftRequest.from.person.firstName + " " + swapShiftRequest.from.person.lastName;
        request.toPersonName = swapShiftRequest.to.person.firstName + " " + swapShiftRequest.to.person.lastName;
        request.description = swapShiftRequest.notes;
        request.response = swapShiftRequest.response;
        return request;
    }

    public static CalDropShiftRequest convertToCalDropShiftRequest(DropShiftRequest dropShiftRequest, int timeZoneOffset) {
        CalDropShiftRequest request = new CalDropShiftRequest();
        request.id = dropShiftRequest.id;
        Calendar start = (Calendar) dropShiftRequest.shift.start.clone();
        Calendar finish = (Calendar) dropShiftRequest.shift.finish.clone();
        // converting the time from UTC to local timezone
        start.add(Calendar.MINUTE,-1*timeZoneOffset);
        finish.add(Calendar.MINUTE,-1*timeZoneOffset);
        request.fromStrStart = convertToReadableString(start);
        request.fromStrFinish = convertToReadableString(finish);
        request.fromDayOfWeek = dropShiftRequest.shift.start.get(Calendar.DAY_OF_WEEK);
        request.fromPersonName = dropShiftRequest.fromPerson.firstName + " " + dropShiftRequest.fromPerson.lastName;
        request.description = dropShiftRequest.notes;
        request.status = dropShiftRequest.status.toString();
        return request;
    }

    public static String convertShiftToReadableString(Shift shift) {
        String startDay = DAY_NAMES[shift.publishedStart.get(Calendar.DAY_OF_WEEK) -1];
        Date d = new Date(shift.publishedStart.getTimeInMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("MMM");
        String monthStr = sdf.format(d);
        String startTime = String.format("%02d",shift.publishedStart.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d",shift.publishedStart.get(Calendar.MINUTE));
        String endTime = String.format("%02d",shift.publishedFinish.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d",shift.publishedFinish.get(Calendar.MINUTE));
        return (startDay + " " + shift.publishedStart.get(Calendar.DAY_OF_MONTH) + " " + monthStr + ", " + startTime + " to " + endTime);
    }

    public static void removeRelatedSwapShiftRequests(SwapShiftRequest swapShiftRequest) {
        String queryStr = "update SwapShiftRequest set response = ? where from_id = ? or from_id = ? or to_id = ? or to_id = ? and response = ?";
        Query query = JPA.em().createQuery(queryStr);
        query.setParameter(1,ResponseType.CANCELLED);
        query.setParameter(2,swapShiftRequest.from.id);
        query.setParameter(3,swapShiftRequest.to.id);
        query.setParameter(4,swapShiftRequest.from.id);
        query.setParameter(5,swapShiftRequest.to.id);
        query.setParameter(6,ResponseType.PENDING);
        query.executeUpdate();
    }
           /*
    public static Person getUnassignedEmployeeForPositions(List<Position> positions) {
        List<Person> unassignedEmployees = new ArrayList<Person>();
        for (Position pos : positions) {
            for (Person per : pos.persons) {
                if (per.misc) {
                    unassignedEmployees.add(per);
                }
            }
        }
        return unassignedEmployees;
    }
       */

    public static List<Person> getUnassignedEmployeeForEmployeeLocations(Person employee) {
        List<Person> persons = new ArrayList<Person>();
        for (Location l : employee.locations) {
            for ( Person p : l.persons )
            {
                if (p.misc) {
                    persons.add(p);
                }
            }
        }
        return persons;
    }

    public static CalLocation convertToCalLocation(Location location) {
        CalLocation calLocation = new CalLocation();
        calLocation.id = location.id;
        calLocation.locationName = location.locationName;
        calLocation.address = location.address;
        calLocation.country = location.country;
        calLocation.phoneNumber = location.contactNumber;
        calLocation.postcode = location.postCode;
        calLocation.state = location.state;
        calLocation.suburb = location.suburb;
        return calLocation;
    }

    public static CalPosition convertToCalPositionWithoutShifts(Position position) {
        CalPosition calPosition = new CalPosition();
        calPosition.id = position.id;
        calPosition.title = position.title;
        calPosition.description = position.description;
        return calPosition;
    }

    public static List<Shift> getEmployeeAndUnassignedShiftsForWeek(Calendar weekStart, Person employee) {
        Calendar nextWeekStart = (Calendar) weekStart.clone();
        nextWeekStart.add(Calendar.DATE,7);
        List<Shift> shifts = new ArrayList<Shift>();
        // get shifts assigned to the unassigned employee
        List<Person> unassigned = getUnassignedEmployeeForEmployeeLocations(employee);
        //If there are unassigned persons
        if (unassigned.size() > 0) {
            for ( Person unassignedPerson : unassigned )
            {
                for (Position position : employee.positions) {
                      for ( Shift s : unassignedPerson.shifts )
                      {
                          if ( s.position.title.equalsIgnoreCase(position.title) && compareDates(s.weekStartDate,weekStart))
                          {
                                shifts.add(s);
                          }
                      }
//                    String queryStr = "select s from Shift s where s.start >= ? and s.start <= ? and position_id = ? and person_id = ? and draft = false";
//                    Query query = JPA.em().createQuery(queryStr);
//                    query.setParameter(1,weekStart);
//                    query.setParameter(2,nextWeekStart);
//                    query.setParameter(3,location.id);
//                    query.setParameter(4,position.id);
//                    query.setParameter(5,unassigned.id);
//                    shifts.addAll((List<Shift>) query.getResultList());
                }
            }
        }
        // get the shifts assigned to the employee
        for ( Shift s : employee.shifts )
        {
            if ( compareDates(s.weekStartDate,weekStart ) && s.draft == false )
            {
                shifts.add(s);
            }
        }

        return shifts;
    }

    public static CalEmployee convertToCalEmployee(Person person)
    {
        CalEmployee returnEmployee = new CalEmployee();
        returnEmployee.firstName = person.firstName;
        returnEmployee.lastName = person.lastName;
        returnEmployee.username = person.emailAddress;
        returnEmployee.id = person.id;
        for ( Position position : person.positions ){
            CalEmployeePosition addPosition = convertToCalEmployeePosition(position);
            returnEmployee.positions.add(addPosition);
        }
        return returnEmployee;
    }

    public static CalPositionEmployee convertToCalPositionEmployee(Person person){
        CalPositionEmployee returnEmployee = new CalPositionEmployee();
        returnEmployee.firstName = person.firstName;
        returnEmployee.lastName = person.lastName;
        returnEmployee.username = person.emailAddress;
        returnEmployee.id = person.id;
        return returnEmployee;
    }

    public static CalPosition convertToCalPosition(Position position )
    {
        CalPosition returnPosition = new CalPosition();
        returnPosition.id = position.id;
        returnPosition.title = position.title;
        for(Person employee : position.persons){
            CalPositionEmployee addEmployee = convertToCalPositionEmployee(employee);
            returnPosition.employees.add(addEmployee);
        }
        return returnPosition;
    }

    public static CalPosition covertToCalPositionWithLocationEmployees(Position position, Long locationId){
        CalPosition returnPosition = new CalPosition();
        returnPosition.id = position.id;
        returnPosition.title = position.title;
        for(Person employee : position.persons){
            for(Location employeeLocation : employee.locations){
                if(employeeLocation.id == locationId){
                    //If the employee is assigned to that location, then only should we include the employee
                    CalPositionEmployee addEmployee = convertToCalPositionEmployee(employee);
                    returnPosition.employees.add(addEmployee);
                }
            }
        }
        return returnPosition;
    }

    public static CalEmployeePosition convertToCalEmployeePosition(Position position){
        CalEmployeePosition returnPosition = new CalEmployeePosition();
        returnPosition.id = position.id;
        returnPosition.title = position.title;
        return returnPosition;
    }

    public static CalEmployeeProfile convertToCalEmployeeProfile(Person person, boolean isAdminView, boolean isSelfView, boolean isManagerView) {
        CalEmployeeProfile employee = new CalEmployeeProfile();
        employee.id = person.id;
        employee.isAdminView = isAdminView;
        employee.isSelfView = isSelfView;
        employee.isManagerView = isManagerView;
        employee.firstName = person.firstName;
        employee.lastName = person.lastName;
        employee.address = person.address;
        employee.suburb = person.suburb;
        employee.contactNumber = person.contactNumber;
        employee.country = person.country;
        if (person.dob != null) {
            employee.dobDD = person.dob.get(Calendar.DAY_OF_MONTH);
            employee.dobMM = person.dob.get(Calendar.MONTH);
            employee.dobYYYY = person.dob.get(Calendar.YEAR);
        }
        employee.postCode = person.postCode;
        // Admin
        employee.locations = new ArrayList<CalLocation>();
        for (Location l : person.locations) {
            employee.locations.add(convertToCalLocation(l));
        }
        employee.positions = new ArrayList<CalPosition>();
        for (Position p : person.positions) {
            employee.positions.add(convertToCalPosition(p));
        }
        employee.wage = person.wage;
        employee.maxHoursPerWeek = person.maxHoursPerWeek;
        employee.minHoursBetweenShifts = person.minHoursBetweenShifts;
        employee.emailAddress = person.emailAddress;
        // Me
        employee.password = Crypto.decryptAES(person.password);
        return employee;
    }

    public static SchedulerUnavailability convertToSchedulerUnavailability(Unavailability unavailability, int timeZoneOffset)
    {
        SchedulerUnavailability returnUnavailability = new SchedulerUnavailability();
        returnUnavailability.id = unavailability.id;
        returnUnavailability.personId = unavailability.person.id;
        Calendar startDateTime = (Calendar) unavailability.startDateTime.clone();
        startDateTime.add(Calendar.MINUTE, -timeZoneOffset);
        Calendar endDateTime = (Calendar) unavailability.endDateTime.clone();
        endDateTime.add(Calendar.MINUTE, -timeZoneOffset);
        returnUnavailability.startTime = convertCalendarTo24HourTimeString(startDateTime);
        returnUnavailability.endTime = convertCalendarTo24HourTimeString(endDateTime);
        returnUnavailability.dayOfWeek = unavailability.dayOfWeek;
        returnUnavailability.unavailabilityBlockId = unavailability.unavailabilityBlock.id;
        returnUnavailability.title = unavailability.unavailabilityBlock.title;
        returnUnavailability.description = unavailability.unavailabilityBlock.description;
        return returnUnavailability;
    }

    public static String convertCalendarTo24HourTimeString(Calendar dateTime) {
        String hh = String.format("%02d",dateTime.get(Calendar.HOUR_OF_DAY));
        String mm = String.format("%02d",dateTime.get(Calendar.MINUTE));
        return hh + ":" + mm;
    }

    public static String getAlphaNumeric(int len) {
        StringBuffer sb = new StringBuffer(len);
        for (int i = 0; i < len; i++) {
            int ndx = (int) (Math.random() * ALPHA_NUM.length());
            sb.append(ALPHA_NUM.charAt(ndx));
        }
        return sb.toString();
    }


}


