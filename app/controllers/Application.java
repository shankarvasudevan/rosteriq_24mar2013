package controllers;

import com.google.gson.Gson;
import controllers.ApplicationHelper.*;
import controllers.deadbolt.*;
import flexjson.JSONSerializer;
import models.*;
import notifiers.EmailNotifier;
import org.json.JSONArray;
import org.json.JSONObject;
import play.Logger;
import play.db.jpa.JPA;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.Scope;
import play.mvc.With;

import javax.persistence.Query;
import java.util.*;

@With(Deadbolt.class)
public class Application extends Controller {
    static final int NUM_DAYS_AHEAD_FOR_PUBLISHED_SHIFTS = 7;
    static String[] months = new String[]{"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
    static final ApplicationLogger applicationLog = new ApplicationLogger();
    static final Scope.Session session = Scope.Session.current();

    public static class ApplicationLogger{
        public boolean index;
        public boolean dashboard;
        public boolean planner;
        public boolean myRoster;
        public boolean leaveRequests;
        public boolean requests;
        public boolean getEmployeeShiftsForWeek;
        public boolean unavailability;
        public boolean publishChanges;
        public boolean createOrUpdateShifts;
        public boolean createOrUpdateUnavailability;
        public boolean createOrUpdateLeaveRequest;
        public boolean getEmployeeLeaveRequests;
        public boolean getEmployeeUnavailabilityBlocks;
        public boolean getUnavailabilityBlock;
        public boolean getEmployeePositions;
        public boolean getPositionEmployees;
        public boolean getEmployeeNameById;
        public boolean getPositionNameById;
        public boolean getEmployeeNameByUsername;
        public boolean getLocationPersons;
        public boolean getLocationPositions;
        public boolean getLocationWeekShifts;
        public boolean getLocationDayShifts;
        public boolean getEmployeeUnavailabilitiesForDay;
        public boolean getEmployeeUnavailabilitiesForWeek;
        public boolean getRosterCost;
        public boolean loadShiftsFromSelectedDay;
        public boolean loadShiftsFromPrevWeek;
        public boolean getPossibleSwapShifts;
        public boolean updateShiftConfirmationStatus;
        public boolean getUserSwapRequestsToPerson;
        public boolean getUserSwapRequestsFromPerson;
        public boolean executeShiftSwap;
        public boolean declineShiftSwap;

        public ApplicationLogger()
        {

            this.index = false;
            this.dashboard = false;
            this.planner = false;
            this.myRoster = false;
            this.leaveRequests = false;
            this.requests = true;
            this.getEmployeeShiftsForWeek = false;
            this.unavailability = false;
            this.publishChanges = false;
            this.createOrUpdateShifts = false;
            this.createOrUpdateUnavailability = false;
            this.createOrUpdateLeaveRequest = false;
            this.getEmployeeLeaveRequests = false;
            this.getEmployeeUnavailabilityBlocks = false;
            this.getUnavailabilityBlock = true;
            this.getEmployeePositions = false;
            this.getPositionEmployees = false;
            this.getEmployeeNameById = false;
            this.getPositionNameById = false;
            this.getEmployeeNameByUsername = false;
            this.getLocationPersons = false;
            this.getLocationPositions = false;
            this.getLocationWeekShifts = false;
            this.getLocationDayShifts = false;
            this.getEmployeeUnavailabilitiesForDay = false;
            this.getEmployeeUnavailabilitiesForWeek = true;
            this.getRosterCost = false;
            this.loadShiftsFromSelectedDay = false;
            this.loadShiftsFromPrevWeek = false;
            this.getPossibleSwapShifts = false;
            this.getUserSwapRequestsToPerson = false;
            this.getUserSwapRequestsFromPerson = false;
            this.executeShiftSwap = false;
            this.declineShiftSwap = false;
            this.updateShiftConfirmationStatus = false;
        }
    }

    //Url rendering methods
    @Unrestricted
    public static void index() {
        if ( applicationLog.index ) Logger.info("Inside the index method");
        redirect("/myRoster");
    }

    public static void timeCreation(){
        render();
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void planner() {
        if ( applicationLog.planner ) Logger.info("Inside the scheduler method");
        String loggedInUser = Scope.Session.current().get("username");
        Person loggedInPerson = Person.findByEmailAddress(loggedInUser);
        List<Location> locations = loggedInPerson.locations;
        //List<Person> persons = Person.all().fetch();
        //List<Position> positions = Position.all().fetch();
        render(locations);
    }

    @RoleHolderPresent
    public static void setTimeZoneOffset(int timeZoneOffset) {
        session.put("timeZoneOffset",timeZoneOffset);
    }

    @RoleHolderPresent
    public static void unavailability() {
        if ( applicationLog.unavailability ) Logger.info("Inside the unavailability method");
//        List<Person> persons = Person.all().fetch();
//        List<Position> positions = Position.all().fetch();
        String loggedInUser = session.get("username");
        Person loggedInPerson = Person.findByEmailAddress(loggedInUser);
        List<Location> locations = loggedInPerson.locations;
        //Then we'll get the persons for the first location
        List<Person> employees = locations.get(0).persons;
        render(locations,employees);
    }

    @RoleHolderPresent
    public static void myRoster(){
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        if ( applicationLog.myRoster ) Logger.info("Inside the myRoster method");
        //Get the current user
        String loggedInUser = session.get("username");
        String timeZoneOffset = session.get("timeZoneOffset");
        Logger.info("The timeZone is: " + timeZoneOffset);
        Person user = Person.findByEmailAddress(loggedInUser);
        Calendar weekStart = new GregorianCalendar();
        weekStart.add(Calendar.MINUTE, Integer.parseInt(timeZoneOffset));
        render(user, weekStart);
    }

    @RoleHolderPresent
    public static void redirect(){
        render();
    }

    @RoleHolderPresent
    public static void userSetup(String timeZoneOffset){
        session.put("timeZoneOffset",timeZoneOffset);
        session.put("username",request.cookies.get("loggedInUser").value);
        Application.myRoster();
    }

    @RoleHolderPresent
    public static void leaveRequests(){
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();

        if ( applicationLog.leaveRequests ) Logger.info("Inside the leaveRequests method");
        render();
    }

    @RoleHolderPresent
    public static void requests(){
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();

        if( applicationLog.requests ) Logger.info("*** Inside the requests method ***");
        render();
    }

    @RoleHolderPresent
    public static void administration(){
        //This will render the administration screen
        //First we'll get the necessary information
        String loggedInUser = session.get("username");
        Person user = Person.findByEmailAddress(loggedInUser);
        Company company = user.company;
        List<Location> companyLocations = company.locations;
        List<Position> companyRoles = company.positions;
        List<Person> companyEmployees = company.persons;
        render(company,companyLocations,companyRoles,companyEmployees);
    }


    public static void emailAlreadyExists(String email)
    {
        Query query = JPA.em().createQuery("select emailAddress from Person");
        List<String> emailAddresses = (List<String>) query.getResultList();
        boolean foundMatch = false;
        for (int i=0; i < emailAddresses.size() && !foundMatch; i++) {
            if (email.equalsIgnoreCase(emailAddresses.get(i))) {
                foundMatch = true;
            }
        }
        EmailExists emailExistsObj = new EmailExists();
        emailExistsObj.exists = foundMatch;
        renderJSON(emailExistsObj);
    }

    public static void createOrUpdateLocation(String locationJson){
        Gson gson = new Gson();
        locationObject location = gson.fromJson(locationJson,locationObject.class);
        String loggedInUser = session.get("username");
        Person user = Person.findByEmailAddress(loggedInUser);
        Company company = user.company;
        Logger.info("locationJson.operation: " + location.operation);
        if(location.operation.compareTo("new") == 0){
            //We'll then create the new location
            Logger.info("Inside the new location section");
            Location newLocation = new Location(company,location.locationName, location.locationAddress);
            newLocation.save();
            Application.administration();
        }else if(location.operation.compareTo("update") == 0){
            Logger.info("Inside the update location section for location id: " + location.id);
            Location.updateLocation(location.id, location.locationName, location.locationAddress);
            Application.administration();
        }else if(location.operation.compareTo("delete") == 0){
            Logger.info("Inside the delete location section for location id: " + location.id);
            Location.deleteLocation(location.id);
            Application.administration();
        }
    }

    public static void createOrUpdateEmployee(String employeeJson){
        Logger.info("locationJsonString: " + employeeJson);
        Gson gson = new Gson();
        employeeObject employee = gson.fromJson(employeeJson,employeeObject.class);
        String loggedInUser = session.get("username");
        Person user = Person.findByEmailAddress(loggedInUser);
        Company company = user.company;
        Logger.info("employeeJson.operation: " + employee.operation);
        if(employee.operation.compareTo("new") == 0){
            //We'll then create the new location
            Logger.info("Inside the new employee section");
            Person newEmployee = new Person(company, ApplicationHelper.getAlphaNumeric(6), employee.firstName, employee.lastName,employee.email,"");
            newEmployee.save();
            for(String locationName : employee.locations){
                List<Location> locationResult = Location.find("locationName",locationName).fetch();
                Location location = locationResult.get(0);
                if(!(newEmployee.locations.contains(location))){
                    newEmployee.locations.add(location);
                    location.persons.add(newEmployee);
                    location.save();
                }
            }
            for(String roleName : employee.roles){
                List<Position> roleResult = Position.find("title",roleName).fetch();
                Position role = roleResult.get(0);
                if(!(newEmployee.positions.contains(role))){
                    newEmployee.positions.add(role);
                    role.persons.add(newEmployee);
                    role.save();
                }
            }
            newEmployee.save();
            Application.administration();
        } else if(employee.operation.compareTo("update") == 0){
            Logger.info("Inside the update employee section for location id: " + employee.id);
            Person.updateEmployee(employee.firstName, employee.lastName, employee.email, employee.id);
            Person updatePerson = Person.findById(employee.id);
            //This will determine if there is a new location to be added
            for(String locationName : employee.locations){
                List<Location> locationResult = Location.find("locationName",locationName).fetch();
                Location location = locationResult.get(0);
                if(!(updatePerson.locations.contains(location))){
                    updatePerson.locations.add(location);
                    location.persons.add(updatePerson);
                    location.save();
                }
            }
            //This will determine if there is a location that should be deleted
            for (Location employeeLocation : updatePerson.locations) {
                if (!(employee.locations.contains(employeeLocation.locationName))) {
                    employeeLocation.persons.remove(updatePerson);
                    employeeLocation.save();
                }
            }
            //This will determine if there is a new location to be added
            for(String roleName : employee.roles){
                List<Position> roleResult = Position.find("title",roleName).fetch();
                Position role = roleResult.get(0);
                if(!(updatePerson.positions.contains(role))){
                    updatePerson.positions.add(role);
                    role.persons.add(updatePerson);
                    role.save();
                }
            }
            //This will determine if there is a role that should be deleted
            for (Position employeeRole : updatePerson.positions) {
                if (!(employee.roles.contains(employeeRole.title))) {
                    employeeRole.persons.remove(updatePerson);
                    employeeRole.save();
                }
            }
            updatePerson.save();
            Application.administration();
        }else if(employee.operation.compareTo("delete") == 0){
            Logger.info("Inside the delete employee section for employee id: " + employee.id);
            //Then we'll find this employee in the company
            Person person = Person.findById(employee.id);
            person.delete();
            Application.administration();
        }
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void settings() {
        String loggedInUser = session.get("username");
        Person user = Person.findByEmailAddress(loggedInUser);
        Company company = user.company;
        List<Location> locations = company.locations;
        List<Position> allPositions = company.positions;
        List<Position> positions = new ArrayList<Position>();
        for (Position p : allPositions) {
            if (!p.misc) {
                positions.add(p);
            }
        }
        List<Settings> allSettings = Settings.findAll();
        Settings settings = null;
        for (Settings s : allSettings)  {
            if (s.company.id == company.id && s.person.id == user.id) {
                settings = s;
            }
        }
        if (settings == null) {
            Logger.info("settings is null");
            if (user.accountType == AccountType.COMPANY_MANAGER || user.accountType == AccountType.LOCATION_MANAGER) {
                settings = new Settings();
                settings.company = company;
                settings.person = user;
                settings.save();
            }  else {
                redirect("");
            }
        }
        render(company,user,settings,locations,positions);
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void dashboard() {
        // fetch from Cookie
        String loggedInUser = session.get("username");
        if (loggedInUser.isEmpty()) {
            error("Cannot find the logged in user");
        } else {
            List<Person> persons = Person.find("emailAddress",loggedInUser).fetch();
            // persons should not be empty because the user has already been authenticated
            if ( applicationLog.dashboard ) Logger.debug("The user's emailAddress = " + loggedInUser);
            Person person = persons.get(0);
            render(person);
        }
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void admin() {

        String loggedInUser = session.get("username");
        Person user = Person.findByEmailAddress(loggedInUser);
        List<Company> companies = new ArrayList<Company>();
        Company company = user.company;
        Settings settings = null;
        if (user.accountType == AccountType.ADMIN) {
            companies = Company.findAll();
        } else {
            companies.add(company);
            List<Settings> allSettings = Settings.findAll();
            for (Settings s : allSettings)  {
                if (s.company.id == company.id && s.person.id == user.id) {
                    settings = s;
                }
            }
            if (settings == null) {
                Logger.info("settings is null");
                if (user.accountType == AccountType.COMPANY_MANAGER || user.accountType == AccountType.LOCATION_MANAGER) {
                    settings = new Settings();
                    settings.company = company;
                    settings.person = user;
                    settings.save();
                }  else {
                    redirect("");
                }
            }
        }
        render(companies,user,company,settings);
    }

    // New controller functions from PHP
    public static void actionGetWeekShiftsFromOtherLocationsForLocationEmployees(long locationId, String weekStart) {
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        Location plannerLocation = Location.findById(locationId);
        String[] weekStartComponents = weekStart.split("-");
        Calendar weekStartCal = new GregorianCalendar(Integer.parseInt(weekStartComponents[0]),Integer.parseInt(weekStartComponents[1]),Integer.parseInt(weekStartComponents[2]),0,0,0);
        Calendar weekEndCal = (Calendar) weekStartCal.clone();
        weekEndCal.add(Calendar.DATE,7);
        List<CalShift> weekShiftsFromOtherLocationsForLocationEmployees = new ArrayList<CalShift>();
        for (Person person : plannerLocation.persons) {
            if (person.locations.size() > 1) {
                List<Shift> personShifts = ApplicationHelper.getAllShiftsForEmployeeWithinPeriod(weekStartCal,weekEndCal,person.id);
                for (Shift ps : personShifts) {
                    if (ps.location.id != plannerLocation.id && !ps.draft) {
                        CalShift cs = ApplicationHelper.convertToCalShift(ps,timeZoneOffset);
                        weekShiftsFromOtherLocationsForLocationEmployees.add(cs);
                    }
                }
            }
        }
        renderJSON(weekShiftsFromOtherLocationsForLocationEmployees);
    }

    public static void actionGetDayShiftsFromOtherLocationsForLocationEmployees(long locationId, String currentDateStr) {
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        Location plannerLocation = Location.findById(locationId);
        String[] currDateComponents = currentDateStr.split("-");
        Calendar currDateCal = new GregorianCalendar(Integer.parseInt(currDateComponents[0]),Integer.parseInt(currDateComponents[1]),Integer.parseInt(currDateComponents[2]),0,0,0);
        Calendar nextDateCal = (Calendar) currDateCal.clone();
        nextDateCal.add(Calendar.DATE,1);
        List<CalShift> weekShiftsFromOtherLocationsForLocationEmployees = new ArrayList<CalShift>();
        for (Person person : plannerLocation.persons) {
            if (person.locations.size() > 1) {
                List<Shift> personShifts = ApplicationHelper.getAllShiftsForEmployeeWithinPeriod(currDateCal,nextDateCal,person.id);
                for (Shift ps : personShifts) {
                    if (ps.location.id != plannerLocation.id && !ps.draft) {
                        CalShift cs = ApplicationHelper.convertToCalShift(ps,timeZoneOffset);
                        weekShiftsFromOtherLocationsForLocationEmployees.add(cs);
                    }
                }
            }
        }
        renderJSON(weekShiftsFromOtherLocationsForLocationEmployees);
    }

    private void evaluateEmployeeConflictShifts(Person employee) {
        for (Shift s : employee.shifts) {
            checkAllConflicts(s);
        }
    }

    private void checkAllConflicts(Shift shift) {
        shift.conflictStatus = checkEmployeeLocationShiftConflict(shift) || checkEmployeeUnavailabilityConflict(shift);
    }

    private boolean checkEmployeeLocationShiftConflict(Shift shift) {
        Person person = Person.findById(shift.person.id);
        for (Shift ps : person.shifts) {
            if (ps.id != shift.id) {
                if (ApplicationHelper.checkIfCalendarsOverlap(ps.start,ps.finish,shift.start,shift.finish)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkEmployeeUnavailabilityConflict(Shift shift) {
        Person person = Person.findById(shift.person.id);
        for (UnavailabilityBlock pub : person.unavailabilityBlocks) {
            for (Unavailability pu : pub.unavailabilities) {
                if (ApplicationHelper.checkIfCalendarsOverlap(pu.startDateTime,pu.endDateTime,shift.start,shift.finish)) {
                    return true;
                }
            }
        }
        return false;
    }

    // UNAVAILABILITY CONTROLLER FUNCTIONS
    public static void getUnavailabilityBlockUnavailabilities(long companyId) {
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        Company company = Company.findById(companyId);
        List<SchedulerUnavailabilityBlock> unavailabilityBlocks = new ArrayList<SchedulerUnavailabilityBlock>(timeZoneOffset);
        for (Person p : company.persons) {
            for (UnavailabilityBlock ub : p.unavailabilityBlocks) {
                for (Unavailability u : ub.unavailabilities) {
                    SchedulerUnavailability schedulerUnavailability = ApplicationHelper.convertToSchedulerUnavailability(u,timeZoneOffset);
                }
            }
        }
    }

    // AJAX METHODS
    @RoleHolderPresent
    public static void getEmployeeShiftsForWeek(String dateString)
    {
        //Set up the logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        Person employee = Person.findByEmailAddress(session.get("username"));

        if ( applicationLog.getEmployeeShiftsForWeek ) Logger.debug("Employee name: " +  employee.firstName);
        List<CalShift> returnShifts = new ArrayList<CalShift>();
        //Now we shall convert the dateString into a Calendar date
        List<String> startDateComponents = Arrays.asList(dateString.split("-"));
        Calendar weekStart = new GregorianCalendar();
        weekStart.set(Integer.parseInt(startDateComponents.get(2)),Integer.parseInt(startDateComponents.get(1)), Integer.parseInt(startDateComponents.get(0)));
        for ( Shift shift : ApplicationHelper.getEmployeeAndUnassignedShiftsForWeek(weekStart, employee))
        {
            if ( applicationLog.getEmployeeShiftsForWeek ) Logger.debug("ShiftWeekStart: " + ApplicationHelper.convertToReadableDate(shift.weekStartDate) + ", RequiredWeekStartDate: " + ApplicationHelper.convertToReadableDate(weekStart) + ", Shift Status( true:Draft, false:Published ): " + shift.draft );
                //So we shall add this date to the returnDates
            CalShift addShift = ApplicationHelper.convertToCalShift(shift,timeZoneOffset);
            returnShifts.add(addShift);
            if ( applicationLog.getEmployeeShiftsForWeek ) Logger.debug("ReturnShifts Count: " + returnShifts.size());
        }
        renderJSON(returnShifts);
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void updateSettings(long settingsId, boolean notifyManagerOfShiftSwaps, boolean notifyEmployeesOfOpenShifts,
                                      boolean showCostOfShiftInScheduler, boolean allowEmployeeToSwapShifts,
                                      boolean approvalRequiredForDropShifts, boolean employeesCanSwapShiftsOnSameDay) {
        Logger.info("*** Inside update Settings ***");
        Settings settings = Settings.findById(settingsId);
        settings.notifyManagerOfShiftSwaps = notifyManagerOfShiftSwaps;
        settings.notifyEmployeesOfOpenShifts = notifyEmployeesOfOpenShifts;
        settings.showCostOfShiftInScheduler = showCostOfShiftInScheduler;
        settings.allowEmployeeToSwapShifts = allowEmployeeToSwapShifts;
        settings.approvalRequiredForDropShifts = approvalRequiredForDropShifts;
        settings.employeesCanSwapShiftsOnSameDay = employeesCanSwapShiftsOnSameDay;
        settings.save();
        admin();
    }

    @Restrictions({@Restrict("ADMIN")})
    public static void encryptAllPasswords() {
        List<Person> persons = Person.findAll();
        for (Person p : persons) {
            p.password = Crypto.encryptAES(p.password);
            p.save();
        }
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void createMiscPosition()
    {
        String loggedInUser = session.get("username");
        Company company = ApplicationHelper.getCurrentUserLocation(loggedInUser).company;
        if (!ApplicationHelper.hasSetMiscPosition(company)) {
            List<Location> allLocations = company.locations;
            Position miscPosition = new Position("Unassigned","",company);
            for (Location location : allLocations) {
                miscPosition.misc = true;
                miscPosition.save();
                for (Person person : location.persons) {
                    person.positions.add(miscPosition);
                    miscPosition.persons.add(person);
                    person.save();
                    miscPosition.save();
                }
            }
            Logger.info("Successfully created the Unassigned position");
        }  else {
            Logger.info("Already previously set an unassigned position. Max 1 unassigned position per location");
        }

    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void getPlannerDataForLocation(long locationId, String startDate, String calDate, long dayOfWeek)
    {
        //Get the current location
        Location location = Location.findById(locationId);
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        //We will the get all the data that is related to this location and the dates provided
        ApplicationHelper.PlannerData plannerData = new ApplicationHelper.PlannerData();
        plannerData.locationEmployees = getLocationPersons(locationId);
        plannerData.locationPositions = getLocationPositions(locationId);
        plannerData.weekShifts = getLocationWeekShifts(startDate,timeZoneOffset,locationId);
        plannerData.dayShifts = getLocationDayShifts(calDate,timeZoneOffset,locationId);
        plannerData.weekUnavailabilities = getEmployeeUnavailabilitiesForWeek(startDate,locationId);
        plannerData.dayUnavailabilities = getEmployeeUnavailabilitiesForDay(calDate,locationId);
        renderJSON(plannerData);
    }

    public static void getUnavailabilityDataForLocation(long locationId)
    {
        //Get the current location
        Location location = Location.findById(locationId);
        //We will the get all the data that is related to this location and the dates provided
        UnavailabilityData unavailabilityData = new UnavailabilityData();
        unavailabilityData.locationEmployees = getLocationPersons(locationId);
        unavailabilityData.locationPositions = getLocationPositions(locationId);
        renderJSON(unavailabilityData);
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void createMiscEmployee()
    {
        String loggedInUser = session.get("username");
        Company company = ApplicationHelper.getCurrentUserLocation(loggedInUser).company;
        if (!ApplicationHelper.hasSetMiscEmployee(company)) {
            List<Location> allLocations = company.locations;
            List<Position> allPositions = company.positions;
            Person miscPerson = new Person(company,"","Unassigned","","misc","");
            miscPerson.misc = true;
            miscPerson.wage = (float) 0;
            miscPerson.save();
            for (Location location : allLocations) {
                miscPerson.locations.add(location);
                location.persons.add(miscPerson);
                location.save();
                miscPerson.save();
            }
            for (Position position : allPositions) {
                miscPerson.positions.add(position);
                position.persons.add(miscPerson);
                position.save();
                miscPerson.save();
            }
            Logger.info("Successfully created the unassigned employee");
        } else {
            Logger.info("Already previously set an Unassigned employee. Max 1 unassigned employee per company");
        }
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void publishChanges(long fromDateTime, String dayOrWeek)
    {
        // Date viewStartDate = new Date(fromDateTime);
        // Date todayDate = new Date(todayDateTime);
        String loggedInUser = session.get("username");
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        Calendar from = new GregorianCalendar();
        from.setTimeInMillis(fromDateTime);
        from.set(Calendar.HOUR_OF_DAY, 0);
        from.set(Calendar.MINUTE,0);
        //offsetting the timeZone, to get the UTC time
        from.add(Calendar.MINUTE,timeZoneOffset);
        Calendar to = (Calendar) from.clone();
        Calendar today = new GregorianCalendar();
        today.set(Calendar.HOUR_OF_DAY,0);
        today.set(Calendar.MINUTE, 0);
        if ("week".compareToIgnoreCase(dayOrWeek) == 0) {
            if ( applicationLog.publishChanges ) Logger.info("*** It is a week view ***");
            to.add(Calendar.DATE,7);
        } else if ("day".compareToIgnoreCase(dayOrWeek) == 0) {
            if ( applicationLog.publishChanges ) Logger.info("*** It is a day view ***");
            to.add(Calendar.DATE,1);
        }
        if ( applicationLog.publishChanges ) Logger.debug("*** About to get all the shifts to be published ***");
        if ( applicationLog.publishChanges ) Logger.info("*** FROM = " + from.getTime().toString());
        if ( applicationLog.publishChanges ) Logger.info("*** TO = " + to.getTime().toString());
        if ( applicationLog.publishChanges ) Logger.info("*** TODAY = " + today.getTime().toString());
        List<Shift> draftShifts = ApplicationHelper.getAllShiftsToBePublished(from, to);
        List<Shift> draftAssignedShifts = new ArrayList<Shift>();
        List<Shift> draftUnassignedShifts = new ArrayList<Shift>();
        for (Shift s : draftShifts) {
            if (s.open == true) {
                draftUnassignedShifts.add(s);
            } else {
                draftAssignedShifts.add(s);
            }
        }
        // sending notifications for unassigned shifts
        ApplicationHelper.sendNotificationsForUnassignedShifts(draftUnassignedShifts, loggedInUser);
        // sending notifications for assigned shifts
        List<Person> assignedEmployees = ApplicationHelper.getAllEmployeesWithDraftShifts(draftAssignedShifts, loggedInUser);
        ApplicationHelper.sendNotificationsAndReminders(draftAssignedShifts, assignedEmployees, today);
        // converting the draft shifts to published
        ApplicationHelper.convertShiftsFromDraftToPublished(draftShifts, loggedInUser);
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void loadShiftsFromSelectedDay(long currDateTime, String selectedDate)
    {
        if (applicationLog.loadShiftsFromSelectedDay) Logger.info("*** Inside loadShiftsFromSelectedDay ***");
        if (applicationLog.loadShiftsFromSelectedDay) Logger.info("currDateTime = " + currDateTime);
        if (applicationLog.loadShiftsFromSelectedDay) Logger.info("selectedDate = " + selectedDate);
        String loggedInUser = session.get("username");
        Location location = ApplicationHelper.getCurrentUserLocation(loggedInUser);
        Calendar selectedDay = ApplicationHelper.createCalFromUSShortDateStr(selectedDate, "/");
        // offsetting the timeZone, to get the UTC time
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        selectedDay.add(Calendar.MINUTE,timeZoneOffset);
        Calendar nextDay = (Calendar) selectedDay.clone();
        nextDay.add(Calendar.DATE,1);
        if (applicationLog.loadShiftsFromSelectedDay) Logger.info("selectedDay="+selectedDay.getTime().toString());
        if (applicationLog.loadShiftsFromSelectedDay) Logger.info("nextDay="+nextDay.getTime().toString());
        List<Shift> selectedDayShifts = ApplicationHelper.getAllShiftsForLocationWithinPeriod(selectedDay, nextDay, location.id);
        Calendar currDay = new GregorianCalendar();
        currDay.setTimeInMillis(currDateTime);
        // offsetting the timeZone, to get the UTC time
        currDay.add(Calendar.MINUTE,timeZoneOffset);
        Calendar currWeekStart = ApplicationHelper.getPrevSunday((Calendar) currDay.clone());
        Calendar lastUpdatedDate = new GregorianCalendar();
        String lastUpdatedBy = session.get("username");
        int numDaysToAdd = currDay.get(Calendar.DAY_OF_YEAR) - selectedDay.get(Calendar.DAY_OF_YEAR);
        List<CalShift> newlyAddedShifts = new ArrayList<CalShift>();
        for (Shift prevShift : selectedDayShifts) {
            Calendar newStart = (Calendar) prevShift.start.clone();
            Calendar newFinish = (Calendar) prevShift.finish.clone();
            newStart.add(Calendar.DATE,numDaysToAdd);
            newFinish.add(Calendar.DATE,numDaysToAdd);
            Shift newShift = new Shift(currWeekStart,newStart,newFinish,prevShift.location,
                    prevShift.person,prevShift.position,lastUpdatedDate,lastUpdatedBy);
            newShift.save();
            CalShift newCalShift = ApplicationHelper.convertToCalShift(newShift,timeZoneOffset);
            newlyAddedShifts.add(newCalShift);
        }
        renderJSON(newlyAddedShifts);
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void loadShiftsFromPrevWeek(long currWeekStartDateTime, int numWeeks)
    {
        if (applicationLog.loadShiftsFromPrevWeek) Logger.info("*** Inside LOADSHIFTSFROMSELECTEDWEEK ***");
        if (applicationLog.loadShiftsFromPrevWeek) Logger.info("currWeekStartDateTime = " + currWeekStartDateTime);
        if (applicationLog.loadShiftsFromPrevWeek) Logger.info("numWeeks = " + numWeeks);
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        Calendar currWeekStart = new GregorianCalendar();
        currWeekStart.setTimeInMillis(currWeekStartDateTime);
        currWeekStart.set(Calendar.HOUR_OF_DAY, 0);
        currWeekStart.set(Calendar.MINUTE,0);
        currWeekStart.set(Calendar.SECOND, 0);
        // offsetting the timeZone, to get the UTC time
        currWeekStart.add(Calendar.MINUTE,timeZoneOffset);
        Calendar currWeekFinish = (Calendar) currWeekStart.clone();
        currWeekFinish.add(Calendar.DATE,7);
        Calendar selectedWeekStart = (Calendar) currWeekStart.clone();
        int weekDecrement = -1 * numWeeks;
        selectedWeekStart.add(Calendar.WEEK_OF_YEAR, weekDecrement);
        Calendar selectedWeekFinish = (Calendar) selectedWeekStart.clone();
        selectedWeekFinish.add(Calendar.DATE,7);
        String loggedInUser = session.get("username");
        Location location = ApplicationHelper.getCurrentUserLocation(loggedInUser);
        Calendar lastUpdatedDate = new GregorianCalendar();
        String lastUpdatedBy = session.get("username");
        List<Shift> selectedWeekShifts =
                ApplicationHelper.getAllShiftsForLocationWithinPeriod(selectedWeekStart, selectedWeekFinish, location.id);
        List<CalShift> newlyAddedShifts = new ArrayList<CalShift>();
        for (Shift prevShift : selectedWeekShifts) {
            Calendar newStart = (Calendar) prevShift.start.clone();
            Calendar newFinish = (Calendar) prevShift.finish.clone();
            newStart.add(Calendar.WEEK_OF_YEAR,numWeeks);
            newFinish.add(Calendar.WEEK_OF_YEAR,numWeeks);
            Shift newShift = new Shift(currWeekStart,newStart,newFinish,prevShift.location,
                    prevShift.person,prevShift.position,lastUpdatedDate,lastUpdatedBy);
            List<Shift> employeeShifts = ApplicationHelper.getAllShiftsForEmployeeWithinPeriod(currWeekStart, currWeekFinish, prevShift.person.id);
            if (!ApplicationHelper.shiftOverlapsWithShifts(employeeShifts, newShift)) {
                newShift.save();
                 CalShift newCalShift = ApplicationHelper.convertToCalShift(newShift,timeZoneOffset);
                newlyAddedShifts.add(newCalShift);
            }
        }
        if (applicationLog.loadShiftsFromPrevWeek) Logger.info("newlyAddedShifts.size = " + newlyAddedShifts.size());
        renderJSON(newlyAddedShifts);
    }

    @RoleHolderPresent
    public static void updateShiftConfirmationStatus(long shiftId, long confirmationStatus) //1: Confirm 0:Un-confirm
    {
        //Set up the logger
        Application.ApplicationLogger applicationLog = new Application.ApplicationLogger();

        if ( applicationLog.updateShiftConfirmationStatus ) Logger.info("*** Inside the updateShiftConfirmationStatus method ***");
        if ( applicationLog.updateShiftConfirmationStatus ) Logger.info("ShiftId: " + shiftId + ", confirmationStatus: " + confirmationStatus);
        Shift shift = Shift.findById(shiftId);
        if (shift != null)
        {
            if ( applicationLog.updateShiftConfirmationStatus ) Logger.info("Shift found with id: " + shift.id);
            if ( confirmationStatus == 1 )  {
                shift.confirmed = true;
            } else {
                shift.confirmed = false;
            }
            if (shift.person.misc || shift.open) {
                String loggedInUser = session.get("username");
                Person person = Person.findByEmailAddress(loggedInUser);
                shift.person = person;
                shift.open = false;
            }
            shift.save();
            if ( applicationLog.updateShiftConfirmationStatus ) Logger.debug("Shift confirmed: " + shift.confirmed);
        }
        else
        if ( applicationLog.updateShiftConfirmationStatus ) Logger.debug("Shift not found");
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void createOrUpdateShift(String operation, long locationId, long personId, long positionId, long shiftId, String startString, String finishString,
                                String recurrence, String seriesEndDate, int increment, String shiftBreak)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        //get all the date and time components
        if ( applicationLog.createOrUpdateShifts ) Logger.info("*** Entering createOrUpdateShift ***");
        String loggedInUser = session.get("username");
        if ( operation.equals("delete"))
        {
            //If we are deleting a shift
            if ( applicationLog.createOrUpdateShifts ) Logger.info("*** Entering delete Shift ***");
            if ( applicationLog.createOrUpdateShifts ) Logger.debug("ShiftId: " + shiftId);
            CalShift returnShift = ApplicationHelper.deleteShift(shiftId,timeZoneOffset);
            renderJSON(returnShift);
        }
        else
        {
            if ( applicationLog.createOrUpdateShifts ) Logger.info("Creating or updating a new shift");
            int seriesRecurrence = Integer.parseInt(recurrence);
            List<String> startComponents = new ArrayList<String>();
            startComponents = Arrays.asList(startString.split(" "));
            List<String> finishComponents = new ArrayList<String>();
            finishComponents = Arrays.asList(finishString.split(" "));

            if ( applicationLog.createOrUpdateShifts ) Logger.debug(startString);
            if ( applicationLog.createOrUpdateShifts ) Logger.debug(finishString);

            Calendar start = new GregorianCalendar();
            start.set(Integer.parseInt(startComponents.get(2)),Integer.parseInt(startComponents.get(1)),Integer.parseInt(startComponents.get(0)), Integer.parseInt(startComponents.get(3)), Integer.parseInt(startComponents.get(4)));

            Calendar finish = new GregorianCalendar();
            finish.set(Integer.parseInt(finishComponents.get(2)),Integer.parseInt(finishComponents.get(1)),Integer.parseInt(finishComponents.get(0)), Integer.parseInt(finishComponents.get(3)), Integer.parseInt(finishComponents.get(4)));

            Calendar weekSunday = ApplicationHelper.getPrevSunday((Calendar) start.clone());

            // Offsetting all the dates for the timezones to store them in UTC
            start.add(Calendar.MINUTE,timeZoneOffset);
            finish.add(Calendar.MINUTE,timeZoneOffset);
            //No need to add the offset to the week start as the times are all 0

            Position position = Position.findById(positionId);
            Person person = Person.findByUserId(personId);
            float numHoursBreak = Float.parseFloat(shiftBreak);
            Location location = Location.findById(locationId);
            if (location == null) {
                error("Cannot find the location");
            } else {
                if ( shiftId < 0 ) {              //If this is a new shift
                    if ( applicationLog.createOrUpdateShifts ) Logger.info("Inside the new shift method");
                    Shift shift = ApplicationHelper.createShift(location, start, finish, numHoursBreak,
                            person, position, new GregorianCalendar(), weekSunday, loggedInUser);
                    if (seriesRecurrence >= 0) {  // Is this a recurring shift
                        Calendar endDate = ApplicationHelper.createCalFromShortDateStr(seriesEndDate, "-");
                        endDate.set(Calendar.MONTH,endDate.get(Calendar.MONTH)-1); // need to do -1 here because Java calendar is 0 based and input is 1 based
                        // offsetting the timeZone, to get the UTC time
                        endDate.add(Calendar.MINUTE,timeZoneOffset);
                        Frequency freq = Frequency.values()[seriesRecurrence];
                        int inc = increment;
                        ShiftSeries shiftSeries = ApplicationHelper.createShiftRecurrence(shift, endDate, freq, inc, loggedInUser);
                        List<CalShift> calShifts = new ArrayList<CalShift>();
                        for (Shift s : ApplicationHelper.getShiftsFromShiftSeries(shiftSeries.id)) {
                            if (applicationLog.createOrUpdateShifts) Logger.info("Next shift date = " + s.start.getTime().toString() );
                            calShifts.add(ApplicationHelper.convertToCalShift(s,timeZoneOffset));
                            if (applicationLog.createOrUpdateShifts) Logger.info("After convert date = " + s.start.getTime().toString() );
                        }
                        renderJSON(calShifts);
                    } else {
                        CalShift returnShift = ApplicationHelper.convertToCalShift(shift,timeZoneOffset);   // Sunday=1
                        List<CalShift> returnShifts = new ArrayList<CalShift>();
                        returnShifts.add(returnShift);
                        renderJSON(returnShifts);
                    }
                } else {         //If a shiftID already exists, this means the shift exists and we are just updating it's information
                    if ( applicationLog.createOrUpdateShifts ) Logger.info("Inside the edit shift method");
                    Shift updatedShift = ApplicationHelper.updateShift(weekSunday, start, finish, numHoursBreak, position, person, shiftId, loggedInUser);
                    CalShift returnShift = ApplicationHelper.convertToCalShift(updatedShift,timeZoneOffset);   // Sunday=1
                    List<CalShift> returnShifts = new ArrayList<CalShift>();
                    returnShifts.add(returnShift);
                    renderJSON(returnShifts);
                }
            }
        }
    }

    @RoleHolderPresent
    public static void createOrUpdateUnavailability(String operation, long unavailabilityBlockId, long employeeId, String title, String description,
                                                    String startDateString, String endDateString, String unavailabilitiesString)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        //We'll first see what type of operation we're doing
        //Operation types are new, update and delete
        if ( operation.equals("new"))
        {
            //Now we'll create a new unavailability block object
            String loggedInUser = session.get("username");
            List<String> startDateComponents = new ArrayList<String>();
            List<String> endDateComponents = new ArrayList<String>();
            startDateComponents = Arrays.asList(startDateString.split("-"));
            endDateComponents = Arrays.asList(endDateString.split("-"));
            Calendar startDate = new GregorianCalendar();
            startDate.set(Integer.parseInt(startDateComponents.get(2)),Integer.parseInt(startDateComponents.get(1)),Integer.parseInt(startDateComponents.get(0)),0,0,0);
            startDate.set(Calendar.MILLISECOND,0);
            Calendar endDate = new GregorianCalendar();
            endDate.set(Integer.parseInt(endDateComponents.get(2)),Integer.parseInt(endDateComponents.get(1)),Integer.parseInt(endDateComponents.get(0)),23,59,59);
            endDate.set(Calendar.MILLISECOND,0);
            Person person = Person.findByUserId(employeeId);

            UnavailabilityBlock unavailabilityBlock;
            if (unavailabilityBlockId < 0) {
                unavailabilityBlock = new UnavailabilityBlock(person,title,description,startDate,endDate,new GregorianCalendar(),loggedInUser);
                unavailabilityBlock.save();
            } else {
                unavailabilityBlock = UnavailabilityBlock.findById(unavailabilityBlockId);
                for (Unavailability u : unavailabilityBlock.unavailabilities) {
                    u.delete();
                }
            }
            List<String> unavailabilities = new ArrayList<String>();
            unavailabilities = Arrays.asList(unavailabilitiesString.split(","));
            for ( String unavailability : unavailabilities)
            {
                //Now we'll add the unavailabilities to the unavailability block
                List<String> unavailabilityComponents = new ArrayList<String>();
                unavailabilityComponents = Arrays.asList(unavailability.split("-"));
                if ( applicationLog.createOrUpdateUnavailability ) Logger.debug("Unavailability: " + unavailabilityComponents.get(0)+ " " + unavailabilityComponents.get(1) + " " + unavailabilityComponents.get(2));
                createUnavailability(startDate,endDate,unavailabilityComponents,unavailabilityBlock,person,loggedInUser,timeZoneOffset);
            }
        }
        else if ( operation.equals("delete"))
        {
            if ( applicationLog.createOrUpdateUnavailability ) Logger.info("Inside the delete unavailability method with blockId: " + unavailabilityBlockId);
            //Then we'll delete the unavailabilityBlock
            UnavailabilityBlock block = UnavailabilityBlock.findById(unavailabilityBlockId);
            //Once we've found the block we will delete the same
            block.delete();
            //Once we've deleted the block we will go back to the view
        }else{
            //This means that we're editing a unavailability block
            if ( applicationLog.createOrUpdateUnavailability ) Logger.info("Inside the edit unavailability method with blockId: " + unavailabilityBlockId);
            //We'll first find the unavailability block
            UnavailabilityBlock block = UnavailabilityBlock.findById(unavailabilityBlockId);
            //We'll then delete the block because we'll be creating new data for it anyway
            block.delete();
            //Now we'll create a new unavailability block object
            String loggedInUser = session.get("username");
            List<String> startDateComponents = new ArrayList<String>();
            List<String> endDateComponents = new ArrayList<String>();
            startDateComponents = Arrays.asList(startDateString.split("-"));
            endDateComponents = Arrays.asList(endDateString.split("-"));
            Calendar startDate = new GregorianCalendar();
            startDate.set(Integer.parseInt(startDateComponents.get(2)),Integer.parseInt(startDateComponents.get(1)),Integer.parseInt(startDateComponents.get(0)),0,0,0);
            Calendar endDate = new GregorianCalendar();
            endDate.set(Integer.parseInt(endDateComponents.get(2)),Integer.parseInt(endDateComponents.get(1)),Integer.parseInt(endDateComponents.get(0)),23,59,59);
            Person person = Person.findByUserId(employeeId);

            UnavailabilityBlock unavailabilityBlock = new UnavailabilityBlock(person,title,description,startDate,endDate,new GregorianCalendar(),loggedInUser);
            unavailabilityBlock.save();
            List<String> unavailabilities = new ArrayList<String>();
            unavailabilities = Arrays.asList(unavailabilitiesString.split(","));
            for ( String unavailability : unavailabilities)
            {
                //Now we'll add the unavailabilities to the unavailability block
                List<String> unavailabilityComponents = new ArrayList<String>();
                unavailabilityComponents = Arrays.asList(unavailability.split("-"));
                if ( applicationLog.createOrUpdateUnavailability ) Logger.debug("Unavailability: " + unavailabilityComponents.get(0)+ " " + unavailabilityComponents.get(1) + " " + unavailabilityComponents.get(2));
                createUnavailability(startDate,endDate,unavailabilityComponents,unavailabilityBlock,person,loggedInUser,timeZoneOffset);
            }
        }
    }

    @RoleHolderPresent
    public static void createOrUpdateLeaveRequest(String startDateString, String finishDateString, String comment, long leaveRequestId, String operation)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        //These were to check if the data was coming in correctly which it was
        if ( applicationLog.createOrUpdateUnavailability ) Logger.info("************ Inside createOrUpdateLeaveRequest ***********");
        if ( applicationLog.createOrUpdateUnavailability ) Logger.debug("Comment: "+comment);
        if ( applicationLog.createOrUpdateUnavailability ) Logger.debug("LeaveRequestId: "+leaveRequestId);
        if ( applicationLog.createOrUpdateUnavailability ) Logger.debug("StartDate: "+startDateString);
        if ( applicationLog.createOrUpdateUnavailability ) Logger.debug("EndDate: "+finishDateString);
        //We'll first see what type of operation we're doing
        //Operation types are new, update and delete

        if ( operation.equals("createOrUpdate") )
        {
            List<String> startDateComponents = new ArrayList<String>();
            List<String> endDateComponents = new ArrayList<String>();
            startDateComponents = Arrays.asList(startDateString.split("-"));
            endDateComponents = Arrays.asList(finishDateString.split("-"));
            Calendar startDate = new GregorianCalendar();
            startDate.set(Integer.parseInt(startDateComponents.get(2)),Integer.parseInt(startDateComponents.get(1)),Integer.parseInt(startDateComponents.get(0)));
            Calendar endDate = new GregorianCalendar();
            endDate.set(Integer.parseInt(endDateComponents.get(2)),Integer.parseInt(endDateComponents.get(1)),Integer.parseInt(endDateComponents.get(0)));
            // offsetting the timeZone, to get the UTC time
            startDate.add(Calendar.MINUTE,timeZoneOffset);
            endDate.add(Calendar.MINUTE,timeZoneOffset);
            LeaveRequest leaveRequest;
            if ( leaveRequestId < 0)
            {
                if ( applicationLog.createOrUpdateUnavailability ) Logger.info("************ Creating a new leave request ***********");
                //Now we'll create a new leave request
                Person employee = Person.findByEmailAddress(session.get("username"));

                leaveRequest = new LeaveRequest(employee,startDate,endDate,comment);
                leaveRequest.save();
                if ( applicationLog.createOrUpdateUnavailability ) Logger.debug("New shift created with id: " + leaveRequest.id);
            }
            else
            {
                leaveRequest = LeaveRequest.findById(leaveRequestId);
                if ( leaveRequest != null)
                    if ( applicationLog.createOrUpdateUnavailability ) Logger.debug("Leave request found with id: " + leaveRequest.id);
                leaveRequest.startDate = startDate;
                leaveRequest.endDate = endDate;
                leaveRequest.comment = comment;
                //Because the leave request particulars have changed, the status of the leave request should be changed back to AWAITING_APPROVAL
                leaveRequest.status = ApprovalStatus.AWAITING_APPROVAL;
                //****TO-DO******Then the manager should be notified and a notification and a new request should be created for the manager
                leaveRequest.save();
                if ( applicationLog.createOrUpdateUnavailability ) Logger.debug("Leave request successfully modified with id: " + leaveRequest.id);
            }
            SchedulerLeaveRequest returnRequest = convertToSchedulerLeaveRequest(leaveRequest, timeZoneOffset);
            renderJSON(returnRequest);
        }
        else if ( operation.equals("delete"))
        {
            if ( applicationLog.createOrUpdateUnavailability ) Logger.info("Inside the delete leave request method with id: " + leaveRequestId);
            //Then we'll delete the leaveRequest
            //Once we've deleted the unavailabilites, we'll delete the unavailabilityBlock
            LeaveRequest request = LeaveRequest.findById(leaveRequestId);
            //Once we've found the block we will delete the same
            request.delete();
            //Once we've deleted the block we will go back to the view
        }
    }

    @RoleHolderPresent
    public static void getEmployeeLeaveRequests()
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        if ( applicationLog.getEmployeeLeaveRequests ) Logger.info("*** Inside the getEmployeeLeaveRequests method ***");
        Person employee = Person.findByEmailAddress(session.get("username"));
        List<SchedulerLeaveRequest> returnLeaveRequests = new ArrayList<SchedulerLeaveRequest>();
        long count = 0;
        for ( LeaveRequest leaveRequest : employee.leaveRequests )
        {
            SchedulerLeaveRequest request = new SchedulerLeaveRequest();
            request = convertToSchedulerLeaveRequest(leaveRequest, timeZoneOffset);
            returnLeaveRequests.add(request);
            ++count;
        }
        if ( applicationLog.getEmployeeLeaveRequests ) Logger.debug("Leave request count: " + count);
        renderJSON(returnLeaveRequests);
    }

    private static SchedulerLeaveRequest convertToSchedulerLeaveRequest(LeaveRequest leaveRequest, int timeZoneOffset)
    {
        SchedulerLeaveRequest returnRequest = new SchedulerLeaveRequest();
        returnRequest.id = leaveRequest.id;
        returnRequest.personId = leaveRequest.person.id;
        returnRequest.personName = leaveRequest.person.firstName + " " + leaveRequest.person.lastName;
        Calendar startDate = (Calendar) leaveRequest.startDate.clone();
        Calendar endDate = (Calendar) leaveRequest.endDate.clone();
        // offsetting the timeZone, to get the UTC time
        startDate.add(Calendar.MINUTE,-1*timeZoneOffset);
        endDate.add(Calendar.MINUTE,-1*timeZoneOffset);
        returnRequest.startDate = ApplicationHelper.convertToReadableDate(startDate);
        returnRequest.endDate = ApplicationHelper.convertToReadableDate(endDate);
        returnRequest.comment = leaveRequest.comment;
        returnRequest.status = leaveRequest.status.toString();
        return returnRequest;
    }

    @RoleHolderPresent
    public static void getEmployeeUnavailabilityBlocks(long employeeId)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        Person employee = Person.findByUserId(employeeId);
        if ( applicationLog.getEmployeeUnavailabilityBlocks ) Logger.debug("EmployeeId: " + Long.toString(employeeId));
        List<SchedulerUnavailabilityBlock> unavailabilityBlocks = new ArrayList<SchedulerUnavailabilityBlock>();
        if ( applicationLog.getEmployeeUnavailabilityBlocks ) Logger.debug("This employee has : " + employee.unavailabilityBlocks.size() + " blocks");
        for ( UnavailabilityBlock block : employee.unavailabilityBlocks)
        {
            SchedulerUnavailabilityBlock addBlock = new SchedulerUnavailabilityBlock();
            addBlock.personId = block.person.id;
            addBlock.title = block.title;
            addBlock.description = block.description;
            addBlock.startDate = ApplicationHelper.convertToReadableDate(block.startDate);
            addBlock.endDate = ApplicationHelper.convertToReadableDate(block.endDate);
            addBlock.id = block.id;
            unavailabilityBlocks.add(addBlock);
        }
        renderJSON(unavailabilityBlocks);
    }

    @RoleHolderPresent
    public static void getUnavailabilityBlock(long unavailabilityBlockId)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        boolean dayAdded;
        if ( applicationLog.getUnavailabilityBlock ) Logger.debug("Inside the getUnavailabilityBlock method");
        UnavailabilityBlock unavailabilityBlock = UnavailabilityBlock.findById(unavailabilityBlockId);
        if ( applicationLog.getUnavailabilityBlock ) Logger.debug("The unavailability block found was: " + Long.toString(unavailabilityBlock.id));
        if ( applicationLog.getUnavailabilityBlock ) Logger.debug("The unavailability block has " + unavailabilityBlock.unavailabilities.size() + " unavailabilities" );
        SchedulerUnavailabilityBlock returnBlock = new SchedulerUnavailabilityBlock();
        returnBlock.id = unavailabilityBlock.id;
        returnBlock.personId = unavailabilityBlock.person.id;
        returnBlock.personName = unavailabilityBlock.person.firstName + " " +  unavailabilityBlock.person.lastName;
        returnBlock.title = unavailabilityBlock.title;
        returnBlock.description = unavailabilityBlock.description;
        returnBlock.startDate = ApplicationHelper.convertToReadableDate(unavailabilityBlock.startDate);
        returnBlock.endDate = ApplicationHelper.convertToReadableDate(unavailabilityBlock.endDate);
        List<Long> unavailabilitiesAdded = new ArrayList<Long>() ;
        for ( Unavailability unavailability : unavailabilityBlock.unavailabilities)
        {
            SchedulerUnavailability returnUnavailability = new SchedulerUnavailability();
            dayAdded = false;
            //We'll see if the day has already been added
            for( Long addedDay : unavailabilitiesAdded){
                if(addedDay == unavailability.dayOfWeek){
                    dayAdded = true;
                }
            }
            if(!dayAdded){
                //This means that this day's unavailability hasn't been added and hence should be added
                unavailabilitiesAdded.add(unavailability.dayOfWeek);
                returnUnavailability.dayOfWeek = unavailability.dayOfWeek;
                returnUnavailability.personId = unavailability.person.id;
                // converting the times back to local
                Calendar startDateTimeLocal = (Calendar) unavailability.startDateTime.clone();
                startDateTimeLocal.add(Calendar.MINUTE,-1*timeZoneOffset);
                Calendar endDateTimeLocal = (Calendar) unavailability.endDateTime.clone();
                endDateTimeLocal.add(Calendar.MINUTE,-1*timeZoneOffset);
                returnUnavailability.startTime = ApplicationHelper.convertCalendarTo24HourTimeString(startDateTimeLocal);  // need to convert to readable String
                returnUnavailability.endTime = ApplicationHelper.convertCalendarTo24HourTimeString(endDateTimeLocal);    // need to convert to readable String
                returnUnavailability.unavailabilityBlockId = unavailability.unavailabilityBlock.id;
                returnBlock.unavailabilities.add(returnUnavailability);
            }
        }
        renderJSON(returnBlock);
    }

    @RoleHolderPresent
    public static void getEmployeePositions(long employeeId,long locationId)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();

        List<Position> positions = new ArrayList<Position>();
        Person employee = Person.findByUserId(employeeId);
        positions = employee.positions;
        /*List<Position> returnPositions = new ArrayList<Position>();
        for ( Position position : positions)
        {
            if (position.location.id == locationId )
                returnPositions.add(position);

        } */
        JSONSerializer positionSerializer = new JSONSerializer().include("title","id").exclude("*");
        String positionJson = positionSerializer.serialize(positions);
        renderJSON(positionJson);
    }

    @RoleHolderPresent
    public static void getPositionEmployees(long positionId, long locationId)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();

        List<Person> employees = new ArrayList<Person>();
        Position position = Position.findPositionById(positionId);
        if ( applicationLog.getPositionEmployees ) Logger.debug("Position name: " + position.title);
        employees = position.persons;
        List<Person> returnEmployees = new ArrayList<Person>();
        for ( Person employee : employees)
        {
            for ( Location location : employee.locations )
            {
                if ( location.id == locationId )
                {
                    returnEmployees.add(employee);
                }
            }
        }
        JSONSerializer employeeSerializer = new JSONSerializer().include("firstName","lastName","id").exclude("*");
        String personJson = employeeSerializer.serialize(returnEmployees);
        renderJSON(personJson);
    }

    @RoleHolderPresent
    public static void getEmployeeNameById(long employeeId)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();

        Person employee = Person.findByUserId(employeeId);
        JSONSerializer employeeNameSerializer = new JSONSerializer().include("firstName", "lastName").exclude("*");
        String employeeName = employeeNameSerializer.serialize(employee);
        renderJSON(employeeName);
    }

    @RoleHolderPresent
    public static void getPositionNameById(long positionId)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();

        Position position = Position.findPositionById(positionId);
        JSONSerializer positionNameSerializer = new JSONSerializer().include("id","title").exclude("*");
        String positionName = positionNameSerializer.serialize(position);
        renderJSON(positionName);
    }

    @RoleHolderPresent
    public static void getEmployeeNameByUsername(String emailAddress)
    {
        Person employee = Person.findByEmailAddress(emailAddress);
        JSONSerializer employeeNameSerializer = new JSONSerializer().include("firstName","lastName").exclude("*");
        String employeeName = employeeNameSerializer.serialize(employee);
        renderJSON(employeeName);
    }

    @RoleHolderPresent
    private static List<CalPosition> getLocationPositions(long locationId)
    {
        String loggedInUser = session.get("username");
        Location selectedLocation = Location.findById(locationId);
        if ( applicationLog.getLocationPositions ) Logger.info(selectedLocation.toString());
        List<CalPosition> returnPositions = new ArrayList<CalPosition>();
        for ( Position p : selectedLocation.company.positions )
        {
            returnPositions.add(ApplicationHelper.covertToCalPositionWithLocationEmployees(p,locationId));
        }
        return returnPositions;
    }

    @RoleHolderPresent
    private static List<CalEmployee> getLocationPersons(long locationId)
    {
        String loggedInUser = session.get("username");
        Location selectedLocation = Location.findById(locationId);
        if ( applicationLog.getLocationPersons ) Logger.info(selectedLocation.toString());
        Person person = Person.findByEmailAddress(loggedInUser);
        List<CalEmployee> returnPersons = new ArrayList<CalEmployee>();
        if (person.accountType == AccountType.EMPLOYEE) {
            returnPersons.add(ApplicationHelper.convertToCalEmployee(person));
        } else {
            for ( Person p : selectedLocation.persons )
            {
                returnPersons.add(ApplicationHelper.convertToCalEmployee(p));
            }
        }
        return returnPersons;
    }

    public static void returnLocationPersons(long locationId){
        renderJSON(getLocationPersons(locationId));
    }

    @RoleHolderPresent
    private static List<CalShift> getLocationWeekShifts(String startDate, int timeZoneOffset, long locationId)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();

        if ( applicationLog.getLocationWeekShifts ) Logger.info("---------------Inside getLocationWeekShifts -------------");
        if ( applicationLog.getLocationWeekShifts ) Logger.info("startDate: " + startDate);
        String loggedInUser = session.get("username");
        Location location = Location.findById(locationId);
        Calendar weekStartDate = ApplicationHelper.createCalFromShortDateStr(startDate, "-");
        weekStartDate.set(Calendar.HOUR_OF_DAY,0);
        weekStartDate.set(Calendar.MINUTE,0);
        weekStartDate.set(Calendar.SECOND,0);
        weekStartDate.set(Calendar.MILLISECOND,0);
        if ( applicationLog.getLocationWeekShifts) Logger.info("weekStartDate: " + ApplicationHelper.convertToReadableDate(weekStartDate));
            //Find all the shifts from the location with the same weekStartDate
        List<CalShift> shifts = new ArrayList<CalShift>();
        for ( Shift s : location.shifts)
        {
            if ( applicationLog.getLocationWeekShifts) Logger.info("ShiftWeekStart: " + ApplicationHelper.convertToReadableDate(s.weekStartDate));
            if ( ApplicationHelper.compareDates(s.weekStartDate, weekStartDate)) //If the shift is part of that week, then add it
            {
                CalShift shift = ApplicationHelper.convertToCalShift(s,timeZoneOffset);
                if ( applicationLog.getLocationWeekShifts ) Logger.info("Date: " + shift.start + " | " + "Day of week: " + shift.dayOfWeek );
                shifts.add(shift);
            }
        }
        return shifts;
    }

    @RoleHolderPresent
    private static List<CalShift> getLocationDayShifts(String date, int timeZoneOffset, long locationId)
    {
        String loggedInUser = session.get("username");
        Location location = Location.findById(locationId);
        Calendar shiftsDay = ApplicationHelper.createCalFromShortDateStr(date, "-");
        shiftsDay.set(Calendar.HOUR,0);
        shiftsDay.set(Calendar.MINUTE,0);
        shiftsDay.set(Calendar.SECOND,0);
        shiftsDay.set(Calendar.MILLISECOND,0);
        // offsetting the timeZone, to get the UTC time
        //Find all the shifts from the location with the same weekStartDate
        List<CalShift> shifts = new ArrayList<CalShift>();
        for ( Shift s : location.shifts)
        {
            Calendar shiftTimeCompare = (Calendar)s.start.clone();
            shiftTimeCompare.add(Calendar.MINUTE,-timeZoneOffset);
            shiftTimeCompare.set(Calendar.MILLISECOND,0);
            shiftTimeCompare.set(Calendar.SECOND,0);
            shiftTimeCompare.set(Calendar.MINUTE,0);
            shiftTimeCompare.set(Calendar.HOUR_OF_DAY,0);
            if ( ApplicationHelper.compareDates(shiftTimeCompare,shiftsDay) ) //If the shift is for the current day then we'll add it to the list
            {
                CalShift shift = ApplicationHelper.convertToCalShift(s,timeZoneOffset);
                shifts.add(shift);
            }
        }
        return shifts;
    }

    @RoleHolderPresent
    private static List<SchedulerUnavailability> getEmployeeUnavailabilitiesForDay(String dateString, long locationId)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        List<String> dateComponents = Arrays.asList(dateString.split("-"));
        Calendar date = new GregorianCalendar();
        date.set(Integer.parseInt(dateComponents.get(2)),Integer.parseInt(dateComponents.get(1)),Integer.parseInt(dateComponents.get(0)),0,0,0);
        date.set(Calendar.MILLISECOND,0);
        Calendar toDate = new GregorianCalendar();
        toDate = (Calendar)date.clone();
        toDate.add(Calendar.HOUR,24);
        //Now we'll get some required information
        //Get the current location
        String loggedInUser = session.get("username");
        Location location = Location.findById(locationId);
        //Get the employees at the location
        List<Person> employees = location.persons;
        //Get the unavailability blocks for these employees
        List<UnavailabilityBlock> unavailabilityBlocks = new ArrayList<UnavailabilityBlock>();
        for(Person employee : employees)
        {
             for ( UnavailabilityBlock block : employee.unavailabilityBlocks )
             {
                unavailabilityBlocks.add(block);
             }
        }
        //So at this point we've got the unavailability blocks for the employees in our view
        //Now let's create the list that will be storing the unavailabilities
        List<SchedulerUnavailability> returnUnavailabilities = new ArrayList<SchedulerUnavailability>();
        //Now we'll go through the unavailability blocks and if our date falls within this range, we will add it's unavailabilities to the list
        for ( UnavailabilityBlock unavailabilityBlock : unavailabilityBlocks)
        {
            if ( date.after(unavailabilityBlock.startDate) && date.before(unavailabilityBlock.endDate) )
            {
                //This means our date is within range of this unavailability block
                //So we'll add the unavailabilities from this block to the list
                for ( Unavailability unavailability : unavailabilityBlock.unavailabilities)
                {
                    if( unavailability.dayOfWeek == date.get(Calendar.DAY_OF_WEEK) )
                    {
                        //And then we need to check if the date of the unavailability is the same as the given date
                        Calendar startDateTimeCompare = (Calendar)unavailability.startDateTime.clone();
                        startDateTimeCompare.add(Calendar.MINUTE, (-timeZoneOffset));
                        if((startDateTimeCompare.after(date) || startDateTimeCompare.equals(date)) && (startDateTimeCompare.before(toDate))){
                            returnUnavailabilities.add(ApplicationHelper.convertToSchedulerUnavailability(unavailability,timeZoneOffset));
                        }
                    }
                }
            }
        }
        //So now we have all the unavailabilities we need and we will convert them to a return class format
        //And while we're converting them, we'll only add the unavailabilities for the day we want
        return returnUnavailabilities;
    }

    @RoleHolderPresent
    public static List<SchedulerUnavailability> getEmployeeUnavailabilitiesForWeek(String startDateString, long locationId)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        //We'll first get the date components for the startDate
        List<String> dateComponents =  Arrays.asList(startDateString.split("-"));
        Calendar startDate = new GregorianCalendar();
        Calendar endDate = new GregorianCalendar();
        startDate.set(Integer.parseInt(dateComponents.get(2)),Integer.parseInt(dateComponents.get(1)),Integer.parseInt(dateComponents.get(0)),0,0,0);
        endDate.set(Integer.parseInt(dateComponents.get(2)),Integer.parseInt(dateComponents.get(1)),Integer.parseInt(dateComponents.get(0)),0,0,0);
        startDate.set(Calendar.MILLISECOND,0);
        endDate.set(Calendar.MILLISECOND,0);
        endDate.add(Calendar.DATE,6);
        //Now we'll get some required information
        //Get the current location
        Location location = Location.findById(locationId);
        //Get the employees at the location
        List<Person> employees = location.persons;
        //Get the unavailability blocks for these employees
        List<UnavailabilityBlock> unavailabilityBlocks = new ArrayList<UnavailabilityBlock>();
        for(Person employee : employees)
        {
            for ( UnavailabilityBlock block : employee.unavailabilityBlocks )
            {
                unavailabilityBlocks.add(block);
            }
        }
        //So at this point we've got the unavailability blocks for the employees in our view
        //Now let's create the list that will be storing the unavailabilities
        List<Unavailability> unavailabilities = new ArrayList<Unavailability>();
        //Now we'll go through the unavailability blocks and if our date falls within this range, we will add it's unavailabilities to the list
        for ( UnavailabilityBlock unavailabilityBlock : unavailabilityBlocks)
        {
            if((startDate.before(unavailabilityBlock.startDate) && endDate.after(unavailabilityBlock.startDate)) || (startDate.after(unavailabilityBlock.startDate) && endDate.before(unavailabilityBlock.endDate)) || (startDate.before(unavailabilityBlock.endDate) && endDate.after(unavailabilityBlock.endDate)) || (startDate.before(unavailabilityBlock.startDate) && endDate.after(unavailabilityBlock.endDate))){
                List<Long> daysAdded = new ArrayList<Long>();
                boolean alreadyAdded;
                for ( Unavailability unavailability : unavailabilityBlock.unavailabilities)
                {
                    alreadyAdded=false;
                    for(Long day : daysAdded){
                        if(unavailability.dayOfWeek == day){
                            alreadyAdded=true;
                        }

                    }
                    if(!alreadyAdded){
                        Calendar startDateTimeCompare = (Calendar)unavailability.startDateTime.clone();
                        startDateTimeCompare.add(Calendar.MINUTE, (-timeZoneOffset));
                        if((startDateTimeCompare.after(startDate) || startDateTimeCompare.equals(startDate)) && (startDateTimeCompare.before(endDate) || startDateTimeCompare.equals(endDate))){
                            daysAdded.add(unavailability.dayOfWeek);
                            unavailabilities.add(unavailability);
                        }
                    }
                }
            }
        }
        List<SchedulerUnavailability> returnUnavailabilities = new ArrayList<SchedulerUnavailability>();
        //Now we'll convert the unavailabilities into a return unavailability
        for ( Unavailability unavailability : unavailabilities)
        {
            returnUnavailabilities.add(ApplicationHelper.convertToSchedulerUnavailability(unavailability,timeZoneOffset));
        }
        return returnUnavailabilities;
    }


    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void getRosterPositionCost(long locationId, long todayDateTime, long weekStartDateTime)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        // Preparing the 4 calendars required (today, tomorrow, weekStart, weekFinish)
        Calendar today = new GregorianCalendar();
        today.setTimeInMillis(todayDateTime);
        // offsetting the timeZone, to get the UTC time
        today.add(Calendar.MINUTE,timeZoneOffset);
        today.set(Calendar.HOUR_OF_DAY,0);
        today.set(Calendar.MINUTE,0);
        today.set(Calendar.SECOND,0);
        today.set(Calendar.MILLISECOND,0);
        Calendar tomorrow = (Calendar) today.clone();
        tomorrow.add(Calendar.DATE, 1);
        Calendar weekStart = new GregorianCalendar();
        weekStart.setTimeInMillis(weekStartDateTime);
        // offsetting the timeZone, to get the UTC time
        weekStart.add(Calendar.MINUTE,timeZoneOffset);
        weekStart.set(Calendar.HOUR_OF_DAY,0);
        weekStart.set(Calendar.MINUTE,0);
        weekStart.set(Calendar.SECOND,0);
        weekStart.set(Calendar.MILLISECOND,0);
        Calendar weekFinish = (Calendar) weekStart.clone();
        weekFinish.add(Calendar.DATE,7);

        // Values to be added into the JSON object
        Location location = Location.findById(locationId);
        List<Shift> shifts = ApplicationHelper.getAllShiftsForLocationWithinPeriod(weekStart,weekFinish,locationId);
        List<CalRosterPositionCost> calRosterPositionCosts = new ArrayList<CalRosterPositionCost>();
        for (Position p : location.company.positions) {
            CalRosterPositionCost positionCost = new CalRosterPositionCost();
            positionCost.id = p.id;
            positionCost.positionTitle = p.title;
            positionCost.dayCost = 0;
            positionCost.dayHoursWorked = 0;
            positionCost.weekCost = 0;
            positionCost.weekHoursWorked = 0;
            calRosterPositionCosts.add(positionCost);
        }
        for (Shift s : shifts) {
            for (CalRosterPositionCost pc : calRosterPositionCosts) {
                if (pc.id == s.position.id) {
                    pc.weekHoursWorked += s.numHoursWorked;
                    pc.weekCost += s.cost;
                    if ( (s.start.getTimeInMillis() >= today.getTimeInMillis())
                            && (s.finish.getTimeInMillis() < tomorrow.getTimeInMillis()) ) {
                        pc.dayHoursWorked += s.numHoursWorked;
                        pc.dayCost += s.cost;
                    }
                }
            }
        }
        renderJSON(calRosterPositionCosts);
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void getRosterEmployeeCost(long locationId, long todayDateTime, long weekStartDateTime)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        // Preparing the 4 calendars required (today, tomorrow, weekStart, weekFinish)
        Calendar today = new GregorianCalendar();
        today.setTimeInMillis(todayDateTime);
        // offsetting the timeZone, to get the UTC time
        today.add(Calendar.MINUTE,timeZoneOffset);
        today.set(Calendar.HOUR_OF_DAY,0);
        today.set(Calendar.MINUTE,0);
        today.set(Calendar.SECOND,0);
        today.set(Calendar.MILLISECOND,0);
        Calendar tomorrow = (Calendar) today.clone();
        tomorrow.add(Calendar.DATE, 1);
        Calendar weekStart = new GregorianCalendar();
        weekStart.setTimeInMillis(weekStartDateTime);
        // offsetting the timeZone, to get the UTC time
        weekStart.add(Calendar.MINUTE,timeZoneOffset);
        weekStart.set(Calendar.HOUR_OF_DAY,0);
        weekStart.set(Calendar.MINUTE,0);
        weekStart.set(Calendar.SECOND,0);
        weekStart.set(Calendar.MILLISECOND,0);
        Calendar weekFinish = (Calendar) weekStart.clone();
        weekFinish.add(Calendar.DATE,7);

        // Values to be added into the JSON object
        Location location = Location.findById(locationId);
        List<Shift> shifts = ApplicationHelper.getAllShiftsForLocationWithinPeriod(weekStart,weekFinish,locationId);
        List<CalRosterEmployeeCost> calRosterEmployeeCosts = new ArrayList<CalRosterEmployeeCost>();
        for (Person p : location.persons) {
            CalRosterEmployeeCost employeeCost = new CalRosterEmployeeCost();
            employeeCost.id = p.id;
            employeeCost.firstName = p.firstName;
            employeeCost.dayCost = 0;
            employeeCost.dayHoursWorked = 0;
            employeeCost.weekCost = 0;
            employeeCost.weekHoursWorked = 0;
            calRosterEmployeeCosts.add(employeeCost);
        }
        for (Shift s : shifts) {
            for (CalRosterEmployeeCost ec : calRosterEmployeeCosts) {
                if (ec.id == s.person.id) {
                    ec.weekHoursWorked += s.numHoursWorked;
                    ec.weekCost += s.cost;
                    if ( (s.start.getTimeInMillis() >= today.getTimeInMillis())
                            && (s.finish.getTimeInMillis() < tomorrow.getTimeInMillis()) ) {
                        ec.dayHoursWorked += s.numHoursWorked;
                        ec.dayCost += s.cost;
                    }
                }
            }
        }
        renderJSON(calRosterEmployeeCosts);
    }


    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void getRosterCost(long positionId, long personId, long todayDateTime, long weekStartDateTime)
    {
        //Set up logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        // Preparing the 4 calendars required (today, tomorrow, weekStart, weekFinish)
        Calendar today = new GregorianCalendar();
        today.setTimeInMillis(todayDateTime);
        // offsetting the timeZone, to get the UTC time
        today.add(Calendar.MINUTE,timeZoneOffset);
        today.set(Calendar.HOUR_OF_DAY,0);
        today.set(Calendar.MINUTE,0);
        today.set(Calendar.SECOND,0);
        today.set(Calendar.MILLISECOND,0);
        Calendar tomorrow = (Calendar) today.clone();
        tomorrow.add(Calendar.DATE, 1);
        Calendar weekStart = new GregorianCalendar();
        weekStart.setTimeInMillis(weekStartDateTime);
        // offsetting the timeZone, to get the UTC time
        weekStart.add(Calendar.MINUTE,timeZoneOffset);
        weekStart.set(Calendar.HOUR_OF_DAY,0);
        weekStart.set(Calendar.MINUTE,0);
        weekStart.set(Calendar.SECOND,0);
        weekStart.set(Calendar.MILLISECOND,0);
        Calendar weekFinish = (Calendar) weekStart.clone();
        weekFinish.add(Calendar.DATE,7);

        // Values to be added into the JSON object
        float totalDayHoursWorked = 0;
        float totalDayCost = 0;
        float totalWeekHoursWorked = 0;
        float totalWeekCost = 0;

        // Get the day and week cost
        if (positionId > 0) {
            List<Shift> shifts = ApplicationHelper.getAllShiftsForPositionWithinPeriod(weekStart, weekFinish, positionId);
            for (Shift s : shifts) {
                List<Shift> singleShiftList = new ArrayList<Shift>();
                singleShiftList.add(s);
                totalWeekHoursWorked += s.numHoursWorked;
                totalWeekCost += s.cost;
                if ( (s.start.getTimeInMillis() >= today.getTimeInMillis())
                        && (s.finish.getTimeInMillis() < tomorrow.getTimeInMillis()) ) {
                    totalDayHoursWorked += s.numHoursWorked;
                    totalDayCost += s.cost;
                }
            }
        } else if (personId > 0) {
            List<Shift> shifts = ApplicationHelper.getAllShiftsForEmployeeWithinPeriod(weekStart, weekFinish, personId);
            for (Shift s : shifts) {
                totalWeekHoursWorked += s.numHoursWorked;
                totalWeekCost += s.cost;
                long startTime = s.start.getTimeInMillis();
                long endTime = s.finish.getTimeInMillis();
                long todayTime = today.getTimeInMillis();
                long tomorrowTime = tomorrow.getTimeInMillis();
                if ( (s.start.getTimeInMillis() >= today.getTimeInMillis())
                        && (s.finish.getTimeInMillis() < tomorrow.getTimeInMillis()) ) {
                    List<Shift> singleShiftList = new ArrayList<Shift>();
                    singleShiftList.add(s);
                    totalDayHoursWorked += s.numHoursWorked;
                    totalDayCost += s.cost;
                }
            }
        }

        // Add it to the JSON object
        CalRosterCost rosterCost = new CalRosterCost();
        rosterCost.dayCost = totalDayCost;
        rosterCost.dayHoursWorked = totalDayHoursWorked;
        rosterCost.weekCost = totalWeekCost;
        rosterCost.weekHoursWorked = totalWeekHoursWorked;

        renderJSON(rosterCost);
    }

    @RoleHolderPresent
    public static void getPossibleSwapShifts(long shiftId) {
        // get all the published shifts from today onwards, at the same location for the same position
        // where the person swapping the shift doesn't have a shift at that time and the potential shift swap recipient
        // does not have an unavailability during the original shift
        Shift shift = Shift.findById(shiftId);
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        Location location = shift.location;
        Position position = shift.position;
        Person person = shift.person;
        // These 3 lists can be optimised by specific queries to get from today onwards
        List<LeaveRequest> leaveRequests = person.leaveRequests;
        List<Shift> personShifts = person.shifts;
        List<Shift> publishedShifts = ApplicationHelper.getPublishedShiftsForLocationAndPosition(location.id, position.id, person.id);
        List<CalShift> possibleSwapShifts = new ArrayList<CalShift>();
        for (Shift ps : publishedShifts) {
            if (!ApplicationHelper.conflictsWithLeaveRequests(leaveRequests,ps) && !ApplicationHelper.conflictsWithExistingShifts(personShifts,ps)) {
                possibleSwapShifts.add(ApplicationHelper.convertToCalShift(ps,timeZoneOffset));
            }
        }

        renderJSON(possibleSwapShifts);
    }


    @RoleHolderPresent
    public static void createSwapShiftRequest(long fromShiftId, String[] toShiftIds, String notes) {
        Shift fromShift = Shift.findById(fromShiftId);
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        Calendar now = new GregorianCalendar();
        String loggedInUser = session.get("username");
        List<CalSwapShiftRequest> calSwapShiftRequests = new ArrayList<CalSwapShiftRequest>();
        for (String shiftIdStr : toShiftIds) {
            Long toShiftId = Long.parseLong(shiftIdStr);
            Shift toShift = Shift.findById(toShiftId);
            Person fromPerson = fromShift.person;
            Person toPerson = toShift.person;
            SwapShiftRequest request = new SwapShiftRequest(fromShift,toShift,notes);
            request.lastUpdatedDate = now;
            request.lastUpdatedBy = loggedInUser;
            request.save();
            fromPerson.swapShiftRequestsFromPerson.add(request);
            toPerson.swapShiftRequestsToPerson.add(request);
            EmailNotifier.swapShiftNotification(request);
            calSwapShiftRequests.add(ApplicationHelper.convertToCalSwapShiftRequest(request, timeZoneOffset));

        }
        fromShift.swapShiftRequestPending = true;
        fromShift.save();
        renderJSON(calSwapShiftRequests);
    }

    @RoleHolderPresent
    public static void createDropShiftRequest(long fromShiftId, String notes) {
        Shift fromShift = Shift.findById(fromShiftId);
        String loggedInUser = session.get("username");
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        Person fromPerson = Person.findByEmailAddress(loggedInUser);
        List<Person> toPersons = new ArrayList<Person>();
        for (Person p : fromShift.location.persons) {
            if (p.accountType != AccountType.EMPLOYEE) {
                toPersons.add(p);
            }
        }
        DropShiftRequest request = new DropShiftRequest(fromShift,fromPerson,toPersons);
        request.lastUpdatedDate = new GregorianCalendar();
        request.lastUpdatedBy = loggedInUser;
        request.notes = notes;
        request.save();
        fromShift.dropShiftRequestPending = true;
        fromShift.save();
        CalDropShiftRequest calDropShiftRequest = ApplicationHelper.convertToCalDropShiftRequest(request, timeZoneOffset);
        renderJSON(calDropShiftRequest);
    }

    @RoleHolderPresent
    public static void getUserSwapRequestsToPerson()
    {
        //Set up the logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        if(applicationLog.getUserSwapRequestsToPerson) Logger.info("*** Getting user requests to person ***");
        String loggedInUser = session.get("username");
        Person person = Person.findByEmailAddress(loggedInUser);
        List<SwapShiftRequest> swapShiftRequestsToPerson = person.swapShiftRequestsToPerson;
        List<CalSwapShiftRequest> calSwapShiftRequests = new ArrayList<CalSwapShiftRequest>();
        for (SwapShiftRequest swapShiftRequest : swapShiftRequestsToPerson) {
            calSwapShiftRequests.add(ApplicationHelper.convertToCalSwapShiftRequest(swapShiftRequest, timeZoneOffset));
        }
        renderJSON(calSwapShiftRequests);
    }

    @RoleHolderPresent
    public static void getUserDropRequestsToPerson()
    {
        String loggedInUser = session.get("username");
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        Person person = Person.findByEmailAddress(loggedInUser);
        List<DropShiftRequest> dropShiftRequestsToPerson = person.dropShiftRequestsToPerson;
        List<CalDropShiftRequest> calDropShiftRequests = new ArrayList<CalDropShiftRequest>();
        for (DropShiftRequest dropShiftRequest : dropShiftRequestsToPerson) {
            calDropShiftRequests.add(ApplicationHelper.convertToCalDropShiftRequest(dropShiftRequest,timeZoneOffset));
        }
        renderJSON(calDropShiftRequests);
    }

    @RoleHolderPresent
    public static void getUserSwapRequestsFromPerson()
    {
        //Set up the logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        if(applicationLog.getUserSwapRequestsFromPerson) Logger.info("*** Getting user requests from person ***");
        String loggedInUser = session.get("username");
        Person person = Person.findByEmailAddress(loggedInUser);
        List<SwapShiftRequest> swapShiftRequestsToPerson = person.swapShiftRequestsFromPerson;
        List<CalSwapShiftRequest> calSwapShiftRequests = new ArrayList<CalSwapShiftRequest>();
        for (SwapShiftRequest swapShiftRequest : swapShiftRequestsToPerson) {
            calSwapShiftRequests.add(ApplicationHelper.convertToCalSwapShiftRequest(swapShiftRequest, timeZoneOffset));
        }
        renderJSON(calSwapShiftRequests);
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void executeShiftDrop(long requestId) {
        DropShiftRequest dropShiftRequest = DropShiftRequest.findById(requestId);
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        Shift shift = dropShiftRequest.shift;
        for (Person p : shift.location.persons) {
            if (p.misc) {
                shift.person = p;
            }
        }
        shift.draft = true;
        shift.save();
        dropShiftRequest.status = ApprovalStatus.APPROVED;
        dropShiftRequest.save();
        CalDropShiftRequest calDropShiftRequest = ApplicationHelper.convertToCalDropShiftRequest(dropShiftRequest, timeZoneOffset);
        renderJSON(calDropShiftRequest);
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void rejectShiftDrop(long requestId) {
        DropShiftRequest dropShiftRequest = DropShiftRequest.findById(requestId);
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        dropShiftRequest.status = ApprovalStatus.DECLINED;
        dropShiftRequest.save();
        CalDropShiftRequest calDropShiftRequest = ApplicationHelper.convertToCalDropShiftRequest(dropShiftRequest,timeZoneOffset);
        renderJSON(dropShiftRequest);
    }

    @RoleHolderPresent
    public static void executeShiftSwap(long requestId) {
        //Set up the logger
        ApplicationLogger applicationLog = new ApplicationLogger();
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        if(applicationLog.executeShiftSwap) Logger.info("*** Executing the shift swap ***");

        SwapShiftRequest swapShiftRequest = SwapShiftRequest.findById(requestId);
        Shift from = swapShiftRequest.from;
        Shift to = swapShiftRequest.to;
        if(applicationLog.executeShiftSwap) Logger.info("Swapping the from person and the to person (and removing any shift series ids)");
        from.shiftSeries = null;
        to.shiftSeries = null;
        long fromPersonId = from.person.id;
        long toPersonId = to.person.id;
        from.person = Person.findById(toPersonId);
        to.person = Person.findById(fromPersonId);
        if(applicationLog.executeShiftSwap) Logger.info("Setting the ResponseType to accepted");
        swapShiftRequest.response = ResponseType.ACCEPTED;
        swapShiftRequest.save();
        if(applicationLog.executeShiftSwap) Logger.debug("ResponseType = " + swapShiftRequest.response);
        CalSwapShiftRequest calSwapShiftRequest = ApplicationHelper.convertToCalSwapShiftRequest(swapShiftRequest, timeZoneOffset);
        from.swapShiftRequestPending = false;
        ApplicationHelper.removeRelatedSwapShiftRequests(swapShiftRequest);
        if(applicationLog.executeShiftSwap) Logger.info("Sending the email notification");
        EmailNotifier.swapShiftResponse(swapShiftRequest);
        EmailNotifier.swapShiftManagerNotification(swapShiftRequest);
        from.save();
        to.save();
        renderJSON(calSwapShiftRequest);
    }

    @RoleHolderPresent
    public static void declineShiftSwap(long requestId) {
        SwapShiftRequest swapShiftRequest = SwapShiftRequest.findById(requestId);
        int timeZoneOffset = Integer.parseInt(session.get("timeZoneOffset"));
        swapShiftRequest.response = ResponseType.DECLINED;
        swapShiftRequest.save();
        CalSwapShiftRequest calSwapShiftRequest = ApplicationHelper.convertToCalSwapShiftRequest(swapShiftRequest, timeZoneOffset);
        EmailNotifier.swapShiftResponse(swapShiftRequest);
        renderJSON(calSwapShiftRequest);
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void getLocationsForCompany(long companyId) {
        Company company = Company.findById(companyId);
        List<Location> locations = company.locations;
        List<CalLocation> calLocations = new ArrayList<CalLocation>();
        for (Location l : locations) {
            calLocations.add(ApplicationHelper.convertToCalLocation(l));
        }
        renderJSON(calLocations);
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void getPositionsForCompany(long companyId) {
        Company company = Company.findById(companyId);
        List<Position> positions = company.positions;
        List<CalPosition> calPositions = new ArrayList<CalPosition>();
        for (Position p : positions) {
            calPositions.add(ApplicationHelper.convertToCalPositionWithoutShifts(p)) ;
        }
        renderJSON(calPositions);
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void getEmployeesForCompany(long companyId) {
        Company company = Company.findById(companyId);
        List<Person> persons = company.persons;
        List<CalEmployee> calEmployees = new ArrayList<CalEmployee>();
        for (Person p : persons) {
            if (!p.misc) {
                calEmployees.add(ApplicationHelper.convertToCalEmployee(p));
            }
        }
        renderJSON(calEmployees);
    }


    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void createLocation(long companyId, String name, String address, String suburb, String  state, String postCode, String country, String phoneNumber) {
        Company company = Company.findById(companyId);
        if (company != null) {
            Location location = new Location(company,name);
            location.locationName = name;
            location.address = address;
            location.contactNumber = phoneNumber;
            location.country = country;
            location.postCode = postCode;
            location.state = state;
            location.suburb = suburb;
            location.save();
            CalLocation calLoc = ApplicationHelper.convertToCalLocation(location);
            renderJSON(calLoc);
        }
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void updateLocation(long locationId, String name, String address, String suburb, String  state, String postCode, String country, String phoneNumber) {
        Location location = Location.findById(locationId);
        if (location != null) {
            location.locationName = name;
            location.address = address;
            location.contactNumber = phoneNumber;
            location.country = country;
            location.postCode = postCode;
            location.state = state;
            location.suburb = suburb;
            location.save();
            CalLocation calLoc = ApplicationHelper.convertToCalLocation(location);
            renderJSON(calLoc);
        }
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void deleteLocation(long locationId) {
        Location location = Location.findById(locationId);
        for (Person p : location.persons) {
            if (p.locations.contains(location)) {
                p.locations.remove(location);
            }
        }
        location.delete();
    }

    @Restrictions({@Restrict("ADMIN"),@Restrict("COMPANY_MANAGER"),@Restrict("LOCATION_MANAGER")})
    public static void updatePositions(String positionsJson) {
        String loggedInUser = session.get("username");
        Person person = Person.findByEmailAddress(loggedInUser);
        Company company = person.company;
        List<CalPosition> calPositions = new ArrayList<CalPosition>();
        // remove any deleted positions
        try {
            JSONArray posJsonArr = new JSONArray(positionsJson);
            for (int i=0; i < posJsonArr.length(); i++) {
                JSONObject posObj = posJsonArr.getJSONObject(i);
                long id = posObj.getLong("id");
                String title = posObj.getString("title");
                String description = posObj.getString("description");
                // checking all the updated positions
                Position position;
                if (id != -1) {
                    position = Position.findById(id);
                    position.title = title;
                    position.description = description;
                    position.save();
                } else {
                    if (title != null && !title.isEmpty() && description != null && !description.isEmpty()) {
                        position = new Position(title,description,company);
                        company.positions.add(position);
                        position.save();
                    } else {
                        continue;
                    }
                }
                CalPosition calPosition = ApplicationHelper.convertToCalPosition(position);
                calPositions.add(calPosition);
            }
            // delete any removed positions
            for (Position p : company.positions) {
                boolean deletedPosition = true;
                int i=0;
                while (i < calPositions.size() && deletedPosition) {
                    if (p.id == calPositions.get(i).id ) {
                        deletedPosition = false;
                    }
                    i++;
                }
                if (deletedPosition) {
                    company.positions.remove(p);
                    p.delete();
                }
            }
            renderJSON(calPositions);
        } catch (Exception e) {
            Logger.info(e.getMessage());
        }
    }

    public static void getEmployeeInfo(long employeeId) {
        Person person = Person.findById(employeeId);
        String loggedInUser = session.get("username");
        Person currentUser = Person.findByEmailAddress(loggedInUser);
        boolean isAdmin = (currentUser.accountType == AccountType.ADMIN);
        boolean isSelf = (currentUser.id == person.id);
        boolean isManager = (currentUser.accountType == AccountType.LOCATION_MANAGER) || (currentUser.accountType == AccountType.COMPANY_MANAGER);
        CalEmployeeProfile employeeProfile = ApplicationHelper.convertToCalEmployeeProfile(person,isAdmin,isSelf,isManager);
        renderJSON(employeeProfile);
    }

    private static void createUnavailability(Calendar startDate, Calendar endDate, List<String> dayUnavailability,
                                             UnavailabilityBlock unavailabilityBlock, Person employee, String loggedInUser, Integer timezoneOffset) {
        //This will create the unavailability
        //We'll get the date of the start of the week for the given start date
        Integer startDateDayOfTheWeek = startDate.get(Calendar.DAY_OF_WEEK);
        //Then we'll change the startDate to the start of the week
        Calendar currentDate = (Calendar)startDate.clone();
        currentDate.add(Calendar.DATE,(-startDateDayOfTheWeek+1));
        while(currentDate.before(endDate)){
            //While the current date is less than the end date
            Calendar unavailabilityDate = (Calendar)currentDate.clone();
            Integer dayOfWeek = Integer.parseInt(dayUnavailability.get(0));
            //Then we'll actually get the date for the unavailability
            unavailabilityDate.add(Calendar.DATE,dayOfWeek-1);
            Calendar unavailabilityStartDateTime = (Calendar)unavailabilityDate.clone();
            unavailabilityStartDateTime.add(Calendar.HOUR,Integer.parseInt(dayUnavailability.get(1).split(":")[0]));
            unavailabilityStartDateTime.add(Calendar.MINUTE,Integer.parseInt(dayUnavailability.get(1).split(":")[1]));
            unavailabilityStartDateTime.set(Calendar.MILLISECOND,0);
            unavailabilityStartDateTime.set(Calendar.SECOND,0);
            //Then we'll add the offset for the timezone
            unavailabilityStartDateTime.add(Calendar.MINUTE,timezoneOffset);
            Calendar unavailabilityEndDateTime = (Calendar)unavailabilityDate.clone();
            unavailabilityEndDateTime.add(Calendar.HOUR,Integer.parseInt(((dayUnavailability.get(2).split(":")[0]).compareToIgnoreCase("24")==0)?"23":(dayUnavailability.get(2).split(":")[0])));
            unavailabilityEndDateTime.add(Calendar.MINUTE,Integer.parseInt(((dayUnavailability.get(2).split(":")[0]).compareToIgnoreCase("24")==0)?"59":(dayUnavailability.get(2).split(":")[1])));
            unavailabilityEndDateTime.set(Calendar.MILLISECOND,0);
            unavailabilityEndDateTime.set(Calendar.SECOND,0);
            //Similarly, we'll add the offset for the timezone to the endDate String
            unavailabilityEndDateTime.add(Calendar.MINUTE,timezoneOffset);
            if((unavailabilityDate.after(startDate) || unavailabilityDate.equals(startDate)) && (unavailabilityDate.before(endDate) || unavailabilityDate.equals(endDate))){
                //Then we can add the unavailability
                //We should create the start times and the end times

                Unavailability unavailability = new Unavailability(employee,dayOfWeek,unavailabilityStartDateTime,unavailabilityEndDateTime,new GregorianCalendar(),loggedInUser, unavailabilityBlock);
                unavailability.save();
                unavailabilityBlock.unavailabilities.add(unavailability);
                unavailabilityBlock.save();
            }
            currentDate.add(Calendar.DATE,7);
        }
    }




}