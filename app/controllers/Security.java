package controllers;

//import com.sun.deploy.util.LoggerTraceListener;
import models.*;
import play.Logger;
import play.cache.Cache;
import play.mvc.Scope;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * UserRole: shankarvasudevan
 * Date: 20/04/12
 * Time: 8:07 AM
 * To change this template use File | Settings | File Templates.
 */

public class Security extends Secure.Security {

    static boolean authenticate(String username, String password)     {
        if (Person.authenticate(username, password)) {
            // set cookie for username
            response.setCookie("loggedInUser",username);
            return true;
        }
        return false;
    }

    static void onAuthenticated() {
        Application.redirect();
    }

    static void onDisconnected() {
        Site.index();
    }


}
