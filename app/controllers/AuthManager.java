package controllers;

import controllers.deadbolt.DeadboltHandler;
import controllers.deadbolt.ExternalizedRestrictionsAccessor;
import controllers.deadbolt.RestrictedResourcesHandler;
import models.Person;
import models.deadbolt.RoleHolder;
import play.Logger;
import play.mvc.*;

/**
 * Created with IntelliJ IDEA.
 * User: shankarvasudevan
 * Date: 8/09/12
 * Time: 5:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class AuthManager extends Controller implements DeadboltHandler {

    @Override
    public void beforeRoleCheck() {
        if (!Secure.Security.isConnected()) {
            try {
                Secure.login();
            } catch (Throwable t) {

            }
        }
    }

    @Override
    public RoleHolder getRoleHolder() {
        String emailAddress = Secure.Security.connected();
        return Person.findByEmailAddress(emailAddress);
    }

    @Override
    public void onAccessFailure(String controllerClassName) {
        forbidden();
    }

    @Override
    public ExternalizedRestrictionsAccessor getExternalizedRestrictionsAccessor() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public RestrictedResourcesHandler getRestrictedResourcesHandler() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
