package models;

import java.util.*;

import javax.persistence.*;

import play.db.jpa.*;

@Entity
public class Template extends Model {

	public String templateName;
	
	@ManyToOne
	public Location location;
	
	@ManyToMany(mappedBy="templates")
	public List<Shift> shifts;

    public Calendar lastUpdatedDate;
    public String lastUpdatedBy;
	
	public Template(String templateName, Location location) {
		this.templateName = templateName;
		this.location = location;
		this.shifts = new ArrayList<Shift>();
	}

}
