package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Entity
public class Position extends Model {
	@Required
	public String title;
	@Required
	public String description;

    @ManyToOne
    @Required
    public Company company;

	//@ManyToOne
	//@Required
	//public Location location;
	
	@ManyToMany
	public List<Person> persons;
	
	@OneToMany(mappedBy="position", cascade=CascadeType.ALL)
	public List<Shift> shifts;

    public boolean misc;
    public Calendar lastUpdatedDate;
    public String lastUpdatedBy;
	
	/*public Position(Location location, String title, String description) {
		this.location = location;
		this.title = title;
		this.description = description;
		this.persons = new ArrayList<Person>();
		this.shifts = new ArrayList<Shift>();
	} */

    public Position(String title, Company company){
        this.title = title;
        this.persons = new ArrayList<Person>();
        this.shifts = new ArrayList<Shift>();
        this.company = company;
    }

    public Position(String title, String description, Company company) {
        this.title = title;
        this.description = description;
        this.persons = new ArrayList<Person>();
        this.shifts = new ArrayList<Shift>();
        this.company = company;
    }

    public static Position findPositionById(long id)
    {
        JPAQuery query = Position.find("from Position where id =?",id);
        return (Position) query.first();
    }

	public String toString() {
		return title;
	}

    @PreRemove
    protected void preRemove() {
        company.positions.remove(this);
        company.save();
        for (Person person : persons) {
            person.positions.remove(this);
            person.save();
        }
    }

	
}
