package models;

import models.deadbolt.Role;
import play.db.jpa.*;
/**
 * Created with IntelliJ IDEA.
 * UserRole: shankarvasudevan
 * Date: 8/09/12
 * Time: 3:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserRole implements Role {

    private String name;

    public UserRole(String name)
    {
        this.name = name;
    }
    public String getRoleName()
    {
        return name;
    }

}
