package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Entity
public class Company extends Model {
	public String companyName;
	
	@OneToMany(mappedBy="company", cascade=CascadeType.ALL)
	public List<Location> locations;

	@OneToMany(mappedBy="company", cascade=CascadeType.ALL)
	public List<Position> positions;
	
	@OneToMany(mappedBy="company", cascade=CascadeType.ALL)
	public List<Person> persons;

    public Calendar lastUpdatedDate;
    public String lastUpdatedBy;
	
	public Company(String companyName) {
		this.companyName = companyName;
		this.locations = new ArrayList<Location>();
		this.positions = new ArrayList<Position>();
		this.persons = new ArrayList<Person>();
	}

	public String toString() {
		return companyName;
	}


}
