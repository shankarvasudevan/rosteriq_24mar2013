package models;

public enum ResponseType {
	PENDING,
	ACCEPTED,
	DECLINED,
    CANCELLED
}
