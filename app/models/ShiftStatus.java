package models;

public enum ShiftStatus {
	PENDING,
	CONFIRMED,
	CANCELLED,
	COMPLETE,
	INCOMPLETE
}
