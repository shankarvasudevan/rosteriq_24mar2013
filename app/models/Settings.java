package models;

/**
 * Created with IntelliJ IDEA.
 * UserRole: shankarvasudevan
 * Date: 2/09/12
 * Time: 10:08 PM
 * To change this template use File | Settings | File Templates.
 */

import javax.persistence.*;
import play.db.jpa.*;

@Entity
public class Settings extends Model {

    @OneToOne
    public Company company;
    @OneToOne
    public Person person;
    public String timeZone;
    public boolean notifyManagerOfShiftSwaps;
    public boolean notifyEmployeesOfOpenShifts;
    public boolean showCostOfShiftInScheduler;
    public boolean allowEmployeeToSwapShifts;
    public boolean approvalRequiredForDropShifts;
    public boolean employeesCanSwapShiftsOnSameDay;

}
