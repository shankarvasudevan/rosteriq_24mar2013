package models;

import java.sql.Time;

import javax.persistence.ManyToOne;
import java.util.*;

import javax.persistence.*;

import controllers.ApplicationHelper;
import play.db.jpa.*;

@Entity
public class Unavailability extends Model {
	
	@ManyToOne
	public UnavailabilityBlock unavailabilityBlock;
	
	@ManyToOne
	public Person person;
	public long dayOfWeek;
    //public  String startTime;
    //public String endTime;
    public Calendar startDateTime;
    public Calendar endDateTime;
    public Calendar lastUpdatedDate;
    public String lastUpdatedBy;

	/*public Unavailability(UnavailabilityBlock unavailabilityBlock, Person person, long dayOfWeek, String startTime, String endTime, Calendar lastUpdatedDate, String lastUpdatedBy) {
		this.dayOfWeek = dayOfWeek;
		this.startTime = startTime;
		this.endTime = endTime;
		this.unavailabilityBlock = unavailabilityBlock;
		this.person = person;
        this.lastUpdatedDate = lastUpdatedDate;
        this.lastUpdatedBy = lastUpdatedBy;
	}   */

    public Unavailability(Person person, long dayOfWeek, Calendar startDateTime, Calendar endDateTime, Calendar lastUpdatedDate, String lastUpdatedBy, UnavailabilityBlock unavailabilityBlock) {
        this.person = person;
        this.dayOfWeek = dayOfWeek;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.lastUpdatedDate = lastUpdatedDate;
        this.lastUpdatedBy = lastUpdatedBy;
        this.unavailabilityBlock = unavailabilityBlock;
    }

    public boolean overlapsWithShift(Shift shift) {
        return false;
    }
	
    /*public boolean overlapsWithShift(Shift shift) {
        long shiftDayOfWeek = (long) shift.start.get(Calendar.DAY_OF_WEEK);
        if (shiftDayOfWeek == 1) {
            shiftDayOfWeek = 7;
        } else {
            shiftDayOfWeek -= 1;
        }
        if (shiftDayOfWeek == this.dayOfWeek) {
            int shiftStartMinOfDay = ApplicationHelper.getMinuteOfDayFromCalendar(shift.start);
            int shiftFinishMinOfDay = ApplicationHelper.getMinuteOfDayFromCalendar(shift.finish);
            int startTimeMinOfDay = ApplicationHelper.getMinuteOfDayFrom24HourTimeString(startTime);
            int endTimeMinOfDay = ApplicationHelper.getMinuteOfDayFrom24HourTimeString(endTime);
            if(( shiftStartMinOfDay >= startTimeMinOfDay && shiftStartMinOfDay <= endTimeMinOfDay)
                || (shiftFinishMinOfDay >= startTimeMinOfDay && shiftFinishMinOfDay <= endTimeMinOfDay)) {
                return true;
            }
        }
        return false;
    }     */



}
