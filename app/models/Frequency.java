package models;

public enum Frequency {
	DAILY,
	WEEKLY,
	FORTNIGHTLY,
	MONTHLY,
	YEARLY
}
