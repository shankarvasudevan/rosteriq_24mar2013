package models;

public enum AccountType {
    ADMIN,
    EMPLOYEE,
    LOCATION_MANAGER,
    COMPANY_MANAGER
}
