package models;

import java.util.*;
import javax.persistence.*;
import play.db.jpa.*;

@Entity
public class LeaveRequest extends Model {

	@ManyToOne
	public Person person;

	public Calendar startDate;
	public Calendar endDate;
    public String comment;
	public ApprovalStatus status;

    public Calendar lastUpdatedDate;
    public String lastUpdatedBy;

	public LeaveRequest(Person person, Calendar startDate, Calendar endDate, String comment) {
		this.person = person;
		this.startDate = startDate;
		this.endDate = endDate;
        this.comment = comment;
		this.status = ApprovalStatus.AWAITING_APPROVAL;
	}

	
}
