package models;

import controllers.ApplicationHelper;
import play.Logger;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@Entity
public class ShiftSeries extends Model {

    public Calendar startDate;
    public Calendar endDate;
    public String days;
    public Frequency frequency;
    public int inc;

    @OneToMany(mappedBy="shiftSeries", cascade=CascadeType.ALL)
    public List<Shift> shifts;

    public Calendar lastUpdatedDate;
    public String lastUpdatedBy;

    public ShiftSeries(Calendar startDate, Calendar endDate, Frequency frequency,
                       int inc, String lastUpdatedBy) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.frequency = frequency;
        this.shifts = new ArrayList<Shift>();
        this.inc = inc;
        this.lastUpdatedDate = new GregorianCalendar();
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public ShiftSeries(Shift sample, Calendar endDate, Frequency frequency,
                       int inc, String lastUpdatedBy) {
        this.startDate = sample.start;
        this.endDate = endDate;
        this.frequency = frequency;
        this.shifts = new ArrayList<Shift>();
        this.shifts.add(sample);
        this.inc = inc;
        this.lastUpdatedDate = new GregorianCalendar();
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public void createRecurrence(Shift firstShift, Calendar endDate,
                                 Frequency frequency, int increment, String lastUpdatedBy) {
        Logger.info("Inside createRecurrence");
        Logger.info("endDate = " + endDate.getTime().toString());
        int units = 0;
        this.inc = increment;
        Logger.info("inc = " + increment);
        int recurrenceIncrement = 0;
        if (frequency.equals(Frequency.DAILY)) {
            units = Calendar.DATE;
            recurrenceIncrement = 1 * increment;
        } else if (frequency.equals(Frequency.WEEKLY)) {
            units = Calendar.DATE;
            recurrenceIncrement = 7 * increment;
        } else if (frequency.equals(Frequency.FORTNIGHTLY)) {
            units = Calendar.DATE;
            recurrenceIncrement = 14 * increment;
        } else if (frequency.equals(Frequency.MONTHLY)) {
            units = Calendar.MONTH;
            recurrenceIncrement = 1 * increment;
        } else if (frequency.equals(Frequency.YEARLY)) {
            units = Calendar.YEAR;
            recurrenceIncrement = 1 * increment;
        }
        Logger.info("units = " + units);
        Logger.info("frequency = " + frequency.toString());
        Logger.info("recurrenceIncrement = " + recurrenceIncrement);
        if (units > 0 && recurrenceIncrement > 0) {
            Calendar nextShiftStart = (Calendar) firstShift.start.clone();
            Calendar nextShiftFinish = (Calendar) firstShift.finish.clone();
            nextShiftStart.add(units,recurrenceIncrement);
            nextShiftFinish.add(units,recurrenceIncrement);
            while (nextShiftFinish.compareTo(endDate) < 0) {
                addNewShiftToSeries(firstShift,nextShiftStart,nextShiftFinish, lastUpdatedBy);
                nextShiftStart.add(units,recurrenceIncrement);
                nextShiftFinish.add(units,recurrenceIncrement);
            }
        }

    }

    public void addNewShiftToSeries(Shift first, Calendar shiftStart,
                                    Calendar shiftFinish, String lastUpdatedBy) {
        Calendar shiftDateCopy = (Calendar) shiftStart.clone();
        Calendar weekStart = ApplicationHelper.getPrevSunday(shiftDateCopy);
        Calendar lastUpdatedDate = new GregorianCalendar();
        Shift next = new Shift(weekStart,shiftStart,shiftFinish,first.location,first.person,first.position, lastUpdatedDate,lastUpdatedBy);
        next.save();
        //next.shiftSeries = this;
        this.shifts.add(next);

    }






}
