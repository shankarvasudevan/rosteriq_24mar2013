package models;


import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Entity
public class DropShiftRequest extends Model {

	@ManyToOne
	public Shift shift;

    public ApprovalStatus status;

    public Calendar lastUpdatedDate;
    public String lastUpdatedBy;
    public String notes;

    @ManyToOne
    public Person fromPerson;

    @ManyToMany
    public List<Person> toPersons;
	
	public DropShiftRequest(Shift shift, Person fromPerson, List<Person> toPersons) {
		this.shift = shift;
        this.fromPerson = fromPerson;
        this.toPersons = new ArrayList<Person>();
        this.toPersons.addAll(toPersons);
		this.status = ApprovalStatus.AWAITING_APPROVAL;
	}

    public DropShiftRequest(Shift shift) {
        this.shift = shift;
        this.status = ApprovalStatus.AWAITING_APPROVAL;
    }

	
}
