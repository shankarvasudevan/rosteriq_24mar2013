package models;

public enum ApprovalStatus {
	AWAITING_APPROVAL ("Awaiting Approval"),
	DECLINED ("Declined"),
	APPROVED ("Approved");

    private String name;

    private ApprovalStatus(String s) {
        name = s;
    }

    public String toString() {
        return name;
    }
}
