package models;

import models.deadbolt.RoleHolder;
import play.data.validation.Required;
import play.db.jpa.Model;
import play.libs.Crypto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


@Entity
public class Person extends Model implements RoleHolder {

	@Required
	public String password;
    @Required
	public String firstName;
	@Required
	public String lastName;
    @Required
    public String emailAddress;

    public String address;
    public String suburb;
    public String state;
    public String country;
    public String postCode;
    public String contactNumber;

	public Calendar dob;
	public Float wage;
	public int minHoursBetweenShifts;
	public int maxHoursPerWeek;
    @Enumerated(EnumType.ORDINAL)
	public AccountType accountType;
    public boolean misc;

	@ManyToOne
	public Company company;

	@ManyToMany(mappedBy="persons")
	public List<Position> positions;

    @ManyToMany(mappedBy="persons")
	public List<Location> locations;
	
	@OneToMany(mappedBy="person", cascade=CascadeType.ALL)
	public List<Shift> shifts;
	
	@OneToMany(mappedBy="person", cascade=CascadeType.ALL)
	public List<LeaveRequest> leaveRequests;
	
	@OneToMany(mappedBy="person", cascade=CascadeType.ALL)
	public List<UnavailabilityBlock> unavailabilityBlocks;

    @OneToMany(mappedBy="fromPerson", cascade=CascadeType.ALL)
    public List<SwapShiftRequest> swapShiftRequestsFromPerson;

    @OneToMany(mappedBy="toPerson", cascade=CascadeType.ALL)
    public List<SwapShiftRequest> swapShiftRequestsToPerson;

    @OneToMany(mappedBy="fromPerson", cascade=CascadeType.ALL)
    public List<DropShiftRequest> dropShiftRequestsFromPerson;

    @ManyToMany(mappedBy="toPersons")
    public List<DropShiftRequest> dropShiftRequestsToPerson;

    public Calendar lastUpdatedDate;
    public String lastUpdatedBy;
	
	public Person(Company company, String password,String firstName, String lastName,
                  String emailAddress, String contactNumber) {
		this.company = company;
		this.password = Crypto.encryptAES(password);
        this.emailAddress = emailAddress;
        this.contactNumber = contactNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.accountType = AccountType.EMPLOYEE;
        this.wage = (float) 0;
		this.positions = new ArrayList<Position>();
		this.locations = new ArrayList<Location>();
		this.shifts = new ArrayList<Shift>();
		this.leaveRequests = new ArrayList<LeaveRequest>();
		this.unavailabilityBlocks = new ArrayList<UnavailabilityBlock>();
        this.swapShiftRequestsFromPerson = new ArrayList<SwapShiftRequest>();
        this.swapShiftRequestsToPerson = new ArrayList<SwapShiftRequest>();
        this.dropShiftRequestsFromPerson = new ArrayList<DropShiftRequest>();
        this.dropShiftRequestsToPerson = new ArrayList<DropShiftRequest>();

	}
	
	public Person(Company company, String username, String password, String emailAddress,
                  String firstName, String lastName, String address, String suburb, String state, String postCode,
                  String country, String contactNumber, Calendar dob) {
		this.company = company;
		this.password = Crypto.encryptAES(password);
		this.firstName = firstName;
		this.lastName = lastName;
        this.emailAddress = emailAddress;
		this.address = address;
		this.suburb = suburb;
		this.state = state;
		this.postCode = postCode;
		this.country = country;
		this.contactNumber = contactNumber;
		this.dob = dob;
		this.accountType = AccountType.EMPLOYEE;
        this.wage = (float) 0;
		this.positions = new ArrayList<Position>();
		this.locations = new ArrayList<Location>();
		this.shifts = new ArrayList<Shift>();
		this.leaveRequests = new ArrayList<LeaveRequest>();
		this.unavailabilityBlocks = new ArrayList<UnavailabilityBlock>();
        this.swapShiftRequestsFromPerson = new ArrayList<SwapShiftRequest>();
        this.swapShiftRequestsToPerson = new ArrayList<SwapShiftRequest>();
        this.dropShiftRequestsFromPerson = new ArrayList<DropShiftRequest>();
        this.dropShiftRequestsToPerson = new ArrayList<DropShiftRequest>();
	}

    public static boolean authenticate(String emailAddress, String password) {
        List<Person> persons = Person.findAll();
        for (Person p : persons) {
            if (p.emailAddress.compareToIgnoreCase(emailAddress) == 0) {
                String decrpytedPassword = Crypto.decryptAES(p.password);
                if(decrpytedPassword.compareTo(password) == 0)
                    return true;
            }
        }
        return false;
    }

    public static Person findByEmailAddress(String emailAddress) {
        JPAQuery query = Person.find("from Person where emailAddress = ?",emailAddress);
        return (Person) query.first();
    }

    public static Person findByUserId(long id)
    {
        JPAQuery query = Person.find("from Person where id =?",id);
        return (Person) query.first();
    }

    public Location getFirstLocation() {
        if (locations != null && !locations.isEmpty()) {
            return locations.get(0);
        }
        return null;
    }

	public String toString() {
		return lastName +"," + firstName;
	}

    public List<UserRole> getRoles() {
        List<UserRole> roles = new ArrayList<UserRole>();
        roles.add(new UserRole(accountType.name()));
        return roles;
    }

    public static void updateEmployee(String firstName, String lastName, String email, long employeeId){
        Person person = Person.findById(employeeId);
        person.firstName = firstName;
        person.lastName = lastName;
        person.emailAddress = email;
        person.save();
    }

    @PreRemove
    protected void preRemove() {
        company.persons.remove(this);
        company.save();
        for (Location location : locations) {
            location.persons.remove(this);
            location.save();
        }

        for (Position position : positions) {
            position.persons.remove(this);
            position.save();
        }
    }

    /*
    public static void deleteEmployee(Person person){
        Company company = person.company;
        List<Location> locations = person.locations;
        List<Position> positions = person.positions;
        company.persons.remove(person);
        for (Location location : locations)
        {
            location.persons.remove(person);
        }
        for (Position position : positions)
        {
            position.persons.remove(person);
        }
        person.delete();
    }
    */

}
