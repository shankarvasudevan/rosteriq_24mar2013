package models;

import java.util.*;
import javax.persistence.*;
import play.db.jpa.*;
import play.data.validation.*;

@Entity
public class Location extends Model {
	
	@Required
	public String locationName;
	public String address;
    public String suburb;
	public String state;
	public String country;
    public String postCode;
	public String contactNumber;
	
	@ManyToOne
	@Required
	public Company company;

    //Has been changed to a one-to-many relationship as an employee should have a base location. But an employee will still be allowed to work at multiple locations
//	@ManyToMany(cascade=CascadeType.ALL)
//	public List<Person> persons;

    @ManyToMany
    public List<Person> persons;

    @OneToMany(mappedBy="location", cascade=CascadeType.ALL)
     public List<Shift> shifts;
	
	public Location(Company company, String locationName) {
		this.company = company;
		this.locationName = locationName;
		this.persons = new ArrayList<Person>();
		this.shifts = new ArrayList<Shift>();
	}

    public Location(Company company, String locationName, String locationAddress){
        this.company = company;
        this.locationName = locationName;
        this.address = locationAddress;
    }
	
	public Location(Company company, String address, String suburb, String state, String postCode, String country,
	String contactNumber) {
		this.company = company;
		this.address = address;
		this.state = state;
        this.suburb = suburb;
		this.postCode = postCode;
		this.country = country;
		this.contactNumber = contactNumber;
		this.persons = new ArrayList<Person>();
		this.shifts = new ArrayList<Shift>();
	}

	public String toString() {
		return company.toString() + " - " + locationName;
	}

    public static Location getLocationFromPersonId(Long personId) {
        Query query = em().createQuery("select locations_id from Location_Person where persons_id = ?1");
        query.setParameter(1,personId);
        Long locationId = (Long) query.getSingleResult();
        if (locationId > 0) {
            return Location.findById(locationId);
        }
        return null;
    }

    //More though required. Need to find out if the save was successful
    public static void updateLocation(long locationId, String name, String address){
        Location location = Location.findById(locationId);
        location.locationName = name;
        location.address = address;
        location.save();
    }

    public static void deleteLocation(long locationId){
        Location deleteLocation = Location.findById(locationId);
        deleteLocation.delete();
    }





}
