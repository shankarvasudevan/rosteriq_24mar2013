package models;

/**
 * Created with IntelliJ IDEA.
 * UserRole: shankarvasudevan
 * Date: 21/08/12
 * Time: 8:22 AM
 * To change this template use File | Settings | File Templates.
 */

import javax.persistence.*;
import java.util.*;
import play.db.jpa.*;

@Entity
public class SwapShiftRequest extends Model {

    @ManyToOne
    public Shift from;
    @ManyToOne
    public Shift to;
    @ManyToOne
    public Person fromPerson;
    @ManyToOne
    public Person toPerson;
    public ResponseType response;
    public Calendar lastUpdatedDate;
    public String lastUpdatedBy;
    public String notes;

    public SwapShiftRequest(Shift from, Shift to, String notes) {
        this.from = from;
        this.to = to;
        this.fromPerson = from.person;
        this.toPerson = to.person;
        this.notes = notes;
        this.response = ResponseType.PENDING;
    }



}
