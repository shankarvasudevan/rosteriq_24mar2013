package models;

import java.sql.Time;
import java.util.*;
import javax.persistence.*;

import notifiers.EmailNotifier;
import org.joda.time.DateTime;
import play.Logger;
import play.db.jpa.*;

@Entity
public class Shift extends Model {

    //We should soon discontinue using the first three
    public Calendar weekStartDate; //This will represent the monday of the week of the shift. This will help in pulling down shifts for a particular week
    //New parameters which I feel are more useful in the long run. These will be useful for shifts across days, months and even years.
    public Calendar start;
    public Calendar finish;
	public boolean open;
	public boolean draft;
    public boolean swapShiftRequestPending;
    public boolean dropShiftRequestPending;
    public boolean confirmed;
	@ManyToOne
	public Location location;
	@ManyToOne
	public Position position;
	@ManyToOne
	public Person person;
	@ManyToOne
	public ShiftSeries shiftSeries;
	@ManyToMany(cascade=CascadeType.ALL)
	public List<Template> templates;
    public Calendar lastUpdatedDate;
    public String lastUpdatedBy;
    public Calendar publishedDate;

    public Calendar publishedStart;
    public Calendar publishedFinish;
    public boolean publishedOpen;
    public long publishedPositionId;
    public long publishedPersonId;
    public float numHoursBreak;
    public boolean cancelled;
    public float numHoursWorked;
    public float cost;
    public boolean conflictStatus;


    //New constructor which uses the new parameters.
    public Shift(Calendar weekStartDate, Calendar start, Calendar finish, Location location,
                  Person person, Position position, Calendar lastUpdatedDate, String lastUpdatedBy)
    {
        this.weekStartDate = weekStartDate;
        this.start = start;
        this.finish = finish;
        resetSeconds(weekStartDate);
        resetSeconds(start);
        resetSeconds(finish);
        this.location = location;
        this.person = person;
        this.position = position;
        this.lastUpdatedDate = lastUpdatedDate;
        this.lastUpdatedBy = lastUpdatedBy;
        this.open = false;
        this.draft = true;
        this.cancelled = false;
        this.confirmed = false;
        this.swapShiftRequestPending = false;
    }

    public void updateHoursWorked() {
        float timeDiff = 0;
        int yearDiff = finish.get(Calendar.YEAR) - start.get(Calendar.YEAR);
        int monthDiff = finish.get(Calendar.MONTH) - start.get(Calendar.MONTH);
        int dayDiff = finish.get(Calendar.DATE) - start.get(Calendar.DATE);
        int hoursDiff = finish.get(Calendar.HOUR_OF_DAY) - start.get(Calendar.HOUR_OF_DAY);
        int minsDiff = finish.get(Calendar.MINUTE) - start.get(Calendar.MINUTE);
        int totalMonths = yearDiff * 12 + monthDiff;
        int numDaysInMonth = start.getActualMaximum(Calendar.MONTH) ;
        int totalDays = totalMonths * numDaysInMonth + dayDiff; // Assumption: shift only spans 1 month
        int totalHours = totalDays * 24 + hoursDiff;
        int totalMins = totalHours * 60 + minsDiff;
        timeDiff = (float) totalMins / 60 ;
        this.numHoursWorked = timeDiff - numHoursBreak;
    }

    public void updateCost() {
        this.cost = this.person.wage * this.numHoursWorked;
    }


    private Calendar resetSeconds(Calendar cal) {
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);
        return cal;
    }






//	public boolean overlapsWith(Shift shift) {
//		if (shift.date.compareTo(this.date) == 0) {
//			if ((this.startTime.compareTo(shift.endTime) <= 0) ||
//				(this.endTime.compareTo(shift.startTime) >= 0)) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	public Shift replicateToDate(Calendar newDate) {
//		Shift replicaShift = new Shift(this.location, newDate,
//				this.startTime, this.endTime);
//		replicaShift.open = this.open;
//		replicaShift.payRate = this.payRate;
//		replicaShift.draft = this.draft;
//		return replicaShift;
//	}

}


