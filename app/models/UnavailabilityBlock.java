package models;

import java.sql.Time;

import java.util.*;

import javax.persistence.*;

import controllers.ApplicationHelper;
import play.db.jpa.*;

@Entity
public class UnavailabilityBlock extends Model {
		
	public String title;
    public String description;
	public Calendar startDate;
	public Calendar endDate;
    public Calendar lastUpdatedDate;
    public String lastUpdatedBy;

	@OneToMany(mappedBy="unavailabilityBlock", cascade=CascadeType.ALL)
	public List<Unavailability> unavailabilities;

    @ManyToOne
    public Person person;


	public UnavailabilityBlock(Person person, String title,String description, Calendar startDate, Calendar endDate, Calendar lastUpdatedDate, String lastUpdatedBy) {
        this.person = person;
        this.title = title;
		this.description = description;
        this.lastUpdatedDate = lastUpdatedDate;
        this.lastUpdatedBy = lastUpdatedBy;
        this.startDate = startDate;
        this.endDate = endDate;
		this.unavailabilities = new ArrayList<Unavailability>();
	}





	
}
