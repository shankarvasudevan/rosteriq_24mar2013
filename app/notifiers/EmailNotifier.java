package notifiers;

import models.*;
import controllers.ApplicationHelper;
import java.util.*;
import javax.persistence.Query;

import play.Logger;
import play.libs.Crypto;
import play.mvc.Mailer;
import play.db.jpa.JPA;

public class EmailNotifier extends Mailer {

   private static final String SENDER_ADDRESS = "noreply@rosteriq.com";
   private static final String NEW_SHIFTS_NOTIFICATION_SUBJECT = "You have been rostered for new shifts";
   private static final String CANCELLED_SHIFTS_NOTIFICATION_SUBJECT = "Some of your shifts have been cancelled";
   private static final String UPDATED_SHIFTS_NOTIFICATION_SUBJECT = "Your roster has been changed";
   private static final String OPEN_SHIFTS_NOTIFICATION_SUBJECT = "New shifts are available";
   private static final String SWAP_SHIFT_NOTIFICATION_SUBJECT_SUFFIX = "wants to swap a shift with you";
   private static final String SWAP_SHIFT_ACCEPTED_SUBJECT_SUFFIX = "has accepted your shift swap";
   private static final String SWAP_SHIFT_DECLINED_SUBJECT_SUFFIX = "has declined your shift swap";
   private static final String TEST_RECIPIENT_ADDRESS = "s.vasudevan297@gmail.com";
   private static final String SWAP_SHIFT_MANAGER_NOTIFICATION_SUBJECT = "Two of your employees have swapped their shifts";
   private static final String ACCOUNT_CREATED = "You have created an account at RosterIQ";
    private static final String EMPLOYEE_INVITATION = "Your account has been created at RosterIQ";

   public static void openShiftNotification(List<Shift> openShifts, Person employee) {
       if (openShifts.size() > 0) {
           setFrom(SENDER_ADDRESS);
           setSubject(OPEN_SHIFTS_NOTIFICATION_SUBJECT);
           List<ShiftNotification> openShiftNotifications = new ArrayList<ShiftNotification>();
           String recipientFirstName = employee.firstName;
           addRecipient(TEST_RECIPIENT_ADDRESS);
          // addRecipient(employee.emailAddress);
           for (Shift s : openShifts) {
               if (s.publishedPersonId == employee.id) {
                    return;  // don't want to send the notification to the person that previously had the shift
               }
               ShiftNotification openShiftNotification = new ShiftNotification();
               openShiftNotification.companyName = s.location.company.companyName;
               openShiftNotification.locationName = s.location.locationName;
               openShiftNotification.start = ApplicationHelper.convertToReadableString(s.start);
               openShiftNotification.finish = ApplicationHelper.convertToReadableString(s.finish);
               openShiftNotification.positionTitle = s.position.title;
               openShiftNotification.person = employee;
               openShiftNotifications.add(openShiftNotification);
           }
           Logger.info("Just about to do the send: ");
           Logger.info("#openShiftNotifications = " + openShiftNotifications.size());
           Logger.info("recipientFirstName = " + recipientFirstName);
      //     Logger.info("managerFirstName = " + managerFirstName);
      //     Logger.info("managerLastName = " + managerLastName);
           send(openShiftNotifications,recipientFirstName); //,managerFirstName,managerLastName);
       }
   }


   public static void newShiftsNotification(List<Shift> newShifts, List<Shift> upcomingShifts) {
       if (newShifts.size() > 0) {
           setFrom(SENDER_ADDRESS);
           setSubject(NEW_SHIFTS_NOTIFICATION_SUBJECT);
           List<ShiftNotification> newShiftNotifications = new ArrayList<ShiftNotification>();
           Person manager = Person.findByEmailAddress(newShifts.get(0).lastUpdatedBy);
           Person recipient = newShifts.get(0).person;
           if (recipient.misc == false) {
               addRecipient(TEST_RECIPIENT_ADDRESS);
               //addRecipient(recipient.emailAddress);
               String recipientFirstName = recipient.firstName;
               String managerFirstName = manager.firstName;
               String managerLastName = manager.lastName;
               for (Shift shift : newShifts) {
                   ShiftNotification newShiftNotification = new ShiftNotification();
                   newShiftNotification.companyName = shift.location.company.companyName;
                   newShiftNotification.locationName = shift.location.locationName;
                   newShiftNotification.start = ApplicationHelper.convertToReadableString(shift.start);
                   newShiftNotification.finish = ApplicationHelper.convertToReadableString(shift.finish);
                   newShiftNotification.positionTitle = shift.position.title;
                   newShiftNotifications.add(newShiftNotification);
               }
               List<ShiftNotification> upcomingShiftReminders = createUpcomingShiftReminders(upcomingShifts);
               Logger.info("Just about to do the send:");
               Logger.info("#newShiftNotifications = " + newShiftNotifications.size());
               Logger.info("#upcomingShiftReminders = " + upcomingShiftReminders.size());
               Logger.info("recipientFirstName = " + recipientFirstName);
               Logger.info("managerFirstName = " + managerFirstName);
               Logger.info("managerLastName = " + managerLastName);
               send(newShiftNotifications, upcomingShiftReminders, recipientFirstName, managerFirstName, managerLastName);
           }
       }
   }

   public static void updatedShiftsNotification(List<Shift> updatedShifts, List<Shift> upcomingShifts) {
       if (updatedShifts.size() > 0) {
           setFrom(SENDER_ADDRESS);
           setSubject(UPDATED_SHIFTS_NOTIFICATION_SUBJECT);
           List<ShiftNotification> updatedShiftNotifications = new ArrayList<ShiftNotification>();
           Person manager = Person.findByEmailAddress(updatedShifts.get(0).lastUpdatedBy);
           Person recipient = updatedShifts.get(0).person;
           if (recipient.misc == false) {
               addRecipient(TEST_RECIPIENT_ADDRESS);
               //addRecipient(recipient.emailAddress);
               String recipientFirstName = recipient.firstName;
               String managerFirstName = manager.firstName;
               String managerLastName = manager.lastName;
               for (Shift shift : updatedShifts) {
                   ShiftNotification updatedShiftNotification = new ShiftNotification();
                   updatedShiftNotification.companyName = shift.location.company.companyName;
                   updatedShiftNotification.locationName = shift.location.locationName;
                   updatedShiftNotification.prevStart = ApplicationHelper.convertToReadableString(shift.publishedStart);
                   updatedShiftNotification.prevFinish = ApplicationHelper.convertToReadableString(shift.publishedFinish);
                   updatedShiftNotification.prevPositionTitle = ((Position) Position.findById(shift.publishedPositionId)).title;
                   updatedShiftNotification.start = ApplicationHelper.convertToReadableString(shift.start);
                   updatedShiftNotification.finish = ApplicationHelper.convertToReadableString(shift.finish);
                   updatedShiftNotification.positionTitle = shift.position.title;
                   updatedShiftNotifications.add(updatedShiftNotification);
               }
               List<ShiftNotification> upcomingShiftReminders = createUpcomingShiftReminders(upcomingShifts);
               Logger.info("#newShiftNotifications = " + updatedShiftNotifications.size());
               Logger.info("#upcomingShiftReminders = " + upcomingShiftReminders.size());
               Logger.info("recipientFirstName = " + recipientFirstName);
               Logger.info("managerFirstName = " + managerFirstName);
               Logger.info("managerLastName = " + managerLastName);
               send(updatedShiftNotifications,upcomingShiftReminders,recipientFirstName,managerFirstName,managerLastName);
           }
       }
   }

   public static void cancelledShiftNotification(List<Shift> cancelledShifts, List<Shift> upcomingShifts) {
       if (cancelledShifts.size() > 0) {
           setFrom(SENDER_ADDRESS);
           setSubject(CANCELLED_SHIFTS_NOTIFICATION_SUBJECT);
           List<ShiftNotification> cancelledShiftNotifications = new ArrayList<ShiftNotification>();
           Person manager = Person.findByEmailAddress(cancelledShifts.get(0).lastUpdatedBy);
           Person recipient = Person.findById(cancelledShifts.get(0).publishedPersonId);
           if (recipient.misc == false) {
               addRecipient(TEST_RECIPIENT_ADDRESS);
               //addRecipient(recipient.emailAddress);
               String recipientFirstName = recipient.firstName;
               String managerFirstName = manager.firstName;
               String managerLastName = manager.lastName;
               for (Shift shift : cancelledShifts) {
                   ShiftNotification cancelledShiftNotification = new ShiftNotification();
                   cancelledShiftNotification.companyName = shift.location.company.companyName;
                   cancelledShiftNotification.locationName = shift.location.locationName;
                   cancelledShiftNotification.start = ApplicationHelper.convertToReadableString(shift.publishedStart);
                   cancelledShiftNotification.finish = ApplicationHelper.convertToReadableString(shift.publishedFinish);
                   Position pos = Position.findById(shift.publishedPositionId);
                   cancelledShiftNotification.positionTitle = pos.title;
                   cancelledShiftNotifications.add(cancelledShiftNotification);
               }
               List<ShiftNotification> upcomingShiftReminders = createUpcomingShiftReminders(upcomingShifts);
               Logger.info("#newShiftNotifications = " + cancelledShiftNotifications.size());
               Logger.info("#upcomingShiftReminders = " + upcomingShiftReminders.size());
               Logger.info("recipientFirstName = " + recipientFirstName);
               Logger.info("managerFirstName = " + managerFirstName);
               Logger.info("managerLastName = " + managerLastName);
               send(cancelledShiftNotifications,upcomingShiftReminders,recipientFirstName,managerFirstName,managerLastName);
           }
       }
   }

   public static void swapShiftNotification(SwapShiftRequest request) {
       if (request.id > 0) {
           String toPersonFirstName = request.toPerson.firstName;
           String fromPersonFirstName = request.fromPerson.firstName;
           String fromPersonLastName = request.fromPerson.lastName;
           setFrom(SENDER_ADDRESS);
           setSubject(fromPersonFirstName + " " + fromPersonLastName + " " + SWAP_SHIFT_NOTIFICATION_SUBJECT_SUFFIX);
           addRecipient(TEST_RECIPIENT_ADDRESS);

           String notes = request.notes;
           String fromShiftStr = ApplicationHelper.convertShiftToReadableString(request.from);
           String toShiftStr = ApplicationHelper.convertShiftToReadableString(request.to);
           String shiftPos = request.from.position.title;
           String shiftLoc = request.from.location.locationName;
           // send the notification
           send(toPersonFirstName,fromPersonFirstName,fromPersonLastName,fromShiftStr,toShiftStr,shiftPos,shiftLoc,notes);
       }
   }

   public static void swapShiftResponse(SwapShiftRequest request) {
       if (request.id > 0) {
           String response = request.response.toString();
           setFrom(SENDER_ADDRESS);
           String fromPersonFirstName = request.fromPerson.firstName;
           String toPersonName = request.toPerson.firstName + " " + request.toPerson.lastName;

           if (request.response == ResponseType.ACCEPTED) {
               setSubject(toPersonName + " " + SWAP_SHIFT_ACCEPTED_SUBJECT_SUFFIX);
           } else if (request.response == ResponseType.DECLINED) {
               setSubject(toPersonName + " " + SWAP_SHIFT_DECLINED_SUBJECT_SUFFIX);
           }
           addRecipient(TEST_RECIPIENT_ADDRESS);
           String notes = request.notes;
           String fromShiftStr = ApplicationHelper.convertShiftToReadableString(request.from);
           String toShiftStr = ApplicationHelper.convertShiftToReadableString(request.to);
           String shiftPos = request.from.position.title;
           String shiftLoc = request.from.location.locationName;
           send(response, toPersonName, fromPersonFirstName,fromShiftStr,toShiftStr,shiftPos,shiftLoc);
       }
   }

   public static void swapShiftManagerNotification(SwapShiftRequest request) {
       String queryStr = "select p from Person where location_id = ? and accounttype = ?";
       Query query = JPA.em().createQuery(queryStr);
       query.setParameter(1,request.from.location.id);
       query.setParameter(2,AccountType.LOCATION_MANAGER);
       List<Person> locationManagers = query.getResultList();
       String fromPersonName = request.fromPerson.firstName + " " + request.fromPerson.lastName;
       String toPersonName = request.toPerson.lastName + " " + request.toPerson.lastName;
       String fromShiftDateTime = ApplicationHelper.convertShiftToReadableString(request.from);
       String toShiftDateTime = ApplicationHelper.convertShiftToReadableString(request.to);
       String fromPosition = request.from.position.title;
       String toPosition = request.to.position.title;
       for (Person manager : locationManagers) {
           setFrom(SENDER_ADDRESS);
           setSubject(SWAP_SHIFT_MANAGER_NOTIFICATION_SUBJECT);
           addRecipient(TEST_RECIPIENT_ADDRESS);
           String managerName = manager.firstName;
           send(managerName,fromPersonName,toPersonName,fromShiftDateTime,toShiftDateTime,fromPosition,toPosition);
       }
   }



   private static List<ShiftNotification> createUpcomingShiftReminders(List<Shift> upcomingShifts) {
       List<ShiftNotification> upcomingShiftReminders = new ArrayList<ShiftNotification>();
       for (Shift shift : upcomingShifts) {
           ShiftNotification upcomingShiftReminder = new ShiftNotification();
           upcomingShiftReminder.companyName = shift.location.company.companyName;
           upcomingShiftReminder.locationName = shift.location.locationName;
           upcomingShiftReminder.start = ApplicationHelper.convertToReadableString(shift.start);
           upcomingShiftReminder.finish = ApplicationHelper.convertToReadableString(shift.finish);
           upcomingShiftReminder.positionTitle = shift.position.title;
           upcomingShiftReminders.add(upcomingShiftReminder);
       }
       return upcomingShiftReminders;
   }

    public static void accountCreated(Long companyManagerId){
        //This will send the email for the account creation
        Person companyManager = Person.findById(companyManagerId);
        String companyManagerPassword = Crypto.decryptAES(companyManager.password);
        Company newCompany = companyManager.company;
        setFrom(SENDER_ADDRESS);
        setSubject(ACCOUNT_CREATED);
        addRecipient(companyManager.emailAddress);
        send(companyManager, newCompany, companyManagerPassword);
    }

    public static void inviteEmployee(Person employee){
        String employeePassword = Crypto.decryptAES(employee.password);
        setFrom(SENDER_ADDRESS);
        setSubject(EMPLOYEE_INVITATION);
        addRecipient(employee.emailAddress);
        send(employee, employeePassword);
    }




}
