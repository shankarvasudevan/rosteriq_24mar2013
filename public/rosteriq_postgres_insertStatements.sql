INSERT INTO Company VALUES(1, 'Fitness First', NULL, NULL);

INSERT INTO Location VALUES(1, '300 Carlingford Rd', '0412345678', 'Australia', 'Carlingford', '2118', 'NSW', 'Carlingford', 1);
INSERT INTO Location VALUES(2, '30 Blacktown Rd', '0412345678', 'Australia', 'Blacktown', '2148','NSW','Blacktown',1);

INSERT INTO Position VALUES(1, 'Someone who manages', NULL, NULL, FALSE, 'Manager', 1);
INSERT INTO Position VALUES(2, 'Someone who trains', NULL, NULL, FALSE, 'Trainer', 1);
INSERT INTO Position VALUES(3, 'Someone who is a receptionist', NULL, NULL, FALSE, 'Receptionist', 1);
INSERT INTO Position VALUES(4, 'Someone who handles accounts', NULL, NULL, FALSE, 'Accounts Desk', 1);
INSERT INTO Position VALUES(7, 'The person who designs', NULL, NULL, FALSE, 'Graphic Designer', 1);
INSERT INTO Position VALUES(8, 'The person who programs', NULL, NULL, FALSE, 'Programmer', 1);
INSERT INTO Position VALUES(9, 'The person who analyses', NULL, NULL, FALSE, 'Business Analyst', 1);
INSERT INTO Position VALUES(10, 'The person who prices', NULL, NULL, FALSE, 'Pricing Motor', 1);
INSERT INTO Position VALUES(23, '', NULL, NULL, TRUE, 'Unassigned', 1);

INSERT INTO Person VALUES(1, 2, '', '', '', NULL, 'dramanan@rosteriq.com', 'Dinesh', 'Ramanan', 'admin', NULL, 40, 0, FALSE, 'dramanan', '', '', '', 20, 1);
INSERT INTO Person VALUES(2, 1, '', '', '', NULL, 'spottekkatt@rosteriq.com', 'Sabinesh', 'Pottekkatt', 'admin', NULL, 40, 0, FALSE, 'spottekkatt', '', '', '', 20, 1);
INSERT INTO Person VALUES(3, 0, '', '', '', NULL, 'pbedi@rosteriq.com', 'Piyush', 'Bedi', 'admin', NULL, 40, 0, FALSE, 'pbedi', '', '', '', 20, 1);
INSERT INTO Person VALUES(4, 0, '', '', '', NULL, 'asingla@rosteriq.com', 'Apresh', 'Singla', 'admin', NULL, 40, 0, FALSE, 'asingla', '', '', '', 20, 1);
INSERT INTO Person VALUES(5, 0, '', '', '', NULL, 'kmalhotra@rosteriq.com', 'Kanav', 'Malhotra', 'admin', NULL, 40, 0, FALSE, 'kmalhotra', '', '', '', 20, 1);
INSERT INTO Person VALUES(6, 0, '', '', '', NULL, 'rlele@rosteriq.com', 'Rucha', 'Lele', 'admin', NULL, 40, 0, FALSE, 'rlele', '', '', '', 20, 1);
INSERT INTO Person VALUES(7, 0, '', '', '', NULL, 'snatarajan@rosteriq.com', 'Sujatha', 'Natarajan', 'admin', NULL, 40, 0, FALSE, 'snatarajan', '', '', '', 20, 1);
INSERT INTO Person VALUES(8, 0, '', '', '', NULL, 'agupta@rosteriq.com', 'Akshat', 'Gupta', 'admin', NULL, 40, 0, FALSE, 'agupta', '', '', '', 20, 1);
INSERT INTO Person VALUES(9, 0, '', '', '', NULL, 'nalur@rosteriq.com', 'Nash', 'Alur', 'admin', NULL, 40, 0, FALSE, 'nalur', '', '', '', 20, 1);
INSERT INTO Person VALUES(14, 0, '','', '', NULL, NULL, 'misc', 'Unassigned', 'admin', NULL, 40, 0, TRUE, '', '', '', '', 20, 1);

INSERT INTO Location_Person VALUES(1, 1);
INSERT INTO Location_Person VALUES(1, 2);
INSERT INTO Location_Person VALUES(1, 4);
INSERT INTO Location_Person VALUES(1, 7);
INSERT INTO Location_Person VALUES(1, 8);
INSERT INTO Location_Person VALUES(1, 14);
INSERT INTO Location_Person VALUES(2, 1);
INSERT INTO Location_Person VALUES(2, 3);
INSERT INTO Location_Person VALUES(2, 5);
INSERT INTO Location_Person VALUES(2, 6);
INSERT INTO Location_Person VALUES(2, 9);
INSERT INTO Location_Person VALUES(2, 14);


INSERT INTO Person_Position VALUES(2, 1);
INSERT INTO Person_Position VALUES(2, 23);
INSERT INTO Person_Position VALUES(4, 2);
INSERT INTO Person_Position VALUES(4, 23);
INSERT INTO Person_Position VALUES(7, 4);
INSERT INTO Person_Position VALUES(7, 23);
INSERT INTO Person_Position VALUES(8, 2);
INSERT INTO Person_Position VALUES(8, 23);
INSERT INTO Person_Position VALUES(1, 1);
INSERT INTO Person_Position VALUES(1, 23);
INSERT INTO Person_Position VALUES(1, 23);
INSERT INTO Person_Position VALUES(3, 1);
INSERT INTO Person_Position VALUES(3, 23);
INSERT INTO Person_Position VALUES(5, 2);
INSERT INTO Person_Position VALUES(5, 23);
INSERT INTO Person_Position VALUES(6, 4);
INSERT INTO Person_Position VALUES(6, 23);
INSERT INTO Person_Position VALUES(9, 3);
INSERT INTO Person_Position VALUES(9, 23);
INSERT INTO Person_Position VALUES(14, 1);
INSERT INTO Person_Position VALUES(14, 2);
INSERT INTO Person_Position VALUES(14, 3);
INSERT INTO Person_Position VALUES(14, 4);
INSERT INTO Person_Position VALUES(14, 7);
INSERT INTO Person_Position VALUES(14, 8);
INSERT INTO Person_Position VALUES(14, 9);
INSERT INTO Person_Position VALUES(14, 10);
INSERT INTO Person_Position VALUES(14, 23);

