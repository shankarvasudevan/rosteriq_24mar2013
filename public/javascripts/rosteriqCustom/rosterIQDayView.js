$("#dayPrev").click(function() {
    $("#loadWait").modal('show');
    var currentDate = getCalDate();
    //We'll then remove all the event handlers for the current cells and shifts
    removeTableEventHandlers();
    setCalDate( currentDate.getTime() - (86400000) );
    //Now that we have the set the date, we have to also to get the week start date for the same
    setStartDate(getPrevMonday(getCalDate()));
    generateScheduler();
    $("#loadWait").modal('hide');
});

$("#dayCurr").click(function() {
    $("#loadWait").modal('show');
    var newDate = new Date();
    setCalDate(newDate.getTime());
    setStartDate ( getPrevMonday(getCalDate()).getTime() );
    //We'll then remove all the event handlers for the current cells and shifts
    removeTableEventHandlers();
    generateScheduler();
    $("#loadWait").modal('hide');
});

$("#dayNext").click(function() {
    $("#loadWait").modal('show');
    var currentDate = getCalDate();
    //We'll then remove all the event handlers for the current cells and shifts
    removeTableEventHandlers();
    setCalDate(currentDate.getTime() + (86400000));
    setStartDate(getPrevMonday(getCalDate()));
    generateScheduler();
    $("#loadWait").modal('hide');
});

$(document).on("click","#dayWeekViewClick",function(){
    if (!($(this).hasClass("active")) )
    {
        setDayOrWeekView("week");
        $("#weekWeekViewClick").addClass("active");
        $("#weekDayViewClick").removeClass("active");
        //Then we'll decide the view depending on the schedulerView click
        if ( getSchedulerView() == "position")
        {
            //We'll first see if the tab is already set, because in that case we won't have to do anything
            if ( ! ( $("#weekPositionViewClick").hasClass("active") ) )
            {
                renderWeekPositionView();
                //Then we have to manually set the classes for the actual views
                $("#weekPositionView").addClass("in active");
                $("#weekEmployeeView").removeClass("in active");
            }
            else
            {
                //Everything is already set correctly
                setSchedulerView("position");
            }
        }
        else
        {
            //If the schedulerView is EmployeeView
            //We'll first see if the tab is already set, because in that case we won't have to do anything
            if ( ! ( $("#weekEmployeeViewClick").hasClass("active") ) )
            {
                renderWeekEmployeeView();
                //Then we have to manually set the classes for the actual views
                $("#weekEmployeeView").addClass("in active");
                $("#weekPositionView").removeClass("in active");
            }
            else
            {
                //Everything is already set correctly
                setSchedulerView("employee");
            }
        }
    }
    renderShiftsAndUnavailabilitiesForView()
})

function renderDayPositionView()
{
    if (!($("#dayPositionViewClick").hasClass("active")) )
    {
        $("#dayPositionViewClick").addClass("active");
        schedulerView = "position";
        $("#dayEmployeeViewClick").removeClass("active");
    }
    renderShiftsAndUnavailabilitiesForView()
}

function renderDayEmployeeView()
{
    if (!($("#dayEmployeeViewClick").hasClass("active")) )
    {
        $("#dayEmployeeViewClick").addClass("active");
        schedulerView = "employee";
        $("#dayPositionViewClick").removeClass("active");
    }
    renderShiftsAndUnavailabilitiesForView()
}

/*---------------------------------------------------------------------------
RENDER UNAVAILABILITIES TO THE DAY VIEW
---------------------------------------------------------------------------*/

//Here we will write the functions that draw the unavailabilities in the scheduler
function renderEmployeeDayUnavailabilities(unavailabilities)
{
    //Now this function will render the unavailabilities in the view
    $.each(unavailabilities,function(i,unavailability){
        var currentUnavailability = unavailability
        $.each($("#employeeDayTable tbody").children(),function(i,employeeRow){
            if (currentUnavailability.personId == getEmployeeId($(employeeRow))){
                //We'll get the width of a one hour cell
                var cellWidth = $("#eh1").position().left - $("#eh0").position().left;
                //Get the hours and the minutes for the start time and then we'll just add width to the object and then add it to the row
                //First we'll do the all day check
                if( currentUnavailability.startTime == "00:00" && currentUnavailability.endTime == "23:59")
                {
                    var unavailabilitySection = $("<div>").addClass("colour-grey unavailabilitySection")
                    unavailabilitySection.attr('id','u'+currentUnavailability.id)
                    //Get the positions to calculate the width ----- This is because width:100% wasn't working properly
                    var leftPosition = $("#eh0").position().left
                    var rightPosition = $("#eh23").position().left + parseInt(cellWidth);
                    var width = rightPosition - leftPosition;
                    var cssObj = {
                        'position':'absolute',
                        'width':(width-1)+'px',
                        'margin-bottom':'0px',
                        'text-align':'center',
                        'padding':'15px 0px 15px 0px'
                    }
                    unavailabilitySection.css(cssObj)
                    unavailabilitySection.html("<b>Unavailable</b> "+currentUnavailability.title)
                    $(this).find(".employeeDayShiftCell").append(unavailabilitySection)
                }
                else if ( !(currentUnavailability.startTime == "00:00" && currentUnavailability.endTime == "00:00")){
                    var unavailabilitySection = $("<div>").addClass("colour-grey unavailabilitySection")
                    unavailabilitySection.attr('id','u'+currentUnavailability.id)
                    //Get the positions to calculate the width ----- This is because width:100% wasn't working properly
                    var startTimeHours = parseInt((unavailability.startTime).split(":")[0],10);
                    var startTimeMinutes = parseInt((unavailability.startTime).split(":")[1]);
                    var endTimeHours = parseInt((unavailability.endTime).split(":")[0]);
                    var endTimeMinutes = parseInt((unavailability.endTime).split(":")[1]);
                    var leftPosition = $("#eh"+startTimeHours).position().left + parseInt(cellWidth*(startTimeMinutes/60));
                    if ( endTimeHours == 24)
                        var rightPosition = $("#eh"+(endTimeHours-1)).position().left + parseInt(cellWidth);
                    else
                        var rightPosition = $("#eh"+(endTimeHours)).position().left + parseInt(cellWidth*(endTimeMinutes/60));
                    var width = rightPosition - leftPosition;
                    var cssObj = {
                        'position':'absolute',
                        'width':(width-1)+'px',
                        'left':(leftPosition-$("#eh0").position().left)+'px',
                        'margin-bottom':'0px',
                        'text-align':'center',
                        'padding':'15px 0px 15px 0px'
                    }
                    unavailabilitySection.css(cssObj)
                    unavailabilitySection.html("<b>Unavailable</b> "+currentUnavailability.title)
                    $(this).find(".employeeDayShiftCell").append(unavailabilitySection)
                }
                $(".employeeDayShiftCell").height(parseInt($(".unavailabilitySection").css('padding-top').replace("px",""))+parseInt($(".unavailabilitySection").css('padding-bottom').replace("px","")) + parseInt($(".unavailabilitySection").height()))
            }
        })
    })
    //Set the boolean
    setEmployeeDayUnavailabilitiesSet()
}

/*---------------------------------------------------------------------------
END RENDER UNAVAILABILITIES TO THE DAY VIEW
---------------------------------------------------------------------------*/

$(document).on("click","#dayPositionViewClick",function(){
   renderDayPositionView();
});

$(document).on("click","#dayEmployeeViewClick",function(){
    renderDayEmployeeView();
});

function createDaySummary()
{
    var theadSummary = $("<thead>");
    var summaryHeading = $("<th style=\"width:10%\">").text("Hours").appendTo(theadSummary);
    for (i=0; i!=24; ++i)
    {
        $("<th style=\"width:3.75%\">").appendTo(theadSummary);
    }
    $("#dayViewSummary").append(theadSummary);
}

function createDayEmployees(d,employees)
{
    //We'll first create the table heads
    var thead = $("<thead>");
    var employeeName = $("<th style=\"width:10%\">").text("Person").appendTo(thead);
    for (i=0; i!=24; ++i)
    {
        ext = (parseInt(i/12)>0?"p":"a")
        numberDisplay = ((i/12)>0?((i%12)==0?12:(i%12)):((i%12)==0?12:(i%12)))
        $("<th id=\"eh"+i+"\" style=\"width:3.75%\">").text(numberDisplay+ext).appendTo(thead);
    }
    $("#dayEmployeeViewHeader").append(thead);
    //We will iterate through the position JSON array
    $("#employeeDayTable tbody").remove();
    var employeeDayTableBody  = $("<tbody>");
    employeeDayTableBody.appendTo($("#employeeDayTable"));
    $.each(employees,function(i,employee){
        var employeeRow = $("<tr>");
        var employeeId = $("<td class=\"hidden employeeId\">").text(employee.id).appendTo(employeeRow);
        var month = d.getMonth();
        var date = $("<td class=\"hidden date\">").text(d.getDate() + "-" + month + "-" + d.getFullYear()).appendTo(employeeRow);
        var employeeName = $("<td style=\"width:10%\" style=\"width:10%\" class=\"employeeName\">").text(employee.firstName).appendTo(employeeRow);
        var employeeDayShiftCell = $("<td style=\"width:90%\" class=\"employeeDayShiftCell\">").appendTo(employeeRow);
        employeeRow.appendTo(employeeDayTableBody);
    });
}

//Position View
function createDayPositions(d,positions)
{
     var thead = $("<thead>");
     var employeeName = $("<th style=\"width:10%\">").text("Position").appendTo(thead);
     for (i=0; i!=24; ++i)
     {
        ext = (parseInt(i/12)>0?"p":"a")
        numberDisplay = ((i/12)>0?((i%12)==0?12:(i%12)):((i%12)==0?12:(i%12)))
        $("<th id=\"ph"+i+"\" style=\"width:3.75%\">").text(numberDisplay+ext).appendTo(thead);
     }
     $("#dayPositionViewHeader").append(thead);
    //We will iterate through the position JSON array
    $("#positionDayTable tbody").remove();
    var positionDayTableBody  = $("<tbody>");
    positionDayTableBody.appendTo($("#positionDayTable"));
    $.each(positions,function(i,position){
        var positionRow = $("<tr>");
        var positionId = $("<td class=\"hidden positionId\">").text(position.id).appendTo(positionRow);
        var month = d.getMonth();
        var date = $("<td class=\"hidden date\">").text(d.getDate() + "-" + month + "-" + d.getFullYear()).appendTo(positionRow);
        var positionTitle = $("<td style=\"width:10%\" class=\"positionName\">").text(position.positionTitle).appendTo(positionRow);
        var positionDayShiftCell = $("<td class=\"positionDayShiftCell\">").appendTo(positionRow)
        positionRow.appendTo(positionDayTableBody);
    });
}

/*---------------------------------------------------------------------------
RENDER SHIFTS TO THE DAY VIEW
---------------------------------------------------------------------------*/

function renderEmployeeDayShifts(shifts){
    $.each(shifts,function(i,shift){
        $.each($("#employeeDayTable tbody").children(),function(i, employeeRow){
            if (shift.personId == getEmployeeId($(employeeRow))){
                var cellWidth = $("#eh1").position().left - $("#eh0").position().left;
                //Get the hours and the minutes for the start time and then we'll just add width to the object and then add it to the row
                //First we'll do the all day check
                if( ((shift.start).split(" "))[1] == "00:00" && ((shift.finish).split(" "))[1] == "23:59")
                {
                    var shiftSection = $("<div>").addClass("btn-primary employeeDayShift")
                    shiftSection.attr('id','s'+shift.id)
                    //Get the positions to calculate the width ----- This is because width:100% wasn't working properly
                    var leftPosition = $("#eh0").position().left
                    var rightPosition = $("#eh23").position().left + parseInt(cellWidth);
                    var width = rightPosition - leftPosition;
                    var cssObj = {
                        'position':'absolute',
                        'width':(width-1)+'px',
                        'margin-bottom':'0px',
                        'text-align':'center',
                        'padding':'15px 0px 15px 0px'
                    }
                    shiftSection.css(cssObj)
                    shiftSection.html("<b>"+shift.positionTitle+"</b>"+"<p>"+getTimeRangeOption((shift.start).split(" ")[1]+"-"+(shift.finish).split(" ")[1],getTimeRangeOptionValues(),getTimeRangeOptions()+"</p>"))
                    $(this).find(".employeeDayShiftCell").append(shiftSection)
                }
                else if ( !(((shift.start).split(" "))[1] == "00:00" && ((shift.start).split(" "))[1] == "00:00")){
                    var shiftSection = $("<div>").addClass("btn-primary employeeDayShift")
                    shiftSection.attr('id','s'+shift.id)
                    //Get the positions to calculate the width ----- This is because width:100% wasn't working properly
                    var startTimeHours = parseInt(((shift.start).split(" ")[1]).split(":")[0],10);
                    var startTimeMinutes = parseInt((shift.start).split(" ")[1].split(":")[1]);
                    var endTimeHours = parseInt((shift.finish).split(" ")[1].split(":")[0]);
                    var endTimeMinutes = parseInt((shift.finish).split(" ")[1].split(":")[1]);
                    var leftPosition = $("#eh"+startTimeHours).position().left + parseInt(cellWidth*(startTimeMinutes/60));
                    if ( endTimeHours == 24)
                        var rightPosition = $("#eh"+(endTimeHours-1)).position().left + parseInt(cellWidth);
                    else
                        var rightPosition = $("#eh"+(endTimeHours)).position().left + parseInt(cellWidth*(endTimeMinutes/60));
                    var width = rightPosition - leftPosition;
                    var cssObj = {
                        'position':'absolute',
                        'width':(width-1)+'px',
                        'left':(leftPosition-$("#eh0").position().left)+'px',
                        'margin-bottom':'0px',
                        'text-align':'center',
                        'padding':'15px 0px 15px 0px'
                    }
                    shiftSection.css(cssObj)
                    shiftSection.html("<b>"+shift.positionTitle+"</b> "+getTimeRangeOption((shift.start).split(" ")[1]+"-"+(shift.finish).split(" ")[1],getTimeRangeOptionValues(),getTimeRangeOptions()))
                    $(this).find(".employeeDayShiftCell").append(shiftSection)
                }
                $(".employeeDayShiftCell").height(parseInt($(".employeeDayShift").css('padding-top').replace("px",""))+parseInt($(".employeeDayShift").css('padding-bottom').replace("px","")) + parseInt($(".employeeDayShift").height()))
            }
        })
    })
    //Once the operation is done, we shall set the boolean
    setEmployeeDayShiftsSet()
}

function renderPositionDayShifts(shifts){
    $.each(shifts,function(i,shift){
        $("#positionDayTable tbody tr").each(function(){
            if (shift.positionId == getPositionId($(this))){
                var cellWidth = $("#ph1").position().left - $("#ph0").position().left;
                //Get the hours and the minutes for the start time and then we'll just add width to the object and then add it to the row
                //First we'll do the all day check
                if( ((shift.start).split(" "))[1] == "00:00" && ((shift.finish).split(" "))[1] == "23:59")
                {
                    var shiftSection = $("<div>").addClass("btn-primary positionDayShift")
                    shiftSection.attr('id','s'+shift.id)
                    //Get the positions to calculate the width ----- This is because width:100% wasn't working properly
                    var leftPosition = $("#ph0").position().left
                    var rightPosition = $("#ph23").position().left + parseInt(cellWidth);
                    var width = rightPosition - leftPosition;
                    var cssObj = {
                        'position':'absolute',
                        'width':(width-1)+'px',
                        'margin-bottom':'0px',
                        'text-align':'center',
                        'padding':'15px 0px 15px 0px'
                    }
                    shiftSection.css(cssObj)
                    shiftSection.html("<b>"+shift.positionTitle+"</b>"+"<p>"+getTimeRangeOption((shift.start).split(" ")[1]+"-"+(shift.finish).split(" ")[1],getTimeRangeOptionValues(),getTimeRangeOptions()+"</p>"))
                    $(this).find(".positionDayShiftCell").append(shiftSection)
                }
                else if ( !(((shift.start).split(" "))[1] == "00:00" && ((shift.start).split(" "))[1] == "00:00")){
                    var shiftSection = $("<div>").addClass("btn-primary positionDayShift")
                    shiftSection.attr('id','s'+shift.id)
                    //Get the positions to calculate the width ----- This is because width:100% wasn't working properly
                    var startTimeHours = parseInt(((shift.start).split(" ")[1]).split(":")[0],10);
                    var startTimeMinutes = parseInt((shift.start).split(" ")[1].split(":")[1]);
                    var endTimeHours = parseInt((shift.finish).split(" ")[1].split(":")[0]);
                    var endTimeMinutes = parseInt((shift.finish).split(" ")[1].split(":")[1]);
                    var leftPosition = $("#ph"+startTimeHours).position().left + parseInt(cellWidth*(startTimeMinutes/60));
                    if ( endTimeHours == 24)
                        var rightPosition = $("#ph"+(endTimeHours-1)).position().left + parseInt(cellWidth);
                    else
                        var rightPosition = $("#ph"+(endTimeHours)).position().left + parseInt(cellWidth*(endTimeMinutes/60));
                    var width = rightPosition - leftPosition;
                    var cssObj = {
                        'position':'absolute',
                        'width':(width-1)+'px',
                        'left':(leftPosition-$("#ph0").position().left)+'px',
                        'margin-bottom':'0px',
                        'text-align':'center',
                        'padding':'15px 0px 15px 0px'
                    }
                    shiftSection.css(cssObj)
                    shiftSection.html("<b>"+shift.positionTitle+"</b> "+getTimeRangeOption((shift.start).split(" ")[1]+"-"+(shift.finish).split(" ")[1],getTimeRangeOptionValues(),getTimeRangeOptions()))
                    $(this).find(".positionDayShiftCell").append(shiftSection)
                }
                $(".positionDayShiftCell").height(parseInt($(".positionDayShift").css('padding-top').replace("px",""))+parseInt($(".positionDayShift").css('padding-bottom').replace("px","")) + parseInt($(".positionDayShift").height()))
            }
        })
    })
    //Set the boolean
    setPositionDayShiftsSet()
}

/*---------------------------------------------------------------------------
END RENDER SHIFTS TO THE DAY VIEW
---------------------------------------------------------------------------*/

function checkUnavailabilityAndDayShiftConflict(startTime, finishTime, unavailabilityRow, shiftSlider)
{
    //Then we'll check for an unavailability conflict on creation
    var cellWidth = $("#h1").position().left - $("#h0").position().left;
    var startTimeHours = parseInt((convertToHours(startTime)).split(":")[0],10);
    var startTimeMinutes = parseInt((convertToHours(startTime)).split(":")[1]);
    var finishTimeHours = parseInt((convertToHours(finishTime)).split(":")[0],10);
    var finishTimeMinutes = parseInt((convertToHours(finishTime)).split(":")[1]);
    var shiftStart = $("#h"+startTimeHours).position().left + parseInt(cellWidth*(startTimeMinutes/60));
    if ( finishTimeHours == 24)
        var shiftFinish = $("#h"+(finishTimeHours-1)).position().left + parseInt(cellWidth);
    else
        var shiftFinish = $("#h"+finishTimeHours).position().left + parseInt(cellWidth*(finishTimeMinutes/60));
    unavailabilityRow.children().each(function(){
        //So we'll go through each unavailability in the row and check for a conflict
        //Here we'll set up the unavailability range
        var unavailabilityStart = parseInt($(this).css("left"));
        var unavailabilityFinish = (parseInt($(this).css("left"))+parseInt($(this).css("width")));
        //So the conflict is basically going to be a long if statement like so
        if ( (shiftStart >= unavailabilityStart && shiftFinish <= unavailabilityFinish) || (shiftStart >= unavailabilityStart && shiftFinish >= unavailabilityFinish && shiftStart < unavailabilityFinish) || (shiftStart <= unavailabilityStart && shiftFinish >= unavailabilityFinish) || (shiftStart <= unavailabilityStart && shiftFinish <= unavailabilityFinish && shiftFinish > unavailabilityStart) )
        {
            //This means there is an unavailability conflict
            if(shiftSlider.children().eq(1).hasClass("btn-primary")) {
                shiftSlider.children().eq(1).removeClass("btn-primary");
                shiftSlider.children().eq(1).addClass("btn-danger");
            }
        }
        else
        {
            //This means there is an unavailability conflict
            if(shiftSlider.children().eq(1).hasClass("btn-danger")) {
                shiftSlider.children().eq(1).removeClass("btn-danger");
                shiftSlider.children().eq(1).addClass("btn-primary");
            }
        }
    })
}

function resizeUnavailabilities(unavailabilities)
{
    //This will readjust the unavailabilities with the new table co-ordinates
    $.each(unavailabilities,function(i,unavailability){
        var cellWidth = $("#eh1").position().left - $("#eh0").position().left;
        //Get the hours and the minutes for the start time and then we'll just add width to the object and then add it to the row
        //First we'll do the all day check
        if( unavailability.startTime == "00:00" && unavailability.endTime == "23:59")
        {
            var unavailabilitySection = $("#u"+unavailability.id);
            //Get the positions to calculate the width ----- This is because width:100% wasn't working properly
            var leftPosition = $("#eh0").position().left
            var rightPosition = $("#eh23").position().left + parseInt(cellWidth);
            var width = rightPosition - leftPosition;
            var cssObj = {
                'position':'absolute',
                'width':(width-1)+'px',
                'margin-bottom':'0px',
                'text-align':'center',
                'padding':'15px 0px 15px 0px'
            }
            $(unavailabilitySection).css(cssObj);
        }
        else if ( !(unavailability.startTime == "00:00" && unavailability.endTime == "00:00")){
            var startTimeHours = parseInt((unavailability.startTime).split(":")[0],10);
            var startTimeMinutes = parseInt((unavailability.startTime).split(":")[1]);
            var endTimeHours = parseInt((unavailability.endTime).split(":")[0]);
            var endTimeMinutes = parseInt((unavailability.endTime).split(":")[1]);
            var unavailabilitySection = $("#u"+unavailability.id);
            var leftPosition = $("#eh"+startTimeHours).position().left + parseInt(cellWidth*(startTimeMinutes/60));
            if ( endTimeHours == 24)
                var rightPosition = $("#eh"+(endTimeHours-1)).position().left + parseInt(cellWidth);
             else
                 var rightPosition = $("#eh"+(endTimeHours)).position().left + parseInt(cellWidth*(endTimeMinutes/60));
            var width = rightPosition - leftPosition;
            var cssObj = {
                'position':'absolute',
                'width':(width-1)+'px',
                'left':(leftPosition-$("#eh0").position().left)+'px',
                'margin-bottom':'0px',
                'text-align':'center',
                'padding':'15px 0px 15px 0px'
            }
           $(unavailabilitySection).css(cssObj);
        }
    })
}
