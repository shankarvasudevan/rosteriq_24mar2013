
/*function setDobFieldsInEditBox() {
    var daySelect = $("#empDobDay");
    var monthSelect = $("#empDobMonth");
    var yearSelect = $("#empDobYear");
    var i;
    for (i=1 ; i <= 31; i++) {
        var option = $("<option class=\"empDobDD\" value=\""+i+">").text(i).appendTo(daySelect);
    }
    var i;
    for (i=1 ; i <= 12; i++) {
        var option = $("<option class=\"empDobMM\" value=\""+i+">").text(i).appendTo(monthSelect);
    }
    var i;
    for (i=1900 ; i <= 2012; i++) {
        var option = $("<option class=\"empDobYYYY\" value=\""+i+">").text(i).appendTo(yearSelect);
    }
} */

function getEmployeesForCompany(companyId) {
    var getEmployeesUrl = "./getEmployeesForCompany.json?companyId="+companyId;
    $.ajax({
        url : getEmployeesUrl,
        async:false,
        dataType:'json',
        success:function(data) {
            $.each(data,function(i,item) {
                var empList = $("#employeesList");
                var empDiv = $("<div class=\"employeeDiv\">");
                empDiv.appendTo(empList);
                $("<a class=\"employeeItem\" id=\""+item.id+"\" href=\"#\">"+item.firstName+" "+item.lastName+"</a>").appendTo(empDiv);
                $("<br>").appendTo(empDiv);
            });
        }
    });
}


function getLocationsForCompany(companyId) {
    var getLocationsUrl = "./getLocationsForCompany.json?companyId="+companyId;
    $.ajax({
        url: getLocationsUrl,
        async:false,
        dataType:'json',
        success:function(data) {
            $.each(data, function(i,item) {
                var locList = $("#locationsList");
                var locDiv = $("<div class=\"locationDiv\">");
                locDiv.appendTo(locList);
                $("<input type=\"hidden\" class=\"locAddress\" value=\""+item.address+"\">").appendTo(locDiv);
                $("<input type=\"hidden\" class=\"locSuburb\" value=\""+item.suburb+"\">").appendTo(locDiv);
                $("<input type=\"hidden\" class=\"locState\" value=\""+item.state+"\">").appendTo(locDiv);
                $("<input type=\"hidden\" class=\"locPostcode\" value=\""+item.postcode+"\">").appendTo(locDiv);
                $("<input type=\"hidden\" class=\"locCountry\" value=\""+item.country+"\">").appendTo(locDiv);
                $("<input type=\"hidden\" class=\"locPhoneNumber\" value=\""+item.phoneNumber+"\">").appendTo(locDiv);
                $("<a class=\"locationItem\" id=\""+item.id+"\" href=\"#\">"+item.locationName+"</a>").appendTo(locDiv);
                $("<br>").appendTo(locDiv);
            });
        }
    });
}

function getPositionsForCompany(companyId) {
    var getPositionsUrl = "./getPositionsForCompany.json?companyId="+companyId;
    $.ajax({
        url: getPositionsUrl,
        async:false,
        dataType:'json',
        success:function(data) {
            $.each(data, function(i,item) {
                var posTbl = $("#positionAdminTableBody");
                var posRow = $("<tr id=\""+item.id+"\">");
                var posColTitle = $("<td>");
                $("<input class=\"posTitle\" type=\"text\" value=\""+item.title+"\">").appendTo(posColTitle);
                posColTitle.appendTo(posRow);
                var posColDesc = $("<td>");
                $("<input class=\"posDesc\" type=\"text\" value=\""+item.description+"\">").appendTo(posColDesc);
                posColDesc.appendTo(posRow);
                var delPosCol = $("<td class=\"removePositionOnAdmin\">");
                var delPosLink = $("<a href=\"#\">").text("-");
                delPosLink.appendTo(delPosCol);
                delPosCol.appendTo(posRow);
                posTbl.prepend(posRow);
            });
        }
    });
}

function clearExistingEmployeeFields() {
    $('#empId').val('-1');
    $('#empFirstName').val('');
    $('#empLastName').val('');
    $('#empEmailAddress').val('');
    $('#empAddress').val('');
    $('#empSuburb').val('');
    $('#empState').val('');
    $('#empPostcode').val('');
    $('#empCountry').val('');
    $('#empPhoneNumber').val('');
    $('#empPassword').val('');
    $('#empAccountType').val('');
    $('#empLocations').val('');
    $('#empPositions').val('');
    $('#empWage').val('');
    $('#empMaxHours').val('');
    $('#empBreakTime').val('');
}

function clearExistingLocationFields() {
    $('#locEditName').val('');
    $('#locEditAddress').val('');
    $('#locEditSuburb').val('');
    $('#locEditState').val('');
    $('#locEditPostcode').val('');
    $('#locEditCountry').val('');
    $('#locEditPhoneNumber').val('');
}


$(document).on("click",".removeLocationOnAdmin",function() {
    $(this).closest('tr').remove();
});

$(document).on("click",".removePositionOnAdmin",function() {
    $(this).closest('tr').remove();
});

$(document).on("click","#addLocationOnAdmin",function() {
    //$('#locationAdminTable tbody>tr:last').clone(true).insertAfter('#locationAdminTable tbody>tr:last');
    //$(this).replaceWith('<td class="removeLocationOnAdmin"><a href="#">-</a></td>');
    $('#locId').val("-1");
    $('#locationEdit').modal('show');
});

$(document).on("click","#addEmployeeOnAdmin", function() {
    clearExistingEmployeeFields();
    $("#empInfoFields").show();
    $("#empPasswordDiv").show();
    $("#empAdminFields").show();
    $("#empCompanyFields").show();
    $("#employeeEditDiv").modal('show');
});

$(document).on("click",".addPositionOnAdmin",function() {
    $('#positionAdminTable tbody>tr:last').clone(true).insertAfter('#positionAdminTable tbody>tr:last');
    $(this).replaceWith('<td class="removePositionOnAdmin"><a href="#">-</a></td>');
});

$(document).on("click", "#updatePositions", function() {
    var posList = [];

    var posRows = $("#positionAdminTable").children().eq(1);//.children();
    posRows.children().each(function(index) {
        var id = $(this).attr('id');
        var title = $(this).children().eq(0).children().eq(0).val();
        var desc = $(this).children().eq(1).children().eq(0).val();

        posList.push({"id" : id, "title" : title, "description" : desc});

    });
    var posListJson = JSON.stringify(posList);
   // data: {positionsJson : posListJson},
    $.ajax({
        type: "POST",
        url: "updatePositions",
        data: {positionsJson : posListJson},
        traditional : true,
        dataType: "json",
        success: function(response) {

        }
    });
});

$(document).on("click", "#editLocationDelete",function() {
    var id = $("#locId").val();
    var locItem = $("#"+id);
    var deleteLocationUrl = "./deleteLocation.json?locationId="+id;
    $.ajax({
        url: deleteLocationUrl,
        async: false,
        dataType: 'json',
        success: function() {
            locItem.parent().remove();
        }
    })
    $("#locationEdit").modal('hide');
});

$(document).on("click", "#editLocationSubmit", function() {
    // get all the document variables
    var companyId = $("#companySelect option:selected").val();
    var id = $("#locId").val();
    var name = $('#locEditName').val();
    var address = $('#locEditAddress').val();
    var suburb = $('#locEditSuburb').val();
    var state = $('#locEditState').val();
    var postCode = $('#locEditPostcode').val();
    var country = $('#locEditCountry').val();
    var phoneNumber = $('#locEditPhoneNumber').val();
    if (id == "-1") {
        var createLocationUrl = "./createLocation.json?companyId="+companyId+"&name="+name+"&address="+address+"&suburb="+suburb+"&state="+state+"&postCode="+postCode+"&country="+country+"&phoneNumber="+phoneNumber;
        $.ajax({
            url: createLocationUrl,
            async: false,
            dataType: 'json',
            success: function(data) {
                // add the newly created location to the list of locations on the Calendar
                var locList = $("#locationsList");
                var locDiv = $("<div class=\"locationDiv\">");
                locDiv.appendTo(locList);
                $("<input type=\"hidden\" class=\"locAddress\" value=\""+data.address+"\">").appendTo(locDiv);
                $("<input type=\"hidden\" class=\"locSuburb\" value=\""+data.suburb+"\">").appendTo(locDiv);
                $("<input type=\"hidden\" class=\"locState\" value=\""+data.state+"\">").appendTo(locDiv);
                $("<input type=\"hidden\" class=\"locPostcode\" value=\""+data.postcode+"\">").appendTo(locDiv);
                $("<input type=\"hidden\" class=\"locCountry\" value=\""+data.country+"\">").appendTo(locDiv);
                $("<input type=\"hidden\" class=\"locPhoneNumber\" value=\""+data.phoneNumber+"\">").appendTo(locDiv);
                $("<a class=\"locationItem \" id=\""+data.id+"\" href=\"#\">"+data.locationName+"</a>").appendTo(locDiv);
                $("<br>").appendTo(locDiv);
            }
        });
    } else {
        var updateLocationUrl = "./updateLocation.json?locationId="+id+"&name="+name+"&address="+address+"&suburb="+suburb+"&state="+state+"&postCode="+postCode+"&country="+country+"&phoneNumber="+phoneNumber;
        $.ajax({
            url: updateLocationUrl,
            async: false,
            dataType: 'json',
            success: function(data) {
                // find the updated location
                var locItem = $("#"+data.id);
                locItem.siblings('.locAddress').val(data.address);
                locItem.siblings('.locSuburb').val(data.suburb);
                locItem.siblings('.locState').val(data.state);
                locItem.siblings('.locPostcode').val(data.postcode);
                locItem.siblings('.locCountry').val(country);
                locItem.siblings('.locPhoneNumber').val(phoneNumber);
                locItem.text(data.locationName);
            }
        });
    }
    $("#locationEdit").modal('hide');
});

$(document).on("click", ".locationItem", function() {
   var locationId = $(this).attr('id');
   var locationName = $(this).text();
   clearExistingLocationFields();
   var address = $(this).siblings('.locAddress').val();
   var suburb = $(this).siblings('.locSuburb').val();
   var state = $(this).siblings('.locState').val();
   var postCode = $(this).siblings('.locPostcode').val();
   var country = $(this).siblings('.locCountry').val();
   var phoneNumber = $(this).siblings('.locPhoneNumber').val();
   $('#locId').val(locationId);
   $('#locEditName').val(locationName);
   if (address != "undefined") $('#locEditAddress').val(address);
   if (suburb != "undefined") $('#locEditSuburb').val(suburb);
   if (state != "undefined") $('#locEditState').val(state);
   if (postCode != "undefined") $('#locEditPostcode').val(postCode);
   if (country != "undefined") $('#locEditCountry').val(country);
   if (phoneNumber != "undefined") $('#locEditPhoneNumber').val(phoneNumber);
   $("#locationEdit").modal('show');
});

$(document).on("click", ".employeeItem", function() {
    // clear previous form data
    clearExistingEmployeeFields();
    // initially hide all the divs
   $("#empInfoFields").hide();
   $("#empPasswordDiv").hide();
   $("#empAdminFields").hide();
   $("#empCompanyFields").hide();
   var employeeId = $(this).attr('id');
   var getEmployeeInfoUrl = "./getEmployeeInfo.json?employeeId="+employeeId;
    // AJAX call to get Employee info
    $.ajax({
        url: getEmployeeInfoUrl,
        async: false,
        dataType: 'json',
        success: function(data) {
            // on success for the data add it to the divs
            $('#empId').val(data.id);
            $('#empFirstName').val(data.firstName);
            $('#empLastName').val(data.lastName);
            $('#empEmailAddress').val(data.emailAddress);
            $('#empAddress').val(data.address);
            $('#empSuburb').val(data.suburb);
            $('#empState').val(data.state);
            $('#empPostcode').val(data.postCode);
            $('#empCountry').val(data.country);
            $('#empPhoneNumber').val(data.contactNumber);
            var empDobDD = $(".empDobDD");
            $.each(empDobDD, function(i, item) {
                if (item.val() == data.dobDD) {
                    item.attr('selected','selected');
                }
            });
            var empDobMM = $(".empDobMM");
            $.each(empDobMM, function(i, item) {
                if (item.val() == data.dobMM) {
                    item.attr('selected','selected');
                }
            });
            var empDobYYYY = $(".empDobYYYY");
            $.each(empDobYYYY, function(i, item) {
                if (item.val() == data.dobYYYY) {
                    item.attr('selected','selected');
                }
            });
            // show the employeeInfo div
            $('#empInfoFields').show();
            if (data.isSelfView == true || data.isAdminView == true) {
                $('#empPassword').val(data.password);
                // show the employee password div
                $('#empPasswordDiv').show();
            }
            if (data.isAdminView == true) {
                $('#empAccountType').val(data.accountType);
                 // show the admin div
                $('#empAdminFields').show();
            }
            if (data.isManagerView == true || data.isAdminView == true) {
                $.each(data.locations,function(i,item) {
                    var locationOptions = $('.empLocationOption');
                    $.each(locationOptions, function(j,locOption) {
                        if (locOption.val() == item.id) {
                            locOption.attr('selected','selected');
                        }
                    });
                });
                $.each(data.positions,function(i,item) {
                    var positionOptions = $('.empPositionOption');
                    $.each(positionOptions, function(j,posOption) {
                        if (posOption.val() == item.id) {
                            posOption.attr('selected','selected');
                        }
                    });
                });
                $("#empWage").val(data.wage);
                $("#empMaxHours").val(data.maxHoursPerWeek);
                $("#empBreakTime").val(data.minHoursBetweenShifts);
                // show the company fields div
                $("#empCompanyFields").show();
            }
            if (data.isManagerView == true) {
                // make the employees private details confidential for the company manager
                $('#empFirstName').attr('readonly','readonly');
                $('#empLastName').attr('readonly','readonly');
                $('#empEmailAddress').attr('readonly','readonly');
                $('#empAddress').attr('readonly','readonly');
                $('#empSuburb').attr('readonly','readonly');
                $('#empState').attr('readonly','readonly');
                $('#empPostcode').attr('readonly','readonly');
                $('#empCountry').attr('readonly','readonly');
                $('#empPhoneNumber').attr('readonly','readonly');
            }

        }
    });
    $("#employeeEdit").modal('show');
});

$("#companySelect").bind("change", function() {
   // if the companyId = -1 then hide the locationAndPersonAdmin else get the location and positions for that company
   var companyId = $("#companySelect option:selected").val();
   if (companyId == -1) {
        $(".locationAndPositionAdmin").hide();
   } else {
        getLocationsForCompany(companyId);
        getPositionsForCompany(companyId);
        getEmployeesForCompany(companyId);
        $(".locationAndPositionAdmin").show();
   }
});

$(document).ready(function(){
    setTimeZoneOffset();
    $(".locationAndPositionAdmin").hide();
   // setDobFieldsInEditBox();
});
