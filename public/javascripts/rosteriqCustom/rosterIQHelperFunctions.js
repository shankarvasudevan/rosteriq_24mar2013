//Global variables
var dayConversion = [
    {"day":"Mon", "dayFull":"Monday", "dayOfWeek":"1"},
    {"day":"Tue", "dayFull":"Tuesday", "dayOfWeek":"2"},
    {"day":"Wed", "dayFull":"Wednesday", "dayOfWeek":"3"},
    {"day":"Thu", "dayFull":"Thursday", "dayOfWeek":"4"},
    {"day":"Fri", "dayFull":"Friday", "dayOfWeek":"5"},
    {"day":"Sat", "dayFull":"Saturday", "dayOfWeek":"6"},
    {"day":"Sun", "dayFull":"Sunday", "dayOfWeek":"7"}
];

var options = generateTimeRangeOptions()[0]
var optionValues = generateTimeRangeOptions()[1]

function getTimeRangeOptions(){
    return options
}

function getTimeRangeOptionValues()
{
    return optionValues
}

//This will return the date in the following format: Thursday, 16 August 2012
//Input: Thu Aug 16 2012
function setDayViewDate(d)
{
    //We'll first format this date to display nicely
    var dateParticulars = new Array();
    dateParticulars = (d.toDateString()).split(" ");
    dateParticulars[0] = convertDayToFullName ( dateParticulars[0]);
    dateParticulars[1] = convertMonthToFullName ( dateParticulars[1]);
    var month = d.getMonth();
    $("#dayViewHiddenDate").html(d.getDate() + "-" + month + "-" + d.getFullYear());
    $("#dayViewDisplayDate").html(dateParticulars[0] + ", " + dateParticulars[2] + " " + dateParticulars[1] + " " + dateParticulars[3] );
}

//This will return the date in the following format: 13 - 19 August, 2012 or 27 August - 02 September, 2012
//Input: Same as above
function setWeekViewDate(startDate)
{
    //We'll first format this date to display nicely
    var endDate = new Date();
    endDate.setTime(startDate.getTime()+ (86400000*6));
    startDateParticulars = (startDate.toDateString()).split(" ");
    endDateParticulars = (endDate.toDateString()).split(" ");
    if ( startDate.getMonth() == endDate.getMonth() )
    {
        $("#weekViewDate").html(startDateParticulars[2] + " - " + endDateParticulars[2] + " " + convertMonthToFullName( startDateParticulars[1] ) + ", " + startDateParticulars[3] );
    }
    else
    {
        $("#weekViewDate").html(startDateParticulars[2] + " " +  convertMonthToFullName ( startDateParticulars[1] ) +  " - " +  endDateParticulars[2] + " " + convertMonthToFullName ( endDateParticulars[1] ) +  ", " + endDateParticulars[3] );
    }
}

//This will return a date in the following format: 09 August 2012
//Input: 09-7-2012 ( This is because the months of 0 based )
function getSimpleDate(d)
{
    dateParticulars = d.split("-");
    dateParticulars[1] = getMonthNameFull( parseInt(dateParticulars[1]));
    return dateParticulars[0] + " " + dateParticulars[1] + " " + dateParticulars[2];
}

function getMonthNameFull(month)
{
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return months[month];
}

function getMonthName(month)
{
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    return months[month];
}

function getSwapDisplay(dateComponents, startTime, finishTime, personName, dayOfWeek)
{
    if(dayOfWeek == 1)
        dayOfWeek = 7;
    else
        dayOfWeek -= 1;
    return convertDayNumberToDayName(dayOfWeek) + " " + dateComponents[0] + " " + getMonthName(dateComponents[1]) + ", "  + startTime + " to " + finishTime + " ( " + personName + " )";
}

function convertToSwapDate(dateComponents, startTime, finishTime,dayOfWeek)
{
    if(dayOfWeek == 1)
        dayOfWeek = 7;
    else
        dayOfWeek -= 1;
    return convertDayNumberToDayName(dayOfWeek) + " " + dateComponents[0] + " " + getMonthName(dateComponents[1]) + ", "  + startTime + " to " + finishTime;
}

function convertToRange(startDate,endDate)
{
    var startDateParticulars = startDate.split("-");
    var endDateParticulars = endDate.split("-");
    var startDisplayMonth = getMonthNameFull(parseInt(startDateParticulars[1]));
    var endDisplayMonth = getMonthNameFull(parseInt(endDateParticulars[1]));
    if ( parseInt(startDateParticulars[1]) == parseInt(endDateParticulars[1]) && parseInt(startDateParticulars[2]) == parseInt(endDateParticulars[2])  )  //If the months are the same and years are not the same
    {
        return startDateParticulars[0] + " to " + endDateParticulars[0] + ", " + getMonthNameFull(parseInt(startDateParticulars[1])) + ", " + startDateParticulars[2];
    }
    else if ( parseInt(startDateParticulars[2]) == parseInt(endDateParticulars[2]) && parseInt(startDateParticulars[1]) != parseInt(endDateParticulars[1])  ) //If the years are the same and months are not the same
    {
        return startDateParticulars[0] + " " + getMonthNameFull(parseInt(startDateParticulars[1])) +  " to " + endDateParticulars[0] + " " +  getMonthNameFull(parseInt(endDateParticulars[1])) +  ", " + startDateParticulars[2];
    }
    else
    {
        return startDateParticulars[0] + " " + getMonthNameFull(parseInt(startDateParticulars[1])) + ", " + startDateParticulars[2] + " to " + endDateParticulars[0] + " " +  getMonthNameFull(parseInt(endDateParticulars[1])) + ", " + endDateParticulars[2];
    }
}

function convertMonthToFullName(month)
{
    var monthConversion = [
        {"month":"Jan", "monthFull":"January"},
        {"month":"Feb", "monthFull":"February"},
        {"month":"Mar", "monthFull":"March"},
        {"month":"Apr", "monthFull":"April"},
        {"month":"May", "monthFull":"May"},
        {"month":"Jun", "monthFull":"June"},
        {"month":"Jul", "monthFull":"July"},
        {"month":"Aug", "monthFull":"August"},
        {"month":"Sep", "monthFull":"September"},
        {"month":"Oct", "monthFull":"October"},
        {"month":"Nov", "monthFull":"November"},
        {"month":"Dec", "monthFull":"December"}
    ];
    counter=0; //Traverse through the JSON object
    while ( counter != 12 ) //There are always going to be 7 days in a week. Will have to change this later ( to use length of the object )
    {
        if ( month == monthConversion[counter].month )
        {
            return monthConversion[counter].monthFull;
        }
        ++counter;
    }

}

function convertToHours(minutes)
{
    var hrs = strPad(Math.floor(minutes/60),2);
    var mins = strPad((minutes%60),2);
    return hrs+":"+mins
}

function strPad(i,l,s) {
    var o = i.toString();
    if (!s) { s = '0'; }
    while (o.length < l) {
        o = s + o;
    }
    return o;
};

function convertToValue(time)
{
    var hrs = (time.split(":")[0]*60);
    var mins = time.split(":")[1];
    return (parseInt(hrs)+parseInt(mins));
}

function getEmployeeId ( employeeRow )
{
     if ( employeeRow )
        return employeeRow.children().eq(0).html();
}

function getPositionId ( positionRow )
{
     if ( positionRow )
        return positionRow.children().eq(0).html();
}

function getShiftId ( button ) //From myRoster.js
{
    return button.parent().parent().prev().find(".shiftId").html();
}

function getMyRosterShiftId (button)
{
    return (button.parent().parent().parent().attr('id')).substring(1);
}

function getMyShiftId( button )     //For swap shift confirmation
{
    return button.parent().parent().parent().children().eq(0).find(".fromShiftId").html();
}

function getRequestId( button )  // For shift swap requests
{
    return button.parent().parent().parent().attr('id');
}

function getSwapShiftId( button )     //For swap shift confirmation
{
    return button.parent().parent().parent().children().eq(0).find(".toShiftId").html();
}

function getDay(row,day)
{
    dayIndex = day + 1;
    return row.children().eq(dayIndex);
}

function getPrevMonday(date) {      //Gets a javascript date object as input
    var d = new Date();
    d.setTime(date.getTime());
    var day = d.getDay();
    d.setTime(d.getTime() - (day*86400000));
    return d;
}

function convertMonthNameToMonthNumber(monthName)
{
    var months =  ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    var monthsFull =  ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    i=0;
    while ( i!=months.length )
    {
        if ( monthName == months[i] || monthName == monthsFull[i])
            return i;
        ++i;
    }
}

function getDateSuffix(n) {
    if (n != 11 && (n + '').match(/1$/)) {
        return "st";
    } else if (n != 12 && (n + '').match(/2$/)) {
        return "nd";
    } else if (n != 13 && (n + '').match(/3$/)) {
        return "rd";
    } else {
        return "th";
    }
}

function displayDate(startDate,i) {
    var next = startDate.getDate() + i;
    var nextDate = new Date();
    nextDate.setDate(next);
    return nextDate.toDateString();
}

function createDateSimpleFormat(date)
{
    //In this function we are going to split the standard Date string and convert it into a very simple string that we require

    var testDateString = date.toDateString();
    var dateParticulars = testDateString.split(" ",4);
    //Now we have the date particulars in the form of example Mon,23,Jul,2012
    var dateString = dateParticulars[2]+"-"+convertMonthNameToMonthNumber((dateParticulars[1]))+"-"+dateParticulars[3];    //Month is zero based
    //And we will return those in the form of a string
    return dateString;
}

function compareDates(currentDayViewDate, shiftDate)
{
    var currentDayViewDateComponents = currentDayViewDate.split("-");
    var shiftDateComponents = (shiftDate.split(" ")[0]).split("-");
    if ( currentDayViewDateComponents[0] == shiftDateComponents[0] && currentDayViewDateComponents[1] == shiftDateComponents[1] && currentDayViewDateComponents[2] == shiftDateComponents[2])
    {
        return true;
    }
    else
        return false;
}

function convertToHours(minutes)
{
    var hrs = strPad(Math.floor(minutes/60),2);
    var mins = strPad((minutes%60),2);
    return hrs+":"+mins
}

function convertToValue(time)
{
    var hrs = (time.split(":")[0]*60);
    var mins = time.split(":")[1];
    return (parseInt(hrs)+parseInt(mins));
}

function strPad(i,l,s) {
    var o = i.toString();
    if (!s) { s = '0'; }
    while (o.length < l) {
        o = s + o;
    }
    return o;
};

function convertDayToFullName(day)
{
    counter=0; //Traverse through the JSON object
    while ( counter != 7 ) //There are always going to be 7 days in a week. Will have to change this later ( to use length of the object )
    {
        if ( day == dayConversion[counter].day )
        {
            return dayConversion[counter].dayFull;
        }
        ++counter;
    }

}

function convertDayNumberToDayName(day)
{
    counter=0; //Traverse through the JSON object
    while ( counter != 7 ) //There are always going to be 7 days in a week. Will have to change this later ( to use length of the object )
    {
        if ( day == dayConversion[counter].dayOfWeek )
        {
            return dayConversion[counter].day;
        }
        ++counter;
    }
}

function setTimeZoneOffset()
{
    var d = new Date();
    timeZoneOffset = d.getTimezoneOffset();

    var setTimeZoneOffsetUrl = "./setTimeZoneOffset.json?timeZoneOffset="+timeZoneOffset;
    $.ajax({
        url : setTimeZoneOffsetUrl,
        async : false,
        dataType:'json',
        success:function() {
        }
    });

}

function validateShiftTime(shiftTime, options, optionValues){
    var returnValue=[false,""]
    $.each(options,function(i,option){
        if(shiftTime.toString() == option.toString()){
            returnValue = [true,optionValues[i]]
        }
    })
    return returnValue
}

function getTimeRangeOption(shiftTime, optionValues, options){
    var returnValue=""
    $.each(optionValues,function(i,optionValue){
        if(shiftTime.toString() == optionValue.toString()){
            returnValue = options[i]
        }
    })
    return returnValue
}

function generateTimeRangeOptions(){
    var options = []
    var optionValues = []
    var numbers = ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23"]
    var subNumbers = ["0","15","30","45"]
    $.each(numbers, function(i,number){
        $.each(subNumbers,function(i,subNumber){
            $.each(numbers,function(i,secondNumber){
                $.each(subNumbers,function(i,secondNumberSubNumber){
                    if(!(parseInt(number)>parseInt(secondNumber))){
                        if(!(parseInt(subNumber)>=parseInt(secondNumberSubNumber) && parseInt(number)==parseInt(secondNumber))){
                            generateOptionAndOptionValue(number,secondNumber, subNumber, secondNumberSubNumber, options, optionValues)
                        }
                    }
                })
            })
        })
    })
    return [options,optionValues]
}

function generateOptionAndOptionValue(number,secondNumber, subNumber, secondNumberSubNumber, options, optionValues){
    var numberDisplay=""
    var secondNumberDisplay=""
    var ext=""
    var secondNumberExt=""
    ext = (parseInt(number/12)>0?"p":"a")
    secondNumberExt = (parseInt(secondNumber/12)>0?"p":"a")
    numberDisplay = ((number/12)>0?((number%12)==0?12:(number%12)):((number%12)==0?12:(number%12)))
    secondNumberDisplay = ((secondNumber/12)>0?((secondNumber%12)==0?12:(secondNumber%12)):((secondNumber%12)==0?12:(secondNumber%12)))
    var optionText = (subNumber=="0"?numberDisplay+ext+"-"+(secondNumberSubNumber=="0"?secondNumberDisplay+secondNumberExt:secondNumberDisplay+"."+secondNumberSubNumber+secondNumberExt):numberDisplay+"."+subNumber+ext+"-"+(secondNumberSubNumber=="0"?secondNumberDisplay+secondNumberExt:secondNumberDisplay+"."+secondNumberSubNumber+secondNumberExt))
    var optionValueText = (strPad(number,2)+":"+strPad(subNumber,2)+"-"+strPad(secondNumber,2)+":"+strPad(secondNumberSubNumber,2))
    options.push(optionText)
    optionValues.push(optionValueText)
}