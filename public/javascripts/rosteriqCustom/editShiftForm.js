/*---------------------------------------
 * GLOBAL VARIABLES
 *---------------------------------------*/
var editShiftMode;

/*---------------------------------------
 * END GLOBAL VARIABLES
 *---------------------------------------*/

 /*---------------------------------------
  * RENDER THE SHIFT EDIT FORM
  *---------------------------------------*/

function renderShiftFormContent(employeeId,positionId,shiftId,cellDate,shiftStart,shiftFinish)
{
    //First we'll see whether we are editing a shift or if we're creating a new shift
    if ( shiftId != -1)
    {
        //If this is an existing shift
        editShiftMode = true;
        $(".deleteButton").attr('id',"deleteButton")
        $('.deleteButton').text("Delete")
        $("#formButton").text("Save")
    }
    else
    {
        editShiftMode = false;
        $("#shiftTime").val("");
        $(".deleteButton").attr('id',"cancelButton")
        $('.deleteButton').text("Cancel")
        $("#formButton").text("Create")
    }

    //Then we'll empty the hidden fields
    $("#shiftId").val("")
    $("#employeeId").val("");
    $("#positionId").val("");
    $("#cellDate").val("");

    //Set the shiftId. It will be set to -1 if a new shift is being created
    $("#shiftId").val(shiftId);
    $("#cellDate").val(cellDate)

    //This function will render the dynamic content in the shift form depending on the view the user is in
    var schedulerLocationId = $("#locationSelect").val();
    if ( getSchedulerView() == "employee" )
    {
        //This means we're in the employee view
        //We will first render the headings for the form
        $.each(getLocationEmployees(),function(i,person){
            if(person.id == employeeId){
                if ( !editShiftMode ){
                    $("#shiftFormHeading").text("New shift for "+ person.firstName + " " + person.lastName);
                }
                else{
                    $("#shiftFormHeading").text("Edit shift for "+ person.firstName + " " + person.lastName);
                }
            }
        })

        //These are used while setting the position or employee in the form
        $("#employeeId").val(employeeId)
        $("#positionId").val(positionId)

        //Then we'll progress depending on the mode we're in
        if ( editShiftMode )
        {
            //This means that we're editing a shift
            //This means that the start and finish times have been provided and we should use those
            var shiftTime = getTimeRangeOption(shiftStart.split(" ")[1]+"-"+shiftFinish.split(" ")[1],getTimeRangeOptionValues(),getTimeRangeOptions())
            $("#shiftTime").val(shiftTime);
        }

        //Then we'll set the position selection
        $("#shiftOptions").empty()
        //If we're editing a shift, that means a position has already been selected. So we'll use the editShiftModePositionId
        $.each(getLocationEmployees(),function(i,employee){
            if(employee.id == employeeId){
                //Then get the positions for this person
                //Now if we're in the editShift mode, that means that a position has already been selected and should be used
                if(editShiftMode){
                    $.each(employee.positions,function(i,position){
                        if(position.id == positionId){
                            var option = $("<option selected=\"selected\"></option>").attr("value",position.id).text(position.title);
                            $("#shiftOptions").attr("value",position.id);
                            $("#shiftOptions").append(option);
                            //Then we'll set the hidden field
                            $("#positionId").val(positionId)
                        }else{
                            var option= $("<option></option>").attr("value",position.id).text(position.title);
                            $("#shiftOptions").append(option);
                        }
                    })
                }else{
                    //If we're creating a new shift, then we'll just add the positions for that employee
                    $.each(employee.positions, function(i,position){
                        var option= $("<option></option>").attr("value",position.id).text(position.title);
                        $("#shiftOptions").append(option);
                    })
                    //Then we'll set the hidden field
                    $("#positionId").val($("#shiftOptions").val())
                }
            }
        })
        //Then we'll take care of the static stuff
        $("#shiftOptionsLabel").empty();
        $("#shiftOptionsLabel").text("Choose a position");
    }
    else
    {
        //This means we're in the position view
        //We will first render the headings for the form
        $.each(getLocationPositions(),function(i,position){
            if(position.id == positionId){
                if ( !editShiftMode ){
                    $("#shiftFormHeading").text("New shift for "+ position.title);
                }
                else{
                    $("#shiftFormHeading").text("Edit shift for "+ position.title);
                }
            }
        })

        $("#employeeId").val(employeeId)
        $("#positionId").val(positionId)

        //Then we'll progress depending on the mode we're in
        if ( editShiftMode )
        {
            //This means that we're editing a shift
            //This means that the start and finish times have been provided and we should use those
            $("#shiftTime").val(getTimeRangeOption(shiftStart.split(" ")[1]+"-"+shiftFinish.split(" ")[1],getTimeRangeOptionValues(),getTimeRangeOptions()));
        }

        //Then we'll set the employee selection
        $("#shiftOptions").empty()
        //If we're editing a shift, that means a position has already been selected. So we'll use the editShiftModePositionId
        $.each(getLocationPositions(),function(i,position){
            if(position.id == positionId){
                //Then get the positions for this person
                //Now if we're in the editShift mode, that means that a position has already been selected and should be used
                if(editShiftMode){
                    $.each(position.employees,function(i,employee){
                        if(employee.id == employeeId){
                            var option = $("<option selected=\"selected\"></option>").attr("value",employee.id).text(employee.firstName + " " + employee.lastName);
                            $("#shiftOptions").attr("value",employee.id);
                            $("#shiftOptions").append(option);
                            //Then we'll set the hidden field
                            $("#employeeId").val(employeeId)
                        }else{
                            var option= $("<option></option>").attr("value",employee.id).text(employee.firstName + " " + employee.lastName);
                            $("#shiftOptions").append(option);
                        }
                    })
                }else{
                    //If we're creating a new shift, then we'll just add the positions for that employee
                    $.each(position.employees, function(i,employee){
                        var option= $("<option></option>").attr("value",employee.id).text(employee.firstName + " " + employee.lastName);
                        $("#shiftOptions").append(option);
                        $("#employeeId").val($("#shiftOptions").val())
                    })
                }
            }
        })
        //Then we'll take care of the static stuff
        $("#shiftOptionsLabel").empty();
        $("#shiftOptionsLabel").text("Choose an employee");
    }
    // Once the content is ready we can then show the modal form
    $("#shiftEdit").modal('show');
}

/*---------------------------------------
 * END RENDER SHIFT EDIT FORM
 *---------------------------------------*/

//$("#shiftEdit").on('shown',function(){
//    $("#shiftTime").focus();
//})

$("#shiftOptions").change(function(){
    if ( getSchedulerView() == "employee")
        $("#positionId").val($("#shiftOptions").val());
    else
        $("#employeeId").val($("#shiftOptions").val());
});

$(document).on("click","#cancelButton",function(){
    $("#shiftTimeControl").removeClass("error")
        $(".shiftTimeValidation").addClass("hide")
    $("#shiftEdit").modal('hide');
})

$("#shiftEdit").on("shown",function(){
    $.each($(this).parent().parent().find(".personShiftCell"),function(){
        $(this).removeClass("selected");
    })
})

 /*---------------------------------------
  * CREATE, UPDATE AND DELETE SHIFTS
  *---------------------------------------*/

$(document).on("click","#formButton",function(){
    //This function will create and update the shifts table
    //So first we'll collect all the data
    $("#shiftTimeError").text("");
    var personId = $("#employeeId").val();
    var positionId = $("#positionId").val();
    var shiftId = $("#shiftId").val();
    var cellDate = $("#cellDate").val()
    var locationId = $(".locationSelect").val()
    // parse the input time string to get the startTime components and the finishTime components
    var shiftTime = $("#shiftTime").val();
    var result = ""

    result = validateShiftTime(shiftTime, getTimeRangeOptions(), getTimeRangeOptionValues())

    if (!result[0])
    {
        $("#shiftTimeControl").addClass("error")
        $(".shiftTimeValidation").removeClass("hide")
    }
    else
    {
        start = cellDate.split("-")[0] + " " + cellDate.split("-")[1] + " " + cellDate.split("-")[2] + " " + result[1].split("-")[0].split(":")[0] + " " + result[1].split("-")[0].split(":")[1]
        finish = cellDate.split("-")[0] + " " + cellDate.split("-")[1] + " " + cellDate.split("-")[2] + " " + result[1].split("-")[1].split(":")[0] + " " + result[1].split("-")[1].split(":")[1]
        var recurrence = $("#recurringShiftOption").val();
        var endDateComponents = ($("#recurrenceEndDate").val()).split("/");
        var seriesEndDate = endDateComponents[0]+"-"+endDateComponents[1]+"-"+endDateComponents[2];
        var increment = $("#recurrenceIncrement").val();
        var shiftBreak = $("#shiftBreak").val();
        if (!shiftBreak) {
            shiftBreak = 0;
        }
        $.ajax({
            type:'POST',
            url: '/planner',
            data:{
                operation:'update',
                locationId:locationId,
                personId:personId,
                positionId:positionId,
                shiftId:shiftId,
                startString:start,
                finishString:finish,
                recurrence:recurrence,
                seriesEndDate:seriesEndDate,
                increment:increment,
                shiftBreak:shiftBreak
            },
            success: function(data){
                $("#shiftEdit").modal('hide');
                generateScheduler()
            }
        })
    }
    return false;
})

$(document).on("click", "#deleteButton", function(){
     var shiftId =  $("#shiftId").val();
     var createOrUpdateShiftUrl = "/scheduler?operation=delete&shiftId="+shiftId+"&recurrence="+"1"; //update and delete are the two operation types
     $.ajax({
         type:'POST',
         url: '/planner',
         data:{
            operation:'delete',
            shiftId: shiftId,
            recurrence:1
         },
         success: function(data)
         {
             // Deleting the shift from employee week view
             $('#employeeTable tbody tr').each(function() {
                 $(this).children(".personShiftCell").each(function(){
                     if($(this).children())
                     {
                         $(this).children().each(function(){
                             //console.log( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() + " " + data.id)
                             if ( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() == data.id) //This is the element that we have to delete
                             {
                                 $(this).remove();
                             }
                         })
                     }
                 });
             });

             // Deleting the shift from employee day view
             $('#employeeDayTable tbody tr').each(function(){
                 $(this).children(".employeeDayShiftCell").each(function() {
                     if ($(this).children())
                     {
                         $(this).children().each(function() {
                             if ( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() == data.id) //This is the element that we have to delete
                             {
                                 $(this).remove();
                             }
                         });
                     }
                 });
             });

             // Deleting the shift from position week view
             $('#positionTable tbody tr').each(function(){
                 $(this).children(".positionShiftCell").each(function(){
                     if($(this).children())
                     {
                         $(this).children().each(function(){
                             //console.log( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() + " " + data.id)
                             if ( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() == data.id) //This is the element that we have to delete
                             {
                                 $(this).remove();
                             }
                         });
                     }
                 });
             });

             // Deleting the shift from position day view
             $('#positionDayTable tbody tr').each(function(){
                 $(this).children(".positionDayShiftCell").each(function(){
                     if($(this).children())
                     {
                         $(this).children().each(function(){
                             //console.log( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() + " " + data.id)
                             if ( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() == data.id) //This is the element that we have to delete
                             {
                                 $(this).remove();
                             }
                         });
                     }
                 });
             });
         }
     })
     $("#shiftEdit").modal('hide');

 })

 /*---------------------------------------
  * END CREATE, UPDATE AND DELETE SHIFT
  *---------------------------------------*/