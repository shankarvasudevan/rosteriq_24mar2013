/* MyRoster.js
----------------------
Version: 1.0
Author: Prathamesh Datar
Company: RosterIQ

Summary: This javascript file will contain the methods which will be used by the myRoster page of the program.

----------------------*/

//File variables
var employeeId; //This will store the current logged in user's id
var employeeWeekShifts; //This will store the week shifts the user
var calDate = new Date();   //This will store the current date
var startDate;  //This will store the startDate for that week which will be used for comparison in the controller
var simpleStartDate; //This will store the current date in a simple RosterIQ format

//Week change buttons
$("#weekPrev").click(function() {
    $("#loadWait").modal('show');
    var oldDate = startDate;
    startDate.setTime( oldDate.getTime() - (86400000*7) );
    calDate.setTime( startDate.getTime() );
    $("#dayEmployeeViewHeader thead").remove();
    generateEmployeeRoster();
    $("#loadWait").modal('hide');
});

$("#weekCurr").click(function() {
    $("#loadWait").modal('show');
    var newDate = new Date();
    calDate.setTime(newDate.getTime());
    startDate.setTime( getPrevMonday(calDate.getTime()) );
    $("#dayEmployeeViewHeader thead").remove();
    generateEmployeeRoster();
    $("#loadWait").modal('hide');
});

$("#weekNext").click(function() {
    $("#loadWait").modal('show');
    var oldDate = startDate;
    startDate.setTime(oldDate.getTime() + (86400000*7));
    calDate.setTime(startDate.getTime());
    $("#dayEmployeeViewHeader thead").remove();
    generateEmployeeRoster();
    $("#loadWait").modal('hide');
});

//Then we'll render the day roster for the employee

function generateEmployeeRoster()
{
    var thead = $("<thead>");
    var dayTitle = $("<th style=\"width:10%\">").text("Day").appendTo(thead);
    for (i=0; i!=24; ++i)
    {
        ext = (parseInt(i/12)>0?"p":"a")
        numberDisplay = ((i/12)>0?((i%12)==0?12:(i%12)):((i%12)==0?12:(i%12)))
        $("<th style=\"width:3.125%\">").text(numberDisplay+ext).appendTo(thead);
    }
    var options = $("<th style=\"width:15%\">").text("Options").appendTo(thead);
    $("#dayEmployeeViewHeader").append(thead);
    //We will iterate through the position JSON array
    $("#employeeDayTable tbody").remove();
    var employeeDayTableBody  = $("<tbody>");
    employeeDayTableBody.appendTo($("#employeeDayTable"));

    $.each(dayConversion,function(i,day){
        var dayRow = $("<tr>");
        var dayOfWeek = $("<td class=\"hidden dayOfWeek\">").text(day.dayOfWeek).appendTo(dayRow);
        var nextDate = new Date();
        nextDate.setTime(startDate.getTime() + i*86400000)
        var dayName = $("<td style=\"width:10%\" class=\"dayName\">").html((createDateSimpleFormat(nextDate).split("-")[0]) + " " + day.dayFull).appendTo(dayRow);
        var employeeDayShiftCellContainer = $("<td style=\"width:75%\" id=\"" + day.dayOfWeek + "\" class=\"employeeDayShiftCellContainer\">").appendTo(dayRow);
        var employeeDayShiftCellContainerTable = $("<table style=\"margin-bottom:0px\" class=\"table table-bordered employeeDayShiftCellContainerTable\">").appendTo(employeeDayShiftCellContainer);
        var employeeDayShiftCellOptionsContainer = $("<td class=\"employeeDayShiftCellOptionsContainer\" style=\"width:15%\">").appendTo(dayRow); //These will include the buttons
        var employeeDayShiftCellOptionsContainerTable = $("<table class=\"table table-bordered employeeDayShiftCellOptionsContainerTable\">").appendTo(employeeDayShiftCellOptionsContainer);
        dayRow.appendTo(employeeDayTableBody);
    });

    $.ajax({
        url: '/getEmployeeShiftsForWeek',
        async: 'false',
        type:'post',
        dataType: 'json',
        data:{
            dateString: createDateSimpleFormat(startDate)
        },
        success: function(data){
            var shiftCount = 0;
            //This function was moved inside as the global variable unavailabilityBlock
            //Now we'll start filling in the information in each of the input elements
            //First we'll set the title
            $.each(data,function(i,shift){
                if(shift.personName!="Unassigned")
                    ++shiftCount;
            })
            if ( shiftCount == 1)
                $("#myRosterDescription").html("You have <b>" + shiftCount + " shift</b> this week");
            else
                $("#myRosterDescription").html("You have <b>" + shiftCount + " shifts</b> this week");
            setWeekViewDate(startDate);
            //So till this point we should have entered all in the data for the unavailabilityBlock
            //Now we fill edit the unavailability edit table
            //So we'll go through each of the unavailabilities
            $.each(data, function(i, shift){
                //So we'll go through all the rows in the table and find the right row
                var dayOfWeek = shift.dayOfWeek
                $("#employeeDayTable tbody tr").each(function(){
                    if ( $(this).children().eq(0).html() == dayOfWeek )
                    {
                        //Then we have found the right row
                        //We will then set the values of the slider in this row
                        var startTime = convertToValue(((shift.start).split(" "))[1]);
                        var finishTime = convertToValue(((shift.finish).split(" "))[1]);
                        var positionTitle = shift.positionTitle;
                        var shiftSlider = $('<div class=\"slider\">').dragSlider({
                            range: true,
                            disabled:true,
                            min: 0,
                            max: 1440,
                            step:15,
                            values: [startTime,finishTime],
                            create: function(event,ui){
                                //We'll set the html depending on the values
                                $(this).children().eq(0).html("<span style=\"font-weight:bold;margin-left:5px;\" class=\"positionTitle\">" + positionTitle + "</span> " + "<span class=\"time\">" + getTimeRangeOption(convertToHours(startTime)+"-"+ convertToHours(finishTime),getTimeRangeOptionValues(),getTimeRangeOptions()) + "</span> - <span class=\"location\">" + shift.locationName + "</span>" );
                            }
                        });
                        var shiftHiddenTable = $("<table style=\"display:none\">")
                        var shiftHiddenTableRow = $("<tr>");
                        var hiddenShiftId = $("<td class=\"shiftId\">").text(shift.id).appendTo(shiftHiddenTableRow);
                        var hiddenPersonId = $("<td class=\"personId\">").text(shift.personId).appendTo(shiftHiddenTableRow);
                        var hiddenPersonName = $("<td class=\"personName\">").text(shift.personName).appendTo(shiftHiddenTableRow);
                        var hiddenPositionTitle = $("<td class=\"positionTitle\">").text(shift.positionTitle).appendTo(shiftHiddenTableRow);
                        var hiddenPositionId = $("<td class=\"positionId\">").text(shift.positionId).appendTo(shiftHiddenTableRow);
                        var hiddenShiftStart = $("<td class=\"start\">").text(shift.start).appendTo(shiftHiddenTableRow);
                        var hiddenShiftFinish = $("<td class=\"finish\">").text(shift.finish).appendTo(shiftHiddenTableRow);
                        var hiddenShiftDayOfWeek = $("<td class=\"dayOfWeek\">").text(shift.dayOfWeek).appendTo(shiftHiddenTableRow);
                        var hiddenSwapShiftRequestPendingStatus= $("<td class=\"swapShiftStatus\">").text(shift.swapShiftRequestPendingStatus).append(shiftHiddenTableRow);
                        shiftHiddenTableRow.appendTo(shiftHiddenTable);
                        shiftHiddenTable.prependTo(shiftSlider);
                        //We will then add the slider to the row that will be added to the shift cell's table
                        var dayShiftRow = $("<tr>").appendTo($(this).find(".employeeDayShiftCellContainerTable"));
                        dayShiftRow.attr("id","s"+shift.id); //Assigns the shiftId to the row
                        var dayShiftCell = $("<td class=\"employeeDayShiftCell\">");
                        dayShiftCell.appendTo(dayShiftRow);
                        dayShiftCell.append(shiftSlider);
                        if ( shift.unassigned == true )
                            (shiftSlider.children().eq(1)).addClass("btn-grey");
                        else
                            (shiftSlider.children().eq(1)).addClass("btn-primary");

                        //We will then add the button that allows the user to confirm the shift
                        //These should be added to the table appended as table rows in the options container table
                        var optionsRow = $("<tr id=\"o"+shift.id+"\">");
                        optionsRow.appendTo($(this).find(".employeeDayShiftCellOptionsContainerTable"));
                        var optionsRowCell = $("<td class=\"employeeDayShiftCellOptions\">");

                        $("<div class=\"btn-group confirmShiftButtonGroup pull-left\"><a class=\"btn btn-success confirmShiftButton\"><icon class=\"icon-ok-circle icon-white\"></icon></a></div>").appendTo(optionsRowCell);
                        $("<div class=\"btn-group dropShiftButtonGroup pull-left\"><a class=\"btn btn-danger dropShiftButton\"><icon class=\"icon-remove-circle icon-white\"></icon></a></div>").appendTo(optionsRowCell);
                        optionsRowCell.appendTo(optionsRow);
                        //If the shift is already confirmed then disable all the buttons
                        if ( shift.confirmed )
                        {
                            $.each(optionsRowCell.children(),function(){
                                $(this).children().eq(0).addClass("disabled");
                            });
                            //We'll change the style of the confirm button to a full circle to show that it's confirmed
                            $(optionsRowCell).find(".confirmShiftButton").children().eq(0).removeClass("icon-ok-circle");
                            $(optionsRowCell).find(".confirmShiftButton").children().eq(0).addClass("icon-ok-sign");
                        }
                        if( shift.swapShiftRequestPendingStatus )
                        {
                            //If the shift has a swapShiftRequestStatusPending, the user shouldn't be able to drop it
                            $(optionsRowCell).find(".dropShiftButton").addClass("disabled");
                        }
                    }
                });
            })
        }
    });
}

//These are the shift buttons

$(document).on("click",".dropShiftButton",function(){
    // ajax call to get the possible shifts

    var shiftId = getMyRosterShiftId($(this));
    $("#swapShiftId").val(shiftId);
    var getPossibleSwapShiftsUrl = "./getPossibleSwapShifts.json?shiftId="+shiftId;
    $.ajax({
        url: getPossibleSwapShiftsUrl,
        async:false,
        dataType: 'json',
        success: function(data){
            $.each(data,function(i,item) {
                var swapSelect = $(".swapSelect");
                swapSelect.empty();
                var dateComponents = (item.start).split(" ")[0].split("-");
                var startTime= (item.start).split(" ")[1];
                var finishTime= (item.finish).split(" ")[1];
                var swapDisplay = getSwapDisplay(dateComponents, startTime, finishTime, item.personName, item.dayOfWeek)
                var nextOption = $("<option value=\"" + item.id + "\">").text(swapDisplay)
                swapSelect.append(nextOption);
            });
        }
    })
    $("#shiftSwapAndDrop").modal('show');
})

$(document).on("click","#declineShiftButton",function() {
    //We'll need to see what type of request it is
    var declineShiftChoice = $(":checked").val();
    var shiftId = $("#swapShiftId").val();
    var notes = $(".requestComment").val();
    if ( declineShiftChoice == "swapShift" )
    {
        var swapSelect = $(".swapSelect");
        var shiftsSelected = swapSelect.val();
        //Now we will create the call that takes us to the requests page
        var createSwapShiftsUrl = "./createSwapShiftRequest.json?fromShiftId="+shiftId+"&toShiftIds=" + shiftsSelected + "&notes=" + notes;
        $.ajax({
            url:createSwapShiftsUrl,
            async:false,
            dataType:'json',
            success: function(data){
                $("#mySwapRequestsTable tbody").remove();
                var tbody = $("<tbody>").appendTo("#mySwapRequestsTable");
                $.each(data,function(i,item) {
                    var requestRow = $("<tr>")
                    var fromDateComponents = (item.fromStrStart).split(" ")[0].split("-");
                    var fromTimeStrStart= (item.fromStrStart).split(" ")[1];
                    var fromTimeStrFinish= (item.fromStrFinish).split(" ")[1];
                    var fromDayOfWeek = (item.fromDayOfWeek);
                    var toDateComponents = (item.toStrStart).split(" ")[0].split("-");
                    var toTimeStrStart= (item.toStrStart).split(" ")[1];
                    var toTimeStrFinish = (item.toStrFinish).split(" ")[1];
                    var toDayOfWeek = (item.toDayOfWeek);
                    var requestId = $("<td class=\"swapRequestId hidden\">").text(item.id).appendTo(requestRow);
                    var fromShiftId = $("<td class=\"fromShiftId hidden\">").text(item.fromShiftId).appendTo(requestRow);
                    var toShiftId = $("<td class=\"toShiftId hidden\">").text(item.toShiftId).appendTo(requestRow);
                    var fromShift = $("<td class=\"fromShift\">").text(convertToSwapDate(fromDateComponents, fromTimeStrStart, fromTimeStrFinish, fromDayOfWeek)).appendTo(requestRow);
                    var toShift = $("<td class=\"toShift\">").text(convertToSwapDate(toDateComponents, toTimeStrStart, toTimeStrFinish, toDayOfWeek)).appendTo(requestRow);
                    var toPersonName = $("<td class=\"toPersonName\">").text(item.personName).appendTo(requestRow);
                    var description = $("<td class=\"description\">").text(item.description).appendTo(requestRow);
                    var response = $("<td class=\"response\">").text(item.response).appendTo(requestRow);
                    tbody.append(requestRow);
                });

            }
        });
    }
    else if ( declineShiftChoice =="dropShift" )
    {
        var createDropShiftUrl = "./createDropShiftRequest.json?fromShiftId="+shiftId+"&notes="+notes;
        $.ajax({
            url:createDropShiftUrl,
            async:false,
            dataType:'json',
            success: function(data){
                $("#myDropRequestsTable tbody").remove();
                var tbody = $("<tbody>").appendTo("#myDropRequestsTable");
                var requestRow = $("<tr>")
                var fromDateComponents = (data.fromStrStart).split(" ")[0].split("-");
                var fromTimeStrStart= (data.fromStrStart).split(" ")[1];
                var fromTimeStrFinish= (data.fromStrFinish).split(" ")[1];
                var fromDayOfWeek = (data.fromDayOfWeek);
                var requestId = $("<td class=\"swapRequestId hidden\">").text(data.id).appendTo(requestRow);
                var fromShiftId = $("<td class=\"fromShiftId hidden\">").text(data.fromShiftId).appendTo(requestRow);
                var fromShift = $("<td class=\"fromShift\">").text(convertToSwapDate(fromDateComponents, fromTimeStrStart, fromTimeStrFinish, fromDayOfWeek)).appendTo(requestRow);
                var description = $("<td class=\"description\">").text(data.description).appendTo(requestRow);
                var status = $("<td class=\"response\">").text(data.status).appendTo(requestRow);
                tbody.append(requestRow);
            }
        });
    }
    $("#shiftSwapAndDrop").modal('hide');

})

$(document).on("click","#dropShiftChoice",function(){
    $(".swapShiftDiv").addClass("hidden");
    $("#declineShiftChoice").text("drop");
})

$(document).on("click","#swapShiftChoice",function(){
    $(".swapShiftDiv").removeClass("hidden");
    $("#declineShiftChoice").text("swap");
})

$(document).on("click",".confirmShiftButton",function(){
    //This should change the button into a confirm and cancel selection
    $(this).parent().append("<div class=\"btn btn-danger cancelConfirmShiftButton\"><icon class=\"icon-remove-sign icon-white\"></div>");
    $(this).parent().children().eq(0).html("<icon class=\"icon-ok-sign icon-white\">");
    $(this).parent().children().eq(0).removeClass("confirmShiftButton");
    $(this).parent().children().eq(0).addClass("confirmShiftButton2");
    $(this).parent().parent().find(".dropShiftButton").addClass("disabled");
})

$(document).on("click",".cancelConfirmShiftButton",function(){
    //This should change the button back to it's original state
    $(this).parent().children().eq(0).html("<icon class=\"icon-ok-circle icon-white\">");
    $(this).parent().children().eq(0).removeClass("confirmShiftButton2");
    $(this).parent().children().eq(0).addClass("confirmShiftButton");
    $(this).parent().parent().find(".dropShiftButton").removeClass("disabled");
    $(this).remove();
})

$(document).on("click",".confirmShiftButton2",function(){
    //This will change the button to the disabled state. This means that the employee has accepted the shift
    $(this).parent().children().eq(1).remove();
    $(this).addClass("disabled");
    $(this).html("<icon class=\"icon-ok-sign icon-white\">");

    //Then the system will change the status of the shift in the database to confirmed
    var shiftId = getMyRosterShiftId($(this));
    var updateShiftConfirmationStatusUrl = "./updateShiftConfirmationStatus.json?shiftId=" + shiftId + "&confirmationStatus=1"
    $.ajax({
        url: updateShiftConfirmationStatusUrl,
        async: false,
        dataType: 'json'
    });
})

function renderUserSwapRequestsFromPerson()
{
    var getUserSwapRequestsFromPersonUrl = "./getUserSwapRequestsFromPerson.json";
    $.ajax({
        url:getUserSwapRequestsFromPersonUrl,
        async:false,
        dataType:'json',
        success:function(data){
            //We've received the requests for this user.
            //We will now show the same in the requests Table
            //We'll first empty the current records in the table
//            console.log("We've received data from the server");
//            console.log(data);
            $("#mySwapRequestsTable tbody").remove();
            var requestListTableBody = $("<tbody>").appendTo("#mySwapRequestsTable");
            $.each(data,function(i,item){
                var requestRow = $("<tr>");
                var fromDateComponents = (item.fromStrStart).split(" ")[0].split("-");
                var fromTimeStrStart= (item.fromStrStart).split(" ")[1];
                var fromTimeStrFinish= (item.fromStrFinish).split(" ")[1];
                var fromDayOfWeek = (item.fromDayOfWeek);
                var toDateComponents = (item.toStrStart).split(" ")[0].split("-");
                var toTimeStrStart= (item.toStrStart).split(" ")[1];
                var toTimeStrFinish = (item.toStrFinish).split(" ")[1];
                var toDayOfWeek = (item.toDayOfWeek);
                var fromShiftId = $("<td class=\"fromShiftId hidden\">").text(item.fromShiftId).appendTo(requestRow);
                var toShiftId = $("<td class=\"toShiftId hidden\">").text(item.toShiftId).appendTo(requestRow);
                var fromShift = $("<td class=\"fromShift\">").text(convertToSwapDate(fromDateComponents, fromTimeStrStart, fromTimeStrFinish, fromDayOfWeek)).appendTo(requestRow);
                var toShift = $("<td class=\"toShift\">").text(convertToSwapDate(toDateComponents, toTimeStrStart, toTimeStrFinish, toDayOfWeek)).appendTo(requestRow);
                var toPersonName = $("<td class=\"toPersonName\">").text(item.toPersonName).appendTo(requestRow);
                var description = $("<td class=\"description\">").text(item.description).appendTo(requestRow);
                var response = $("<td class=\"response\">").text(item.response).appendTo(requestRow);
                requestListTableBody.append(requestRow);
            })
        }
    })
}

function mainMenuAdjustment()
{
    //Change the menu to active
    $("#myRosterLink").addClass("active");
    $("#myRosterLink").children().eq(0).removeClass("icon-white");
    //Then we need to move the sub nav menu to align with the current tab
}

$(document).ready(function(){
    setTimeZoneOffset();
    startDate = getPrevMonday(calDate);
    simpleStartDate = createDateSimpleFormat(startDate);
    generateEmployeeRoster();
    renderUserSwapRequestsFromPerson();
    mainMenuAdjustment();
})

