/*---------------------------------------
 * GLOBAL VARIABLES
 *---------------------------------------*/

var calDate = new Date();
var simpleDate;
var startDate;

var dragShift;
var dragShiftColIndex;
var prevPopoverShiftId;
var prevPopover;
var employeePositions;
var positionEmployees;
var schedulerLocation;

var plannerData;
var rosterCostEmployees;
var rosterCostPositions;

var weekShifts;
var dayShifts;
var getLocationWeekShiftsUrl = "./getLocationWeekShifts.json?startDate=";
var getLocationDayShiftsUrl = "./getLocationDayShifts.json?date=";
var i=0;

var schedulerView;
var dayOrWeekView;
var mouseEnterShift = false;
var mouseEnterShiftAddButton = false;

var clipboardObject;
var clipboardObjectCommand;

var ctrlDown = false;
var cmdDown = false;
var ctrlKey = 17;
var cmdKey = 91;
var xKey = 88;
var cKey = 67;
var vKey = 86;

var employeeDayShiftsSet
var employeeDayUnavailabilitiesSet
var positionDayShiftsSet
var employeeWeekShiftsSet
var employeeWeekUnavailabilitiesSet
var positionWeekShiftsSet


/*---------------------------------------
 * END GLOBAL VARIABLES
 *---------------------------------------*/

/*---------------------------------------
 * GETTERS AND SETTERS
 *---------------------------------------*/

function getSchedulerView()
{
    return schedulerView;
}

function setSchedulerView(view)
{
    schedulerView = view;
}

function getDayOrWeekView()
{
    return dayOrWeekView;
}

function setDayOrWeekView(view)
{
    dayOrWeekView = view;
}

function getStartDate()
{
    return startDate;
}

function setStartDate(time)
{
    startDate.setTime(time);
}

function getCalDate()
{
    return calDate;
}

function setCalDate(time)
{
    calDate.setTime(time);
}

function getLocationEmployees(){
    return plannerData.locationEmployees
}

function getLocationPositions(){
    return plannerData.locationPositions
}

function setEmployeeDayShiftsSet(){
    employeeDayShiftsSet=true
}

function setEmployeeDayUnavailabilitiesSet(){
    employeeDayUnavailabilitiesSet=true
}

function setPositionDayShiftsSet(){
    positionDayShiftsSet=true
}

function setEmployeeWeekShiftsSet(){
    employeeWeekShiftsSet=true
}

function setEmployeeWeekUnavailabilitiesSet(){
    employeeWeekUnavailabilities=true
}

function setPositionWeekShiftsSet(){
    positionWeekShiftsSet=true
}

function resetViewSets(){
    employeeDayShiftsSet=false
    employeeDayUnavailabilitiesSet=false
    positionDayShiftsSet=false
    employeeWeekShiftsSet=false
    employeeWeekUnavailabilitiesSet=false
    positionWeekShiftsSet=false
}

/*---------------------------------------
 * END GETTERS AND SETTERS
 *---------------------------------------*/

/*---------------------------------------
 * INITIAL SETUP
 *---------------------------------------*/

 $(document).ready(function() {
     //We will first fill out the dynamic content in the modal form view
     setTimeZoneOffset();
     schedulerView = "employee"; //This will set the view of the scheduler to employee. This depends on the active class in the html
     dayOrWeekView = "day";//dayOrWeekView = "day";

     // Show the correct loadShifts boxes
     $("#loadShiftsWeekDiv").hide();
     $("#loadShiftsDayDiv").show();


     //Activate the modal functionality
     $("#shiftEdit").modal({
         show:false,
     });

     $("#loadWait").modal({
         show:false
     });

     simpleDate = createDateSimpleFormat(calDate);
     startDate = getPrevMonday(calDate);

     //We'll then set the time range options
     var options = generateTimeRangeOptions()[0]
     var timeSelects = $("#shiftTime").typeahead()
     timeSelects.data('typeahead').source = options

     generateScheduler();

     $("#plannerLink").addClass("active");
     $("#plannerLink").children().eq(0).removeClass("icon-white");

     $('#recurrenceEndDate').datepicker({
         inline: true,
         dateFormat: "dd/mm/yy",
         changeMonth:true,
         changeYear:true,
         showOtherMonths:true,
         selectOtherMonths:true
     });

     $("#moreOptionsButton").click(function(){
         //This will display the other options that the user has while creating a shift
         if($("#iconSymbol").hasClass("icon-plus")){
             //This means that the user wishes to see more options
             $("#moreOptions").removeClass("hidden");
             $("#iconSymbol").removeClass("icon-plus");
             $("#iconSymbol").addClass("icon-minus");
         }else{
             //This means that the user doesn't need any more options
             $("#moreOptions").addClass("hidden");
             $("#iconSymbol").removeClass("icon-minus");
             $("#iconSymbol").addClass("icon-plus");
         }
     })

 });

function generateEmployeeSchedulerTableHead(d)
{
   var thead = $("<thead>");
   var trow = $("<tr>");
   trow.appendTo(thead);
   $("<th style=\"width:9%\">").text("Person").appendTo(trow);
   for (var i=0; i < 7; i++) {
       var nextDate = new Date();        //Create a date object
       nextDate.setTime(d.getTime()+ (86400000*i)); // Set the date of the current object. It is zero at the start so it will start with the Monday
//           nextDate.setMonth(d.getMonth()); //Set the month of the date we are on
//           nextDate.setFullYear(d.getFullYear()); // Set the year of the date we are on
       nextDateParticulars = (nextDate.toDateString()).split(" ");
       $("<th style=\"width:13%\">").text(nextDateParticulars[2] + " " + convertDayToFullName(nextDateParticulars[0])).appendTo(trow);    //appendTo means that it will add the content to the end of the parent object
   }
   //prependTo means that it will add the content to the start of the parent object
   thead.prependTo($("#employeeTable"));
}

function generatePositionSchedulerTableHead(d)
{
   var thead = $("<thead>");
   var trow = $("<tr>");
   trow.appendTo(thead);
   $("<th style=\"width:9%\">").text("Position").appendTo(trow);
   for (var i=0; i < 7; i++) {
       var nextDate = new Date();        //Create a date object
       nextDate.setTime(d.getTime()+ (86400000*i)); // Set the date of the current object. It is zero at the start so it will start with the Monday
//           nextDate.setMonth(d.getMonth()); //Set the month of the date we are on
//           nextDate.setFullYear(d.getFullYear()); // Set the year of the date we are on
       nextDateParticulars = (nextDate.toDateString()).split(" ");
       $("<th style=\"width:13%\">").text(nextDateParticulars[2] + " " + convertDayToFullName(nextDateParticulars[0])).appendTo(trow);    //appendTo means that it will add the content to the end of the parent object
   }
   //prependTo means that it will add the content to the start of the parent object
   thead.prependTo($("#positionTable"));
}

function loadRosterCost(positionId, personId, dayCostDiv, weekCostDiv) {
    //alert("Inside loadRosterCost: positionId = " + positionId + ", personId = " + personId + ", dayCostDiv = " + dayCostDiv.attr("class") + ", weekCostDiv = " + weekCostDiv.attr("class"));
    var todayDateTime = calDate.getTime();
    var weekStartDateTime = startDate.getTime();
    var rosterCostUrl = "./getRosterCost.json?positionId="+positionId+"&personId="+personId+"&todayDateTime="+todayDateTime+"&weekStartDateTime="+weekStartDateTime;
    $.ajax({
        url: rosterCostUrl,
        async: false,
        dataType: 'json',
        success: function(data){
            var displayedDayCost = data.dayHoursWorked + " hrs ($" + data.dayCost + ")";
            var displayedWeekCost = data.weekHoursWorked + " hrs ($" + data.weekCost + ")";
            dayCostDiv.text(displayedDayCost);
            weekCostDiv.text(displayedWeekCost);
        }
    });
}

function loadEmployeeRosterCost() {
    var todayDateTime = calDate.getTime();
    var weekStartDateTime = startDate.getTime();
    var getRosterEmployeeCostUrl = "./getRosterEmployeeCost.json?locationId="+schedulerLocation+"&todayDateTime="+todayDateTime+"&weekStartDateTime="+weekStartDateTime;
    $.ajax({
        url: getRosterEmployeeCostUrl,
        async: false,
        dataType: 'json',
        success: function(data){
            rosterCostEmployees = data;
        }
    });
}

function loadPositionRosterCost() {
    var todayDateTime = calDate.getTime();
    var weekStartDateTime = startDate.getTime();
    var getRosterPositionCostUrl = "./getRosterPositionCost.json?locationId="+schedulerLocation+"&todayDateTime="+todayDateTime+"&weekStartDateTime="+weekStartDateTime;
    $.ajax({
        url: getRosterPositionCostUrl,
        async: false,
        dataType: 'json',
        success: function(data){
            rosterCostPositions = data;
        }
    });
}

function createEmployeeBody(d)
{
    //We will iterate through the employee JSON array
    $("#employeeTable tbody").remove();
    var tbody = $("<tbody>");
    tbody.appendTo($("#employeeTable"));
    $.each(rosterCostEmployees,function(i,person) {
        var trow = $("<tr>");
        $("<td class=\"hidden rowPersonId\">").text(person.id).appendTo(trow);
        var tName = $("<td class=\"employeeName\">");
        tName.text(person.firstName).appendTo(trow);
        for (var i=0 ; i!=7 ; ++i)
        {
            var nextDate = new Date();        //Create a date object
            nextDate.setTime(d.getTime()+ (86400000*i)); // Set the date of the current object. It is zero at the start so it will start with the Monday
            var hiddenDiv = $("<div class=\"hidden\">");
            var month = nextDate.getMonth();
            var cellDate = $("<div class='cellDate'>").html(nextDate.getDate() + "-" + month + "-"+ nextDate.getFullYear());
            var cellUnavailabilities = $("<div class=\"unavailabilities\">");
            cellDate.appendTo(hiddenDiv);
            cellUnavailabilities.appendTo(hiddenDiv);
            ($("<td class=\"personShiftCell\">").append(hiddenDiv)).appendTo(trow);
        }
        trow.appendTo(tbody);
    });
}

function createPositionBody(d)
{
    //We will iterate through the employee JSON array
    $("#positionTable tbody").remove();
    var tbody = $("<tbody>");
    tbody.appendTo($("#positionTable"));
    $.each(rosterCostPositions,function(i,position) {
        var trow = $("<tr>");
        $("<td class=\"hidden rowPositionBody\">").text(position.id).appendTo(trow);
        var tTitle = $("<td class=\"positionTitle\">");
        tTitle.text(position.positionTitle).appendTo(trow);
        for (var i=0 ; i!=7 ; ++i)
        {
            var nextDate = new Date();        //Create a date object
            nextDate.setTime(d.getTime()+ (86400000*i)); // Set the date of the current object. It is zero at the start so it will start with the Monday
            var hiddenDiv = $("<div class=\"hidden\">");
            var month = nextDate.getMonth();
            var cellDate = $("<div class='cellDate'>").html(nextDate.getDate() + "-" + month + "-"+ nextDate.getFullYear());
            cellDate.appendTo(hiddenDiv);
            ($("<td class=\"positionShiftCell\">").append(hiddenDiv)).appendTo(trow);
        }
        trow.appendTo(tbody);
    });

}

function generateScheduler() {

    //Generate the scheduler table
    //Get all the data for the location after which we'll add all the data to the scheduler
    //Get the scheduler location from the select options menu
    $("thead").remove();
    schedulerLocation = $(".locationSelect").val();
    getPlannerDataForLocation(schedulerLocation, startDate, calDate);
    loadEmployeeRosterCost();
    loadPositionRosterCost();
    generateEmployeeSchedulerTableHead(startDate);
    createEmployeeBody(startDate);
    generatePositionSchedulerTableHead(startDate);
    createPositionBody(startDate);
    setDayViewDate(calDate);
    setWeekViewDate(startDate);
    createDayEmployees(calDate,rosterCostEmployees);
    $(".employeeWeekCost").hide();
    createDayPositions(calDate,rosterCostPositions);
    $(".positionWeekCost").hide();
    createDaySummary();

    resetViewSets()
    renderShiftsAndUnavailabilitiesForView()

    $(".employeeDayShiftCell").hover(
        function(){
            $(this).addClass("hoverCell");
        },
        function(){
            $(this).removeClass("hoverCell");
        }
    );

    $(".positionDayShiftCell").hover(
       function(){
           $(this).addClass("hoverCell");
       },
       function(){
           $(this).removeClass("hoverCell");
       }
    );

    $(".personShiftCell").hover(
        function(){
            $(this).addClass("hoverCell");
        },
        function(){
            $(this).removeClass("hoverCell");
        }
    );

    $(".positionShiftCell").hover(
       function(){
           $(this).addClass("hoverCell");
       },
       function(){
           $(this).removeClass("hoverCell");
       }
    );

    //Then we'll set the event handlers for the drag and drop functionality
    $(".personShift").draggable({revert:true,revertDuration:20});
    $(".positionShift").draggable({revert:true,revertDuration:20});
    $(".positionShiftCell").droppable();
    $(".personShiftCell").droppable();

    //Then we'll make the cells and shifts clickable
    enableClickOperationsOnCellsAndShifts()
    //We'll enable the popover functionality for the load shifts
    enableLoadShiftsFunctionality()
}

function getPlannerDataForLocation(locationId, startDate, calDate, dayOfWeek)
{
    $.ajax({
        url:'/getPlannerDataForLocation',
        type:'POST',
        async:false,
        dataType:'json',
        data:{
            locationId:locationId,
            startDate:createDateSimpleFormat(startDate),
            calDate:createDateSimpleFormat(calDate),
            dayOfWeek:dayOfWeek
        },
        success:function(data){
            plannerData = data;
        }
    });
}

function renderShiftsAndUnavailabilitiesForView(){
    if(schedulerView == "employee" && dayOrWeekView =="day"){
        if(!employeeDayShiftsSet) renderEmployeeDayShifts(plannerData.dayShifts)
        if(!employeeDayUnavailabilitiesSet) renderEmployeeDayUnavailabilities(plannerData.dayUnavailabilities)
    }else if(schedulerView=="employee" && dayOrWeekView=="week"){
        if(!employeeWeekShiftsSet) renderEmployeeWeekShifts(plannerData.weekShifts)
        if(!employeeWeekUnavailabilitiesSet) renderEmployeeWeekUnavailabilities(plannerData.weekUnavailabilities)
    }else if(schedulerView=="position" && dayOrWeekView=="day"){
        if(!positionDayShiftsSet) renderPositionDayShifts(plannerData.dayShifts)
    }else if(schedulerView=="position" && dayOrWeekView=="week"){
        if(!positionWeekShiftsSet) renderPositionWeekShifts(plannerData.weekShifts)
    }
}

/*---------------------------------------
 * END INITIAL SETUP
 *---------------------------------------*/

/*---------------------------------------
 * KEYBOARD NAVIGATION
 *---------------------------------------*/

$(document).keydown(function(){
    var keyCode = (event.keyCode ? event.keyCode : event.which );
    if(keyCode == 37){
        //This is the keycode for the left arrow key then, we should move the selected item left
        if($(".selected").hasClass("personShiftCell")){
            //This means the selected item is a cell and should be moved to the prev cell if possible
            var currentObject = $(".selected");
            if($(currentObject).prev().hasClass("personShiftCell")){
                //If the item to the left is a personShiftCell, then move the selected class to it
                $(currentObject).prev().addClass("selected");
                $(currentObject).removeClass("selected");
            }
        }else if($(".selected").hasClass("positionShiftCell")){
            //This means the selected item is a cell and should be moved to the prev cell if possible
            var currentObject = $(".selected");
            if($(currentObject).prev().hasClass("positionShiftCell")){
                //If the item to the left is a personShiftCell, then move the selected class to it
                $(currentObject).prev().addClass("selected");
                $(currentObject).removeClass("selected");
            }
        }else if($(".selected").hasClass("personShift")){
            //This means that the selected item is a person shift
            var currentObject = $(".selected");
            if($(currentObject).prev().hasClass(".personShift")){
                //If the item to the left is a personShift, the move the selected class to it
                $(currentObject).prev().addClass("selected");
                $(currentObject).prev().addClass("btn-warning");
                $(currentObject).prev().removeClass("btn-primary")
                $(currentObject).removeClass("selected");
                $(currentObject).removeClass("btn-primary");
                $(currentObject).removeClass("btn-warning")
            }else{
                //If the selected item is the first shift in the cell, then we should move the selected class to the shift cell to it's left
                $(currentObject).parent().addClass("selected");
                $(currentObject).removeClass("selected");
                $(currentObject).removeClass("btn-warning");
                $(currentObject).addClass("btn-primary")
            }
        }else if($(".selected").hasClass("positionShift")){
            //This means that the selected item is a person shift
            var currentObject = $(".selected");
            if($(currentObject).prev().hasClass(".positionShift")){
                 //If the item to the left is a personShift, the move the selected class to it
                 $(currentObject).prev().addClass("selected");
                 $(currentObject).prev().addClass("btn-warning");
                 $(currentObject).prev().removeClass("btn-primary")
                 $(currentObject).removeClass("selected");
                 $(currentObject).removeClass("btn-primary");
                 $(currentObject).removeClass("btn-warning")
            }else{
                 //If the selected item is the first shift in the cell, then we should move the selected class to the shift cell to it's left
                 $(currentObject).parent().addClass("selected");
                 $(currentObject).removeClass("selected");
                 $(currentObject).removeClass("btn-warning");
                 $(currentObject).addClass("btn-primary")
            }
        }
    }else if(keyCode == 39){
        //This means that the keycode for the right arrow key, then we should move the selected item right
        if($(".selected").hasClass("personShiftCell")){
            //This means the selected item is a cell and should be moved to the prev cell if possible
            //Now for the right arrow key there will be a slightly different behaviour
            //If the cell contains any shifts, the cursor will first move into those shifts and if it's the last shift it will move to the next cell
            var currentObject = $(".selected");
            if($(currentObject).children().length > 1 ){
                //If the cell has shifts > 1 because there is a hidden child in all cells )
                var nextObject = $(currentObject).children().get(1);
                $(nextObject).addClass("selected");
                $(nextObject).addClass("btn-warning");
                $(nextObject).removeClass("btn-primary")
                $(currentObject).removeClass("selected");
            }else if($(currentObject).next().hasClass("personShiftCell")){
                //If the item to the right is a personShiftCell, then move the selected class to it
                $(currentObject).next().addClass("selected");
                $(currentObject).removeClass("selected");
            }
        }else if($(".selected").hasClass("positionShiftCell")){
            //This means the selected item is a cell and should be moved to the prev cell if possible
            var currentObject = $(".selected");
            if($(currentObject).children().length > 1 ){
                //If the cell has shifts > 1 because there is a hidden child in all cells )
                var nextObject = $(currentObject).children().get(1);
                $(nextObject).addClass("selected");
                $(nextObject).addClass("btn-warning");
                $(nextObject).removeClass("btn-primary")
                $(currentObject).removeClass("selected");
            }else if($(currentObject).next().hasClass("positionShiftCell")){
                //If the item to the right is a personShiftCell, then move the selected class to it
                $(currentObject).next().addClass("selected");
                $(currentObject).removeClass("selected");
            }
        }else if($(".selected").hasClass("personShift")){
            //This means that the selected item is a person shift
            var currentObject = $(".selected");
            if($(currentObject).next().hasClass("personShift")){
                //If the next item is also a personShift then move the .selected class to it
                $(currentObject).next().addClass("selected");
                $(currentObject).next().addClass("btn-warning")
                $(currentObject).next().removeClass("btn-primary")
                $(currentObject).removeClass("selected");
                $(currentObject).removeClass("btn-warning")
                $(currentObject).addClass("btn-primary")
            }else if($(currentObject).parent().next().hasClass("personShiftCell")){
                //This means that the selected item is the last shift in the cell and the selected class should be moved to the next cell
                $(currentObject).parent().next().addClass("selected");
                $(currentObject).removeClass("selected");
                $(currentObject).removeClass("btn-warning")
                $(currentObject).addClass("btn-primary")
            }
        }else if($(".selected").hasClass("positionShift")){
            //This means that the selected item is a person shift
            var currentObject = $(".selected");
            if($(currentObject).next().hasClass("positionShift")){
                //If the next item is also a personShift then move the .selected class to it
                $(currentObject).next().addClass("selected");
                $(currentObject).next().addClass("btn-warning")
                $(currentObject).next().removeClass("btn-primary")
                $(currentObject).removeClass("selected");
                $(currentObject).removeClass("btn-warning")
                $(currentObject).addClass("btn-primary")
            }else if($(currentObject).parent().next().hasClass("positionShiftCell")){
                //This means that the selected item is the last shift in the cell and the selected class should be moved to the next cell
                $(currentObject).parent().next().addClass("selected");
                $(currentObject).removeClass("selected");
                $(currentObject).removeClass("btn-warning")
                $(currentObject).addClass("btn-primary")
            }
        }
    }else if(keyCode == 38){
        //This means that the keyCode for the up arrow key, then we should move the selected item up
        var currentObject = $(".selected");
        var objectIndex = $(".selected").index();
        if($(currentObject).hasClass("personShiftCell")){
            //If the item is a personShiftCell
            var nextObject = $(currentObject).parent().prev().children().get(objectIndex);
            if($(nextObject).hasClass("personShiftCell")){
                $(nextObject).addClass("selected");
                $(currentObject).removeClass("selected");
            }
        }else if($(currentObject).hasClass("positionShiftCell")){
            //If the item is a personShiftCell
            var nextObject = $(currentObject).parent().prev().children().get(objectIndex);
            if($(nextObject).hasClass("positionShiftCell")){
                $(nextObject).addClass("selected");
                $(currentObject).removeClass("selected");
            }
        }
    }else if(keyCode == 40){
        //This means that the keyCode for the up arrow key, then we should move the selected item up
        var currentObject = $(".selected");
        var objectIndex = $(".selected").index();
        if($(currentObject).hasClass("personShiftCell")){
            //If the item is a personShiftCell
            var nextObject = $(currentObject).parent().next().children().get(objectIndex);
            if($(nextObject).hasClass("personShiftCell")){
                $(nextObject).addClass("selected");
                $(currentObject).removeClass("selected");
            }
        }else if($(currentObject).hasClass("positionShiftCell")){
            //If the item is a personShiftCell
            var nextObject = $(currentObject).parent().next().children().get(objectIndex);
            if($(nextObject).hasClass("positionShiftCell")){
                $(nextObject).addClass("selected");
                $(currentObject).removeClass("selected");
            }
        }
    }else if(keyCode == 13 ){
        //This means that the enter key has been pressed.
        //This will open the create shift or edit shift form
        //First we'll find the object that has been selected
        var object = $(".selected");
        //Now we'll see what kind of object it is
        if($(object).hasClass("personShiftCell")){
            //If this is a person shift cell
            //Then that means the user wishes to create a new shift for that cell
            var cellDate=$(object).find('.cellDate').html()
            var employeeId=$(object).parent().find(".rowPersonId").html()
            renderShiftFormContent(employeeId,"", "-1", cellDate,"");
        }else if ($(object).hasClass("personShift")){
            //If this is a person shift
            //This means that the user wishes to edit an existing shift
            event.stopPropagation(); //To stop this event being fired higher up in the heirarchy
            //Now we first need to get all the information from the shift div element
            var cellDate = $(this).parent().find(".cellDate").html()
            var shiftId = $(object).find(".shiftId").html()
            var employeeId = $(object).find(".personId").html()
            var positionId = $(object).find(".positionId").html()
            var start = $(object).find(".start").html()
            var finish = $(object).find(".finish").html()
            renderShiftFormContent(employeeId, positionId, shiftId, cellDate,start, finish);
        }else if($(object).hasClass("positionShiftCell")){
            //If this is a person shift cell
            //Then that means the user wishes to create a new shift for that cell
            var cellDate=$(object).find('.cellDate').html()
            var positionId=$(object).parent().find(".rowPositionId").html()
            renderShiftFormContent("",positionId, "-1", cellDate,"");
        }else if ($(object).hasClass("positionShift")){
            //If this is a person shift
            //This means that the user wishes to edit an existing shift
            event.stopPropagation(); //To stop this event being fired higher up in the heirarchy
            //Now we first need to get all the information from the shift div element
            var cellDate = $(this).parent().find(".cellDate").html()
            var shiftId = $(object).find(".shiftId").html()
            var employeeId = $(object).find(".personId").html()
            var positionId = $(object).find(".positionId").html()
            var start = $(object).find(".start").html()
            var finish = $(object).find(".finish").html()
            renderShiftFormContent(employeeId, positionId, shiftId, cellDate,start, finish);
        }
    }
})

/*---------------------------------------
 * END KEYBOARD NAVIGATION
 *---------------------------------------*/

/*---------------------------------------
 * COPY AND PASTE EVENTS
 *---------------------------------------*/

$(document).keydown(function(e){
    if(e.keyCode == ctrlKey ) ctrlDown = true;
    else if ( e.keyCode == cmdKey ) cmdDown = true;
}).keyup(function(e){
    if(e.keyCode == ctrlKey ) ctrlDown = false;
    else if ( e.keyCode == cmdKey ) cmdDown = false;
})

$(document).keydown(function(e){
    if((ctrlDown || cmdDown ) && (e.keyCode == cKey)){
        //This will handle the copy method
        //The selected item can either be a personShift or cell, positionShift or cell for both the views
        var currentObject = $(".selected");
        clipboardObject = $(currentObject).clone();
        clipboardObjectCommand = "copy";
    }
    else if((ctrlDown || cmdDown) && (e.keyCode == xKey)){
        //This will handle the cut method
        //The selected item can either be a personShift or cell, positionShift or cell for both the views
        var currentObject = $(".selected");
        clipboardObject = $(currentObject).clone();
        if($(currentObject).hasClass("personShift") || $(currentObject).hasClass("positionShift")){
            $(currentObject).remove();
        }else if($(currentObject).hasClass("personShiftCell") || $(currentObject).hasClass("positionShiftCell")){
            $.each($(currentObject).children(),function(){
                $(this).remove();
            })
        }
        clipboardObjectCommand = "cut";
    }
    else if((ctrlDown || cmdDown) && (e.keyCode == vKey)){
        //Now the paste can only happen inside the cell
        var currentObject = $(".selected");
        var clipboardObjectCopy = clipboardObject.clone();
        if($(currentObject).hasClass("personShiftCell")){
            //We have to see if the clipboardContents have either a personShift or is a personShift Cell
            if($(clipboardObjectCopy).hasClass("personShift")){
                //This means we just have to append the shift item from the clipboard to the cell
                $(currentObject).append(clipboardObject);
            }else if($(clipboardObjectCopy).hasClass("personShiftCell")){
                $.each(clipboardObjectCopy.find(".personShift"),function(){
                    $(currentObject).append($(this));
                })
            }
        }
        if(clipboardObjectCommand == "cut"){
            clipboardObject = $();
        }
    }
})

/*---------------------------------------
 * END COPY AND PASTE
 *---------------------------------------*/


/*---------------------------------------
 * PLUS BOX OVER CELLS
 *---------------------------------------*/

$(document).on("mouseenter",".personShiftCell",function(event){
    //Once we've entered a shift, an add shift item should show inside the shift cell / shift ( As they are both the same size )
   if (!mouseEnterShift && !mouseEnterShiftAddButton)
   {
     var addShiftObject = $("<div class=\"addPersonShift btn\" style=\"position:absolute;right:0px;bottom:0px;height:25px;padding-bottom:2px;\">").html("<i class=\"icon-plus\"></i>");
     $(this).append(addShiftObject);
     //mouseEnterShift = true;
   }
});

$(document).on("mouseleave",".personShiftCell",function(event){
    if(!mouseEnterShiftAddButton){
        $(this).find(".addPersonShift").remove();
        //mouseEnterShift = false;
        //mouseEnterShiftAddButton = false;
    }
})

$(document).on("click",".addPersonShift",function(event){
    event.stopPropagation();
    //This will add the new shift to the shift cell
    var cellDate=$(this).parent().find(".cellDate").html();
    var employeeId=$(this).parent().parent().find(".rowPersonId").text();
    renderShiftFormContent(employeeId,"", "-1", cellDate,"","");
})

$(document).on("mouseenter",".positionShiftCell",function(event){
    //Once we've entered a shift, an add shift item should show inside the shift cell / shift ( As they are both the same size )
   if (!mouseEnterShift && !mouseEnterShiftAddButton)
   {
     var addShiftObject = $("<div class=\"addPositionShift btn\" style=\"position:absolute;right:0px;bottom:0px;height:25px;padding-bottom:2px;\">").html("<i class=\"icon-plus\"></i>");
     $(this).append(addShiftObject);
     //mouseEnterShift = true;
   }
});

$(document).on("mouseleave",".positionShiftCell",function(event){
    if(!mouseEnterShiftAddButton){
        $(this).find(".addPositionShift").remove();
        //mouseEnterShift = false;
        //mouseEnterShiftAddButton = false;
    }
})

$(document).on("click",".addPositionShift",function(event){
    event.stopPropagation();
    //This will add the new shift to the shift cell
    var cellDate=$(this).parent().find(".cellDate").html();
    var positionId=$(this).parent().parent().find(".positionId").text();
    renderShiftFormContent("",positionId, "-1", cellDate,"","");
})

/*---------------------------------------
 * END PLUS BOX OVER CELLS
 *---------------------------------------*/

/*---------------------------------------
 * CELL AND SHIFT BLOCK CLICK AND SELECTION
 *---------------------------------------*/

 function enableClickOperationsOnCellsAndShifts(){
    $(document).on("dblclick", ".personShiftCell", function(){
       var cellDate=$(this).find(".cellDate").html();
       var employeeId=$(this).parent().find(".rowPersonId").text();
       renderShiftFormContent(employeeId,"", "-1", cellDate, "","");
    });

    $(document).on("dblclick", ".personShift", function(event){
       event.stopPropagation(); //To stop this event being fired higher up in the heirarchy
       //Now we first need to get all the information from the shift div element
       var cellDate = $(this).parent().find(".cellDate").html()
       var shiftId = $(this).find(".shiftId").html();
       var employeeId = $(this).find(".personId").html();
       var positionId = $(this).find(".positionId").html();
       var start = $(this).find(".start").html();
       var finish = $(this).find(".finish").html();
       renderShiftFormContent(employeeId, positionId, shiftId, cellDate, start, finish);
    });

    $(document).on("dblclick", ".positionShiftCell", function() {
        var cellDate=$(this).find(".cellDate").html();
        var positionId=$(this).parent().find(".rowPositionId").text();
        renderShiftFormContent("",positionId, "-1", cellDate, "","");
    });

    $(document).on("dblclick", ".positionShift", function(event) {
        event.stopPropagation(); //To stop this event being fired higher up in the heirarchy
        //Now we first need to get all the information from the shift div element
        var cellDate = $(this).parent().find(".cellDate").html()
        var shiftId = $(this).find(".shiftId").html();
        var employeeId = $(this).find(".personId").html();
        var positionId = $(this).find(".positionId").html();
        var start = $(this).find(".start").html();
        var finish = $(this).find(".finish").html();
        renderShiftFormContent(employeeId, positionId, shiftId, cellDate, start, finish);
    });

    $(document).on("click",".personShiftCell",function(){
        //Remove all other clickCells
        $.each($(document).find(".selected"),function(){
            $(this).removeClass("selected");
            if($(this).hasClass("personShift") || $(this).hasClass("positionShift")){
                $(this).removeClass("btn-warning")
                $(this).addClass("btn-primary")
            }
        })
        $(this).addClass("selected");
    })

    $(document).on("click",".positionShiftCell",function(){
        //Remove all other clickCells
        $.each($(document).find(".selected"),function(){
            $(this).removeClass("selected");
            if($(this).hasClass("personShift") || $(this).hasClass("positionShift")){
                $(this).removeClass("btn-warning")
                $(this).addClass("btn-primary")
            }
        })
        $(this).addClass("selected");
    })

    $(document).on("dblclick",".employeeDayShiftCell",function(){
      var employeeId = $(this).parent().find(".employeeId").html();
      var cellDate = $(this).parent().find(".date").html();
      renderShiftFormContent(employeeId,"", "-1", cellDate,"","");
    })

    $(document).on("dblclick",".positionDayShiftCell",function(){
      var positionId = $(this).parent().find(".rowPositionId").html();
      var cellDate = $(this).parent().find(".date").html();
      renderShiftFormContent("",positionId, "-1", cellDate,"","");
    })
 }

/*---------------------------------------
 * END CELL AND SHIFT BLOCK CLICK AND SELECTION
 *---------------------------------------*/

/*---------------------------------------
 * OTHER CLICK OPERATIONS
 *---------------------------------------*/


 $(document).on("click", ".publishButton", function() {
    var fromDateTime = calDate.getTime();
    var dayOrWeek = getDayOrWeekView();
    if (dayOrWeek == "week") {
         fromDateTime = startDate.getTime();
    }
    var publishChangesUrl = "/publishChanges?fromDateTime="+fromDateTime+"&dayOrWeek="+dayOrWeek;
    $.ajax({
         type:'POST',
         url: publishChangesUrl,
         success: function(data) {

         }
    })
 });

 $(document).on("dblclick",".ui-slider-range",function(event){
     event.stopPropagation();
 })

 $(document).on("change",".locationSelect",function(){
     removeTableEventHandlers();
     schedulerLocation = $(this).val();
     $.each($(".locationSelect"),function(){
        $(this).val(schedulerLocation)
     })
     generateScheduler();
 })


 // loading shifts from previous template functions
 $(document).on("click","#loadShiftsDayButton", function(event) {
     $("#loadDayDropDown").toggle();
     $("#loadShiftsDayButton").toggleClass('active');
 });

 $(document).on("click","#loadShiftsDayBtn", function(event) {
     var selectedDate = $("#loadShiftsDatePicker").val();
     var currDateTime = getCalDate().getTime();
     var loadDayShiftsUrl = "./loadShiftsFromSelectedDay?currDateTime="+currDateTime+"&selectedDate="+selectedDate;
     $.ajax({
         url: loadDayShiftsUrl,
         async: false,
         dataType: 'json',
         success: function(data) {
             $.each(data,function(i,item){
                 addDayShiftToScheduler(item);
             })
         }
     })
 })

 function enableLoadShiftsFunctionality(){
    $("#loadWeekShiftsButton").popover({
        placement:'bottom',
        title:'Load shifts from a previous week',
        html:true,
        content:$("#loadWeekShiftsContent").html()
    })

    $("#loadDayShiftsButton").popover({
        placement:'bottom',
        title:'Load shifts from a previous day',
        html:true,
        content:$("#loadDayShiftsContent").html()
    })

    $(document).on("click","#loadShiftsWeekBtn", function(event) {
        var numWeeks = $("#loadWeek").val();
        var currWeekStartDateTime = getStartDate().getTime();
        var loadWeekShiftsUrl = "./loadShiftsFromPrevWeek?currWeekStartDateTime="+currWeekStartDateTime+"&numWeeks="+numWeeks;
        $.ajax({
            url: loadWeekShiftsUrl,
            async: false,
            dataType: 'json',
            success: function(data) {
                $.each(data,function(i,item) {
                    addWeekShiftToScheduler(item);
                })
            }
        });
    });

    $(".loadDayShiftInput").datepicker({
        inline: true,
        dateFormat: "dd/mm/yy",
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        selectOtherMonths:true
    });

    $(document).on("click",".loadDayShiftInput",function(){
        $("#ui-datepicker-div").css("z-index","5");
    })
 }

/*---------------------------------------
 * END OTHER CLICK OPERATIONS
 *---------------------------------------*/

 $(window).resize(function(){
     resizeUnavailabilities(planner.dayUnavailabilities);
 })

/*---------------------------------------
 * DRAG AND DROP
 *---------------------------------------*/

// $(document).on("drag",".draggable",function(){
//    dragShift = $(this);
// });
//
// $(document).on("drop",".positionShiftCell",function(){
//     //First we'll see if we need to do anything i.e if the user drops the element back in the same cell we don't need to do anything
//     if($(this).children().html())
//     {
//         $(this).children().each(function(){
//             console.log($(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() + " " + dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() );
//             if ( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() == dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() ) //This is the element that we have to move
//             {
//                 return;
//             }
//         }) //Go through each div element
//     }
//
//     //We will first change the database record and on success of that, we will make the visual change on the scheduler.
//     var shiftId = dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(0).html();
//     var personId = dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(1).html();
//     var positionId = dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(2).html();
//     var startTimeComponents = dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(3).html().split(" ")[1].split(":");
//     var finishTimeComponents = dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(4).html().split(" ")[1].split(":");
//     var dragShiftColIndex = $(this).parent().children().index($(this));
//     var transferToDateString = $(this).parent().parent().parent().children().eq(0).children().eq(0).children().eq(dragShiftColIndex-1).html();
//     var start = transferToDateString.split(" ")[2] + " " + convertMonthNameToMonthNumber(transferToDateString.split(" ")[1]) + " " + transferToDateString.split(" ")[3] + " " + startTimeComponents[0] + " " + startTimeComponents[1];
//     var finish = transferToDateString.split(" ")[2] + " " + convertMonthNameToMonthNumber(transferToDateString.split(" ")[1]) + " " + transferToDateString.split(" ")[3] + " " + finishTimeComponents[0] + " " + finishTimeComponents[1]; //This will have to change but for the time being we will keep it this way. This will have to change to the difference between the starting dates of the shifts
//
//     //We will then attempt to update the record on the server
//     var createOrUpdateShiftUrl = "/scheduler?operation=update&personId="+personId+"&positionId="+positionId+"&shiftId="+shiftId+"&startString="+start+"&finishString="+finish+"&recurrence="+"1";
//     $.ajax({
//         type:'POST',
//         url: createOrUpdateShiftUrl,
//         success: function(data)
//         {
//             $('#positionTable tbody tr').each(function(){
//                 $(this).children(".positionShiftCell").each(function(){
//                     if($(this).children())
//                     {
//                         $(this).children().each(function(){
//
//                             //console.log( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() + " " + data.id)
//                             if ( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() == data.id) //This is the element that we have to move
//                             {
//                                 copyShift = $(this);
//                                 //Then we need to move it to the same day as the shift in the positionView
//                                 $(this).parent().parent().children().eq(dragShiftColIndex).append(copyShift);
//                                 //Then we need to change the time contents of this element to the new data
//                                 $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(3).html(data.start);
//                                 $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(4).html(data.finish);
//                                 $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(5).html(data.dayOfWeek);
//                             }
//                         });
//                     }
//                 });
//             });
//
//             //Now we should change the position of the div element in the employeeView as well
//             //So we should first find the shift element using the shiftId in the table
//             $('#employeeTable tbody tr').each(function(){
//                 $(this).children(".personShiftCell").each(function(){
//                     if($(this).children())
//                     {
//                         $(this).children().each(function(){
//
//                             if ( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() == data.id) //This is the element that we have to move
//                             {
//                                 copyShift = $(this);
//                                 //Then we need to move it to the same day as the shift in the positionView
//                                 $(this).parent().parent().children().eq(dragShiftColIndex).append(copyShift);
//                                 //Then we need to change the time contents of this element to the new data
//                                 $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(3).html(data.start);
//                                 $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(4).html(data.finish);
//                                 $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(5).html(data.dayOfWeek);
//                             }
//
//                         }); //Go through each div element
//                     }
//                 });
//             });
//         }
//
//     });
// });
//
// $(document).on("drop",".personShiftCell",function(){
//     //First we'll see if we need to do anything i.e if the user drops the element back in the same cell we don't need to do anything
//     if($(this).children().html())
//     {
//         $(this).children().each(function(){
//             console.log($(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() + " " + dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() );
//             if ( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() == dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() ) //This is the element that we have to move
//             {
//                 return;
//             }
//         }) //Go through each div element
//     }
//
//     //We will first change the database record and on success of that, we will make the visual change on the scheduler.
//     var shiftId = dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(0).html();
//     var personId = dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(1).html();
//     var positionId = dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(2).html();
//     var startTimeComponents = dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(3).html().split(" ")[1].split(":");
//     var finishTimeComponents = dragShift.children().eq(0).children().eq(0).children().eq(0).children().eq(4).html().split(" ")[1].split(":");
//     var dragShiftColIndex = $(this).parent().children().index($(this));
//     var transferToDateString = $(this).parent().parent().parent().children().eq(0).children().eq(0).children().eq(dragShiftColIndex-1).html();
//     var start = transferToDateString.split(" ")[2] + " " + convertMonthNameToMonthNumber(transferToDateString.split(" ")[1]) + " " + transferToDateString.split(" ")[3] + " " + startTimeComponents[0] + " " + startTimeComponents[1];
//     var finish = transferToDateString.split(" ")[2] + " " + convertMonthNameToMonthNumber(transferToDateString.split(" ")[1]) + " " + transferToDateString.split(" ")[3] + " " + finishTimeComponents[0] + " " + finishTimeComponents[1]; //This will have to change but for the time being we will keep it this way. This will have to change to the difference between the starting dates of the shifts
//
//     //We will then attempt to update the record on the server
//     var createOrUpdateShiftUrl = "/scheduler?operation=update&personId="+personId+"&positionId="+positionId+"&shiftId="+shiftId+"&startString="+start+"&finishString="+finish+"&recurrence="+"1";
//     $.ajax({
//         type:'POST',
//         url: createOrUpdateShiftUrl,
//         success: function(data)
//         {
//             $('#employeeTable tbody tr').each(function(){
//                 $(this).children(".personShiftCell").each(function(){
//                     if($(this).children())
//                     {
//                         $(this).children().each(function(){
//
//                             //console.log( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() + " " + data.id)
//                             if ( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() == data.id) //This is the element that we have to move
//                             {
//                                 copyShift = $(this);
//                                 //Then we need to move it to the same day as the shift in the positionView
//                                 $(this).parent().parent().children().eq(dragShiftColIndex).append(copyShift);
//                                 //Then we need to change the time contents of this element to the new data
//                                 $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(3).html(data.start);
//                                 $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(4).html(data.finish);
//                                 $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(5).html(data.dayOfWeek);
//                             }
//                         });
//                     }
//                 });
//             });
//
//             //Now we should change the position of the div element in the employeeView as well
//             //So we should first find the shift element using the shiftId in the table
//             $('#positionTable tbody tr').each(function(){
//                 $(this).children(".positionShiftCell").each(function(){
//                     if($(this).children())
//                     {
//                         $(this).children().each(function(){
//
//                             if ( $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(0).html() == data.id) //This is the element that we have to move
//                             {
//                                 copyShift = $(this);
//                                 //Then we need to move it to the same day as the shift in the positionView
//                                 $(this).parent().parent().children().eq(dragShiftColIndex).append(copyShift);
//                                 //Then we need to change the time contents of this element to the new data
//                                 $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(3).html(data.start);
//                                 $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(4).html(data.finish);
//                                 $(this).children().eq(0).children().eq(0).children().eq(0).children().eq(5).html(data.dayOfWeek);
//                             }
//
//                         }); //Go through each div element
//                     }
//                 });
//             });
//         }
//
//     });
// });

/*---------------------------------------
 * END DRAG AND DROP
 *---------------------------------------*/




