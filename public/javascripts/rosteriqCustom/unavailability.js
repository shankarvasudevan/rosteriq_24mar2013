//Global variables

var unavailabilityData;
var unavailabilityBlock = [{}];

/*---------------------------------------------------------------------------
UNAVAILABILITY SELECTION CONTROL SECTION
---------------------------------------------------------------------------*/

$(document).on("change","#locationSelect",function(){
    //This will handle the function that handles the change of the location
    //It should bring in the persons for that location
    $.ajax({
        url: '/returnLocationPersons',
        async:false,
        dataType:'json',
        type:'POST',
        data:{
            locationId:$("#locationSelect").val()
        },
        success:function(data){
            //We'l then update the employee selection
            generateEmployeeSelection(data)
            renderUnavailabilityBlocks($("#employeeSelect").val())

        }
    })
})

$("#employeeSelect").change(function(){
    var employeeId = $(this).val();
    renderUnavailabilityBlocks(employeeId);
})

/*---------------------------------------------------------------------------
END UNAVAILABILITY SELECTION CONTROL SECTION
---------------------------------------------------------------------------*/


function renderUnavailabilityTable(unavailabilityBlockId)
{
    var thead = $("<thead>");
    var dayTitle = $("<th style=\"width:10%\">").text("Day").appendTo(thead);
    for (i=0; i!=24; ++i)
    {
        ext = (parseInt(i/12)>0?"p":"a")
        numberDisplay = ((i/12)>0?((i%12)==0?12:(i%12)):((i%12)==0?12:(i%12)))
        $("<th style=\"width:3.5%\">").text(numberDisplay+ext).appendTo(thead);
    }
    $("#unavailabilityHeader").append(thead);
    //We will iterate through the position JSON array
    $("#unavailabilityTable tbody").remove();
    var unavailabilityTableBody  = $("<tbody>");
    unavailabilityTableBody.appendTo($("#unavailabilityTable"));

    days = [
        {"dayName":"Sun","dayOfWeek":"1"},
        {"dayName":"Mon","dayOfWeek":"2"},
        {"dayName":"Tue","dayOfWeek":"3"},
        {"dayName":"Wed","dayOfWeek":"4"},
        {"dayName":"Thu","dayOfWeek":"5"},
        {"dayName":"Fri","dayOfWeek":"6"},
        {"dayName":"Sat","dayOfWeek":"7"},
    ];

    $.each(days,function(i,day){
        var dayRow = $("<tr>");
        var dayOfWeek = $("<td class=\"hidden dayOfWeek\">").text(day.dayOfWeek).appendTo(dayRow);
        var dayName = $("<td style=\"width:10%\" class=\"dayName\">").text(day.dayName).appendTo(dayRow);
        //var clearUnavailabilityOption = $("<div class=\"clearClick pull-right btn\">X</div>").appendTo(dayName);
        var allDayOption = $("<div class=\"allDayClick pull-right btn\"><i class=\"icon-repeat\"></i></div>").appendTo(dayName);
        var dayUnavailabilityCell = $("<td style=\"width:84%\" id=\"" + day.dayOfWeek + "\" class=\"unavailabilityCell\">").appendTo(dayRow);
        var dayHasUnavailability = $("<td class=\"hidden hasUnavailability\">").text('false').appendTo(dayRow);
        dayRow.appendTo(unavailabilityTableBody);
//        for (i=0; i!=24; ++i)
//        {
//            var hourCell = $("<td class=\"hourCell\" style=\"width:3.75%\">").appendTo(dayRow);
//            hourCell.attr('id',getDayOfWeek(hourCell)+","+i);
//        }
//        dayRow.appendTo(unavailabilityTableBody);
    });
    $("#unavailabilityBlockId").text(unavailabilityBlockId);
    //If an unavailabilityBlock has been provided ( which means we're editing an unavailabilityBlock ), then we'll pull down the information, otherwise we've completed this operation
    if( unavailabilityBlockId > 0 ) //Because if we're creating a new unavailability the passed in value is -1
    {
        //This means we're editing a shift
        //So we should first pull down the unavailabilities for the unavailability block
        $.ajax({
            url: 'getUnavailabilityBlock',
            type:'POST',
            async: 'false',
            dataType: 'json',
            data:{
                unavailabilityBlockId: unavailabilityBlockId
            },
            success: function(data){
                //This function was moved inside as the global variable unavailabilityBlock
                //Now we'll start filling in the information in each of the input elements
                //First we'll set the title
                $("#unavailabilityTitle").html("Edit unavailability for: " + data.personName );
                $("#title").val(data.title);
                $("#description").val(data.description);
                $("#startDatePicker").val(getSimpleDate(data.startDate));
                $("#endDatePicker").val(getSimpleDate(data.endDate));
                //So till this point we should have entered all in the data for the unavailabilityBlock
                //Now we fill edit the unavailability edit table
                //First we'll put in the sliders for each day
                $("#unavailabilityTable tbody tr").each(function(){
                    var unavailabilitySlider = $('<div class=\"slider\">').dragSlider({
                        animate:true,
                        range: true,
                        rangeDrag:true,
                        min: 0,
                        max: 1440,
                        step:15,
                        values: [0,0],
                        create: function(event,ui){
                            $(this).children().eq(0).html("");
                        },
                        slide: function(event, ui) {
                            if(ui.values[0] == 0 && ui.values[1] == 1440 )
                            {
                                //Then we'll set the text to all day
                                $(this).children().eq(0).html("All day");
                                //Then we'll set the button to pressed state
                                $(this).parent().parent().children().eq(1).children().eq(0).addClass("active");
                            }
                            else
                            {
                                $(this).children().eq(0).html(getTimeRangeOption(convertToHours(ui.values[0])+"-"+ convertToHours(ui.values[1]),getTimeRangeOptionValues(),getTimeRangeOptions()));
                                //And we'll remove the pressed state from the button if it has been set
                                if ($(this).parent().parent().children().eq(1).children().eq(0).hasClass("active"))
                                    $(this).parent().parent().children().eq(1).children().eq(0).removeClass("active");
                            }
                        }
                    });
                    $(this).children().eq(2).append(unavailabilitySlider);
                    (unavailabilitySlider.children().eq(0)).addClass("btn-grey");
                });
                //Then we'll put each unavailability
                $.each(data.unavailabilities, function(i, unavailability){
                    //So we'll go through all the rows in the table and find the right row
                    $("#unavailabilityTable tbody tr").each(function(){
                        if ( $(this).children().eq(0).html() == unavailability.dayOfWeek )  // need to put a check in here that the unavailability hasn't already been displayed.
                        {
                            //Then we have found the right row
                            //We will then set the values of the slider in this row
                            $(this).find(".slider").dragSlider("values",0,convertToValue(unavailability.startTime))
                            $(this).find(".slider").dragSlider("values",1,convertToValue(unavailability.endTime))
                            $(this).find(".btn-grey").text(getTimeRangeOption(unavailability.startTime+"-"+unavailability.endTime,getTimeRangeOptionValues(),getTimeRangeOptions()))
                            if ( unavailability.startTime == "00:00" && unavailability.endTime == "23:59" )
                            {
                                //Then we'll set the allDay button to set
                                $(this).find(".allDayClick").addClass("active");
                                $(this).find(".btn-grey").text("All day")
                            }
                        }
                    });
                })
            }
        });
    }
    else
    {
        //This means we are creating a new unavailability block
        $("#unavailabilityTitle").html("New unavailability for: " + $("#employeeSelection option:selected").text() );
        //If we are not editing an existing unavailability block we'll just add empty sliders
        $("#unavailabilityTable tbody tr").each(function(){
            var unavailabilitySlider = $('<div class=\"slider\">').dragSlider({
                animate:true,
                range: true,
                rangeDrag:true,
                min: 0,
                max: 1440,
                step:15,
                values: [0,0],
                create: function(event,ui){
                    $(this).children().eq(0).html("");
                },
                slide: function(event, ui) {
                    if(ui.values[0] == 0 && ui.values[1] == 1440 )
                    {
                        //Then we'll set the text to all day
                        $(this).children().eq(0).html("All day");
                        //Then we'll set the button to pressed state
                        $(this).parent().parent().children().eq(1).children().eq(0).addClass("active");
                    }
                    else
                    {
                        $(this).children().eq(0).html(getTimeRangeOption(convertToHours(ui.values[0])+"-"+ convertToHours(ui.values[1]),getTimeRangeOptionValues(),getTimeRangeOptions()));
                        //And we'll remove the pressed state from the button if it has been set
                        if ($(this).parent().parent().children().eq(1).children().eq(0).hasClass("active"))
                            $(this).parent().parent().children().eq(1).children().eq(0).removeClass("active");
                    }
                }
            });
            $(this).children().eq(2).append(unavailabilitySlider);
            (unavailabilitySlider.children().eq(0)).addClass("btn-grey");
        });
    }
}

$(document).on("click",".allDayClick",function(){
    //This will set the slider range to full length for that day or remove the same
    //So we'll first see if the slider is set to the full day
    if ( $(this).hasClass("active"))
    {
        //This means that the slider has been set to the full day and if the user is clicking the button again, he wants to take it off
        var sliderElement = $(this).parent().parent().children().eq(2).children().eq(0);
        //console.log(sliderElement); //This is to check if we've got the right element
        //Then we'll set the values of the slider to 0 and 1440 which are the min and max values of the slider
        sliderElement.dragSlider("values",0,0);
        sliderElement.dragSlider("values",1,0);
        sliderElement.children().eq(0).html("");
        $(this).removeClass("active");
    }
    else
    {
        //This means that the slider isn't set and we want to the set the slider to the full day
        var sliderElement = $(this).parent().parent().children().eq(2).children().eq(0);
        //console.log(sliderElement); //This is to check if we've got the right element
        //Then we'll set the values of the slider to 0 and 1440 which are the min and max values of the slider
        sliderElement.dragSlider("values",0,0);
        sliderElement.dragSlider("values",1,1440);
        sliderElement.children().eq(0).html("All day");
        $(this).addClass("active");
        //console.log(sliderElement.slider("values",0));
    }
}) ;


$(document).on("click",".clearClick",function() {
    var sliderElement = $(this).parent().parent().children().eq(2).children().eq(0);
    //console.log(sliderElement); //This is to check if we've got the right element
    //Then we'll set the values of the slider to 0 and 1440 which are the min and max values of the slider
    sliderElement.dragSlider("values",0,0);
    sliderElement.dragSlider("values",1,0);
    sliderElement.children().eq(0).html("");
    $(this).removeClass("active");
});



//$(document).on("click",".hourCell",function(){
//    //alert("We will create the div here at hourCell: " + $(this).attr('id')); //To test if the click was being created in the right spot
//    //This will create the unavailability div for the person
//    var unavailabilityDiv = $("<div class=\"unavailabilityDiv btn btn-primary\">");
//    //The div will be attached to the top and left of the current cell
//    unavailabilityDiv.appendTo($("body"));
//    unavailabilityDiv.css({
//        'height':$(this).height()+6,
//        'width':'200'
//    });
//    unavailabilityDiv.position({
//        at:"left top",
//        my: "left top",
//        of: $(this),
//        offset: "0 0"
//    })
//    unavailabilityDiv.resizable({
//        grid: [50,0],
//        handles: "e,w"
//    })
//
//})

//
//$(document).on("click",".hourCell",function(){
//    if ( $(this).css('background-color') == "rgb(179, 198, 255)")
//        $(this).css('background-color',"rgb(255,255,255)");
//    else
//        $(this).css('background-color',"rgb(179, 198, 255)");
//})


function getDayOfWeek(hourCell)
{
    return hourCell.parent().children().eq(0).html();
}

//Now we'll work on the rendering of unavailabilities for a particular unavailability block
//So I guess all this information should be loaded in the first table so that it can be used as references

$(document).on("click",".editBlock",function(event){
    event.stopPropagation();
    //We first need to get the blockId and the personId of the block
    event.stopPropagation();
    var blockId = $(this).parent().parent().children().eq(0).html();
    renderUnavailabilityTable(blockId);
    $(".unavailabilityCreationContent").show(); // to make the block editable
    $(".unavailabilityCreationRow").show();
    $(".unavailabilityListRow").hide();
    $(".firstRow").hide();
})

$(document).on("click",".deleteBlock",function(event){
    event.stopPropagation();
    //We first need to get the blockId and the personId of the block
    event.stopPropagation();
    var blockId = $(this).parent().parent().children().eq(0).html();
    //Then we'll delete the unavailabilityBlock
    $.ajax({
        type: 'POST',
        url: '/unavailability',
        data:{
            operation:'delete',
            unavailabilityBlockId:blockId
        },
        async: false,
        dateType: 'json',
        success: function(){
            //Once we have deleted the records in the database, we will refresh the view
            window.location = '/unavailability'
        }
    })
})

$(document).on("click",".unavailabilityBlockRow",function(){
    event.stopPropagation();
    var blockId = $(this).children().eq(0).html();
    renderUnavailabilityTable(blockId);
    $(".unavailabilityCreationRow").show();
    $(".unavailabilityListRow").hide();
    $(".firstRow").hide();
})

$(document).on("click",'.saveUnavailabilityBlock',function(){
    //This will be the main function that saves the unavailability
    //So we'll list all that the function should do
    //Get the employee
    var employeeId = $("#employeeSelect").val();
    //Get all the information from the page like title, description etc.
    var title = $("#title").val();
    var description = $("#description").val();
    //Get the start date and end date in the format of 11-8-2-2012 which is the date standard on the javascript side of things
    var startDateComponents = $("#startDatePicker").val().split(" ");
    var startDateString = startDateComponents[0] + "-" + convertMonthNameToMonthNumber(startDateComponents[1]) + "-" + startDateComponents[2];
    var endDateComponents = $("#endDatePicker").val().split(" ");
    var endDateString = endDateComponents[0] + "-" + convertMonthNameToMonthNumber(endDateComponents[1]) + "-" + endDateComponents[2];
    //We should then store all the unavailabilities in an array
    var unavailabilities = []; //This is the empty array that we will fill in
    var count=0;
    //Then the function should go through all the day unavailabilities and save them as dayOfWeek-startTime-endTime ( this will be inside the array object
    $("#unavailabilityTable tr").each(function(){
        //We'll go through all the table rows
        //And we'll get the shift slider values and put them into a JSON object
        var dayOfWeek = $(this).children().eq(0).html();
        $(this).children().eq(2).children().each(function(){
            //console.log($(this));//This is to test if we're iterating through the right objects
            //First we have to check if there is an unavailability
            if($(this).dragSlider("values",1) != 0){
                var unavailability = dayOfWeek+"-"+convertToHours($(this).dragSlider("values",0))+"-"+convertToHours($(this).dragSlider("values",1));
                //console.log(unavailability);
                unavailabilities[count] = unavailability;
                ++count;
            }
        })
    })

    var blockId = $("#unavailabilityBlockId").text();

    //console.log(unavailabilities); // This was to check if it the unavailabilities were getting stored correctly
    //Pass the array object to the backend where it will unpacked and applied to the database.
    $.ajax({
        type:'POST',
        url: '/unavailability',
        data:{
            operation: (blockId==-1?'new':'update'),
            unavailabilityBlockId: blockId,
            employeeId: employeeId,
            title: title,
            description: description,
            startDateString: startDateString,
            endDateString: endDateString,
            unavailabilitiesString: unavailabilities.toString()
        },
        success: function()
        {
            window.location = '/unavailability'
        }
    });
    return true;
})

$(document).on("click","#closeUnavailabilityBlock",function(){
    //We'll delete the creationTable
    $("#unavailabilityHeader thead").remove();
    $("#unavailabilityTable tbody").remove();
    //Then we'll show the main user screen again
    renderUnavailabilityBlocks($("#employeeSelection").val());
    $(".unavailabilityCreationRow").hide();
    $(".unavailabilityListRow").show();
    $(".firstRow").show()
})

function renderUnavailabilityBlocks(employeeId)
{
    //This will generate the unavailability block tables for an employee
    //We'll first remove the existing the heading row
    $("#unavailabilityBlocksTable thead").remove();
    var thead = $("<thead>");
    var title = $("<th style=\"width:40%\">").text("Title").appendTo(thead);
    var range = $("<th style=\"width:40%\">").text("Range").appendTo(thead);
    var modify = $("<th style=\"width:20%\">").text("Modify").appendTo(thead);
    $("#unavailabilityBlocksTable").append(thead);
    $("#unavailabilityBlocksTable tbody").remove();
    var unavailabilityBlocksTableBody  = $("<tbody>");
    unavailabilityBlocksTableBody.appendTo($("#unavailabilityBlocksTable"));
    $.ajax({
        url: '/getEmployeeUnavailabilityBlocks',
        type:'POST',
        async: false,
        dataType: 'json',
        data:{
            employeeId: $("#employeeSelect").val()
        },
        success: function(data){
            //So here is when we've got the unavailability blocks for that user
            var count = 0;
            $.each(data,function(i,unavailabilityBlock){
                ++count;
            })
            if(count>0) //If there are unavailabilityBlocks
            {
                $.each(data,function(i,unavailabilityBlock){
//                    console.log(unavailabilityBlock);
                    var unavailabilityBlockTableRow = $("<tr class=\"unavailabilityBlockRow\">");
                    var unavailabilityBlockId = $("<td class=\"hidden\">").text(unavailabilityBlock.id).appendTo(unavailabilityBlockTableRow);
                    var unavailabilityBlockId = $("<td class=\"hidden\">").text(unavailabilityBlock.personId).appendTo(unavailabilityBlockTableRow);
                    var unavailabilityBlockTitle = $("<td>").text(unavailabilityBlock.title).appendTo(unavailabilityBlockTableRow);
                    var unavailabilityBlockRange = $("<td>").text(convertToRange(unavailabilityBlock.startDate, unavailabilityBlock.endDate)).appendTo(unavailabilityBlockTableRow);
                    var unavailabilityBlockModifyButtons = $("<td>");
                    var editButton = $("<div style=\"margin-left:5px\" class=\"btn editBlock\">").html("<i class=\"icon-pencil\"></i>").appendTo(unavailabilityBlockModifyButtons);
                    var deleteButton = $("<div style=\"margin-left:5px\" class=\"btn btn-danger deleteBlock\">").html("<i class=\"icon-minus icon-white\"></i>").appendTo(unavailabilityBlockModifyButtons);
                    unavailabilityBlockModifyButtons.appendTo(unavailabilityBlockTableRow);
                    unavailabilityBlockTableRow.appendTo(unavailabilityBlocksTableBody);
                })
            }
            else
            {
                //We will add an empty row which spans the 3 columns and says that there are no unavailabilityBlocks for this user
                var unavailabilityBlockTableRow = $("<tr>");
                var unavailabilityBlockEmpty = $("<td style=\"text-align:middle\" colspan=\"3\">").text("No unavailabilities for this user").appendTo(unavailabilityBlockTableRow);
                unavailabilityBlockTableRow.appendTo(unavailabilityBlocksTableBody);
            }
            $(".unavailabilityListRow").show();
            $(".firstRow").show();
        }
    });
}

$(document).on("click","#createNewUnavailability",function(){
    renderUnavailabilityTable(-1);
    $(".unavailabilityCreationRow").show();
    $(".unavailabilityListRow").hide();
    $(".firstRow").hide();
    //Then we need to clear the content from the inputs
    $("#title").val("");
    $("#description").val("");
    $("#title").val("");
    $("#startDatePicker").val("");
    $("#endDatePicker").val("");
})

function generateEmployeeSelection(employees)
{
    $("#employeeSelect").empty();
    $.each(employees,function(i,employee){
        var option = $("<option></option>").attr("value",employee.id).text( employee.firstName + " " +  employee.lastName );
        $("#employeeSelect").append(option);
    })
}

$(document).ready(function(){
    $('#startDatePicker').datepicker({
        inline: true,
        dateFormat: "dd MM yy",
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        selectOtherMonths:true
    });

    $('#endDatePicker').datepicker({
        inline: true,
        dateFormat: "dd MM yy",
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        selectOtherMonths:true
    });
    $(document).on("click","#endDatePicker",function(){
        $("#ui-datepicker-div").css("z-index","5");
    })

    $(document).on("click","#startDatePicker",function(){
        $("#ui-datepicker-div").css("z-index","5");
    })
    renderUnavailabilityBlocks($("#employeeSelect").val());
    $(".unavailabilityCreationRow").hide();
    $("#unavailabilityLink").addClass("active");
    $("#unavailabilityLink").children().eq(0).removeClass("icon-white");
    setTimeZoneOffset();

})