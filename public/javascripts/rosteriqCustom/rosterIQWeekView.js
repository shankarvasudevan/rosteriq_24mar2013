$("#weekPrev").click(function() {
    $("#loadWait").modal('show');
    var oldDate = getStartDate();
    //We'll then remove all the event handlers for the current cells and shifts
    removeTableEventHandlers();
    setStartDate( oldDate.getTime() - (86400000*7) );
    setCalDate( getStartDate() );
    generateScheduler();
    $("#loadWait").modal('hide');
});

$("#weekCurr").click(function() {
    $("#loadWait").modal('show');
    var newDate = new Date();
    setCalDate(newDate.getTime());
    setStartDate ( getPrevMonday(getCalDate()).getTime() );
    //We'll then remove all the event handlers for the current cells and shifts
    removeTableEventHandlers();
    generateScheduler();
    $("#loadWait").modal('hide');
});

$("#weekNext").click(function() {
    $("#loadWait").modal('show');
    var oldDate = getStartDate();
    //We'll then remove all the event handlers for the current cells and shifts
    removeTableEventHandlers();
    setStartDate(oldDate.getTime() + (86400000*7));
    setCalDate(getStartDate());
    generateScheduler();
    $("#loadWait").modal('hide');
});

$(document).on("click","#weekDayViewClick",function(){
    if (!($(this).hasClass("active")) )
    {
        setDayOrWeekView("day");
        $("#dayDayViewClick").addClass("active");
        $("#dayWeekViewClick").removeClass("active")
        //Then we'll decide the view depending on the schedulerView click
        if ( getSchedulerView() == "position")
        {
            //We'll first see if the tab is already set, because in that case we won't have to do anything
            if ( ! ( $("#dayPositionViewClick").hasClass("active") ) )
            {
                renderDayPositionView();
                $("#dayPositionView").addClass("in active");
                $("#dayEmployeeView").removeClass("in active");
            }
            else
            {
                //Everything is already set correctly
                setSchedulerView("position");
            }
        }
        else
        {
            //If the schedulerView is EmployeeView
            //We'll first see if the tab is already set, because in that case we won't have to do anything
            if ( ! ( $("#dayEmployeeViewClick").hasClass("active") ) )
            {
                renderDayEmployeeView();
                $("#dayEmployeeView").addClass("in active");
                $("#dayPositionView").removeClass("in active");
            }
            else
            {
                //Everything is already set correctly
                setSchedulerView("employee");
            }
        }
    }

})

function renderWeekPositionView()
{
    if (!($("#weekPositionViewClick").hasClass("active")) )
    {
        $("#weekPositionViewClick").addClass("active");
        setSchedulerView("position");
        $("#weekEmployeeViewClick").removeClass("active");
    }
    renderShiftsAndUnavailabilitiesForView()
}

function renderWeekEmployeeView()
{
    if (!($("#weekEmployeeViewClick").hasClass("active")) )
    {
        $("#weekEmployeeViewClick").addClass("active");
        setSchedulerView("employee");
        $("#weekPositionViewClick").removeClass("active");
    }
    renderShiftsAndUnavailabilitiesForView()
}

$(document).on("click","#weekPositionViewClick",function(){
    renderWeekPositionView();
});

$(document).on("click","#weekEmployeeViewClick",function(){
    renderWeekEmployeeView();
});

function removeTableEventHandlers()
{
    $(document).off("dblclick",".personShiftCell");
    $(document).off("dblclick",".personShift");
    $(document).off("click",".personShift");
    $(document).off("dblclick",".positionShiftCell");
    $(document).off("dblclick",".positionShift");
    $(document).off("click",".positionShift");
}

/*---------------------------------------------------------------------------
RENDER SHIFTS TO THE WEEK VIEW
---------------------------------------------------------------------------*/

function renderEmployeeWeekShifts(shifts){
    $.each(shifts, function(i,shift){
        $('#employeeTable tbody tr').each(function(){
            //We'll first find the employee that it is related to
            $(this).each(function(){
                if ( $(this).children().eq(0).html() == shift.personId )
                {
                    // We know the row that this shift is in
                    var empName = $(this).children().eq(1);
                    var costDayDiv = empName.children().eq(0);
                    var costWkDiv = empName.children().eq(1);
                    //Once we have found the employee it belongs to, we will find the day it belongs to
                    //Before that we will create the div element that we will put in
                    var shiftElement = $("<div class=\"btn btn-primary personShift\">");
                    if ( shift.draft )
                        shiftElement.addClass("draftShift");
                    else
                    {
                        shiftElement.addClass("publishedShift");
                        if ( shift.confirmed )
                        {
                                shiftElement.removeClass("btn-primary");
                                shiftElement.addClass("btn-success");
                        }
                    }
                    //We will then store the shift properties in a hidden table
                    var shiftHiddenTable = $("<table style=\"display:none\">")
                    var shiftHiddenTableRow = $("<tr>");
                    var hiddenShiftId = $("<td class=\"shiftId\">").text(shift.id).appendTo(shiftHiddenTableRow);
                    var hiddenPersonId = $("<td class=\"personId\">").text(shift.personId).appendTo(shiftHiddenTableRow);
                    var hiddenPositionId = $("<td class=\"positionId\">").text(shift.positionId).appendTo(shiftHiddenTableRow);
                    var hiddenShiftStart = $("<td class=\"start\">").text(shift.start).appendTo(shiftHiddenTableRow);
                    var hiddenShiftFinish = $("<td class=\"finish\">").text(shift.finish).appendTo(shiftHiddenTableRow);
                    var hiddenShiftDayOfWeek = $("<td class=\"dayOfWeek\">").text(shift.dayOfWeek).appendTo(shiftHiddenTableRow);
                    var hiddenShiftDraftStatus = $("<td class=\"draft\">").text(shift.draft).appendTo(shiftHiddenTableRow);
                    var hiddenShiftSeriesStatus = $("<td class=\"shiftSeriesId\">").text(shift.shiftSeriesId).appendTo(shiftHiddenTableRow);
                    shiftHiddenTableRow.appendTo(shiftHiddenTable);
                    shiftHiddenTable.appendTo(shiftElement);
                    //Then we'll add the visual content
                    //We'll first extract the start and finish time from the given times
                    var startTime = ((shift.start).split(" "))[1];
                    var finishTime = ((shift.finish).split(" "))[1];
                    var shiftPositionTitle = $("<b>"+shift.positionTitle+"</b>");
                    var shiftTime = $("<p>"+getTimeRangeOption(startTime+"-"+finishTime,getTimeRangeOptionValues(),getTimeRangeOptions())+"</p>");
                    shiftPositionTitle.appendTo(shiftElement);
                    shiftTime.appendTo(shiftElement);
                    getDay($(this),(shift.dayOfWeek)).append(shiftElement);
                }
            })
        })
    })
    //Set the boolean
    setEmployeeWeekShiftsSet()
}

function renderPositionWeekShifts(shifts){
    $.each(shifts,function(i,shift){
        $('#positionTable tbody tr').each(function(){
            //We'll first find the position that it is related to
            $(this).each(function(){
                if ( $(this).children().eq(0).html() == shift.positionId )
                {
                    //Once we have found the employee it belongs to, we will find the day it belongs to
                    //Before that we will create the div element that we will put in
                    var posTitle = $(this).children().eq(1);
                    var costDayDiv = posTitle.children().eq(0);
                    var costWkDiv = posTitle.children().eq(1);

                    var shiftElement = $("<div class=\"btn btn-primary positionShift\">");
                    if ( shift.draft )
                        shiftElement.addClass("draftShift");
                    else
                    {
                        shiftElement.addClass("publishedShift");
                        if ( shift.confirmed )
                        {
                                shiftElement.removeClass("btn-primary");
                                shiftElement.addClass("btn-success");
                        }
                    }
                    //We will then store the shift properties in a hidden table
                    var shiftHiddenTable = $("<table style=\"display:none\">")
                    var shiftHiddenTableRow = $("<tr>");
                    var hiddenShiftId = $("<td class=\"shiftId\">").text(shift.id).appendTo(shiftHiddenTableRow);
                    var hiddenPersonId = $("<td class=\"personId\">").text(shift.personId).appendTo(shiftHiddenTableRow);
                    var hiddenPositionId = $("<td class=\"positionId\">").text(shift.positionId).appendTo(shiftHiddenTableRow);
                    var hiddenShiftStart = $("<td class=\"start\">").text(shift.start).appendTo(shiftHiddenTableRow);
                    var hiddenShiftFinish = $("<td class=\"finish\">").text(shift.finish).appendTo(shiftHiddenTableRow);
                    var hiddenShiftDayOfWeek = $("<td class=\"dayOfWeek\">").text(shift.dayOfWeek).appendTo(shiftHiddenTableRow);
                    shiftHiddenTableRow.appendTo(shiftHiddenTable);
                    shiftHiddenTable.appendTo(shiftElement);
                    //Then we'll add the visual content
                    //We'll first extract the start and finish time from the given times
                    var startTime = ((shift.start).split(" "))[1];
                    var finishTime = ((shift.finish).split(" "))[1];
                    var shiftPersonName = $("<b>"+shift.personName+"</b>");
                    var shiftTime = $("<p>"+getTimeRangeOption(startTime+"-"+finishTime,getTimeRangeOptionValues(),getTimeRangeOptions())+"</p>");
                    shiftPersonName.appendTo(shiftElement);
                    shiftTime.appendTo(shiftElement);
                    getDay($(this),shift.dayOfWeek).append(shiftElement);
                }
            })
        })
    })
    setPositionWeekShiftsSet()
}

/*---------------------------------------------------------------------------
END RENDER SHIFTS TO THE WEEK VIEW
---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
RENDER UNAVAILABILITIES TO THE WEEK VIEW
---------------------------------------------------------------------------*/

function renderEmployeeWeekUnavailabilities(unavailabilities)
{
    //Now this function will render the unavailabilities in the view
    $.each(unavailabilities,function(i,unavailability){
        //We'll find the person the unavailability belongs to
        $('#employeeTable tbody tr').each(function(){
            //We'll first find the employee that it is related to
            $(this).each(function(){
                if ( $(this).children().eq(0).html() == unavailability.personId )
                {
                    if(!(unavailability.startTime == "00:00" && unavailability.endTime == "00:00"))
                    {
                        //Once we've found the employee we'll add it to the unavailabilities hidden table
                        var unavailabilityDiv = $("<div>");
                        unavailabilityDiv.html(unavailability.startTime + "-" + unavailability.endTime);
                        getDay($(this),unavailability.dayOfWeek).children().eq(0).children().eq(1).append(unavailabilityDiv);
                        if(unavailability.startTime=="00:00" && unavailability.endTime=="23:59")
                        {
                            getDay($(this),unavailability.dayOfWeek).addClass("unavailable");
                            getDay($(this),unavailability.dayOfWeek).html("<div class=\"unavailabilityDescription\">Unavailable<br/><i>" + unavailability.title + "</i></div>");
                        }
                        else
                        {
                            //We need to figure out how to show partial unavailability
                        }
                    }
                }
            })
        })
    })
    //Set the boolean
    setEmployeeWeekUnavailabilitiesSet()
}

/*---------------------------------------------------------------------------
END RENDER UNAVAILABILITIES TO THE WEEK VIEW
---------------------------------------------------------------------------*/