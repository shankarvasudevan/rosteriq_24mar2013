/* MyRoster.js
----------------------
Version: 1.0
Author: Prathamesh Datar
Company: RosterIQ

Summary: This javascript file will contain the methods which will be used by the requests page of the program.
The requests page of the program will show the user the requests that other users have sent to the logged in user
The user can then respond to the request

----------------------*/

function renderUserDropRequestsToPerson()
{
    var getUserDropRequestsToPersonUrl = "./getUserDropRequestsToPerson.json";
    $.ajax({
        url:getUserDropRequestsToPersonUrl,
        async:false,
        dataType:'json',
        success:function(data) {
            $("#dropRequestListTable tbody").remove();
            var requestListTableBody = $("<tbody>").appendTo("#dropRequestListTable");
            $.each(data,function(i,item){
                var requestRow = $("<tr id=\"" + item.id + "\">");
                var fromDateComponents = (item.fromStrStart).split(" ")[0].split("-");
                var fromTimeStrStart= (item.fromStrStart).split(" ")[1];
                var fromTimeStrFinish= (item.fromStrFinish).split(" ")[1];
                var fromDayOfWeek = (item.fromDayOfWeek);
                var fromShiftId = $("<td class=\"fromShiftId hidden\">").text(item.fromShiftId).appendTo(requestRow);
                var fromShift = $("<td class=\"fromShift\">").text(convertToSwapDate(fromDateComponents, fromTimeStrStart, fromTimeStrFinish, fromDayOfWeek)).appendTo(requestRow);
                var fromPersonName = $("<td class=\"fromPersonName\">").text(item.fromPersonName).appendTo(requestRow);
                var description = $("<td class=\"description\">").text(item.description).appendTo(requestRow);
                var actionButtons = $("<td class=\"dropActionButtons\">").appendTo(requestRow);
                if ( item.status == "AWAITING_APPROVAL" )
                {
                    var confirmSwapButton = $("<div class=\"btn-group approveDropButtonGroup pull-left\"><a class=\"btn btn-success approveDropButton\"><icon class=\"icon-ok-circle icon-white\"></icon></a></div>").appendTo(actionButtons);
                    var declineSwapButton = $("<div class=\"btn-group rejectDropButtonsGroup pull-left\"><a class=\"btn btn-danger rejectDropButton\"><icon class=\"icon-remove-circle icon-white\"></icon></a></div>").appendTo(actionButtons);
                }
                else
                actionButtons.html(item.status);
                requestListTableBody.append(requestRow);
            })
        }
    });
}

function renderUserSwapRequestsToPerson()
{
    var getUserSwapRequestsToPersonUrl = "./getUserSwapRequestsToPerson.json";
    $.ajax({
        url:getUserSwapRequestsToPersonUrl,
        async:false,
        dataType:'json',
        success:function(data){
            //We've received the requests for this user.
            //We will now show the same in the requests Table
            //We'll first empty the current records in the table
            $("#swapRequestListTable tbody").remove();
            var requestListTableBody = $("<tbody>").appendTo("#swapRequestListTable");
            $.each(data,function(i,item){
                var requestRow = $("<tr id=\"" + item.id + "\">");
                var fromDateComponents = (item.fromStrStart).split(" ")[0].split("-");
                var fromTimeStrStart= (item.fromStrStart).split(" ")[1];
                var fromTimeStrFinish= (item.fromStrFinish).split(" ")[1];
                var fromDayOfWeek = (item.fromDayOfWeek);
                var toDateComponents = (item.toStrStart).split(" ")[0].split("-");
                var toTimeStrStart= (item.toStrStart).split(" ")[1];
                var toTimeStrFinish = (item.toStrFinish).split(" ")[1];
                var toDayOfWeek = (item.toDayOfWeek);
                var fromShiftId = $("<td class=\"fromShiftId hidden\">").text(item.fromShiftId).appendTo(requestRow);
                var toShiftId = $("<td class=\"toShiftId hidden\">").text(item.toShiftId).appendTo(requestRow);
                var fromShift = $("<td class=\"fromShift\">").text(convertToSwapDate(fromDateComponents, fromTimeStrStart, fromTimeStrFinish, fromDayOfWeek)).appendTo(requestRow);
                var toShift = $("<td class=\"toShift\">").text(convertToSwapDate(toDateComponents, toTimeStrStart, toTimeStrFinish, toDayOfWeek)).appendTo(requestRow);
                var toPersonName = $("<td class=\"fromPersonName\">").text(item.fromPersonName).appendTo(requestRow);
                var description = $("<td class=\"description\">").text(item.description).appendTo(requestRow);
                var actionButtons = $("<td class=\"actionButtons\">").appendTo(requestRow);
                if ( item.response == "PENDING" )
                {
                    var confirmSwapButton = $("<div class=\"btn-group confirmDropButtonGroup pull-left\"><a class=\"btn btn-success confirmSwapButton\"><icon class=\"icon-ok-circle icon-white\"></icon></a></div>").appendTo(actionButtons);
                    var declineSwapButton = $("<div class=\"btn-group declineDropButtonsGroup pull-left\"><a class=\"btn btn-danger declineSwapButton\"><icon class=\"icon-remove-circle icon-white\"></icon></a></div>").appendTo(actionButtons);
                }
                else
                    actionButtons.html(item.response);
                requestListTableBody.append(requestRow);
            })
        }
    })
}

$(document).on("click",".approveDropButton", function() {
    var requestId = getRequestId($(this));
    var executeShiftDropUrl = "./executeShiftDrop.json?requestId=" + requestId;
    $.ajax({
        url: executeShiftDropUrl,
        async:false,
        dataType:'json',
        success:function(data) {
            $("#" + data.id).find(".dropActionButtons").html(data.status);
        }
    })
    alert("This shift will now be in the scheduler as draft mode");
})

$(document).on("click",".rejectDropButton", function() {
    var requestId = getRequestId($(this));
    var rejectDropShiftUrl = "./rejectShiftDrop.json?requestId=" + requestId;
    $.ajax({
        url : rejectDropShiftUrl,
        async:false,
        dataType:'json',
        success:function(data) {
            $("#" + data.id).find(".dropActionButtons").html(data.status);
        }
    })
})

$(document).on("click",".confirmSwapButton",function(){
    //This will confirm the swap shift
    //Let's get the shiftIds
  //  var myShiftId = getMyShiftId($(this));     //helper function
  //  var swapShiftId = getSwapShiftId($(this));
    var requestId = getRequestId($(this));
    var executeShiftSwapUrl = "./executeShiftSwap.json?requestId=" + requestId;
    $.ajax({
        url: executeShiftSwapUrl,
        async:false,
        dataType:'json',
        success:function(data){
            //This will remove the request row from the table
            $("#" + data.id).find(".actionButtons").html("data.response");
        }
    })
})

$(document).on("click",".declineSwapButton",function(){
    //This will decline the swap shift
    //Let's get the shiftIds
  //  var myShiftId = getMyShiftId($(this));     //helper function
  //  var swapShiftId = getSwapShiftId($(this));
    var requestId = getRequestId($(this));
    var declineShiftSwapUrl = "./declineShiftSwap.json?requestId=" + requestId;
    $.ajax({
        url: declineShiftSwapUrl,
        async:false,
        dataType:'json',
        success:function(data){
            //This will remove the request row from the table
            $("#" + data.id).find(".actionButtons").html(data.response);
        }
    })
})


$(document).ready(function(){
    setTimeZoneOffset();
    renderUserSwapRequestsToPerson();
    renderUserDropRequestsToPerson();
    $("#requestsLink").addClass("active");
    $("#requestsLink").children().eq(0).removeClass("icon-white");
})