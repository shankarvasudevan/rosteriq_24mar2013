/* leaveRequests.js
----------------------
Version: 1.0
Author: Prathamesh Datar
Company: RosterIQ

Summary: This javascript will render the leave requests page and will handle the events on the page

----------------------*/

///File variables
var employeeLeaveRequests = [{}]; //This will store the week shifts the user
function renderLeaveRequests()
{
    //This will generate the unavailability block tables for an employee
    var getEmployeeLeaveRequestsUrl = "./getEmployeeLeaveRequests.json";
    //We'll first remove the existing the heading row
    $("#leaveRequestsTable thead").remove();
    var thead = $("<thead>");
    var dateRange = $("<th style=\"width:20%\">").text("Date range").appendTo(thead);
    var status = $("<th style=\"width:10%\">").text("Status").appendTo(thead);
    var comment = $("<th style=\"width:50%\">").text("Comment").appendTo(thead);
    var modifyButtons = $("<th style=\"width:20%\">").text("Edit").appendTo(thead);
    $("#leaveRequestsTable").append(thead);
    $("#leaveRequestsTable tbody").remove();
    var leaveRequestsTableBody  = $("<tbody>");
    leaveRequestsTableBody.appendTo($("#leaveRequestsTable"));
    $.ajax({
        url: getEmployeeLeaveRequestsUrl,
        async: false,
        dataType: 'json',
        success: function(data){
            //So here is when we've got the leave requests for that user
            employeeLeaveRequests = data;
            var count = 0;
            $.each(data,function(i,leaveRequest){
                ++count;
            })
            if(count>0) //If there are leaveRequests
            {
                $.each(data,function(i,leaveRequest){
                    var leaveRequestRow = $("<tr class=\"leaveRequestRow\">");
                    $("<td id=\"" + leaveRequest.id + "\" class=\"hidden leaveRequestId\">").text(leaveRequest.id).appendTo(leaveRequestRow);
                    $("<td class=\"hidden leaveRequestDateRangeHidden\">").text(leaveRequest.startDate + " " + leaveRequest.endDate).appendTo(leaveRequestRow);
                    $("<td class=\"dateRange\">").text(convertToRange(leaveRequest.startDate, leaveRequest.endDate)).appendTo(leaveRequestRow);
                    $("<td class=\"leaveRequestStatus\">").text(leaveRequest.status).appendTo(leaveRequestRow);
                    $("<td class=\"leaveRequestComment\">").text(leaveRequest.comment).appendTo(leaveRequestRow);
                    var leaveRequestModifyButtons = $("<td>");
                    var editButton = $("<div style=\"margin-left:5px\" class=\"btn editRequest pull-left\">").html("<i class=\"icon-pencil\"></i>").appendTo(leaveRequestModifyButtons);
                    var deleteButton = $("<div style=\"margin-left:5px\" class=\"btn-group pull-left\">").html("<div class=\"btn btn-danger deleteLeaveRequest\"><i class=\"icon-minus icon-white\"></i></div>").appendTo(leaveRequestModifyButtons);
                    leaveRequestModifyButtons.appendTo(leaveRequestRow);
                    leaveRequestRow.appendTo(leaveRequestsTableBody);
                })
            }
            else
            {
                //We will add an empty row which spans the 3 columns and says that there are no unavailabilityBlocks for this user
                var leaveRequestRow = $("<tr>");
                var leaveRequestRowEmpty = $("<td style=\"text-align:middle\" colspan=\"4\">").text("You don't have any leave requests").appendTo(leaveRequestRow);
                leaveRequestRow.appendTo(leaveRequestsTableBody);
            }

        }
    });
}

$(document).on("click","#saveLeaveRequest", function(){
    //So this should create or update the leave request table
    //First we'll get all necessary fields
    var leaveRequestId = $("#leaveRequestId").val();
    var startDateComponents = $("#startDatePicker").val().split(" ");
    var startDateString = startDateComponents[0] + "-" + convertMonthNameToMonthNumber(startDateComponents[1]) + "-" + startDateComponents[2];
    var endDateComponents = $("#endDatePicker").val().split(" ");
    var finishDateString = endDateComponents[0] + "-" + convertMonthNameToMonthNumber(endDateComponents[1]) + "-" + endDateComponents[2];

    var startDateYear = parseInt(startDateString.split("-")[2]);
    var startDateMonth = parseInt(startDateString.split("-")[1]);
    var startDateDay = parseInt(startDateString.split("-")[0]);

    var endDateYear = parseInt(finishDateString.split("-")[2]);
    var endDateMonth = parseInt(finishDateString.split("-")[1]);
    var endDateDay = parseInt(finishDateString.split("-")[0]);

    var startDate = new Date(startDateYear,startDateMonth,startDateDay);
    var endDate = new Date(endDateYear,endDateMonth,endDateDay);

    if (startDate.getTime() < endDate.getTime()) {
        var comment = $("#comment").val();
        var createOrUpdateLeaveRequestUrl = "./leaveRequests?operation=createOrUpdate&leaveRequestId=" + leaveRequestId + "&startDateString=" + startDateString + "&finishDateString=" + finishDateString + "&comment=" + comment;
        $.ajax({
            type:'POST',
            url: createOrUpdateLeaveRequestUrl,
            dataType:'json',
            success: function(data){
                //Once the request is added to the database we will refresh the database. This will take care of updated leave requests as well
                renderLeaveRequests();
                $("#editTitle").text("New Leave Request");
                $("#leaveRequestId").val(-1);
                $("#comment").val("");
                $("#startDatePicker").val("");
                $("#endDatePicker").val("");
                $("#cancelLeaveRequestEdit").addClass("hidden");
            }
        }) ;
        return true;
    } else {
        alert("The start date of the leave request is after the end date");
    }
})

$(document).on("click","#cancelLeaveRequestEdit",function(){
    $(this).addClass("hidden");
    $("#editTitle").text("New Leave Request");
    $("#leaveRequestId").val(-1);
    $("#comment").val("");
    $("#startDatePicker").val("");
    $("#endDatePicker").val("");
})

$(document).on("click",".editRequest",function(){
    //On clicking this button we'll load the values into the input fields and change the heading
    $("#leaveRequestId").val($(this).parent().parent().find(".leaveRequestId").text());
    $("#comment").val($(this).parent().parent().find(".leaveRequestComment").text());
    $("#cancelLeaveRequestEdit").removeClass("hidden");
    $("#startDatePicker").val(getSimpleDate(($(this).parent().parent().find(".leaveRequestDateRangeHidden").text()).split(" ")[0]));
    $("#endDatePicker").val(getSimpleDate(($(this).parent().parent().find(".leaveRequestDateRangeHidden").text()).split(" ")[1]));
    $("#editTitle").html("Edit Leave Request " + "<small>( " + $(this).parent().parent().find(".dateRange").text() + " )</small>" );
})

$(document).on("click",".deleteLeaveRequest", function(){
    $(this).parent().append("<div class=\"btn cancelLeaveRequestDeletion\"><icon class=\"icon-remove-sign\"></div>");
    $(this).parent().children().eq(0).html("<icon class=\"icon-ok-sign icon-white\">");
    $(this).parent().children().eq(0).removeClass("deleteLeaveRequest");
    $(this).parent().children().eq(0).addClass("deleteLeaveRequest2");
    $(this).parent().parent().find(".editRequest").addClass("disabled");
})

$(document).on("click",".deleteLeaveRequest2", function(){
    var leaveRequestId = $(this).parent().parent().parent().find(".leaveRequestId").html();
    var createOrUpdateLeaveRequestUrl = "./leaveRequests?operation=delete&leaveRequestId=" + leaveRequestId;
    $.ajax({
        type:'POST',
        url: createOrUpdateLeaveRequestUrl,
        dataType:'json',
        success: function(data){
            //Once the request is added to the database we will refresh the database. This will take care of updated leave requests as well
            renderLeaveRequests();
            $("#editTitle").text("New Leave Request");
            $("#leaveRequestId").val(-1);
            $("#comment").val("");
            $("#startDatePicker").val("");
            $("#endDatePicker").val("");
            if (!($("#cancelLeaveRequestEdit").hasClass("hidden"))) $("#cancelLeaveRequestEdit").addClass("hidden");
        }
    })
})

$(document).on("click",".cancelLeaveRequestDeletion",function(){
    //This should change the button back to it's original state
    $(this).parent().children().eq(0).html("<icon class=\"icon-minus icon-white\">");
    $(this).parent().children().eq(0).removeClass("deleteLeaveRequest2");
    $(this).parent().children().eq(0).addClass("deleteLeaveRequest");
    $(this).parent().parent().find(".editRequest").removeClass("disabled");
    $(this).remove();
})

$(document).ready(function(){
    setTimeZoneOffset();
    renderLeaveRequests();

    $('#startDatePicker').datepicker({
        inline: true,
        dateFormat: "dd MM yy",
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        selectOtherMonths:true
    });

    $('#endDatePicker').datepicker({
        inline: true,
        dateFormat: "dd MM yy",
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        selectOtherMonths:true
    });

    $("#leaveRequestId").val(-1);

    $(document).on("click","#endDatePicker",function(){
        $("#ui-datepicker-div").css("z-index","5");
    })

    $(document).on("click","#startDatePicker",function(){
        $("#ui-datepicker-div").css("z-index","5");
    })

})