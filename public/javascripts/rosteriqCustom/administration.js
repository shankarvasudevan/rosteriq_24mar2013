$(document).ready(function(){

    var companyLocations
    var companyRoles
    var companyLocationsForAutoCompletion = []
    var companyRolesForAutoCompletion = []
    var newRoleCounter = 0;

    $.ajax({
    url: '/getCompanyLocations',
    async:false,
    dataType:'json',
    type:'POST',
    data:{
        companyId:$("#companyId").text()
    },
    success:function(data){
        companyLocations = data
    }
    })

    $.ajax({
    url: '/getCompanyRoles',
    async:false,
    dataType:'json',
    type:'POST',
    data:{
        companyId:$("#companyId").text()
    },
    success:function(data){
        companyRoles = data
    }
    })

    $.each(companyLocations,function(i, companyLocation){
    companyLocationsForAutoCompletion.push(companyLocation.locationName)
    })

    $.each(companyRoles,function(i, companyRole){
    companyRolesForAutoCompletion.push(companyRole.title)
    })

    $.each($(".employeeLocationInput"),function(){
    //We'll convert the json into a typeahead source json
    var inputObject = $(this);
    var companyLocationInput = $(this).typeahead({updater:function(item){
        var location = (item).replace(',','');
        if(location){
            var locationObject = $("<div>").addClass("btn btn-primary").text(location);
            $(inputObject).next().append(locationObject);
        }
    }});
    companyLocationInput.data('typeahead').source = companyLocationsForAutoCompletion; //where newSource is your own array

    //We will then write the event handler for the backspace
    $(inputObject).keyup(function(event){
        var keyCode = (event.keyCode ? event.keyCode : event.which );
        if(keyCode == 8){
            //This is the keycode for the backspace
            //But this should only be called when there is no text in the input field
            if($(inputObject).prev().text() == ''){
                var locationName = backspace($(this));
            }
        }
        $(inputObject).prev().text($(inputObject).val());
    });
    })

    $.each($(".employeeRoleInput"),function(){
    //We'll convert the json into a typeahead source json
    var inputObject = $(this);
    var companyRoleInput = $(this).typeahead({updater:function(item){
        var role = (item).replace(',','');
        if(role){
            var roleObject = $("<div>").addClass("btn btn-primary").text(role);
            $(inputObject).next().append(roleObject);
        }
    }});
    companyRoleInput.data('typeahead').source = companyRolesForAutoCompletion; //where newSource is your own array

    //We will then write the event handler for the backspace
    $(inputObject).keyup(function(event){
        var keyCode = (event.keyCode ? event.keyCode : event.which );
        if(keyCode == 8){
            //This is the keycode for the backspace
            //But this should only be called when there is no text in the input field
            if($(inputObject).prev().text() == ''){
                var roleName = backspace($(this));
            }
        }
        $(inputObject).prev().text($(inputObject).val());
    });
    })


    /*---------------------------------------
    * CREATE DELETE OR UPDATE POSITIONS
    *---------------------------------------*/

    // Add roles in Admin screen
    $(document).on("click",".addRole",function() {
    var companyTableBody = $("#company_roles");
    var tRow = $("<tr class=\"newRole\">");
    tRow.appendTo(companyTableBody  );
    var tRoleName = $("<td>");
    var tInputRoleName = $("<input type=\"text\" class=\"span12 tableInput role_name\">");
    tInputRoleName.appendTo(tRoleName);
    tRoleName.appendTo(tRow);
    var tRoleDesc = $("<td>");
    var tInputRoleDesc = $("<input type=\"text\" class=\"span12 tableInput role_description\">");
    tInputRoleDesc.appendTo(tRoleDesc);
    tRoleDesc.appendTo(tRow);
    var tDelete = $("<td>");
    var tDeleteBtn = $("<button style=\"margin-left:5px;\" class=\"btn btn-danger deleteRole\">");
    $("<i class=\"icon-minus icon-white\">").appendTo(tDeleteBtn);
    tDeleteBtn.appendTo(tDelete);
    tDelete.appendTo(tRow);
    });

    // Delete roles in Admin screen
    $(document).on("click",".deleteRole", function() {
    var tRow = $(this).parent().parent();
    tRow.remove();
    });

    // Update roles in Admin section
    $(document).on("click",".saveRoles", function() {
    /* var companyTableBody = $("#company_roles");

    companyTableBody.childen().each(function(){
        var tRow = $(this);
        var roleId = tRow.attr('id');
        var roleName = tRow.find('role_name').val();
        var roleDesc = tRow.find('role_description').val();

    });*/
    var rolesJsonObj = buildPositionsJSON();
    var rolesJsonInput = $(".rolesJson");
    rolesJsonInput.val(JSON.stringify(rolesJsonObj));
    // submit the post form
    rolesJsonInput.parent().submit();
    });

    function buildPositionsJSON(){
    var roles = {"roles":[]};
    $.each($("#company_roles").children(),function(i,role) {
        var role = {"roleName":$(role).find(".role_name").val(),"roleDesc":$(role).find(".role_description").val()};
        roles.roles.push(role);
    });
    return roles;
    }
    /*---------------------------------------
    * END CREATE UPDATE OR DELTE POSITIONS
    *---------------------------------------*/

    /*---------------------------------------
    * CREATE UPDATE OR DELETE LOCATIONS
    *---------------------------------------*/

    $(document).on("click",".addLocation",function(){
    //This will remove all the active classes from the location selections
    $.each($(".allLocations").children(),function(){
        $(this).removeClass("active")
    });
    });

    $(document).on("click",".submitNewLocationForm",function(){
    //This will submit the new location
    //First we'll build the new location json string
    var locationJson = buildLocationJson("","new",-1)
    $("#newLocationJson").val(JSON.stringify(locationJson))
    $("#newLocationForm").submit()
    })

    $(document).on("click",".deleteLocation",function(){
    //This will delete the existing location
    //We'll get the location object
    var location = $(this).closest(".tab-pane")
    var locationId = $(location).attr('id').replace("location","")
    var locationJson = buildLocationJson("", "delete", locationId)
    $(location).find(".locationJson").val(JSON.stringify(locationJson))
    $(location).find(".postForm").submit()
    })

    $(document).on("click",".submitLocationForm",function(){
    //This will edit the existing location
    //First we'll build the new location json string
    //We'll get the main container for this location
    var location =  $(this).closest(".tab-pane")
    var locationId = $(location).attr('id').replace("location","")
    var locationJson = buildLocationJson(location, "update",locationId)
    $(location).find(".locationJson").val(JSON.stringify(locationJson))
    $(location).find(".postForm").submit()
    })

    function buildLocationJson(locationObject, type, locationId){
    var returnJson = {"locationName":"","locationAddress":"","id":"","operation":""}
    if(type=="new"){
        returnJson.locationName = $(".newLocationName").val()
        returnJson.locationAddress = $(".newLocationAddress").val()
    }else if(type=="update"){
        returnJson.locationName = $(locationObject).find(".locationName").val()
        returnJson.locationAddress = $(locationObject).find(".locationAddress").val()
    }
    returnJson.id = locationId
    returnJson.operation = type
    return returnJson
    }

    /*---------------------------------------
    * END CREATE UPDATE OR DELETE LOCATIONS
    *---------------------------------------*/

    /*---------------------------------------
    * CREATE UPDATE OR DELETE EMPLOYEES
    *---------------------------------------*/

    $(document).on("click",".addEmployee",function(){
      //This will remove all the active classes from the location selections
      $.each($(".allEmployees").children(),function(){
          $(this).removeClass("active")
      })
    })

    $(document).on("click",".submitNewEmployeeForm",function(){
        //This will submit the new location
        //First we'll build the new location json string
        var employeeJson = buildEmployeeJson("","new",-1)
        $("#newEmployeeJson").val(JSON.stringify(employeeJson))
        $("#newEmployeeForm").submit()
    })

    $(document).on("click",".submitEmployeeForm",function(){
        //This will submit the new location
        //First we'll build the new location json string
        var employee = $(this).closest(".tab-pane")
        var employeeId = $(employee).attr('id').replace("employee","")
        var employeeJson = buildEmployeeJson(employee,"update",employeeId)
        $(employee).find(".employeeJson").val(JSON.stringify(employeeJson))
        $(employee).find(".postForm").submit()
    })

    $(document).on("click",".deleteEmployee",function(){
        //This will delete the current employee
        //First we'll get the employee container for the location
        var employee = $(this).closest(".tab-pane")
        var employeeId = $(employee).attr('id').replace("employee","")
        var employeeJson = buildEmployeeJson("","delete",employeeId)
        employee.find(".employeeJson").val(JSON.stringify(employeeJson))
        employee.find(".postForm").submit()
    })

    function buildEmployeeJson(employeeObject, type, employeeId){
        var returnJson = {"firstName":"","lastName":"","email":"","locations":[],"roles":[], "id":"","operation":""}
        if(type=="new"){
          returnJson.firstName = $(".newEmployeeFirstName").val()
          returnJson.lastName = $(".newEmployeeLastName").val()
          returnJson.email = $(".newEmployeeEmail").val()
          $.each($(".newEmployeeLocations").children(),function(i,location){
            returnJson.locations.push($(location).text())
          })
          $.each($(".newEmployeeRoles").children(),function(i,role){
            returnJson.roles.push($(role).text())
          })
        }else if(type=="update"){
            returnJson.firstName = $(employeeObject).find(".employeeFirstName").val()
            returnJson.lastName = $(employeeObject).find(".employeeLastName").val()
            returnJson.email = $(employeeObject).find(".employeeEmail").val()
            $.each($(employeeObject).find(".employeeLocations").children(),function(i,employeeLocation){
                returnJson.locations.push($(employeeLocation).text())
            })
            $.each($(employeeObject).find(".employeeRoles").children(),function(i,employeeRole){
                returnJson.roles.push($(employeeRole).text())
            })
        }
        returnJson.id = employeeId
        returnJson.operation = type
        return returnJson
    }


    /*---------------------------------------
    * END CREATE UPDATE OR DELETE EMPLOYEES
    *---------------------------------------*/

    function backspace(element){
       //This will bring the element back into the input box
       var elementValue = $(element).next().children().last().text();
       $(element).next().children().last().remove();
       $(element).val(elementValue);
       return elementValue;
       //We should also then remove the last location role row in the location row table
    }

})