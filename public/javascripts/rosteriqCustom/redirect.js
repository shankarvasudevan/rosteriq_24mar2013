$(document).ready(function(){
    //This will set the timezone offset for the user logging in
    var d = new Date();
    timeZoneOffset = d.getTimezoneOffset();
    $("#timeZoneOffset").val(timeZoneOffset);
    $("#redirectForm").submit();
})