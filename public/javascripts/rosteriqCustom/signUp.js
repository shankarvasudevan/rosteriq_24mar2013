//The script will clone and employee menu at the start of the script for convenience
var companyEmployeeCopy
//The script will also keep a view count
var view
var companyManagerCreated=false
var companyManagerRolesAndPositionsCreated=false


$(document).ready(function(){
    
    /*---------------------------------------------------------------------------
    COMPANY AND ROLE CREATION - Handles creation of company location and roles
    ---------------------------------------------------------------------------*/

    $(document).on('keyup','#companyLocationInput',function(event){

        //This function will handle the keypress for the company location  input

        var keyCode = (event.keyCode ? event.keyCode : event.which );
        if(keyCode == 186 ){
            //This means that the comma is pressed
            //We should then collect the input contents without the comma
            var location = ($(this).val()).replace(';','');
            if(location){
                var locationObject = $("<div>").addClass("btn companyLocationInput btn-success").text(location);
                $("#locations").append(locationObject);
            }
            $(this).val('');
            updateLocations();
        }else if(keyCode == 8){
            //This is the keycode for the backspace
            //But this should only be called when there is no text in the input field
            if($("#lastLocationInput").text()==''){
                backspace($(this));
            }
        }
        $("#lastLocationInput").text($(this).val());
    })

    $(document).on('keyup','#companyRoleInput',function(event){

        //This function will handle the keypress for the company location  input

        var keyCode = (event.keyCode ? event.keyCode : event.which );
        if(keyCode == 186 ){
            //This means that the comma is pressed
            //We should then collect the input contents without the comma
            var role = ($(this).val()).replace(';','');
            if(role){
                var roleObject = $("<div>").addClass("btn companyRoleInput btn-success").text(role);
                $("#roles").append(roleObject);
            }
            $(this).val('');
            updateRoles();
        }else if(keyCode == 8){
            //This is the keycode for the backspace
            //But this should only be called when there is no text in the input field
            if($("#lastRoleInput").text() == ''){
                backspace($(this));
            }
        }
        $("#lastRoleInput").text($(this).val());
    })

    /*---------------------------------------------------------------------------
    END COMPANY AND ROLE CREATION
    ---------------------------------------------------------------------------*/

    /*---------------------------------------------------------------------------
    CREATE EMPLOYEE ROLES AND EMPLOYEE LOCATIONS
    ---------------------------------------------------------------------------*/


    $(".employeeLocationInput").each(function(){
        var inputObject = $(this);
        inputObject.typeahead({updater:function(item){
            var location = (item).replace(';','');
            if(location){
                var locationObject = $("<div>").addClass("btn employeeLocation btn-success").text(location);
                $(inputObject).next().append(locationObject);
            }
            $(this).val('');
        }})
    });

    $(".employeeRoleInput").each(function(){
        var inputObject = $(this);
        inputObject.typeahead({updater:function(item){
            var role = (item).replace(';','');
            if(role){
                var roleObject = $("<div>").addClass("btn employeeRole btn-success").text(role);
                $(inputObject).next().append(roleObject);
            }
            $(this).val('');
        }})
    })

    //Edit employee role and employee location input


//    $(document).on("keyup",".employeeLocationInput",function(){
//        var keyCode = (event.keyCode ? event.keyCode : event.which );
//        if(keyCode == 8){
//            //This is the keycode for the backspace
//            //But this should only be called when there is no text in the input field
//            if($(this).prev().text() == ''){
//                backspace($(this));
//            }
//        }
//        $(this).prev().text($(this).val());
//    })
//
//    $(document).on("keyup",".employeeRoleInput",function(){
//        var keyCode = (event.keyCode ? event.keyCode : event.which );
//        if(keyCode == 8){
//            //This is the keycode for the backspace
//            //But this should only be called when there is no text in the input field
//            if($(this).prev().text() == ''){
//                backspace($(this));
//            }
//        }
//        $(this).prev().text($(this).val());
//    })

    $(".employeeLocationInput").keyup(function(){
        var keyCode = (event.keyCode ? event.keyCode : event.which );
        if(keyCode == 8){
            //This is the keycode for the backspace
            //But this should only be called when there is no text in the input field
            if($(this).prev().text() == ''){
                backspace($(this));
            }
        }
        $(this).prev().text($(this).val());
    });

    $(".employeeRoleInput").keyup(function(){
        var keyCode = (event.keyCode ? event.keyCode : event.which );
        if(keyCode == 8){
            //This is the keycode for the backspace
            //But this should only be called when there is no text in the input field
            if($(this).prev().text() == ''){
                backspace($(this));
            }
        }
        $(this).prev().text($(this).val());
    });

    //Clone the current employee section for future use
    var companyEmployeeCopy = $(".companyEmployee").clone();
    employeeCount = 0;
    $(".companyEmployee").remove();

    //Add another employee

    $(document).on("click",".addEmployee",function(){
        addEmployee();
    })

    $(document).on('click','.deleteEmployee',function(){
        var employee = $(this).parent().parent();
        $(employee).remove();
    })

    function addEmployee(){
        var companyEmployee = companyEmployeeCopy.clone();
        var employeeElements = new Array('employeeName','employeeEmail');
        employeeCount += 1;
        $.each(employeeElements,function(i, employeeElement){
            (companyEmployee.find('.'+employeeElement+'Control')).attr('id',employeeElement+employeeCount+'Control');
            (companyEmployee.find('.'+employeeElement)).attr({'id':employeeElement+employeeCount, 'name':employeeElement+employeeCount});
        })
        $("#companyEmployees").append(companyEmployee);
        $(companyEmployee).find(".employeeRoleInput").each(function(){
            var inputObject = $(this);
            $(inputObject).typeahead({updater:function(item){
                var role = (item).replace(';','');
                if(role){
                    var roleObject = $("<div>").addClass("btn employeeRole btn-success").text(role);
                    $(inputObject).next().append(roleObject);
                }
                $(this).val('');
                $(inputObject).prev().text($(inputObject).val());
            }});
            $(inputObject).keyup(function(){
                var keyCode = (event.keyCode ? event.keyCode : event.which );
                if(keyCode == 8){
                    //This is the keycode for the backspace
                    //But this should only be called when there is no text in the input field
                    if($(inputObject).prev().text() == ''){
                        backspace($(this));
                    }
                }
                $(inputObject).prev().text($(inputObject).val());
            });
        });
        updateRoles();
        $(companyEmployee).find(".employeeLocationInput").each(function(){
            var inputObject = $(this);
            $(inputObject).typeahead({updater:function(item){
                var location = (item).replace(';','');
                if(location){
                    var locationObject = $("<div>").addClass("btn employeeLocation btn-success").text(location);
                    $(inputObject).next().append(locationObject);
                }
                $(this).val('');
                $(inputObject).prev().text($(inputObject).val());
            }});
            $(inputObject).keyup(function(){
                var keyCode = (event.keyCode ? event.keyCode : event.which );
                if(keyCode == 8){
                    //This is the keycode for the backspace
                    //But this should only be called when there is no text in the input field
                    if($(inputObject).prev().text() == ''){
                        backspace($(this));
                    }
                }
                $(inputObject).prev().text($(inputObject).val());
            });
        });
        updateLocations();
    }


    /*---------------------------------------------------------------------------
    END CREATE EMPLOYEE ROLES AND EMPLOYEE LOCATIONS
    ---------------------------------------------------------------------------*/

    /*---------------------------------------------------------------------------
    UPDATE ROLE AND LOCATION SELECTION FOR EMPLOYEE - Handles the update for the roles and locations for an employee
    ---------------------------------------------------------------------------*/

    function updateRoles(){
        //This function will update the roles in the employee selection
        //First we need to get the available roles
        //We shall put those roles into a json object
        var roles=[];
        $.each($("#roles").children(),function(i,role){
            roles.push($(role).text());
        })
        $(".employeeRoleInput").each(function(){
             var employeeRoles = $(this).typeahead();
             employeeRoles.data('typeahead').source = roles; //where newSource is your own array
        })
    }

    function addRolesAndPositionsToCompanyManager(){
        var roles = []
        var locations = []
        $.each($("#roles").children(),function(i,role){
            roles.push($(role).text());
        })
        $.each($("#locations").children(),function(i,location){
            locations.push($(location).text());
        })
        $.each(roles,function(i,role){
            //Add the role to the company manager by default
            var roleObject = $("<div>").addClass("btn employeeRole btn-success").text(role)
            $($(".employeeRoles").get(0)).append(roleObject)
        })
        $.each(locations,function(i,location){
            //Add the role to the company manager by default
            var locationObject = $("<div>").addClass("btn employeeLocation btn-success").text(location)
            $($(".employeeLocations").get(0)).append(locationObject)
        })
    }

    function updateLocations(){
        //This function will update the roles in the employee selection
        //First we need to get the available roles
        //We shall put those roles into a json object
        var locations=[];
        $.each($("#locations").children(),function(i,location){
            locations.push($(location).text());
        })
        $(".employeeLocationInput").each(function(){
            var employeeLocations = $(this).typeahead();
            employeeLocations.data('typeahead').source = locations; //where newSource is your own array
        })
    }

    function backspace(element){
        //This will bring the element back into the input box
        var elementValue = $(element).next().children().last().text();
        $(element).next().children().last().remove();
        $(element).val(elementValue);
    }

    /*---------------------------------------------------------------------------
    END UPDATE ROLE AND LOCATION SELECTION FOR EMPLOYEE
    ---------------------------------------------------------------------------*/

    /*---------------------------------------------------------------------------
    VALIDATE INPUT
    ---------------------------------------------------------------------------*/

    var view=1;
    $(".btnNext").on("click", function(){
        //We'll be doing a view by view check
        var errors = new Array();
        var duplicateEmail = false;
        if ( view == 1 ){
            //So we'll now do the check that all the fields have been filled correctly
            //So the conditions are:
            //The first name, last name should not be empty and should have at least 2 characters in both
            if ( $("#firstName").val().length < 2 )
                errors.push("firstName");
            if ( $("#lastName").val().length < 2 )
                errors.push("lastName");
            //The email should not be empty, and if it isn't empty it should be a valid email string !!!! TODO --- Later on we'll need to add a check to see if a particular email address already exists ---!!!
            if ( !(isValidEmailAddress($("#email").val())) || !($("#email").val().length > 0) )
                errors.push("email");

            if ( emailAlreadyExists($("#email").val()))  {
                duplicateEmail = true;
            }


            //The two password fields should not be empty and if they're aren't empty they should be equal
            if ( $("#password1").val().length < 6 || $("#password2").val().length < 6 || !( $("#password1").val() == $("#password2").val() && $("#password1").val().length > 0 && $("#password2").val().length > 0 ) ){
                errors.push("password1","password2");
            }
            if ( errors.length == 0 && !duplicateEmail ){
                //This means that all the conditions have been met
                //Then we'll add the company manager by default to an employee edit section
                if(!companyManagerCreated){
                    addEmployee();
                    var companyManagerName = $(".employeeName").get(0)
                    var companyManagerEmail = $(".employeeEmail").get(0)
                    $(companyManagerName).val($("#firstName").val() + " " + $("#lastName").val())
                    $(companyManagerEmail).val($("#email").val())
                    //Then we'll make sure the user can't edit these fields
                    $(companyManagerEmail).prop('disabled',true)
                    $(companyManagerName).prop('disabled',true)
                    //And the user can't delete this section
                    $($(".deleteEmployee").get(0)).prop('disabled','disabled')
                    $($(".deleteEmployee").get(0)).addClass('disabled')
                    companyManagerCreated=true
                }
                //removeErrors(); -- Don't need this anymore
                view += 1;
                $("#signUpCarousel").carousel('next');
            }else{
                if (duplicateEmail ) {
                    showDuplicateEmailError();
                }
                showErrors(errors);

            }
        }else if(view==2){
            //The company should have a name
            if ( $("#companyName").val().length < 2 )
                errors.push("companyName");
            if(errors.length == 0 ){
                //This means that all the conditions have been satisfied
                //Then we should add the roles and locations to the company manager by default
                if(!companyManagerRolesAndPositionsCreated){
                    addRolesAndPositionsToCompanyManager()
                    companyManagerRolesAndPositionsCreated=true
                }
                view += 1;
                $("#signUpCarousel").carousel('next');
            }else{
                showErrors(errors);
            }
        }
    });

    $(document).on("click","#submit",function(){
        //We will then validate the user employee inputs
        //The validation is that the employee should have a name and that the email provided should be valid
        var errors = new Array();
        $.each($("#companyEmployees").children(),function(i,employee){
            var employeeName = $(employee).find(".employeeName")
            if($(employeeName).val().length < 2 ){
                errors.push($(employeeName).attr('id'));
            }
        })
        $.each($("#companyEmployees").children(),function(i,employee){
            var employeeEmail = $(employee).find(".employeeEmail")
            if(!(isValidEmailAddress($(employeeEmail).val())) || !($(employeeEmail).val().length > 0) ){
                errors.push($(employeeEmail).attr('id'));
            }
        })
        if(errors.length == 0 ){
            var attempts=0;
            var returnJson = buildJson();
            //Then we'll check if the json built was correct otherwise we'll build it and check it again
            while(!checkJson(returnJson) && attempts<3){
                attempts += 1;
                returnJson = buildJson();
            }
            if(attempts>2){
                alert("System error. Please try again later");
                return false;
            }else{
                $("#signUpJson").val(JSON.stringify(returnJson));
                $("#postForm").submit();
            }
        }else{
            showErrors(errors);
        }
    })


    //Individual input validation

    $("#firstName").keyup(function(){
        if ( $("#firstName").val().length > 1 ){
            $("#firstNameControl").removeClass("error");
            $("#firstNameControl").children().find(".help-inline").addClass('hide');
        }
    })

    $("#lastName").keyup(function(){
        if ( $("#lastName").val().length > 1 ){
            $("#lastNameControl").removeClass("error");
            $("#lastNameControl").children().find(".help-inline").addClass('hide');
        }
    })

    $("#email").keyup(function(){
        if ( isValidEmailAddress($("#email").val()) ){
            $("#emailControl").removeClass("error");
            $("#emailControl").children().find(".help-inline").addClass('hide');
        }
    })
    $("#password1").keyup(function(){
        if ( $("#password1").val().length > 5 && $("#password2").val().length > 5 && ( $("#password1").val() == $("#password2").val() ) ){
            $("#password1Control").removeClass("error");
            $("#password2Control").removeClass("error");
            $("#password1Control").children().find(".help-inline").addClass('hide');
            $("#password2Control").children().find(".help-inline").addClass('hide');
        }
    })
    $("#password2").keyup(function(){
        if ( $("#password1").val().length > 5 && $("#password2").val().length > 5 && ( $("#password1").val() == $("#password2").val() ) ){
            $("#password1Control").removeClass("error");
            $("#password2Control").removeClass("error");
            $("#password1Control").children().find(".help-inline").addClass('hide');
            $("#password2Control").children().find(".help-inline").addClass('hide');
        }
    })

    $("#companyName").keyup(function(){
        if ( $("#companyName").val().length > 1 ){
            $("#companyNameControl").removeClass("error");
            $("#companyNameControl").children().find(".help-inline").addClass('hide');
            $("#companyNameControl").children().find(".help-block").addClass('hide');
        }
    })

    $(document).on("keyup",".employeeName", function(){
        if($(this).val().length > 1)
            $(this).parent().removeClass("error");
    })

    $(document).on("keyup",".employeeEmail", function(){
        if((isValidEmailAddress($(this).val())) || !($(this).val().length > 0))
            $(this).parent().removeClass("error");
    })

    /*---------------------------------------------------------------------------
    END VALIDATE INPUT
    ---------------------------------------------------------------------------*/


    /*---------------------------------------------------------------------------
    SIGN UP - Handles the gathering of information for the sign up process
    ---------------------------------------------------------------------------*/

    function buildJson(){

        var returnJson = {"companyManager":[],"company":[]};

        //Each section thereafter will have it's own json object will be added to the larger one

        //We need to collect the user details
        var companyManagerJson;
        //Name ( First and last )
        //Email
        //Password
        companyManagerJson = { "firstName": $("#firstName").val(), "lastName": $("#lastName").val(), "email":$("#email").val(), "password": $("#password1").val()};
        //Then we'll add that object to the main json object
        returnJson.companyManager.push(companyManagerJson);

        //Then will create the company json object
        var companyJson = {"name":"","companyLocations":[],"companyRoles":[], "companyEmployees":[]};
        //We will add the company name
        companyJson.name = $("#companyName").val();
        //We'll get the company roles
        $.each($("#locations").children(), function(i, location){
            //We will go through all the locations that were entered
            companyJson.companyLocations.push({"location":$(location).text()});
        })
        $.each($("#roles").children(), function(i, role){
            //We will go through all the roles that were entered
            companyJson.companyRoles.push({"role":$(role).text()});
        })
        //Then we will collect the company employees
        $.each($("#companyEmployees").children(),function(i,employee){
            //We will go through all the employees and add them to the json object
            //We will also then go through their assigned roles and locations and add them into the json object as well
            var employeeJson = {"name":"","email":"","locations":[],"roles":[]};
            employeeJson.name = $(employee).find(".employeeName").val()
            employeeJson.email = $(employee).find(".employeeEmail").val()
            $.each($(employee).find(".employeeLocations").children(),function(i,employeeLocation){
                //Then we will add the locations assigned to that employee
                if($(employeeLocation).text())
                    employeeJson.locations.push({"location":$(employeeLocation).text()})
            })
            $.each($(employee).find(".employeeRoles").children(),function(i,employeeRole){
                //Then we will add the locations assigned to that employee
                if($(employeeRole).text())
                    employeeJson.roles.push({"role":$(employeeRole).text()})
            })
            companyJson.companyEmployees.push(employeeJson);
        })
        //After this the companyJson object should be complete and we can add it to the json object
        returnJson.company.push(companyJson);
        return returnJson;

    }

    function checkJson(returnJson){
        //This function will see if the json function correctly loaded all the data
        //We will assume that the result is going to be true and we will change it if there is an anomaly found later
        var result = true;
        //First we will see if the company manager details are correct
        if(!(returnJson.companyManager[0].firstName == $("#firstName").val()))
            result = false;
        if(!(returnJson.companyManager[0].lastName == $("#lastName").val()))
            result = false;
        if(!(returnJson.companyManager[0].email == $("#email").val()))
            result = false;
        if(!(returnJson.companyManager[0].password == $("#password1").val()))
            result = false;
        //Then we will check if the company details are correct
        if(!(returnJson.company[0].name == $("#companyName").val()))
            result = false;
        //Then from here will only count if the number of items in each section were correct
        if(!(returnJson.company[0].companyLocations.length == $("#locations").children().length ))
            result = false;
        if(!(returnJson.company[0].companyRoles.length == $("#roles").children().length ))
            result = false;
        if(!(returnJson.company[0].companyEmployees.length == $("#companyEmployees").children().length ))
            result = false;
        //Then for each employee we'll see if the roles and location counts are correct
        $.each($("#companyEmployees").children(),function(i,companyEmployee){
            if(!(returnJson.company[0].companyEmployees[i].locations.length == $(companyEmployee).find(".employeeLocations").children().length))
                result = false
            if(!(returnJson.company[0].companyEmployees[i].roles.length == $(companyEmployee).find(".employeeRoles").children().length))
                result = false
        })
        //After this point we've done all the checks and we'll return the result
        return result;
    }

    /*---------------------------------------------------------------------------
    END SIGN UP
    ---------------------------------------------------------------------------*/

    /*---------------------------------------------------------------------------
    HELPER FUNCTIONS
    ---------------------------------------------------------------------------*/

    $("#signUpCarousel").carousel("pause");

    $(document).on("click",".btnPrev",function(){
        $("#signUpCarousel").carousel("prev");
        view -= 1;
    })

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        return pattern.test(emailAddress);
    };

    function emailAlreadyExists(emailAddress) {
        var checkEmailUrl = "./emailAlreadyExists";
        var emailExists = false;
        $.ajax({
            url: checkEmailUrl,
            async: false,
            type: 'POST',
            dataType: 'json',
            data:{
                email:emailAddress
            },
            success: function(data) {
                if (data.exists === true) {
                   emailExists = true;
                }
            }
        });
        return emailExists;
    }

    function showErrors(errors){
        $.each(errors,function(i,error){
            $("#"+error+"Control").addClass("error");
            $("#"+error+"Control").children().find(".help-inline").removeClass("hide");
            $("#"+error+"Control").children().find(".help-block").removeClass("hide");
        })
    }

    function showDuplicateEmailError() {
        $("#emailControl").addClass("error");
        $("#emailControl").children().find(".help-inline-dupEmail").removeClass("hide");
        $("#emailControl").children().find(".help-block").removeClass("hide");
    }

    /*---------------------------------------------------------------------------
    END HELPER FUNCTIONS
    ---------------------------------------------------------------------------*/

})