-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 30, 2012 at 12:44 AM
-- Server version: 5.5.9
-- PHP Version: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `rosteriq`
--

-- --------------------------------------------------------

--
-- Table structure for table `Company`
--

CREATE TABLE `Company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(255) DEFAULT NULL,
  `lastUpdatedBy` varchar(255) DEFAULT NULL,
  `lastUpdatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Company`
--

INSERT INTO `Company` VALUES(1, 'Fitness First', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `DropShiftRequest`
--

CREATE TABLE `DropShiftRequest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lastUpdatedBy` varchar(255) DEFAULT NULL,
  `lastUpdatedDate` datetime DEFAULT NULL,
  `response` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `shift_id` bigint(20) DEFAULT NULL,
  `toPerson_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2E66405C4B20BDA3` (`toPerson_id`),
  KEY `FK2E66405C8BD979D6` (`shift_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `DropShiftRequest`
--


-- --------------------------------------------------------

--
-- Table structure for table `LeaveRequest`
--

CREATE TABLE `LeaveRequest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `endDate` datetime DEFAULT NULL,
  `lastUpdatedBy` varchar(255) DEFAULT NULL,
  `lastUpdatedDate` datetime DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `person_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8EB4E3F8E7A7B1BE` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `LeaveRequest`
--


-- --------------------------------------------------------

--
-- Table structure for table `Location`
--

CREATE TABLE `Location` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contactNumber` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `locationName` varchar(255) DEFAULT NULL,
  `postCode` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `streetName` varchar(255) DEFAULT NULL,
  `streetNumber` varchar(255) DEFAULT NULL,
  `streetType` int(11) DEFAULT NULL,
  `suburb` varchar(255) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK752A03D51366CCF6` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Location`
--

INSERT INTO `Location` VALUES(1, '0412345678', 'Australia', 'Carlingford', '2118', 'NSW', 'Carlingford', '100', 1, 'Carlingford', 1);
INSERT INTO `Location` VALUES(2, '0412345678', 'Australia', 'Blacktown', '2148', 'NSW', 'Blacktown', '30', 1, 'Blacktown', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Location_Person`
--

CREATE TABLE `Location_Person` (
  `locations_id` bigint(20) NOT NULL,
  `persons_id` bigint(20) NOT NULL,
  KEY `FK892F771FDD4EB8B5` (`persons_id`),
  KEY `FK892F771F95F1875` (`locations_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Location_Person`
--

INSERT INTO `Location_Person` VALUES(1, 1);
INSERT INTO `Location_Person` VALUES(1, 2);
INSERT INTO `Location_Person` VALUES(1, 4);
INSERT INTO `Location_Person` VALUES(1, 7);
INSERT INTO `Location_Person` VALUES(1, 8);
INSERT INTO `Location_Person` VALUES(1, 14);
INSERT INTO `Location_Person` VALUES(2, 1);
INSERT INTO `Location_Person` VALUES(2, 3);
INSERT INTO `Location_Person` VALUES(2, 5);
INSERT INTO `Location_Person` VALUES(2, 6);
INSERT INTO `Location_Person` VALUES(2, 9);
INSERT INTO `Location_Person` VALUES(2, 14);

-- --------------------------------------------------------

--
-- Table structure for table `Person`
--

CREATE TABLE `Person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountType` int(11) DEFAULT NULL,
  `contactNumber` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `emailAddress` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `lastUpdatedBy` varchar(255) DEFAULT NULL,
  `lastUpdatedDate` datetime DEFAULT NULL,
  `maxHoursPerWeek` int(11) NOT NULL,
  `minHoursBetweenShifts` int(11) NOT NULL,
  `misc` bit(1) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `postCode` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `streetName` varchar(255) DEFAULT NULL,
  `streetNumber` varchar(255) DEFAULT NULL,
  `streetType` int(11) DEFAULT NULL,
  `suburb` varchar(255) DEFAULT NULL,
  `wage` float DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8E4887751366CCF6` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `Person`
--

INSERT INTO `Person` VALUES(1, 2, '', '', NULL, 'dramanan@rosteriq.com', 'Dinesh', 'Ramanan', 'admin', NULL, 40, 0, '\0', 'dramanan', '', '', '', '', 0, '', 20, 1);
INSERT INTO `Person` VALUES(2, 1, '', '', NULL, 'spottekkatt@rosteriq.com', 'Sabinesh', 'Pottekkatt', '', NULL, 40, 0, '\0', 'spottekkatt', '', '', '', '', 0, '', 20, 1);
INSERT INTO `Person` VALUES(3, 0, '', '', NULL, 'pbedi@rosteriq.com', 'Piyush', 'Bedi', '', NULL, 40, 0, '\0', 'pbedi', '', '', '', '', 0, '', 20, 1);
INSERT INTO `Person` VALUES(4, 0, '', '', NULL, 'asingla@rosteriq.com', 'Apresh', 'Singla', 'admin', NULL, 40, 0, '\0', 'asingla', '', '', '', '', 0, '', 20, 1);
INSERT INTO `Person` VALUES(5, 0, '', '', NULL, 'kmalhotra@rosteriq.com', 'Kanav', 'Malhotra', 'admin', NULL, 40, 0, '\0', 'kmalhotra', '', '', '', '', 0, '', 20, 1);
INSERT INTO `Person` VALUES(6, 0, '', '', NULL, 'rlele@rosteriq.com', 'Rucha', 'Lele', 'admin', NULL, 40, 0, '\0', 'rlele', '', '', '', '', 0, '', 20, 1);
INSERT INTO `Person` VALUES(7, 0, '', '', NULL, 'snatarajan@rosteriq.com', 'Sujatha', 'Natarajan', 'admin', NULL, 40, 0, '\0', 'snatarajan', '', '', '', '', 0, '', 20, 1);
INSERT INTO `Person` VALUES(8, 0, '', '', NULL, 'agupta@rosteriq.com', 'Akshat', 'Gupta', 'admin', NULL, 40, 0, '\0', 'agupta', '', '', '', '', 0, '', 20, 1);
INSERT INTO `Person` VALUES(9, 0, '', '', NULL, 'nalur@rosteriq.com', 'Nash', 'Alur', 'admin', NULL, 40, 0, '\0', 'nalur', '', '', '', '', 0, '', 20, 1);
INSERT INTO `Person` VALUES(14, 0, '', NULL, NULL, 'misc', 'Unassigned', '', NULL, NULL, 0, 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Person_Position`
--

CREATE TABLE `Person_Position` (
  `persons_id` bigint(20) NOT NULL,
  `positions_id` bigint(20) NOT NULL,
  KEY `FK98C23AD3CA23315D` (`positions_id`),
  KEY `FK98C23AD3DD4EB8B5` (`persons_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Person_Position`
--

INSERT INTO `Person_Position` VALUES(2, 1);
INSERT INTO `Person_Position` VALUES(2, 23);
INSERT INTO `Person_Position` VALUES(4, 2);
INSERT INTO `Person_Position` VALUES(4, 23);
INSERT INTO `Person_Position` VALUES(7, 4);
INSERT INTO `Person_Position` VALUES(7, 23);
INSERT INTO `Person_Position` VALUES(8, 2);
INSERT INTO `Person_Position` VALUES(8, 23);
INSERT INTO `Person_Position` VALUES(1, 1);
INSERT INTO `Person_Position` VALUES(1, 23);
INSERT INTO `Person_Position` VALUES(1, 24);
INSERT INTO `Person_Position` VALUES(3, 1);
INSERT INTO `Person_Position` VALUES(3, 24);
INSERT INTO `Person_Position` VALUES(5, 2);
INSERT INTO `Person_Position` VALUES(5, 24);
INSERT INTO `Person_Position` VALUES(6, 4);
INSERT INTO `Person_Position` VALUES(6, 24);
INSERT INTO `Person_Position` VALUES(9, 3);
INSERT INTO `Person_Position` VALUES(9, 24);
INSERT INTO `Person_Position` VALUES(14, 1);
INSERT INTO `Person_Position` VALUES(14, 2);
INSERT INTO `Person_Position` VALUES(14, 3);
INSERT INTO `Person_Position` VALUES(14, 4);
INSERT INTO `Person_Position` VALUES(14, 6);
INSERT INTO `Person_Position` VALUES(14, 7);
INSERT INTO `Person_Position` VALUES(14, 8);
INSERT INTO `Person_Position` VALUES(14, 9);
INSERT INTO `Person_Position` VALUES(14, 10);
INSERT INTO `Person_Position` VALUES(14, 23);
INSERT INTO `Person_Position` VALUES(14, 24);

-- --------------------------------------------------------

--
-- Table structure for table `Position`
--

CREATE TABLE `Position` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `lastUpdatedBy` varchar(255) DEFAULT NULL,
  `lastUpdatedDate` datetime DEFAULT NULL,
  `misc` bit(1) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK306CFD491366CCF6` (`company_id`),
  KEY `FK306CFD492DB65CBE` (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `Position`
--

INSERT INTO `Position` VALUES(1, 'Someone who manages', NULL, NULL, '\0', 'Manager', 1, 1);
INSERT INTO `Position` VALUES(2, 'Someone who trains', NULL, NULL, '\0', 'Trainer', 1, 1);
INSERT INTO `Position` VALUES(3, 'Someone who is a receptionist', NULL, NULL, '\0', 'Receptionist', 1, 1);
INSERT INTO `Position` VALUES(4, 'Someone who handles accounts', NULL, NULL, '\0', 'Accounts Desk', 1, 1);
INSERT INTO `Position` VALUES(6, 'The person who manages', NULL, NULL, '\0', 'Manager', 1, 2);
INSERT INTO `Position` VALUES(7, 'The person who designs', NULL, NULL, '\0', 'Graphic Designer', 1, 2);
INSERT INTO `Position` VALUES(8, 'The person who programs', NULL, NULL, '\0', 'Programmer', 1, 2);
INSERT INTO `Position` VALUES(9, 'The person who analyses', NULL, NULL, '\0', 'Business Analyst', 1, 2);
INSERT INTO `Position` VALUES(10, 'The person who prices', NULL, NULL, '\0', 'Pricing Motor', 1, 2);
INSERT INTO `Position` VALUES(23, '', NULL, NULL, '', 'Unassigned', 1, 1);
INSERT INTO `Position` VALUES(24, '', NULL, NULL, '', 'Unassigned', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Shift`
--

CREATE TABLE `Shift` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cancelled` bit(1) NOT NULL,
  `draft` bit(1) NOT NULL,
  `finish` datetime DEFAULT NULL,
  `lastUpdatedBy` varchar(255) DEFAULT NULL,
  `lastUpdatedDate` datetime DEFAULT NULL,
  `numHoursBreak` float NOT NULL,
  `open` bit(1) NOT NULL,
  `publishedDate` datetime DEFAULT NULL,
  `publishedFinish` datetime DEFAULT NULL,
  `publishedOpen` bit(1) NOT NULL,
  `publishedPersonId` bigint(20) NOT NULL,
  `publishedPositionId` bigint(20) NOT NULL,
  `publishedStart` datetime DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `weekStartDate` datetime DEFAULT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  `person_id` bigint(20) DEFAULT NULL,
  `position_id` bigint(20) DEFAULT NULL,
  `shiftSeries_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4C27C62E7A7B1BE` (`person_id`),
  KEY `FK4C27C622DB65CBE` (`location_id`),
  KEY `FK4C27C62BFDC783E` (`position_id`),
  KEY `FK4C27C62B6543996` (`shiftSeries_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `Shift`
--

INSERT INTO `Shift` VALUES(8, '\0', '\0', '2012-08-28 12:00:18', 'dramanan@rosteriq.com', '2012-08-30 00:33:19', 0, '', '2012-08-30 00:33:19', '2012-08-28 12:00:18', '', 14, 2, '2012-08-28 08:00:18', '2012-08-28 08:00:18', '2012-08-28 00:33:18', 1, 14, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ShiftSeries`
--

CREATE TABLE `ShiftSeries` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `days` varchar(255) DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `increment` int(11) NOT NULL,
  `lastUpdatedBy` varchar(255) DEFAULT NULL,
  `lastUpdatedDate` datetime DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ShiftSeries`
--


-- --------------------------------------------------------

--
-- Table structure for table `Shift_Template`
--

CREATE TABLE `Shift_Template` (
  `shifts_id` bigint(20) NOT NULL,
  `templates_id` bigint(20) NOT NULL,
  KEY `FK6E0B3D7E076EFC7` (`shifts_id`),
  KEY `FK6E0B3D71297877F` (`templates_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Shift_Template`
--


-- --------------------------------------------------------

--
-- Table structure for table `Template`
--

CREATE TABLE `Template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lastUpdatedBy` varchar(255) DEFAULT NULL,
  `lastUpdatedDate` datetime DEFAULT NULL,
  `templateName` varchar(255) DEFAULT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKB515309A2DB65CBE` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `Template`
--


-- --------------------------------------------------------

--
-- Table structure for table `Unavailability`
--

CREATE TABLE `Unavailability` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dayOfWeek` bigint(20) NOT NULL,
  `endTime` varchar(255) DEFAULT NULL,
  `lastUpdatedBy` varchar(255) DEFAULT NULL,
  `lastUpdatedDate` datetime DEFAULT NULL,
  `startTime` varchar(255) DEFAULT NULL,
  `person_id` bigint(20) DEFAULT NULL,
  `unavailabilityBlock_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF200D294E7A7B1BE` (`person_id`),
  KEY `FKF200D294F6AAAD56` (`unavailabilityBlock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `Unavailability`
--


-- --------------------------------------------------------

--
-- Table structure for table `UnavailabilityBlock`
--

CREATE TABLE `UnavailabilityBlock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `frequency` varchar(255) DEFAULT NULL,
  `lastUpdatedBy` varchar(255) DEFAULT NULL,
  `lastUpdatedDate` datetime DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `person_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA8147E59E7A7B1BE` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `UnavailabilityBlock`
--


--
-- Constraints for dumped tables
--

--
-- Constraints for table `DropShiftRequest`
--
ALTER TABLE `DropShiftRequest`
  ADD CONSTRAINT `FK2E66405C8BD979D6` FOREIGN KEY (`shift_id`) REFERENCES `Shift` (`id`),
  ADD CONSTRAINT `FK2E66405C4B20BDA3` FOREIGN KEY (`toPerson_id`) REFERENCES `Person` (`id`);

--
-- Constraints for table `LeaveRequest`
--
ALTER TABLE `LeaveRequest`
  ADD CONSTRAINT `FK8EB4E3F8E7A7B1BE` FOREIGN KEY (`person_id`) REFERENCES `Person` (`id`);

--
-- Constraints for table `Location`
--
ALTER TABLE `Location`
  ADD CONSTRAINT `FK752A03D51366CCF6` FOREIGN KEY (`company_id`) REFERENCES `Company` (`id`);

--
-- Constraints for table `Location_Person`
--
ALTER TABLE `Location_Person`
  ADD CONSTRAINT `FK892F771F95F1875` FOREIGN KEY (`locations_id`) REFERENCES `Location` (`id`),
  ADD CONSTRAINT `FK892F771FDD4EB8B5` FOREIGN KEY (`persons_id`) REFERENCES `Person` (`id`);

--
-- Constraints for table `Person`
--
ALTER TABLE `Person`
  ADD CONSTRAINT `FK8E4887751366CCF6` FOREIGN KEY (`company_id`) REFERENCES `Company` (`id`);

--
-- Constraints for table `Person_Position`
--
ALTER TABLE `Person_Position`
  ADD CONSTRAINT `FK98C23AD3DD4EB8B5` FOREIGN KEY (`persons_id`) REFERENCES `Person` (`id`),
  ADD CONSTRAINT `FK98C23AD3CA23315D` FOREIGN KEY (`positions_id`) REFERENCES `Position` (`id`);

--
-- Constraints for table `Position`
--
ALTER TABLE `Position`
  ADD CONSTRAINT `FK306CFD492DB65CBE` FOREIGN KEY (`location_id`) REFERENCES `Location` (`id`),
  ADD CONSTRAINT `FK306CFD491366CCF6` FOREIGN KEY (`company_id`) REFERENCES `Company` (`id`);

--
-- Constraints for table `Shift`
--
ALTER TABLE `Shift`
  ADD CONSTRAINT `FK4C27C62B6543996` FOREIGN KEY (`shiftSeries_id`) REFERENCES `ShiftSeries` (`id`),
  ADD CONSTRAINT `FK4C27C622DB65CBE` FOREIGN KEY (`location_id`) REFERENCES `Location` (`id`),
  ADD CONSTRAINT `FK4C27C62BFDC783E` FOREIGN KEY (`position_id`) REFERENCES `Position` (`id`),
  ADD CONSTRAINT `FK4C27C62E7A7B1BE` FOREIGN KEY (`person_id`) REFERENCES `Person` (`id`);

--
-- Constraints for table `Shift_Template`
--
ALTER TABLE `Shift_Template`
  ADD CONSTRAINT `FK6E0B3D71297877F` FOREIGN KEY (`templates_id`) REFERENCES `Template` (`id`),
  ADD CONSTRAINT `FK6E0B3D7E076EFC7` FOREIGN KEY (`shifts_id`) REFERENCES `Shift` (`id`);

--
-- Constraints for table `Template`
--
ALTER TABLE `Template`
  ADD CONSTRAINT `FKB515309A2DB65CBE` FOREIGN KEY (`location_id`) REFERENCES `Location` (`id`);

--
-- Constraints for table `Unavailability`
--
ALTER TABLE `Unavailability`
  ADD CONSTRAINT `FKF200D294F6AAAD56` FOREIGN KEY (`unavailabilityBlock_id`) REFERENCES `UnavailabilityBlock` (`id`),
  ADD CONSTRAINT `FKF200D294E7A7B1BE` FOREIGN KEY (`person_id`) REFERENCES `Person` (`id`);

--
-- Constraints for table `UnavailabilityBlock`
--
ALTER TABLE `UnavailabilityBlock`
  ADD CONSTRAINT `FKA8147E59E7A7B1BE` FOREIGN KEY (`person_id`) REFERENCES `Person` (`id`);
